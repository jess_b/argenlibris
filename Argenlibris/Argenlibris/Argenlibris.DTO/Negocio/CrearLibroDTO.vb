﻿Imports Argenlibris.Domain

Public Class CrearLibroDTO

    Public Property ID As Guid
    Public Property ISBN As String
    Public Property Formato As EnumTipoFormato
    Public Property Titulo As String
    Public Property Sinopsis As String
    Public Property Autor As Autor
    Public Property Editorial As Editorial

End Class
