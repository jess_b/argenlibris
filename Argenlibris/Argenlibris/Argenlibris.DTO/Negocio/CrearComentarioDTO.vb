﻿Imports Argenlibris.Domain

Public Class CrearComentarioDTO

    Public Property Libro As Libro
    Public Property Comentario As String
    Public Property Fecha As Date
    Public Property Nombre As String

End Class
