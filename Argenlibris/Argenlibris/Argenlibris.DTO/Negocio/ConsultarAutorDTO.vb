﻿Imports Argenlibris.Domain

Public Class ConsultarAutorDTO

    Public Property ID As Guid
    Public Property Nombre As String
    Public Property Apellido As String
    Public Property Pais As Guid

End Class
