﻿Imports Argenlibris.Domain

Public Class CrearNewsletterDTO

    Public Property Id As Guid
    Public Property Fecha As DateTime
    Public Property Nombre As String
    Public Property Asunto As String
    Public Property Cuerpo As String
    Public Property Libros As New List(Of Libro)


End Class
