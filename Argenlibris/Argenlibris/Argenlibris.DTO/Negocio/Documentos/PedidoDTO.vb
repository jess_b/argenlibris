﻿Imports Argenlibris.Domain
Public Class PedidoDTO

    Public Property Id As Guid
    Public Property Direccion As String
    Public Property Cliente As Usuario
    Public Property Total As Double
    Public Property Posiciones As IList(Of PedidoPosicionDTO)
    Public Property Despachado As Boolean
    Public Property Recibido As Boolean
    Public Property Reclamado As Boolean
    Public Property Resuelto As Boolean
    Public Property Observacion As String
    Public Property ObservacionContable As String

End Class
