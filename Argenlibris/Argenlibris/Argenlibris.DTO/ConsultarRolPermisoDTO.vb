﻿Imports Argenlibris.Domain

Public Class ConsultarRolPermisoDTO

    Public Property Id As Guid
    Public Property Nombre As String
    Public Property Permisos As List(Of Permiso)

End Class
