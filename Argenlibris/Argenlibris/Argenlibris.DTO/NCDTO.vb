﻿Imports Argenlibris.Domain
Public Class NCDTO

    Public Property Id As Guid
    Public Property Numero As Integer
    Public Property Fecha As Date
    Public Property Total As Double
    Public Property Cliente As Usuario
    Public Property Posiciones As IList(Of DocumentoPosicion)
    Public Property Direccion As String
    Public Property LetraDocumento As String
    Public Property TipoDocumento As EnumTipoDocumento
    Public Property Observacion As String
    Public Property MontoADebitar As Double
    Public Property Credito As Double
    Public Property Acreditado As Boolean

    Sub New()
        Posiciones = New List(Of DocumentoPosicion)
    End Sub


End Class
