﻿Imports Argenlibris.Domain

Public Class ConsultarBitacoraDTO

    Public Property Usuario As Guid
    Public Property FechaDesde As Date
    Public Property FechaHasta As Date
    Public Property TipoOperacion As String

End Class
