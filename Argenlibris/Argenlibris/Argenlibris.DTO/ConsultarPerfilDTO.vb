﻿Imports Argenlibris.Domain

Public Class ConsultarPerfilDTO

    Public Property Id As Guid
    Public Property NombreUsuario As String
    Public Property Nombre As String
    Public Property PasswordNueva As String
    Public Property Email As String
    Public Property Apellido As String
    Public Property FechaNacimiento As DateTime?
    Public Property Rol As IList(Of Rol)

End Class
