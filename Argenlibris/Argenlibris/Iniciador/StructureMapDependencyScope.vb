﻿Imports System.Web.Http.Dependencies
Imports StructureMap

Public Class StructureMapDependencyScope
    Implements IDependencyScope
    Protected Container As IContainer
    Public Sub New(container As IContainer)
        Me.Container = container
    End Sub

    Public Function GetService(serviceType As Type) As Object Implements IDependencyScope.GetService
        If (Container Is Nothing) Then
            Throw New ObjectDisposedException("this", "este contexto ya se ha eliminado")
        End If

        If (serviceType Is Nothing) Then
            Return Nothing
        End If
        If (serviceType.IsAbstract Or serviceType.IsInterface) Then
            Return Me.Container.TryGetInstance(serviceType)
        End If

        Return Me.Container.GetInstance(serviceType)

    End Function

    Public Iterator Function GetServices(serviceType As Type) As IEnumerable(Of Object) Implements IDependencyScope.GetServices
        If (Me.Container Is Nothing) Then
            Throw New ObjectDisposedException("this", "este contexto ya se ha eliminado")

        End If

        For Each instance As Object In Container.GetAllInstances(serviceType)
            Yield instance
        Next
    End Function

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose

        Dim disposable As IDisposable = Me.Container

        If Not disposable Is Nothing Then
            disposable.Dispose()

        End If

        Me.Container = Nothing
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        
    End Sub
#End Region

End Class
