﻿Imports StructureMap
Imports Repositorio
Imports NHibernate
Imports System.Web.Http
Imports Argenlibris.NHibernate
Imports BLL
Imports StructureMap.Graph
Imports AutoMapper
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Microsoft.AspNet.SignalR

Public Module Iniciador
    Dim _usuarioBLL As UsuarioBLL
    Public ReadOnly Property ObtenerUsuarioBLL As UsuarioBLL
        Get
            Return _usuarioBLL
        End Get
    End Property

    Dim _usuarioRolBLL As UsuarioRolBLL
    Public ReadOnly Property ObtenerUsuarioRolBLL As UsuarioRolBLL
        Get
            Return _usuarioRolBLL
        End Get
    End Property

    Dim _rolPermisoBLL As RolPermisoBLL
    Public ReadOnly Property ObtenerRolPermisoBLL As RolPermisoBLL
        Get
            Return _rolPermisoBLL
        End Get
    End Property

    Public Sub Iniciar()
        Dim c As NHibernate.Cfg.Configuration
        c = Argenlibris.NHibernate.NHibernateInitializer.Initialize()



        ObjectFactory.Initialize(Sub(x As IInitializationExpression)
                                     x.For(Of ISessionFactory).Singleton().Use(c.BuildSessionFactory())
                                     x.For(Of IValidadorFactory).Singleton().Use(New ValidadorFactory())
                                     ' x.[For](GetType(IHubContext)).Use(GetType(HubContext))
                                     x.[For](GetType(IRepositorio(Of ))).Use(GetType(Repositorio(Of )))
                                     x.For(GetType(IBackupRepositorio)).Use(GetType(BackupRepositorio))

                                     'x.For(Of IEncriptador).Singleton().Use(new Encriptador())
                                     ' x.[For](GetType(IBLL(Of ))).Use(GetType(BaseBLL(Of )))
                                     x.AddRegistry(New BLLRegistry())
                                     x.Scan(
                                         Sub(s)
                                             s.Assembly("Argenlibris.NHibernate")
                                             s.Convention(Of DefaultConventionScanner)()
                                         End Sub)
                                     x.Scan(
                                        Sub(s)
                                            s.Assembly("Repositorio")
                                            s.Convention(Of DefaultConventionScanner)()
                                        End Sub)
                                     x.Scan(
                                        Sub(s)
                                            s.Assembly("Argenlibris.Domain")
                                            s.Convention(Of DefaultConventionScanner)()
                                        End Sub)


                                 End Sub)

        '                             x.AddRegistry()
        '                             x.For()
        '                             x.Scan()

        '                         End Function)
        Argenlibris.NHibernate.NHibernateInitializer.UpdateSchema(c, ObjectFactory.GetInstance(Of ISessionFactory))
        _usuarioBLL = ObjectFactory.GetInstance(Of IUsuarioBLL)()
        _usuarioRolBLL = ObjectFactory.GetInstance(Of UsuarioRolBLL)()
        _rolPermisoBLL = ObjectFactory.GetInstance(Of RolPermisoBLL)()


        Mapper.CreateMap(Of RepresentacionCuenta, RepresentacionCuenta)()

        Mapper.CreateMap(Of RepresentacionDTO, RepresentacionDTO)()
        Mapper.CreateMap(Of Usuario, Usuario)()
        Mapper.CreateMap(Of ConsultarUsuarioDTO, Usuario)()
        Mapper.CreateMap(Of Permiso, Permiso)()
        Mapper.CreateMap(Of Rol, Rol)()
        Mapper.CreateMap(Of UsuarioRol, UsuarioRol)()
        Mapper.CreateMap(Of RolPermiso, RolPermiso)()
        Mapper.CreateMap(Of Bitacora, Bitacora)()
        Mapper.CreateMap(Of Autor, Autor)()
        Mapper.CreateMap(Of Pais, Pais)()
        Mapper.CreateMap(Of Libro, Libro)()
        Mapper.CreateMap(Of Editorial, Editorial)()
        Mapper.CreateMap(Of Categoria, Categoria)()
        Mapper.CreateMap(Of CategoriaCliente, CategoriaCliente)()
        Mapper.CreateMap(Of CategoriaVisitante, CategoriaVisitante)()
        Mapper.CreateMap(Of PedidoCabecera, PedidoCabecera)()
        Mapper.CreateMap(Of PedidoPosicion, PedidoPosicion)()
        Mapper.CreateMap(Of DocumentoCabecera, DocumentoCabecera)()
        Mapper.CreateMap(Of DocumentoPosicion, DocumentoPosicion)()
        Mapper.CreateMap(Of RemitoCabecera, RemitoCabecera)()
        Mapper.CreateMap(Of RemitoPosicion, RemitoPosicion)()
        Mapper.CreateMap(Of Comentario, Comentario)()
        Mapper.CreateMap(Of Puntuacion, Puntuacion)()
        Mapper.CreateMap(Of FichaDeOpinion, FichaDeOpinion)()
        Mapper.CreateMap(Of Encuesta, Encuesta)()
        Mapper.CreateMap(Of Pregunta, Pregunta)()
        Mapper.CreateMap(Of Respuesta, Respuesta)()
        Mapper.CreateMap(Of Newsletter, Newsletter)()
        Mapper.CreateMap(Of Novedad, Novedad)()
        Mapper.CreateMap(Of CrearNewsletterDTO, Newsletter)()
        Mapper.CreateMap(Of LibroNewsletter, LibroNewsletter)()
        Mapper.CreateMap(Of EncuestaBase, EncuestaBase)()
        Mapper.CreateMap(Of FavoritoCliente, FavoritoCliente)()
        Mapper.CreateMap(Of LeidoCliente, LeidoCliente)()
        Mapper.CreateMap(Of Tarjeta, Tarjeta)()
        GlobalConfiguration.Configuration.DependencyResolver = New StructureMapDependenctResolver(ObjectFactory.Container)
        GlobalConfiguration.Configuration.Filters.Add(New NhSessionManagementAttribute())
    End Sub
End Module
