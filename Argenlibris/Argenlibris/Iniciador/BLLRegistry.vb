﻿Imports StructureMap.Configuration.DSL
Imports BLL
Public Class BLLRegistry
    Inherits Registry
    Public Sub New()
        Scan(Sub(a)
                 'a.AssemblyContainingType(GetType(BaseBLL(Of )))
                 'a.IncludeNamespace(GetType(BaseBLL(Of )).Namespace)
                 a.AssemblyContainingType(GetType(IBLL(Of )))
                 a.IncludeNamespace(GetType(IBLL(Of )).Namespace)
                 a.WithDefaultConventions()
             End Sub)

    End Sub

End Class
