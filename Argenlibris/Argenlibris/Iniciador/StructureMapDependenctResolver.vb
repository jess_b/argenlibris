﻿Imports System.Web.Http.Dependencies
Imports System.Web.Http.Dispatcher
Imports StructureMap

Class StructureMapDependenctResolver
    Inherits StructureMapDependencyScope
    Implements IDependencyResolver, IHttpControllerActivator


    Dim container As IContainer
    Public Sub New(container As IContainer)
        MyBase.New(container)
        If container Is Nothing Then
            Throw New ArgumentNullException("contenedor")

        End If
        Me.container = container
        container.Inject(GetType(IHttpControllerActivator), Me)


    End Sub


    Public Function BeginScope() As IDependencyScope Implements IDependencyResolver.BeginScope
        Return New StructureMapDependencyScope(Me.container.GetNestedContainer())
    End Function

    Public Function Create(request As Net.Http.HttpRequestMessage, controllerDescriptor As Web.Http.Controllers.HttpControllerDescriptor, controllerType As Type) As Web.Http.Controllers.IHttpController Implements IHttpControllerActivator.Create
        Return Me.container.GetNestedContainer().GetInstance(controllerType)
    End Function
End Class
