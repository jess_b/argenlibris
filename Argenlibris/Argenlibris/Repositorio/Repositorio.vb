﻿
Imports NHibernate
Imports Argenlibris.NHibernate
Imports NHibernate.Linq
Imports Argenlibris.Domain
Imports System.Threading


Public Class Repositorio(Of T As Entity)
    Implements IRepositorio(Of T)




    Public Overridable Property DBContext As IDbContext Implements IRepositorio(Of T).DbContext
        Get
            Return New DbContext(_sessionFactory)
        End Get

        Set(value As IDbContext)

        End Set
    End Property
    Friend ReadOnly _sessionFactory As ISessionFactory
    Protected Overridable ReadOnly Property Session As ISession
        Get
            Return _sessionFactory.GetCurrentSession()
        End Get

    End Property



    Public Sub New(sessionFactory As ISessionFactory)
        _sessionFactory = sessionFactory


    End Sub



    Public Sub Delete(entidad_ As T) Implements IRepositorio(Of T).Delete

        Session.Delete(entidad_)
        If Not TypeOf (entidad_) Is Bitacora Then
            Dim b As New Bitacora
            b.Fecha = Date.Now
            b.TipoDeMensaje = EnumTipoDeError.S
            b.Mensaje = EnumTipoMensaje.Delete
            b.IdEntidad = entidad_.Id
            b.TipoDeEntidad = entidad_.GetType.ToString
            If Not Thread.CurrentPrincipal.Identity.Name.Equals(String.Empty) Then
                Dim _idUsuario As Guid
                _idUsuario = Guid.Parse(Thread.CurrentPrincipal.Identity.Name)
                b.Usuario = Session.Get(Of Usuario)(_idUsuario)
            End If
            Session.SaveOrUpdate(b)
        End If


    End Sub

    Public Function GetAll() As IQueryable(Of T) Implements IRepositorio(Of T).GetAll
        Return Session.Query(Of T)()
    End Function



    Public Function SaveOrUpdate(entidad_ As T) As T Implements IRepositorio(Of T).SaveOrUpdate
        Dim _id As Guid
        _id = entidad_.Id

        Session.SaveOrUpdate(entidad_)

    

        If Not TypeOf (entidad_) Is Bitacora Then
            Dim b As New Bitacora
            b.Fecha = Date.Now
            b.TipoDeMensaje = EnumTipoDeError.S
            'If IsNothing(entidad_.Id) Then
            If _id.Equals(Guid.Empty) Then
                b.Mensaje = EnumTipoMensaje.Create
            Else
                b.Mensaje = EnumTipoMensaje.Save
            End If
            b.IdEntidad = entidad_.Id
            b.TipoDeEntidad = entidad_.GetType.ToString
            If Not Thread.CurrentPrincipal.Identity.Name.Equals(String.Empty) Then
                Dim _idUsuario As Guid
                _idUsuario = Guid.Parse(Thread.CurrentPrincipal.Identity.Name)
                b.Usuario = Session.Get(Of Usuario)(_idUsuario)
            End If
            Session.SaveOrUpdate(b)
        End If



        Return entidad_
    End Function

    Public Function Obtain(id_ As Object) As T Implements IRepositorio(Of T).Obtain
        Dim _t As T
        _t = Session.Get(Of T)(id_)
        Return _t
    End Function


End Class
