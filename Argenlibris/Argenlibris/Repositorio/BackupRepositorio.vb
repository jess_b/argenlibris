﻿Imports Argenlibris.Domain
Imports NHibernate

Public Interface IBackupRepositorio
    Inherits IRepositorio(Of BackupFile)

    Function DoBackup() As String
    Sub RestoreBackup(file As String)
End Interface
Public Class BackupRepositorio
    Inherits Repositorio(Of BackupFile)
    Implements IBackupRepositorio



    Public Sub New(factory As ISessionFactory)
        MyBase.New(factory)
    End Sub


    Public Sub RestoreBackup(file As String) Implements IBackupRepositorio.RestoreBackup
        Dim folder As String = System.Configuration.ConfigurationManager.AppSettings("backup-folder")

        'Dim qry As String = "USE master; RESTORE DATABASE argenlibrisDB FROM DISK = '" & file & "' WITH REPLACE;"

        Dim qry As String = "use master; ALTER DATABASE argenlibrisDB SET SINGLE_USER WITH ROLLBACK IMMEDIATE; RESTORE DATABASE argenlibrisDB FROM DISK = '" & folder & file & "' WITH REPLACE; "
        qry = qry & "ALTER DATABASE argenlibrisDB SET MULTI_USER WITH ROLLBACK IMMEDIATE;"
        Dim sql As ISQLQuery = Session.CreateSQLQuery(qry)

        Dim o As Object = sql.UniqueResult

    End Sub

    Public Function DoBackup() As String Implements IBackupRepositorio.DoBackup
        Dim folder As String = System.Configuration.ConfigurationManager.AppSettings("backup-folder")

        Dim file As String = "BACKUP_ARGENLIBRIS_" & Date.Now.ToString("HHmmffffyyyyddMM") & ".BAK"
        Dim qry As String = "BACKUP  DATABASE argenlibrisDB  TO DISK='" & folder & file & "' WITH FORMAT; " ', NORECOVERY;"


        Dim sql As ISQLQuery = Session.CreateSQLQuery(qry)

        Dim o As Object = sql.UniqueResult

        Return file

    End Function


End Class
