﻿Imports Newtonsoft.Json

Public Module ManejadorToken
    Public Function Autorizar(token As String) As TokenSeguridad
        Dim t As TokenSeguridad

        t = desencriptarToken(token)
        t.Validar()
        Return t
    End Function

    Private Function encriptarToken(tk As TokenSeguridad) As String

        Dim o As String
        o = JsonConvert.SerializeObject(tk)
        Return Encriptador.GetInstance().CypherTripleDES(o, Nothing, False)


    End Function
    Private Function desencriptarToken(token As String) As TokenSeguridad

        Dim s As String
        s = Encriptador.GetInstance.DecypherTripleDES(token, Nothing, False)

        Dim t As TokenSeguridad
        t = JsonConvert.DeserializeObject(Of TokenSeguridad)(s)

        Return t

    End Function


    Public Function Login(id As Guid) As String
        Dim tk As New TokenSeguridad(id)
        Return encriptarToken(tk)
    End Function
End Module
