﻿Imports System.Security.Cryptography
Imports System.Text
Imports System.Configuration

Public Class Encriptador

    'Private _key As String = "506C1686-82BF-4d9a-83D5-0DF341A7AD7C"
    Private _key As String = "3A8B7284FBFF53A8B72853A8"

    Private Shared _instance As Encriptador
    Public Shared Function GetInstance() As Encriptador
        If (_instance Is Nothing) Then
            _instance = New Encriptador()
        End If
        Return _instance
    End Function
    Protected Sub New()

    End Sub

    '/// <summary>
    '/// Hashes a text using MD5 algorithm
    '/// </summary>
    '/// <param name="value">text to hash</param>
    '/// <returns>hashed text (Base64String)</returns>
    Public Function GetHashMD5(ByVal value As String) As String

        Dim Ue As UnicodeEncoding = New UnicodeEncoding()
        Dim ByteSourceText As Byte() = Ue.GetBytes(value)
        Dim Md5 As MD5CryptoServiceProvider = New MD5CryptoServiceProvider()
        '//obtener el valor del hash value desde el origen (byteSurceText)
        Dim ByteHash As Byte() = Md5.ComputeHash(ByteSourceText)
        Md5.Clear()
        '//retornarlo como un string
        Return Convert.ToBase64String(ByteHash)
    End Function

    '/// <summary>
    '/// Compares a Hash agains a non hashed value
    '/// </summary>
    '/// <param name="value">value to evaluate</param>
    '/// <param name="hash">hash to evaluate against</param>
    '/// <returns>True if the hash matches</returns>
    Public Function CompareHashMD5(ByVal value As String, ByVal hash As String) As Boolean

        Return (hash.Equals(Me.GetHashMD5(value)))
    End Function

    '/// <summary>
    '/// Cypher a text with TripleDES algorithm
    '/// </summary>
    '/// <param name="toEncrypt">text to cypher</param>
    '/// <param name="key">key to cypher</param>
    '/// <param name="useHashing">true if the key must be hashed (more secure)</param>
    '/// <returns>cypher text</returns>
    Public Function CypherTripleDES(ByVal toEncrypt As String, ByVal key As String, ByVal useHashing As Boolean) As String

        Dim keyArray As Byte()
        Dim toEncryptArray As Byte() = UTF8Encoding.UTF8.GetBytes(toEncrypt)

        If key Is Nothing Then
            key = _key
        End If

        Dim settingsReader As AppSettingsReader = New AppSettingsReader()

        '//System.Windows.Forms.MessageBox.Show(key);
        '//If hashing use get hashcode regards to your key
        If (useHashing) Then

            Dim hashmd5 As MD5CryptoServiceProvider = New MD5CryptoServiceProvider()
            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key))
            '//Always release the resources and flush data
            '// of the Cryptographic service provide. Best Practice
            hashmd5.Clear()

        Else

            keyArray = UTF8Encoding.UTF8.GetBytes(key)
        End If

        Dim tdes As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider()
        '//set the secret key for the tripleDES algorithm
        tdes.Key = keyArray
        '//mode of operation. there are other 4 modes.
        '//We choose ECB(Electronic code Book)
        tdes.Mode = CipherMode.ECB
        '//padding mode(if any extra byte added)
        tdes.Padding = PaddingMode.PKCS7
        Dim cTransform As ICryptoTransform = tdes.CreateEncryptor()
        '//transform the specified region of bytes array to resultArray
        Dim resultArray As Byte() = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length)
        '//Release resources held by TripleDes Encryptor
        tdes.Clear()
        '//Return the encrypted data into unreadable string format
        Return Convert.ToBase64String(resultArray, 0, resultArray.Length)

    End Function

    '/// <summary>
    '/// Decypher a text using the TripleDES algorithm
    '/// </summary>
    '/// <param name="toEncrypt">text to decypher</param>
    '/// <param name="key">key to decypher</param>
    '/// <param name="useHashing">true if the key must be hashed (more secure)</param>
    '/// <returns>clear text</returns>
    Public Function DecypherTripleDES(ByVal cipherString As String, ByVal key As String, ByVal useHashing As Boolean) As String

        Dim strResult As String = cipherString

        If key Is Nothing Then
            key = _key
        End If

        Try

            Dim keyArray As Byte()
            '//get the byte code of the string
            Dim toEncryptArray As Byte() = Convert.FromBase64String(cipherString)
            If useHashing Then

                '//if hashing was used get the hash code with regards to your key
                Dim hashmd5 As MD5CryptoServiceProvider = New MD5CryptoServiceProvider()
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key))
                '//release any resource held by the MD5CryptoServiceProvider
                hashmd5.Clear()

            Else

                '//if hashing was not implemented get the byte code of the key
                keyArray = UTF8Encoding.UTF8.GetBytes(key)
            End If
            Dim tdes As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider()
            '//set the secret key for the tripleDES algorithm
            tdes.Key = keyArray
            '//mode of operation. there are other 4 modes. 
            '//We choose ECB(Electronic code Book)
            tdes.Mode = CipherMode.ECB
            '//padding mode(if any extra byte added)
            tdes.Padding = PaddingMode.PKCS7
            Dim cTransform As ICryptoTransform = tdes.CreateDecryptor()
            Dim resultArray As Byte() = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length)
            '//Release resources held by TripleDes Encryptor                
            tdes.Clear()
            '//return the Clear decrypted TEXT
            strResult = UTF8Encoding.UTF8.GetString(resultArray)

        Catch

            '//the string may not be a base64 string
        End Try
        Return strResult
    End Function


    Public Function EncriptarAHash(ByVal textoEntrada_ As String) As String

        ' Objeto de codificación
        Dim _ue As New UnicodeEncoding()

        ' Objeto para instanciar las codificación
        Dim _md5 As New MD5CryptoServiceProvider()

        ' Calcula el valor hash de la cadena recibida
        Dim _hash() As Byte = _md5.ComputeHash(_ue.GetBytes(textoEntrada_))

        ' Convierte el hash en una cadena y lo devuelve
        Return Convert.ToBase64String(_hash)

    End Function

    Public Function Encriptar(ByVal strText As Object) As String

        Dim i As Integer, C As Integer
        Dim strBuff As String = ""

        If strText Is Nothing Or TypeOf strText Is System.DBNull Then

            Return String.Empty

        Else

            Convert.ToString(strText)

        End If

        If Len(_key) > 0 Then
            For i = 1 To Len(strText)
                C = Asc(Mid$(strText.ToString, i, 1))
                C = C + Asc(Mid$(_key, (i Mod Len(_key)) + 1, 1))
                strBuff = strBuff & Chr(C And &HFF)
            Next i
        Else
            strBuff = strText.ToString
        End If

        Return strBuff

    End Function

    Public Function Desencriptar(ByVal strText As Object) As String

        Dim i As Integer, C As Integer
        Dim strBuff As String = ""

        If strText Is Nothing Or TypeOf strText Is System.DBNull Then

            Return String.Empty

        Else

            Convert.ToString(strText)

        End If

        If Len(_key) > 0 Then
            For i = 1 To Len(strText)
                C = Asc(Mid$(strText.ToString, i, 1))
                C = C - Asc(Mid$(_key, (i Mod Len(_key)) + 1, 1))
                strBuff = strBuff & Chr(C And &HFF)
            Next i
        Else
            strBuff = strText.ToString
        End If

        Return strBuff

    End Function

End Class
