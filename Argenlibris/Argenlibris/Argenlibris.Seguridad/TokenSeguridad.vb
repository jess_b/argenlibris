﻿Public Class TokenSeguridad


    Dim _id As Guid
    Dim _fechaVencimiento As DateTime

    Public Sub New(id As Guid)
        _id = id
        _fechaVencimiento = DateTime.Now.AddHours(6)
    End Sub

    Public ReadOnly Property Id As Guid
        Get
            Return _id
        End Get

    End Property

    Public ReadOnly Property FechaVencimiento As DateTime
        Get
            Return _fechaVencimiento
        End Get
    End Property
    Public Sub Validar()
        If DateTime.Now >= _fechaVencimiento Then Throw New TokenInvalidoException("token vencido")


    End Sub
End Class
