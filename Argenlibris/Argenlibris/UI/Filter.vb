﻿Imports System.Web.Http.Filters
Imports BLL
Imports System.Net.Http
Imports System.Net.Http.Formatting

Public Class Filter
    Inherits FilterAttribute


    Public Enum EnumTipoDeError

        E
        W
        I
        S

    End Enum



    Dim _roles As EnumTipoDeError()

    Sub New(ByVal ParamArray roles As EnumTipoDeError())
        _roles = roles

    End Sub

    Public Function ChequearRoles(actionContext As Http.Controllers.HttpActionContext, cancellationToken As Threading.CancellationToken) As Threading.Tasks.Task


        For Each r As EnumTipoDeError In _roles
            If Not HttpContext.Current.User.IsInRole(r.ToString()) Then
                'actionContext.cResponse.StatusCode = 403
                actionContext.Response = New HttpResponseMessage(Net.HttpStatusCode.Forbidden)




                'Return Task.FromResult(Of Object)(Nothing)
            End If
        Next


        'Return MyBase.OnActionExecutingAsync(actionContext, cancellationToken)

    End Function
End Class
