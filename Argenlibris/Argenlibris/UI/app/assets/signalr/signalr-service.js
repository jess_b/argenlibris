﻿app.constant('$', window.jQuery);
app.factory('backendHubProxy', ['$', '$rootScope', function ($, $rootScope) {

    function backendFactory(hubName) {
        var connection = $.hubConnection('/signalr');
        connection.qs = { "userId": $rootScope.userId }
        var proxy = connection.createHubProxy(hubName);


        //if (angular.isUndefined(connection)) {
        //    $.connection.hub.logging = true;
        //} else {
        //    connection.logging = true;
        //}
        //  $.connection.hub.logging = true;
        connection.start(['webSockets', 'longPolling', 'serverSentEvents', 'foreverFrame']).done(function () { });

        return {
            on: function (eventName, callback) {

                proxy.on(eventName, function (result, result2,result3) {
                    $rootScope.$apply(function () {
                        if (callback) {
                            callback(result, result2,result3);
                        }
                    });
                });
            },
            invoke: function (methodName, callback, params, param2, param3) {
                connection.start(['webSockets', 'longPolling', 'serverSentEvents', 'foreverFrame']).done(function () {
                    proxy.invoke(methodName, params, param2, param3).done(function (result) {
                        $rootScope.$apply(function () {
                            if (callback) {
                                callback(result);
                            }
                        });
                    });


                });
            }
        };
    };

    return backendFactory;
}]);
