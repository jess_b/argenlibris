﻿(function () {
    'use strict';

    app.directive('jointStencil', ['$window', function ($window) {

        var directive = {
            link: link,
            restrict: 'E',
            scope: {
                height: '=',
                width: '=',
                graph: '=',
            }
        };

        return directive;

        function link(scope, element, attrs) {

            var stencil = newStencil(scope.height, scope.width,$window.graph, scope.paper, element[0]);
                        

            
        }

        function newStencil(height,width, graph, paper, targetElement) {

            

            var stencil = new joint.ui.Stencil({
                graph: graph,
                paper: paper,
                width: 200,
                height: 300,
                groups: {
                basic: { label: 'Formas básicas', index: 1 },
                uml: { label: 'UML', index: 2, closed: true }
                    }
                    });
            var html = stencil.render().el;
            
            targetElement.appendChild(html);


            var r = new joint.shapes.basic.Rect({
                position: { x: 10, y: 10 }, size: { width: 50, height: 30 },
                attrs: { rect: { fill: '#2ECC71' }, text: { text: 'rect', fill: 'black' } }
            });
            var c = new joint.shapes.basic.Circle({
                position: { x: 70, y: 10 }, size: { width: 50, height: 30 },
                attrs: { circle: { fill: '#9B59B6' }, text: { text: 'circle', fill: 'white' } }
            });

            var t = new joint.shapes.basic.Text({
                position: { x: 10, y: 10 }, size: { width: 50, height: 30 },
                attrs: { text: { text: 'Text' } }
            });


            stencil.load([r, c],'basic');
            stencil.load([t], 'uml');


            return stencil;
        }

    }]);

})();