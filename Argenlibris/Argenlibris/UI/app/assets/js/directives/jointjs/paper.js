﻿(function () {
    'use strict';

    app.directive('jointPaper', ['$window',function ($window) {

        var directive = {
            link: link,
            restrict: 'E',
            scope: {
                height: '=',
                width: '=',
                gridSize: '=',
                graph: '=',
            }
        };

        return directive;

        function link(scope, element, attrs) {


            
            var paper = newPaper(scope.height, scope.width, scope.gridSize, $window.graph, element[0]);

            ////add event handlers to interact with the diagram
            //paper.on('cell:pointerclick', function (cellView, evt, x, y) {

            //    //your logic here e.g. select the element

            //});

            //paper.on('blank:pointerclick', function (evt, x, y) {

            //    // your logic here e.g. unselect the element by clicking on a blank part of the diagram
            //});

            //paper.on('link:options', function (evt, cellView, x, y) {

            //    // your logic here: e.g. select a link by its options tool
            //});
        }

        function newPaper(height, width, gridSize, graph, targetElement) {

            var paper = new joint.dia.Paper({
                el: targetElement,
                width: width,
                height: height,
                gridSize: gridSize,
                model: graph,
            });

            return paper;
        }

    }]);

})();