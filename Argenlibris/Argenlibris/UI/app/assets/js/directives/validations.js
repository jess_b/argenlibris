﻿app.directive('validUsername', ['$http','usuarioService',
            function ($http,usuarioService) {
                return {
                    restrict: 'A',
                    require: 'ngModel',
                    link: function (scope, elm, attrs, ctrl) {
                        var validateFn = function (viewValue) {

                             
                            usuarioService.check(viewValue).then(function (response) {
                                    {
                                        ctrl.$setValidity('validPassword', !response.data);
                                    }
                                }, function (error) {
                                    ctrl.$setValidity('validPassword', false);
                                });
                            
                            
                            return viewValue;

                        };
                        ctrl.$parsers.push(validateFn);
                        ctrl.$formatters.push(validateFn);
                    }
                }
            }]);

//app.directive('validPassword', [
//            function () {
//                return {
//                    restrict: 'A',
//                    require: 'ngModel',
//                    link: function (scope, elm, attrs, ctrl) {


//                        var validateFn = function (viewValue) {
//                            var pass = scope.myForm.password.$viewValue; //name="password" en el campo a comparar y form name="myForm"
//                            var noMatch = !(viewValue != pass);
                            
//                                ctrl.$setValidity('validPassword', noMatch);
                                
//                                return viewValue;
                            
//                        };
//                        ctrl.$parsers.push(validateFn);
//                        ctrl.$formatters.push(validateFn);
//                    }
//                }
//            }]);

