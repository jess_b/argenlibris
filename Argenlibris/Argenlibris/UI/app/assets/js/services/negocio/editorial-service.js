﻿app.factory('editorialService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (editorial) {
        return $http.put("/api/editorial/", editorial);

    }


    var del = function (id) {
        return $http.delete("/api/editorial/" + id);

    }

    var all = function () {
        return $http.post("/api/editorial/");

    }

    var one = function (id) {
        return $http.get("/api/editorial/" + id);

    }



    return {
        one: one,
        put: put,
        del: del,
        all: all


    };

}]);