﻿app.factory('libroService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (libro) {
        return $http.put("/api/libro/", libro);

    }


    var del = function (id) {
        return $http.delete("/api/libro/" + id);

    }

    var all = function () {
        return $http.post("/api/libro/");

    }

    var one = function (id) {
        return $http.get("/api/libro/" + id);

    }

    var send = function (id) {
        $http.head("/api/libro/" + id);

    }

    var dos = function () {
        return $http.post("/api/librodos/");

    }

    return {
        one: one,
        put: put,
        del: del,
        all: all,
        dos: dos,
        send: send

    };

}]);