﻿app.factory('paisService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (pais) {
        return $http.put("/api/pais/", pais);

    }


    var del = function (id) {
        return $http.delete("/api/pais/" + id);

    }

    var all = function () {
        return $http.post("/api/pais/");

    }

    var one = function (id) {
        return $http.get("/api/pais/" + id);

    }



    return {
        one: one,
        put: put,
        del: del,
        all: all


    };

}]);