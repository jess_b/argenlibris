﻿app.factory('autorService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (autor) {
        return $http.put("/api/autor/", autor);

    }


    var del = function (id) {
        return $http.delete("/api/autor/" + id);

    }

    var all = function () {
        return $http.post("/api/autor/");

    }

    var one = function (id) {
        return $http.get("/api/autor/" + id);

    }



    return {
        one: one,
        put: put,
        del: del,
        all: all


    };

}]);