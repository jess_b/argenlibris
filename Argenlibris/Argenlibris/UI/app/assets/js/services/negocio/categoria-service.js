﻿app.factory('categoriaService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (categoria) {
        return $http.put("/api/categoria/", categoria);

    }


    var del = function (id) {
        return $http.delete("/api/categoria/" + id);

    }

    var all = function () {
        return $http.post("/api/categoria/");

    }

    var one = function () {
        return $http.get("/api/categoria/");

    }

    var mis = function (id) {
        return $http.get("/api/categoriacliente/" + id);

    }

    var act = function (cscs) {
        return $http.post("/api/categoriaclientedos/", cscs);

    }



    return {
        one: one,
        put: put,
        del: del,
        mis: mis,
        act: act,
        all: all


    };

}]);