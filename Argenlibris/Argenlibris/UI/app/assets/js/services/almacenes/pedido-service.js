﻿app.factory('pedidoService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (pedido) {
        return $http.put("/api/pedido/", pedido);

    }


    var del = function (id) {
        return $http.delete("/api/pedido/" + id);

    }

    var all = function () {
        return $http.post("/api/pedido/");

    }

    var one = function (id) {
        return $http.get("/api/pedido/" + id);

    }



    return {
        one: one,
        put: put,
        del: del,
        all: all


    };

}]);