﻿app.factory('remitoService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (remito) {
        return $http.put("/api/remito/", remito);

    }


    var del = function (id) {
        return $http.delete("/api/remito/" + id);

    }

    var all = function () {
        return $http.post("/api/remito/");

    }

    var one = function (id) {
        return $http.get("/api/remito/" + id);

    }



    return {
        one: one,
        put: put,
        del: del,
        all: all


    };

}]);