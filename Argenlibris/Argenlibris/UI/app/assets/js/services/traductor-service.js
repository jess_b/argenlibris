﻿app.factory('traductorService', ['$http', '$rootScope', function ($http, $rootScope) {

    var clavePalabra = [];
    var valorPalabra = [];
    //var traducir = function (scope) {
       
    //    $http.post("/api/traductor/" + $rootScope.idiomaActual, clavePalabra).then(function (response) {
    //        valorPalabra = response.data;
    //        scope.traductor = valorPalabra;
    //    });


        
    //}
    var traducir = function (scope) {

        $http.post("/api/traductordos/" + $rootScope.idiomaActual, clavePalabra).then(function (response) {
            valorPalabra = response.data;
            scope.traductor = valorPalabra;
        });



    }
    var palabras = function (arrPalabras)
    {
        clavePalabra = arrPalabras;
       
    };
   
   
    var traductor = function ()
    {
       
        
        return valorPalabra;
    
    
    }

    var put = function (lista) {
        return $http.put("/api/traductor/", lista);
    }

    var all = function (idioma) {
        return $http.get("/api/traductor/" + idioma);
    }

    var otro = function (idioma) {
        return $http.get("/api/traductordos/" + idioma);
    }

    return {
        all: all,
        put: put,
        otro: otro,
        palabras: palabras,
        traductor:traductor, 
        traducir: traducir

    };

}]);