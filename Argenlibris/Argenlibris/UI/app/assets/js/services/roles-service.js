﻿app.factory('rolService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (rol) {
        return $http.put("/api/rol/", rol);

    }


    var del = function (id) {
        return $http.delete("/api/rol/" + id);

    }

    var all = function () {
        return $http.post("/api/rol/");

    }

    var one = function (id) {
        return $http.get("/api/rol/" + id);

    }


    var miRol = function () {
        return $http.post("/api/mirol/");
    }

    var miRolGet = function (id) {
        return $http.get("/api/mirol/" + id);
    }

    return {
        one: one,
        put: put,
        del: del,
        all: all,
        miRol: miRol,
        miRolGet: miRolGet


    };

}]);