﻿app.factory('proyectoService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (proyecto) {
        return $http.put("/api/proyecto/", proyecto);

    }

   
    var all = function (id) {
        return $http.get("/api/proyecto/");

    }

    var one = function (id) {
        return $http.get("/api/proyecto/" + id);

    }

    var count = function () {
        return $http.get("/api/proyecto/count" );
    }
    var resumen= function () {
        return $http.get("/api/proyecto/resumen");
    }

    return {
        one: one,
        put: put,
        all: all,
        count: count,
        resumen: resumen

    };

}]);