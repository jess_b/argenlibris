﻿app.factory('usuarioService', ['$http','$rootScope',function ($http,$rootScope) {
    
    var put = function (usuario) {
        return $http.put("/api/usuario/", usuario).then(function (data) {
            $rootScope.entitySaved();
        });
        
    }

    var cambiaPassword = function (dto) {
        return $http.post("/api/password/",dto);
    }
    var all = function () {
        return $http.post("/api/usuario/");

    }
    var one = function (id) {
            return $http.get("/api/usuario/" + id);
        
    }
    var check = function (id) {
        return $http.get("/api/usuario/check/" + id);

    }
  return {
      one: one,
      put: put,
      check: check,
      all: all,
      cambiaPassword: cambiaPassword
    };
   
}]);