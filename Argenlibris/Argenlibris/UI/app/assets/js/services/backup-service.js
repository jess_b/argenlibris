﻿app.factory('backupService', ['$http', '$rootScope', function ($http, $rootScope) {

    var one = function () {
        return $http.put("/api/backup/");

    }

    var all = function () {
        return $http.get("/api/backup/");

    }

    var restore = function (id) {
        return $http.get("/api/backup/"+id);

    }


    return {
     
        one: one,
        all:all,
        restore:restore


    };

}]);
