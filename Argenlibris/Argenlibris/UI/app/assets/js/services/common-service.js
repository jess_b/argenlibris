﻿app.factory('commonService', ['$http', '$rootScope', function ($http, $rootScope) {



    var dias = function () {
        return $http.get("/api/common/dias");

    }
    var turnos = function () {
        return $http.get("/api/common/turnos");

    }
    var sedes = function () {
        return $http.get("/api/common/sedes");

    }
    return {
        turnos: turnos,
        dias: dias,
        sedes: sedes

    };

}]);