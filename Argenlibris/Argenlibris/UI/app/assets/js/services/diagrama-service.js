﻿app.factory('diagramaService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (diagrama) {
        return $http.put("/api/diagrama/"+$rootScope.selectedDiagram.Id, diagrama);

    }


    var oneDiagram = function(id)
    {
        
        return $http.get("/api/diagrama/" + id);
    }

     var one = function (id) {
        return $http.get("/api/diagrama/" + id);

    }



    return {
        one: one,
        put: put,
        oneDiagram: oneDiagram


    };

}]);