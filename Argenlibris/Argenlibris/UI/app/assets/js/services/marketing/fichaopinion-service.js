﻿app.factory('fichaopinionService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (fichaopinion) {
        return $http.put("/api/fichaopinion/", fichaopinion);

    }


    var del = function (id) {
        return $http.delete("/api/fichaopinion/" + id);

    }

    var all = function () {
        return $http.post("/api/fichaopinion/");

    }

    var dos = function () {
        return $http.post("/api/fichaopiniondos/");

    }

    var one = function (id) {
        return $http.get("/api/fichaopinion/" + id);

    }



    return {
        one: one,
        put: put,
        dos: dos,
        del: del,
        all: all


    };

}]);