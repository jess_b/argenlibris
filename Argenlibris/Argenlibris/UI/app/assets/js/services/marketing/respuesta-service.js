﻿app.factory('respuestaService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (respuesta) {
        return $http.put("/api/respuesta/", respuesta);

    }


    var del = function (id) {
        return $http.delete("/api/respuesta/" + id);

    }

    //var all = function (respuesta) {
    //    return $http.post("/api/respuesta/", respuesta);

    //}

    var all = function (pregunta) {
        return $http.post("/api/respuesta/", pregunta);

    }

    var one = function (id) {
        return $http.get("/api/respuesta/" + id);

    }

    var misrespuestas = function (id) {
        return $http.get("/api/respuesta/" + id);

    }
    

    return {
        //one: one,
        put: put,
        del: del,
        all: all,
        one: one,
        misrespuestas: misrespuestas


    };

}]);