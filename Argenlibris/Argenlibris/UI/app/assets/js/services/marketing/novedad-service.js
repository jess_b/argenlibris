﻿app.factory('NovedadService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (novedad) {
        return $http.put("/api/novedad/", novedad);

    }


    var del = function (id) {
        return $http.delete("/api/novedad/" + id);

    }

    var all = function () {
        return $http.post("/api/novedad/");

    }

    var one = function () {
        return $http.get("/api/novedad/" );

    }

    var send = function (id) {
        $http.head("/api/novedad/" + id);
        //return $http.head("/api/newsletter/", item);

    }

    return {
        one: one,
        put: put,
        del: del,
        all: all,
        send: send


    };

}]);