﻿app.factory('clienterespuestaService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (clienterespuesta) {
        return $http.put("/api/clienterespuesta/", clienterespuesta);

    }


    var del = function (id) {
        return $http.delete("/api/clienterespuesta/" + id);

    }

    //var all = function () {
    //    return $http.post("/api/clienterespuesta/");

    //}
    var all = function (respuesta) {
        return $http.post("/api/clienterespuesta/", respuesta);

    }

    var one = function (id) {
        return $http.get("/api/clienterespuesta/" + id);

    }



    return {
        one: one,
        put: put,
        del: del,
        all: all


    };

}]);