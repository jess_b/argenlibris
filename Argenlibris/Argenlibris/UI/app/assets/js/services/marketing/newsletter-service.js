﻿app.factory('newsletterService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (newsletter) {
        return $http.put("/api/newsletter/", newsletter);

    }


    var del = function (id) {
        return $http.delete("/api/newsletter/" + id);

    }

    var all = function () {
        return $http.post("/api/newsletter/");

    }

    var dos = function () {
        return $http.post("/api/newsletterdos/");

    }

    var one = function (id) {
        return $http.get("/api/newsletter/" + id);

    }

    var send = function (id) {
        $http.head("/api/newsletter/" + id);
        //return $http.head("/api/newsletter/", item);

    }

    return {
        one: one,
        put: put,
        del: del,
        all: all,
        dos: dos,
        send: send


    };

}]);