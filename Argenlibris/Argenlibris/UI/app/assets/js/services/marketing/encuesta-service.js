﻿app.factory('encuestaService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (encuesta) {
        return $http.put("/api/encuesta/", encuesta);

    }


    var del = function (id) {
        return $http.delete("/api/encuesta/" + id);

    }

    var dos = function () {
        return $http.post("/api/encuestados/");

    }

    var all = function () {
        return $http.post("/api/encuesta/");

    }

    //var one = function (id) {
    //    return $http.get("/api/encuesta/" + id);

    //}



    return {
        //one: one,
        put: put,
        del: del,
        dos: dos,
        all: all


    };

}]);