﻿app.factory('preguntaService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (pregunta) {
        return $http.put("/api/pregunta/", pregunta);

    }


    var del = function (id) {
        return $http.delete("/api/pregunta/" + id);

    }

    var all = function () {
        return $http.post("/api/pregunta/");

    }

    var one = function (id) {
        return $http.get("/api/pregunta/" + id);

    }



    return {
        one: one,
        put: put,
        del: del,
        all: all


    };

}]);