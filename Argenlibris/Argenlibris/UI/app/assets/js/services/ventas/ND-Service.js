﻿app.factory('NDService', ['$http', '$rootScope', function ($http, $rootScope) {


    var put = function (pedido) {
        return $http.put("/api/ND/", pedido);

    }


    var del = function (id) {
        return $http.delete("/api/ND/" + id);

    }

    var all = function () {
        return $http.get("/api/ND/");

    }

    var one = function (facturas) {
        return $http.post("/api/ND/", facturas);

    }



    return {
        one: one,
        put: put,
        del: del,
        all: all

    };

}]);