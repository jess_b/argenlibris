﻿app.factory('informeService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (fechadesde, fechahasta) {
        return $http.put("/api/totaldocumentos/", { FechaDesde: fechadesde, FechaHasta: fechahasta });

    }


    var all = function (fechadesde, fechahasta) {

        return $http.post("/api/totaldocumentos/", { FechaDesde: fechadesde, FechaHasta: fechahasta });

    }


    var one = function () {

        return $http.get("/api/totaldocumentos/");

    }

    //var one = function (id) {
    //    return $http.get("/api/totaldocumentos/" + id);

    //}



    return {
        one: one,
        put: put,
        all: all


    };

}]);
