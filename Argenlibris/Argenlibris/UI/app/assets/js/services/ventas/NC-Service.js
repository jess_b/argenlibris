﻿app.factory('NCService', ['$http', '$rootScope', function ($http, $rootScope) {


    var put = function (pedido) {
        return $http.put("/api/NC/", pedido);

    }


    var del = function (id) {
        return $http.delete("/api/NC/" + id);

    }

    var all = function () {
        return $http.get("/api/NC/");

    }

    //var all = function () {
    //    return $http.post("/api/NC/");

    //}

    var one = function (nc) {
        return $http.post("/api/NC/", nc);

    }



    return {
        one: one,
        put: put,
        del: del,
        all: all

    };

}]);