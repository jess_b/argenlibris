﻿app.factory('representantesService', ['$http', '$rootScope', function ($http, $rootScope) {



  
    var put = function (pedido) {
        return $http.put("/api/representantes/", pedido);

    }


    var del = function (id) {
        return $http.delete("/api/representantes/" + id);

    }

    var allClientesVendedores = function () {
        return $http.get("/api/representantes/");

    }
    var all = function () {
        return $http.post("/api/representantes/");

    }

   

    var one = function (id) {
        return $http.get("/api/representantes/" + id);

    }



    return {
        one: one,
        put: put,
        del: del,
        all: all,
        allClientesVendedores: allClientesVendedores


    };

}]);