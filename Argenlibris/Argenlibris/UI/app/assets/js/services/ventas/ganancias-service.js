﻿app.factory('gananciasService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (fechadesde, fechahasta) {
        return $http.put("/api/totalganancias/", { FechaDesde: fechadesde, FechaHasta: fechahasta });

    }



    var all = function (fechadesde, fechahasta) {

        return $http.post("/api/totalganancias/", { FechaDesde: fechadesde, FechaHasta: fechahasta });

    }


    var one = function () {

        return $http.get("/api/totalganancias/");

    }





    return {
        one: one,
        put: put,
        all: all



    };

}]);
