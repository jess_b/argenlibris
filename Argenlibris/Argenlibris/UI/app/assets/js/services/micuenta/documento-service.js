﻿app.factory('documentoService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (item) {
        return $http.put("/api/documentos/", item);

    }


    var del = function (id) {
        return $http.delete("/api/documentos/" + id);

    }

    var all = function () {
        return $http.post("/api/documentos/");

    }

    var one = function (dto) {
        return $http.head("/api/documentos/", dto);

    }



    return {
        one: one,
        put: put,
        del: del,
        all: all


    };

}]);