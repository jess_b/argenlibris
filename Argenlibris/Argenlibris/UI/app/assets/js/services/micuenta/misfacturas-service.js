﻿app.factory('misfacturasService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (pedido) {
        return $http.put("/api/factura/", pedido);

    }


    var del = function (id) {
        return $http.delete("/api/factura/" + id);

    }

    var all = function () {
        return $http.post("/api/factura/");

    }

    var one = function (id) {
        return $http.get("/api/factura/" + id);

    }



    return {
        one: one,
        put: put,
        del: del,
        all: all


    };

}]);