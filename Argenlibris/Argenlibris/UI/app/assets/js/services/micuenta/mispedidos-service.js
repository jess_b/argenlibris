﻿app.factory('mispedidosService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (pedido) {
        return $http.put("/api/mispedidos/", pedido);

    }


    var del = function (id) {
        return $http.delete("/api/mispedidos/" + id);

    }

    var all = function () {
        return $http.post("/api/mispedidos/");

    }

    var one = function (id) {
        return $http.get("/api/mispedidos/" + id);

    }



    return {
        one: one,
        put: put,
        del: del,
        all: all


    };

}]);