﻿app.factory('bitacoraService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function () {
        return $http.put("/api/bitacora/");

    }


    var all = function (idusuario, fechadesde, fechahasta, TipoOperacion) {

        return $http.post("/api/bitacora/", { Usuario: idusuario, FechaDesde: fechadesde, FechaHasta: fechahasta, TipoOperacion: TipoOperacion });

    }

    var one = function (id) {
        return $http.get("/api/bitacora/" + id);

    }



    return {
        one: one,
        put: put,
        all: all


    };

}]);
