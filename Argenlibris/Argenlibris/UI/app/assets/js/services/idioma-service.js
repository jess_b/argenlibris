﻿app.factory('idiomaService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (idioma) {
        return $http.put("/api/idioma/", idioma);

    }


    var del = function (id) {
        return $http.delete("/api/idioma/" + id);

    }

    var all = function ( ) {
        return $http.post("/api/idioma/");

    }

    var one = function () {
        return $http.get("/api/idioma/");

    }



    return {
        one: one,
        put: put,
        del: del,
        all: all


    };

}]);