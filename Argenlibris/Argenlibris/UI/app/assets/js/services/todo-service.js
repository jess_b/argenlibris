app.factory('todoService', ['localStorageService', '$rootScope', '$filter','$http', function (localStorageService, $rootScope, $filter,$http) {
  function Todo ($scope) {
    this.$scope = $scope;
    this.todoFilter = {};
    this.activeFilter = 0;

    this.input = angular.element('#todo-title');

    this.filters = [
      {
        'title': 'Todas',
        'method': 'all'
      },
      {
        'title': 'Pendientes',
        'method': 'active'
      },
      {
        'title': 'Finalizadas',
        'method': 'completed'
      }
    ];

    this.newTodo = {
      Title: '',
      Done: false,
      editing: false
    };



    this.completedTodos = function () {



        return $filter('filter')(this.$scope.todos, { Done: !true });
    };


   this.loadData=function()
    {
        var res = $http.get("/api/todo");

        $scope.todos = [];
        res.then(function (data) {
            $scope.todos = data.data;
            var pendientes = ($filter('filter')($scope.todos, { Done: !true }));
            $rootScope.$broadcast('todos:count', pendientes.length);
          


            
        }, function () { }).finally(function () {

        });
        //$rootScope.$broadcast('todos:count', this.count());
        this.restore();

        
    }
    //this.loadData();
    this.restore();
    
    
    


    
    


    this.addTodo = function() {
        if (this.todo.Title !== '' && this.todo.Title !== undefined) {
            var editing = this.todo.editing;
          $http.put("/api/todo", this.todo).then(function (response) {
              
              this.todo = response.data;
              if (!editing)
              $scope.todos.push(this.todo);
              var pendientes = ($filter('filter')($scope.todos, { Done: !true }));
              console.log(pendientes);
              $rootScope.$broadcast('todos:count', pendientes.length);
         

          }, function (error) {
            }).finally(function () {
                
          });


      
        this.restore();
      }
    };

    this.updateTodo = function() {
      this.restore();
    };
  }

  Todo.prototype.saveTodo = function(todo) {
      if (todo!=undefined)
        this.todo = todo;
      this.addTodo();
    
  };

  Todo.prototype.editTodo = function(todo) {
    this.todo = todo;
    this.todo.editing = true;
    this.input.focus();
  };

  Todo.prototype.toggleDone = function (todo) {
      //todo.Done = !todo.Done;
      todo.editing = true;
      this.saveTodo(todo);
          $rootScope.$broadcast('todos:count', this.count());
      
  };

  Todo.prototype.clearCompleted = function() {
    this.$scope.todos = this.completedTodos();
    this.restore();
  };

  Todo.prototype.count = function() {
    c = this.completedTodos();
    return c.length;
  };

  Todo.prototype.restore = function(focus) {
    focus = typeof focus !== 'undefined' ? focus : true;

    this.todo = angular.copy(this.newTodo);
    this.input.val('');

    if ( focus === true )
      this.input.focus();
  };

  Todo.prototype.filter = function(filter) {
    if ( filter === 'active' ) {
      this.activeFilter = 1;
      this.todoFilter = { Done: false };
    } else if ( filter === 'completed' ) {
      this.activeFilter = 2;
      this.todoFilter = { Done: true };
    } else {
      this.activeFilter = 0;
      this.todoFilter = {};
    }
  };

  return Todo;
}]);
