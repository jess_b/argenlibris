﻿app.factory('chatService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (msg) {
        return $http.put("/api/chat/",msg);

    }


    var all = function (idRepresentante) {

        return $http.get("/api/chat/"+idRepresentante );

    }




    return {
     
        put: put,
        all: all


    };

}]);