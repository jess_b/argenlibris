﻿app.factory('usuariorolService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (usuariosrol) {
            return $http.put("/api/rolusuario/", usuariosrol);
    }

    var all = function () {
        return $http.post("/api/rolusuario/");

    }
    var one = function (id) {
        return $http.get("/api/rolusuario/" + id);

    }

    return {
        one: one,
        put: put,
        all: all
    };

}]);