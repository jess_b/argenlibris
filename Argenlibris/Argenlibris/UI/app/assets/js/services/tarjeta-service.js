﻿app.factory('tarjetaService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (tarjeta) {
        return $http.put("/api/tarjeta/", tarjeta);

    }
    var all = function () {
        return $http.post("/api/tarjeta/");

    }
    var one = function (id) {
        return $http.get("/api/tarjeta/" + id);

    }
    var check = function (id) {
        return $http.get("/api/tarjeta/check/" + id);

    }

    var del = function (id) {
        return $http.delete("/api/tarjeta/" + id);

    }
    return {
        one: one,
        put: put,
        check: check,
        del: del,
        all: all
    };

}]);