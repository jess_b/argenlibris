﻿app.factory('permisoService', ['$http', '$rootScope', function ($http, $rootScope) {

    var put = function (permiso) {
        return $http.put("/api/permiso/", permiso);

    }


    var del = function (id) {
        return $http.delete("/api/permiso/" + id);

    }

    var all = function () {
        return $http.post("/api/permiso/");

    }

    var one = function (id) {
        return $http.get("/api/permiso/" + id);

    }



    return {
        one: one,
        put: put,
        del: del,
        all: all


    };

}]);