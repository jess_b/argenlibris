angular.module('app.constants', [])

.constant('APP', { version: '1.1.0' })
.constant('USER_ROLES', {
    admin: 'admin',
    alumno: 'alumno',
    docente: 'docente'
});