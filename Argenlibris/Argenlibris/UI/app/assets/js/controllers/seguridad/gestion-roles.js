﻿app.controller('GestionRolesController', ['$scope', '$window', '$aside', 'rolService', 'permisoService', '$rootScope', function ($scope, $window, $aside,  rolService, permisoService, $rootScope) {
    $scope.item = {};
    $scope.item.Permisos = [];

    // settings
    $scope.settings = {
        singular: 'rol',
        plural: 'roles',
        cmd: 'Agregar'
    };


    permisoService.all().then(function (response) {
        $scope.permisos = response.data;
    });


    $scope.agregarPermiso = function (item) {

        var agregar = true;

        //if ($scope.item.Permisos == undefined) {
        //    $scope.item.Permisos = [];
        //}

        $scope.item.Permisos.forEach(
            function (p) {
                if (item.Id == p.Id) {
                    agregar = false;
                }
            });

        if (agregar == true) {
            $scope.item.Permisos.push(item);
        }


        
    }

    $scope.loadData = function (rol) {

        var usu = rolService.all().then(function (items) {
            $scope.data = items.data;

        });

    }

    $scope.loadData($rootScope.rolActual);



    // defining template
    var formTpl = $aside({
        scope: $scope,
        template: '/app/assets/tpl/seguridad/gestion-roles-form.html',
        show: false,
        placement: 'left',
        backdrop: false,
        animation: 'am-slide-left'
    });

    // methods
    $scope.checkAll = function () {
        angular.forEach($scope.data, function (item) {
            item.selected = !item.selected;
        });
    };

    $scope.editItem = function (item) {
        if (item) {
            item.editing = true;
            $scope.item = item;
            $scope.settings.cmd = 'Editar';

            rolService.one(item.Id).then(function (response) {
                $scope.item.Permisos = response.data;
            });
            
            showForm();
        }
    };

    $scope.viewItem = function (item) {
        if (item) {
            item.editing = false;
            $scope.item = item;
            $scope.settings.cmd = 'Ver';

            rolService.one(item.Id).then(function (response) {
                $scope.item.Permisos = response.data;
            });

            showForm();
        }
    };




    $scope.rolPut = function () {

        rolService.put($scope.item).then(function (rol) {
            //if (proyecto.data!=undefined && proyecto.data!="")
            //$rootScope.roles.push(rol.data);
            $rootScope.entitySaved();
            if ($scope.item.Estado == "Nuevo")
                $scope.data.push(rol.data);
            $rootScope.$broadcast('rol:nuevo', $scope.data);

        }, function (error) {

        }).finally(function () {

            $scope.loadData();

        });


    };


    $scope.createItem = function () {
        var item = {
            editing: true,
            Estado: "Nuevo",
            Descripcion: ""
        };
        $scope.item = item;
        $scope.settings.cmd = 'Nuevo';
        showForm();
    };



    $scope.saveItem = function () {
        if ($scope.settings.cmd == 'Guardar') {
            $scope.data.push($scope.item);

        }
        hideForm();
    };

    $scope.remove = function (item) {
        if (confirm('¿Está seguro?')) {
            if (item) {
                //if (item.id !== $rootScope.rolActual) {

                    rolService.del(item.Id).then(function (item) {
                        $scope.loadData();
                        $rootScope.roles.splice($scope.data.indexOf(item), 1);
                        $scope.data.splice($scope.data.indexOf(item), 1);
                    }, function (error) {

                    })

                //}
            } else {
                $scope.data = $scope.data.filter(
                  function (item) {
                      //if (item.id != $rootScope.rolActual) {
                          if (item.selected) {
                              rolService.del(item.Id).then(function (item) {
                                  $scope.loadData();
                              },
                              function (error) {
                              })
                              return !item.selected;
                          }
                      //}
                  }
                );
                $scope.selectAll = false;
            }
        }
    };

    showForm = function () {
        angular.element('.tooltip').remove();
        formTpl.show();
    };

    hideForm = function () {
        formTpl.hide();
    };

    $scope.$on('$destroy', function () {
        hideForm();
    });

}]);
