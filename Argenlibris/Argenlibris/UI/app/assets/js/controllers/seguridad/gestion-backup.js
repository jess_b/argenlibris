﻿
app.controller('GestionBackupController', ['$scope', '$window', '$aside', 'backupService', '$rootScope', function ($scope, $window, $aside, backupService, $rootScope) {

    $scope.backups = [];

    var loadData = function () {
        backupService.all().then(function (resp) {
            $scope.backups = resp.data;
        })
    }


    $scope.restoreBackup = function (idBackup) {
        backupService.restore(idBackup).then(function (resp) {
            $rootScope.logout();
        });
    }

    $scope.doBackup = function () {
        backupService.one().then(function(resp){
            $scope.backups.push(resp.data);
        });
    }

    loadData();
}]);
