﻿app.controller('GestionIdiomasController', ['$scope', '$window', '$aside', 'idiomaService', '$rootScope', function ($scope, $window, $aside, idiomaService, $rootScope) {



    // settings
    $scope.settings = {
        singular: 'Idioma',
        plural: 'Idiomas',
        cmd: 'Agregar'
    };



    $scope.loadData = function (idioma) {

        var usu = idiomaService.all().then(function (items) {
            $scope.data = items.data;

        });

    }

    $scope.loadData($rootScope.idiomaActual);



    // defining template
    var formTpl = $aside({
        scope: $scope,
        template: '/app/assets/tpl/seguridad/gestion-idiomas-form.html',
        show: false,
        placement: 'left',
        backdrop: false,
        animation: 'am-slide-left'
    });

    // methods
    $scope.checkAll = function () {
        angular.forEach($scope.data, function (item) {
            item.selected = !item.selected;
        });
    };

    $scope.editItem = function (item) {

        if (item) {

            item.editing = true;
            $scope.item = item;
            $scope.settings.cmd = 'Editar';
            showForm();
        }
    };

    $scope.viewItem = function (item) {

            
        if (item) {
            item.editing = false;
            $scope.item = item;
            $scope.settings.cmd = 'Ver';
            showForm();
        }
    };

    

    $scope.idiomaPut = function () {


      
        idiomaService.put($scope.item).then(function (idioma) {

                $rootScope.idiomas.push(idioma.data);
                $rootScope.entitySaved();
                if ($scope.item.Estado == "Nuevo")
                    $scope.data.push(idioma.data);
                $rootScope.$broadcast('idioma:nuevo', $scope.data); 


        }, function (error) {

        }).finally(function () {

            loadData();

        });

            
    };


    $scope.createItem = function () {

        var item = {
            editing: true,
            Estado: "Nuevo",
            Descripcion: ""
        };
        $scope.item = item;
        $scope.settings.cmd = 'Nuevo';
        showForm();
    };



    $scope.saveItem = function () {
        if ($scope.settings.cmd == 'Guardar') {
            $scope.data.push($scope.item);

        }
        hideForm();
    };

    $scope.remove = function (item) {
        if (confirm('¿Está seguro?')) {
            if (item) {

                if (item.Id !== $rootScope.idiomaActual && item.Codigo !== 'es') {

                    $rootScope.idiomas.splice($scope.data.indexOf(item), 1);
                    //$scope.data.splice($scope.data.indexOf(item), 1);
                    
                    idiomaService.del(item.Id).then(function (item) {
                        $scope.loadData();
                        //$rootScope.idiomas.splice($scope.data.indexOf(item), 1);
                        $scope.data.splice($scope.data.indexOf(item), 1);
                    }, function (error) {

                    })

                }
            } else {
                $scope.data = $scope.data.filter(
                  function (item) {
                      if (item.Id !== $rootScope.idiomaActual && item.Codigo !== 'es') {
                          if (item.selected) {
                                  
                              $rootScope.idiomas.splice($scope.data.indexOf(item), 1);
                                      //$scope.data.splice($scope.data.indexOf(item), 1);
            
                              idiomaService.del(item.Id).then(function (item) {
                                  $scope.loadData();

                              },
                              function (error) {
                              })
                              return !item.selected;
                          }
                      }
                  }
                );
                $scope.selectAll = false;
            }
        }
    };

    showForm = function () {
        angular.element('.tooltip').remove();
        formTpl.show();
    };

    hideForm = function () {
        formTpl.hide();
    };

    $scope.$on('$destroy', function () {
        hideForm();
    });

}]);
