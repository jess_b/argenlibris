﻿app.controller('GestionTraduccionesController', ['traductorService', '$scope', '$window', '$aside', 'idiomaService', '$rootScope', function (traductorService,$scope, $window, $aside, idiomaService, $rootScope) {




    $scope.loadData = function (idioma) {
        var usu = traductorService.otro(idioma).then(function (items) {
            $scope.data = items.data;

        });

    }

    $scope.loadData($rootScope.idiomaActual);

    $scope.guardarCambios=function()
    {
        traductorService.put($scope.data).then(function (response) {
        $rootScope.cambiarIdioma($rootScope.idiomaActual);
        });
    }

    

}]);
