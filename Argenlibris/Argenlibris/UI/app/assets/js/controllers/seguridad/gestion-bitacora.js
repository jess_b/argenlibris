﻿app.controller('GestionBitacoraController', ['$scope', '$window', '$aside', 'bitacoraService', 'usuarioService', '$rootScope', function ($scope, $window, $aside, bitacoraService, usuarioService, $rootScope) {




    usuarioService.all().then(function (response) {
        $scope.usuarios = response.data;
    });

    // settings
    $scope.settings = {
        singular: 'Bitacora',
        cmd: 'Agregar'
    };

    var data = [
        { "Save": "Save" },
        { "Create": "Create" },
        { "Delete": "Delete" }
    ];

    $scope.options = data.reduce(function (memo, obj) {
        return angular.extend(memo, obj);
    }, {});

    $scope.estado = function (item) {
        if (item == undefined)
            return null;
        var iconVerified = "<i class='md md-verified-user' data-placement='top' data-title='Usuario valido en el sistema' bs-tooltip></i>";
        var iconNoVerified = "<i class='md md-alarm' data-placement='top' data-title='Usuario inexistente' bs-tooltip></i>";

        if (item.Username != undefined && item.Password != undefined)
            return iconVerified;
        return iconNoVerified;

    }

    $scope.loadData = function (idusuario, fechadesde, fechahasta) {

        var TipoOperacion = "";
        if ($scope.TipoOperacion != undefined) {
            TipoOperacion = $scope.TipoOperacion;
        }

        if (idusuario == undefined) {

            if ($scope.idusuario == undefined) {
                idusuario = "00000000-0000-0000-0000-000000000000";
            } else {
                idusuario = $scope.idusuario;
            }

        } else {
            $scope.idusuario = idusuario;
        }


        if (fechadesde == undefined) {
            
            if ($scope.fechadesde == undefined) {
                fechadesde = "2015-01-01";
            } else {
                fechadesde = $scope.fechadesde;
            }
        }
        if (fechahasta == undefined) {

            if ($scope.fechahasta == undefined) {
                fechahasta = "2099-01-01";
            } else {
                fechahasta = $scope.fechahasta;
            }
        }

        var usu = bitacoraService.all(idusuario, fechadesde, fechahasta, TipoOperacion).then(function (items) {
            $scope.data = items.data;
        });

    }

    $scope.getdatefrom = function (date) {
        $scope.fechadesde = date;
        $scope.loadData();
    }
    $scope.getdateto = function (date) {
        $scope.fechahasta = date;
        $scope.loadData();
    }
    

    $scope.loadData();

    // methods
    $scope.checkAll = function () {
        angular.forEach($scope.data, function (item) {
            item.selected = !item.selected;
        });
    };

    $scope.editItem = function (item) {
        if (item) {
            item.editing = true;
            $scope.item = item;
            $scope.settings.cmd = 'Editar';
            showForm();
        }
    };

    $scope.viewItem = function (item) {
        if (item) {
            item.editing = false;
            $scope.item = item;
            $scope.settings.cmd = 'Ver';
            showForm();
        }
    };




    //$scope.bitacoraPut = function () {

    //    bitacoraService.put($scope.item).then(function (alumno) {
    //        //if (proyecto.data!=undefined && proyecto.data!="")

    //        $rootScope.entitySaved();
    //        if ($scope.item.Estado=="Nuevo")
    //            $scope.data.push(bitacora.data);
    //        $rootScope.$broadcast('bitacora:nuevo', $scope.data);

    //    }, function (error) {

    //    }).finally(function () {

    //        loadData();

    //    });


    //};


    $scope.createItem = function () {
        var item = {
            editing: true,
            Estado: "Nuevo"
        };
        $scope.item = item;
        $scope.settings.cmd = 'Nuevo';
        showForm();
    };



    $scope.saveItem = function () {
        if ($scope.settings.cmd == 'Guardar') {
            $scope.data.push($scope.item);

        }
        hideForm();
    };

    $scope.remove = function (item) {
        if (confirm('¿Está seguro?')) {
            if (item) {
                $scope.data.splice($scope.data.indexOf(item), 1);
            } else {
                $scope.data = $scope.data.filter(
                  function (item) {
                      return !item.selected;
                  }
                );
                $scope.selectAll = false;
            }
        }
    };

}]);
