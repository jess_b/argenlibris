﻿app.controller('GestionUsuController', ['$scope', '$window', '$aside', 'usuarioService', 'rolService', 'usuariorolService', '$rootScope', function ($scope, $window, $aside, usuarioService, rolService,usuariorolService, $rootScope) {
    

    $scope.item = {};
    $scope.misroles = [];
    $scope.roles = [];
    $scope.usuariosrol = [];
    $scope.usuariorol = {};


    // settings
    $scope.settings = {
        singular: 'usuario',
        plural: 'usuarios',
        cmd: 'Agregar'
    };




    $scope.agregarRol = function (item) {

        var agregar = true;

        $scope.misroles.forEach(
            function (p) {
                if (item.Id == p.Id) {
                    agregar = false;
                }
            });

        if (agregar == true) {
            $scope.misroles.push(item);
        }



    }

    $scope.loadData = function () {
        //$scope.misroles = [];
        var usu = usuarioService.all().then(function (items) {
            $scope.data = items.data;

        });

    }

    $scope.loadData();



    // defining template
    var formTpl = $aside({
        scope: $scope,
        template: '/app/assets/tpl/seguridad/gestion-usuarios-form.html',
        show: false,
        placement: 'left',
        backdrop: false,
        animation: 'am-slide-left'
    });


    $scope.editItem = function (item) {
        if (item) {
            item.editing = true;
            $scope.item = item;
            $scope.settings.cmd = 'Editar';
            $scope.misroles = [];
            rolService.miRolGet(item.Id).then(function (response) {
                $scope.misroles = response.data;
            });

            rolService.all().then(function (response) {
                $scope.roles = response.data;
            });


            showForm();
        }
    };

    $scope.viewItem = function (item) {
        if (item) {
            item.editing = false;
            $scope.item = item;
            $scope.settings.cmd = 'Ver';
            $scope.misroles = [];
            rolService.miRolGet(item.Id).then(function (response) {
                $scope.misroles = response.data;
            });

            rolService.all().then(function (response) {
                $scope.roles = response.data;
            });


            showForm();
        }
    };




    $scope.rolPut = function () {

      

        $scope.misroles.forEach(
            function (p) {
                $scope.usuariorol = {};
                //me traigo el usuario
                var usu = usuarioService.one($scope.item.Id).then(function (items) {
                    $scope.usuariorol.Usuario = items.data;
                    $scope.usuariorol.Rol = p;

                    var usu = usuariorolService.put($scope.usuariorol).then(function (items) {
                        $rootScope.entitySaved();
                        $rootScope.$broadcast('rol:nuevo', $scope.data);
                    });

                }, function (error) {

                }).finally(function () {

                    $scope.loadData();

                });

            });

        $scope.loadData();
    };


    $scope.createItem = function () {
        var item = {
            editing: true,
            Estado: "Nuevo",
            Descripcion: ""
        };
        $scope.item = item;
        $scope.settings.cmd = 'Nuevo';
        showForm();
    };



    $scope.saveItem = function () {
        if ($scope.settings.cmd == 'Guardar') {
            $scope.data.push($scope.item);

        }
        hideForm();
    };

    $scope.remove = function (item) {
        if (confirm('¿Está seguro?')) {
            if (item) {
                //if (item.id !== $rootScope.usuarioActual) {

                usuarioService.del(item.Id).then(function (item) {
                    $scope.loadData();
                    $rootScope.usuarios.splice($scope.data.indexOf(item), 1);
                    $scope.data.splice($scope.data.indexOf(item), 1);
                }, function (error) {

                })

                //}
            } else {
                $scope.data = $scope.data.filter(
                  function (item) {
                      //if (item.id != $rootScope.usuarioActual) {
                      if (item.selected) {
                          usuarioService.del(item.Id).then(function (item) {
                              $scope.loadData();
                          },
                          function (error) {
                          })
                          return !item.selected;
                      }
                      //}
                  }
                );
                $scope.selectAll = false;
            }
        }
    };

    showForm = function () {
        angular.element('.tooltip').remove();
        formTpl.show();
    };

    hideForm = function () {
        formTpl.hide();
    };

    $scope.$on('$destroy', function () {
        hideForm();
    });

}]);
