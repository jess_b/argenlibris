app.controller('MainController', ['chatService', 'backendHubProxy', 'idiomaService', 'traductorService', '$scope', '$animate', 'localStorageService', '$alert', '$timeout', '$rootScope', '$aside',
                           function (chatService, backendHubProxy, idiomaService, traductorService, $scope, $animate, localStorageService, $alert, $timeout, $rootScope, $aside) {


                               idiomaService.one().then(function (response) {
                                   $rootScope.idiomas = response.data;
                               });

                               $scope.theme_colors = [
                                 'pink', 'red', 'purple', 'indigo', 'blue',
                                 'light-blue', 'cyan', 'teal', 'green', 'light-green',
                                 'lime', 'yellow', 'amber', 'orange', 'deep-orange'
                               ];

                               // Add todoService to scope
                               //service = new todoService($scope);

                               traductorService.palabras(["Perfil", "Salir", "Tablero", "Seguridad", "Gestion de idiomas", "Gestion de traducciones", // 0 a 5 
                                                          "Gestion de Bitacora", "Mi perfil", "Mantenga su perfil actualizado", "Datos del perfil", "Nombre de usuario", // 6 a 10
                                                          "Apellido", "Nombre", "Email", "Fecha de nacimiento", "Contrasenia nueva", // de 11 a 15
                                                          "Repita la contrasenia", "Guardar", "Resumen", "Logs", "Eventos", // de 16 a 20
                                                          "Mensajes", "Aqui encontrara todos los idiomas del sistema", "Idiomas", "Codigo", "Descripcion", // de 21 a 25
                                                          "Acciones", "Nuevo", "Idioma", "Aqui encontrara todas las traducciones del sistema", "Clave", // de 26 a 30
                                                          "Valor", "Aqui encontrara bitacora del sistema", "Usuario", "Fecha Desde", "Fecha Hasta", // de 31 a 35
                                                          "Fecha", "Mensaje", "Tipo Entidad", "Entidad", "Tipo Mensaje", // de 36 a 40
                                                          "Politicas De Seguridad", "Productos", "Gestion de Editoriales", "Editorial", "Editoriales", // 41 a 45
                                                          "Gestion de Pais", "Pais", "Paises", "Gestion de Autores", "Autor",// 46 a 50
                                                          "Autores", "Gestion de Libros", "Libro", "Libros", "ISBN",// de 51 a 55
                                                          "Formato", "Titulo", "Sinopsis", "Aqui encontrara todas las editoriales del sistema", // 56 a 59
                                                          "Aqui encontrara todos los paises del sistema", "Aqui encontrara todos los autores del sistema", // 60 a 61
                                                          "Aqui encontrara todos los libros del sistema", "Disponibilidad", "Precio", "Mi cuenta", // 62 a 65
                                                          "Mis Pedidos", "Pedidos", "Numero", "Total", "Almacenes ",// 66  a 70
                                                          "Despachado", "Direccion", "Remitos", "Facturas", "Mis Facturas", // 71 a 75
                                                          "Factura", "Precio", "Cantidad", "Gestion de Encuestas", "Encuesta", // 76 a 80
                                                          "Gestion de Preguntas", "Pregunta", "Respuestas", "Agregar", "Gestion de Categorias", // 81 a 85
                                                          "Categoria", "Recibido", "Reclamar", "Reclamo", // 86 a 89
                                                          "Gestion de Reclamos", "Nota de Credito", "Generar Nota De Credito", "Nota de Debito", "Ventas", "Mi Cuenta Corriente", //90 a 95
                                                          "Debe", "Haber", "Observaciones", "Total", "Saldo", // 95 a 100
                                                          "Cuenta Corriente", "Cuentas Corrientes", "Tipo de Documento", "Documento", "Reclamos",// 101 a 105
                                                          "Tarjeta de Credito", "Numero de Tarjeta", "Fecha de Vencimiento", "Tipo de Tarjeta", "Tarjetas", // 106 a 110 
                                                          "Empresa", //111
                                                          "Gestion de Newsletters", "Gestion de Pedidos", "Gestion de Remitos", "Gestion de Fichas de Opinion", "Gestion de Newsletter",  //112 a 115
                                                          "Asunto", "Cuerpo", "Gestion de Representantes", "Representantes", "Representante",// 116 a 120
                                                          "Operario de Ventas", "Reporte de Ventas", "Cliente",  //121 a 123
                                                          "Gestion de Usuarios", "Aqui podra encontrar y gestionar todos los usuarios del sistema", "Activo", "Rol", "Usuarios", "Gestion de Roles",// 125 a 130
                                                          "Gestion de Novedades", "Novedad", "Novedades", "Mostrar",// 131 a 134
                                                          "FIN"]);
                               traductorService.traducir($scope);




                               $rootScope.cambiarIdioma = function (idioma) {
                                   $rootScope.idiomaActual = idioma;
                                   traductorService.traducir($scope);


                               }






                               $scope.$on('evento:nuevo', function (event, count) {

                               });

                               $scope.$on('proyecto:nuevo', function (event, count) {

                                   element = angular.element('.proyectsCount');

                                   $rootScope.$broadcast('evento:nuevo');
                                   if (!element.hasClass('animated')) {
                                       $animate.addClass(element, 'animated bounce', function () {
                                           $animate.removeClass(element, 'animated bounce');
                                       });
                                   }
                               });







                               $rootScope.$broadcast('evento:nuevo'); //carlo los datos del dashboard


                               $scope.$on('todos:count', function (event, count) {


                                   element = angular.element('#todosCount');

                                   if (!element.hasClass('animated')) {
                                       $animate.addClass(element, 'animated bounce', function () {
                                           $animate.removeClass(element, 'animated bounce');
                                       });
                                   }
                               });

                               $scope.fillinContent = function () {
                                   $scope.htmlContent = 'content content';
                               };

                               // theme changing
                               $scope.changeColorTheme = function (cls) {
                                   $rootScope.$broadcast('theme:change', 'Choose template');//@grep dev
                                   $scope.theme.color = cls;
                               };

                               $scope.changeTemplateTheme = function (cls) {
                                   $rootScope.$broadcast('theme:change', 'Choose color');//@grep dev
                                   $scope.theme.template = cls;
                               };

                               if (!localStorageService.get('theme')) {
                                   theme = {
                                       color: 'theme-pink',
                                       template: 'theme-template-dark'
                                   };
                                   localStorageService.set('theme', theme);
                               }
                               localStorageService.bind($scope, 'theme');






                               if (document.referrer === '' || document.referrer.indexOf('themeforest.net') !== 0) {
                                   $timeout(function () {
                                       //  refererNotThemeforest.show();
                                   }, 1750);
                               }

                               $rootScope.chatMensajes = [];
                               //conecta el socket chathub
                               $rootScope.chat = backendHubProxy("ChatHub");
                               $rootScope.chat.on("HayUnNuevoMensaje", function (message, usuario, fecha) {
                                   var msg = {};
                                   msg.Mensaje = message;
                                   msg.NombreUsuario = usuario;
                                   msg.FechaMensaje = fecha;
                                   $rootScope.chatMensajes.push(msg);
                               });

                               // manda mensaje
                               $scope.enviarMensaje = function (mensaje) {


                                   var msg = {};
                                   msg.NombreUsuario = $rootScope.user.NombreUsuario;
                                   msg.Mensaje = mensaje;
                                   msg.IdRepresentante = $rootScope.chatChannel;
                                   chatService.put(msg);
                                   //  $rootScope  .chat.invoke("EnviarMensaje", function cb() { }, mensaje, usuario, $rootScope.chatChannel);
                               }

                               // $rootScope.chat.invoke("UnirseCanalChat", function cb() { }, "chatChannel");
                           }]);
