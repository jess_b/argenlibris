﻿app.controller('userProfileController', ['Cards', '$scope', '$rootScope', 'usuarioService', 'tarjetaService', 'rolService', 'categoriaService','bootstrap3ElementModifier',
    function (Cards, $scope, $rootScope, usuarioService, tarjetaService, rolService, categoriaService,bootstrap3ElementModifier) {


    $scope.categoriacliente = {};

    $scope.handleStripe = function (status, response) {
        if (response.error) {
            // there was an error. Fix it.
        } else {
            // got stripe token, now charge it or smt

        }
    }
    bootstrap3ElementModifier.enableValidationStateIcons(false);

        
    $rootScope.pageTitle = 'Perfil de usuario';
    
    $scope.verTarjeta = false;
   
    $scope.cambiarPassword = function () {

        usuarioService.cambiaPassword($scope.dtopassword).then(function(resp)
        {
            $scope.usuario = data.data;
            alert("Cambio exitoso!"); //cambiar a traductor 

        }, function (error) {
            if (error.status==304)
                alert("Password invalido!");

        })
        
    }
    $scope.loadUser = function () {


        var usu = usuarioService.one($rootScope.userId).then(function (data)
        {
          $scope.usuario = data.data;
         
            
        });

        tarjetaService.all().then(function (data) {
            $scope.tarjetas = data.data;


        });

        rolService.miRol().then(function (data) {
            $scope.roles = data.data;


        });

        categoriaService.all().then(function (data) {

            $scope.categorias = data.data;

            categoriaService.mis($rootScope.userId).then(function (data) {
                $scope.miscategorias = data.data;

                $scope.categorias.forEach(function (c) {

                    $scope.miscategorias.forEach(function (m) {

                        if (c.Id == m.Categoria.Id) 
                            c.Eleccion = true;

                    });

                   });
                });

        });
        
    }

    $scope.loadUser();

    $scope.guardar=function()
    {
        usuarioService.put($scope.usuario).then(function(response){  $rootScope.user = $scope.usuario });
    }

    $scope.agregarTarjeta = function () {
        $scope.verTarjeta = true;
    }
    
    $scope.suscribir = function () {

        var cscs = [];

        $scope.categorias.forEach(function (e) {
            var cc = {};
                cc.idcategoria = e.Id;
                cc.idcliente = $rootScope.userId;   
                cc.Eleccion = e.Eleccion;
                cscs.unshift(cc);
        });

        categoriaService.act(cscs).then(function (data) {

        });




    }

    $scope.aceptar = function () {

        //var tarjeta;
        //tarjeta.NumeroTarjeta = $scope.tarjeta.NumeroTarjeta;
        $scope.tipo = Cards.fromNumber($scope.tarjeta.NumeroTarjeta);
        $scope.tarjeta.TipoDeTarjeta = $scope.tipo.type;
        tarjetaService.put($scope.tarjeta).then(function (data) {

            data.data.NumeroTarjeta = $scope.tarjeta.NumeroTarjeta; // en el UI le mando la tarjeta desencriptada
            $scope.tarjeta = data.data;
            $scope.tarjetas.push(data.data);
            $scope.limpiarTarjeta();

        });
      
    }

    $scope.cancelar = function () {
        $scope.limpiarTarjeta();
    }

    $scope.limpiarTarjeta = function () {
        $scope.tarjeta = "";
        $scope.verTarjeta = false;
    }


    $scope.remove = function (item) {
        if (confirm('¿Está seguro?')) {
            var index = $scope.tarjetas.indexOf(item);
                tarjetaService.del(item.Id).then(function (item) {
                    $scope.tarjetas.splice(index, 1);

                }, function (error) {

                })


        }
    };


}]);