﻿app.controller('GestionRemitosController', ['$scope', '$window', '$aside', 'remitoService', '$rootScope', function ($scope, $window, $aside, remitoService, $rootScope) {



    // settings
    $scope.settings = {
        singular: 'remito',
        plural: 'remitos',
        cmd: 'Agregar'
    };



    $scope.loadData = function (remito) {
        var usu = remitoService.all().then(function (items) {
            $scope.data = items.data;

        });

    }

    $scope.loadData();

    // defining template
    var formTpl = $aside({
        scope: $scope,
        template: '/app/assets/tpl/almacenes/gestion-remitos-form.html',
        show: false,
        placement: 'left',
        backdrop: false,
        animation: 'am-slide-left'
    });

    // methods
    $scope.checkAll = function () {
        angular.forEach($scope.data, function (item) {
            item.selected = !item.selected;
        });
    };

    $scope.editItem = function (item) {
        if (item) {
            item.editing = true;
            $scope.item = item;
            $scope.settings.cmd = 'Editar';
            showForm();
        }
    };

    $scope.viewItem = function (item) {
        if (item) {
            item.editing = false;
            $scope.item = item;
            $scope.settings.cmd = 'Ver';
            showForm();
        }
    };




    $scope.remitoPut = function () {

        remitoService.put($scope.item).then(function (remito) {

            $rootScope.entitySaved();
            if ($scope.item.Estado == "Nuevo")
                $scope.data.push(remito.data);
            $rootScope.$broadcast('remito:nuevo', $scope.data);

        }, function (error) {

        }).finally(function () {

            loadData();

        });


    };


    $scope.createItem = function () {
        var item = {
            editing: true,
            Estado: "Nuevo",
            Descripcion: ""
        };
        $scope.item = item;
        $scope.settings.cmd = 'Nuevo';
        showForm();
    };



    $scope.saveItem = function () {
        if ($scope.settings.cmd == 'Guardar') {
            $scope.data.push($scope.item);

        }
        hideForm();
    };

    $scope.guardarCambios = function () {
        $scope.data = $scope.data.forEach(
                  function (item) {
                      remitoService.put(item).then(function (response) {

                      }
                  );

                  });
    }


    $scope.remove = function (item) {
        if (confirm('¿Está seguro?')) {
            if (item) {
                remitoService.del(item.Id).then(function (item) {
                    $scope.loadData();
                    $rootScope.remitos.splice($scope.data.indexOf(item), 1);
                    $scope.data.splice($scope.data.indexOf(item), 1);
                }, function (error) {

                })

            } else {
                $scope.data = $scope.data.forEach(
                  function (item) {
                      if (item.selected) {
                          remitoService.del(item.Id).then(function (item) {
                              $scope.loadData();
                          }, function (error) { })
                          return !item.selected;
                      }
                  }
                );
                $scope.selectAll = false;
            }
        }
    };

    showForm = function () {
        angular.element('.tooltip').remove();
        formTpl.show();
    };

    hideForm = function () {
        formTpl.hide();
    };

    $scope.$on('$destroy', function () {
        hideForm();
    });

}]);
