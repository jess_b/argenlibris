﻿app.controller('GestionPaisesController', ['$scope', '$window', '$aside', 'paisService', '$rootScope', function ($scope, $window, $aside, paisService, $rootScope) {



    // settings
    $scope.settings = {
        singular: 'Pais',
        plural: 'Paises',
        cmd: 'Agregar'
    };



    $scope.loadData = function (pais) {
        var usu = paisService.all().then(function (items) {
            $scope.data = items.data;

        });

    }

    $scope.loadData();

    // defining template
    var formTpl = $aside({
        scope: $scope,
        template: '/app/assets/tpl/productos/gestion-paises-form.html',
        show: false,
        placement: 'left',
        backdrop: false,
        animation: 'am-slide-left'
    });

    // methods
    $scope.checkAll = function () {
        angular.forEach($scope.data, function (item) {
            item.selected = !item.selected;
        });
    };

    $scope.editItem = function (item) {
        if (item) {
            item.editing = true;
            $scope.item = angular.copy(item);


            $scope.settings.cmd = 'Editar';
            showForm();
        }
    };

    $scope.viewItem = function (item) {
        if (item) {
            item.editing = false;
            $scope.item = item;
            $scope.settings.cmd = 'Ver';
            showForm();
        }
    };




    $scope.paisPut = function () {

        paisService.put($scope.item).then(function (pais) {


            

            $rootScope.entitySaved();
            if ($scope.item.Estado == "Nuevo")
                $scope.data.push(pais.data);

            if ($scope.item.editing)
            {

            }
            $rootScope.$broadcast('pais:nuevo', $scope.data);

          //  loadData();
        }, function (error) {
            var e = error.status;
        }).finally(function () {

            $scope.loadData();

        });


    };


    $scope.createItem = function () {
        var item = {
            editing: true,
            Estado: "Nuevo",
            Descripcion: ""
        };
        $scope.item = item;
        $scope.settings.cmd = 'Nuevo';
        showForm();
    };



    $scope.saveItem = function () {
        if ($scope.settings.cmd == 'Guardar') {
            $scope.data.push($scope.item);

        }
        hideForm();
    };

    $scope.remove = function (item) {
        if (confirm('¿Está seguro?')) {
            if (item) {
                paisService.del(item.Id).then(function (item) {
                    $scope.loadData();
                    $rootScope.paises.splice($scope.data.indexOf(item), 1);
                    $scope.data.splice($scope.data.indexOf(item), 1);
                }, function (error) {
                    //error.statusText = 'Error al borrar entidad';
                    $rootScope.$broadcast('pais:nuevo', $scope.data);
                })

            } else {
                $scope.data = $scope.data.forEach(
                  function (item) {
                      if (item.selected) {
                          paisService.del(item.Id).then(function (item) {
                              $scope.loadData();
                          }, function (error) { })
                          return !item.selected;
                      }
                  }
                );
                $scope.selectAll = false;
            }
        }
    };

    showForm = function () {
        angular.element('.tooltip').remove();
        formTpl.show();
    };

    hideForm = function () {
        formTpl.hide();
    };

    $scope.$on('$destroy', function () {
        hideForm();
    });

}]);
