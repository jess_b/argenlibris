﻿app.controller('GestionEditorialesController', ['$scope', '$window', '$aside', 'editorialService', '$rootScope', function ($scope, $window, $aside, editorialService, $rootScope) {



    // settings
    $scope.settings = {
        singular: 'Editorial',
        plural: 'Editoriales',
        cmd: 'Agregar'
    };



    $scope.loadData = function (editorial) {
        var usu = editorialService.all().then(function (items) {
            $scope.data = items.data;

        });

    }

    $scope.loadData();

    // defining template
    var formTpl = $aside({
        scope: $scope,
        template: '/app/assets/tpl/productos/gestion-editoriales-form.html',
        show: false,
        placement: 'left',
        backdrop: false,
        animation: 'am-slide-left'
    });

    // methods
    $scope.checkAll = function () {
        angular.forEach($scope.data, function (item) {
            item.selected = !item.selected;
        });
    };

    $scope.editItem = function (item) {
        if (item) {
            item.editing = true;
            $scope.item = item;
            $scope.settings.cmd = 'Editar';
            showForm();
        }
    };

    $scope.viewItem = function (item) {
        if (item) {
            item.editing = false;
            $scope.item = item;
            $scope.settings.cmd = 'Ver';
            showForm();
        }
    };




    $scope.editorialPut = function () {

        editorialService.put($scope.item).then(function (editorial) {
          
            //$rootScope.editoriales.push(editorial.data);
            $rootScope.entitySaved();
            if ($scope.item.Estado == "Nuevo")
                $scope.data.push(editorial.data);
            $rootScope.$broadcast('editorial:nuevo', $scope.data);

        }, function (error) {

        }).finally(function () {

            //loadData();

        });


    };


    $scope.createItem = function () {
        var item = {
            editing: true,
            Estado: "Nuevo",
            Descripcion: ""
        };
        $scope.item = item;
        $scope.settings.cmd = 'Nuevo';
        showForm();
    };



    $scope.saveItem = function () {
        if ($scope.settings.cmd == 'Guardar') {
            $scope.data.push($scope.item);

        }
        hideForm();
    };

    $scope.remove = function (item) {
        if (confirm('¿Está seguro?')) {
            if (item) {
                editorialService.del(item.Id).then(function (item) {
                    $scope.loadData();
                    //$rootScope.editoriales.splice($scope.data.indexOf(item), 1);
                    $scope.data.splice($scope.data.indexOf(item), 1);
                }, function (error) {
                    
                })

            } else {
              
                $scope.data.forEach(
                   function (item) {
                       if (item.selected) {
                           //$scope.data.splice($scope.data.indexOf(item.Id), 1);             
                           editorialService.del(item.Id).then(function (item) {
                               //$scope.data.splice(count, 1);
                               $scope.loadData();
                               //$scope.selectAll = false;

                           },
                           function (error) { })
                           
                           //return !item.selected;
                       }
                      
                   }
                  
                );

                //$scope.loadData();
                
                //$scope.selectAll = false;
                //$scope.loadData();
            }
        }
    };

    showForm = function () {
        angular.element('.tooltip').remove();
        formTpl.show();
    };

    hideForm = function () {
        formTpl.hide();
    };

    $scope.$on('$destroy', function () {
        hideForm();
    });

}]);
