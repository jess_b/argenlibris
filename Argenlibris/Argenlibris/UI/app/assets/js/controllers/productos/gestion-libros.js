﻿app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);
app.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl, id){
        var fd = new FormData();
        fd.append('file', file);
      
        
        $http.put(uploadUrl+"/"+id, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(){
        })
        .error(function(){
        });
    }
}]);
app.controller('GestionLibrosController', ['fileUpload','$scope', '$window', '$aside', 'libroService', 'autorService', 'editorialService', 'categoriaService', '$rootScope',
    function (fileUpload,$scope, $window, $aside, libroService, autorService, editorialService, categoriaService, $rootScope) {



    // settings
    $scope.settings = {
        singular: 'libro',
        plural: 'libros',
        cmd: 'Agregar'
    };

    autorService.all().then(function (response) {
        $scope.autores = response.data;
    });

    editorialService.all().then(function (response) {
        $scope.editoriales = response.data;
    });

    categoriaService.all().then(function (response) {
        $scope.categorias = response.data;
    });

    $scope.loadData = function (idAutor) {
        var usu = libroService.dos().then(function (items) {
            //$scope.data = items.data;

            if ($scope.autor == undefined) {
                //traigo todos los libros
                $scope.data = items.data;

            } else {
                $scope.data = [];
                items.data.forEach(function (e) {
                    if (e.Autor.Id == $scope.autor.Id) {
                        //guardo solo los libros por autor
                        $scope.data.push(e);
                    }

                });
               
            }

        });

    }

    $scope.guardarCambios = function (item) {
        if (item) {

            libroService.put(item).then(function (response) {


            }, function (error) {

            }).finally(function () {

                $scope.loadData();

            });

        }

    }


    //$scope.cambiarAutor = function (id) {
    //    $scope.item.Autor = id;
    //}

    //$scope.cambiarEditorial = function (id) {
    //    $scope.item.Editorial = id;
    //}

    $scope.loadData();

    var data = [
        { "eBook": "eBook" },
        { "TapaDura": "TapaDura" },
        { "TapaBlanda": "TapaBlanda" },
        { "Coleccion": "Coleccion" }
        ];

    $scope.options = data.reduce(function (memo, obj) {
        return angular.extend(memo, obj);
    }, {});

    // defining template
    var formTpl = $aside({
        scope: $scope,
        template: '/app/assets/tpl/productos/gestion-libros-form.html',
        show: false,
        placement: 'left',
        backdrop: false,
        animation: 'am-slide-left'
    });

    // methods
    $scope.checkAll = function () {
        angular.forEach($scope.data, function (item) {
            item.selected = !item.selected;
        });
    };

    $scope.editItem = function (item) {
        if (item) {
            item.editing = true;
            $scope.item = angular.copy(item);

            $scope.settings.cmd = 'Editar';
            showForm();
        }
    };

    $scope.viewItem = function (item) {
        if (item) {
            item.editing = false;
            $scope.item = item;
            $scope.settings.cmd = 'Ver';
            showForm();
        }
    };

    $scope.sendItem = function (item) {

        libroService.send(item.Id).then(function (response) {

        });

    }


    $scope.libroPut = function () {


        var file = $scope.item.image;
        var uploadUrl = '/api/image';
        if (file != undefined) {
            var extn = $scope.item.Id + "." + file.name.split(".").pop()
            $scope.item.FileName = extn;
        }
    

       
        libroService.put($scope.item).then(function (libro) {

            $rootScope.entitySaved();
            if ($scope.item.Estado == "Nuevo")
                $scope.data.push(libro.data);
            $rootScope.$broadcast('libro:nuevo', $scope.data);
            fileUpload.uploadFileToUrl(file, uploadUrl, libro.data.Id);
        }, function (error) {

        }).finally(function () {

            $scope.loadData();

        });


    };


    $scope.createItem = function () {
        var item = {
            editing: true,
            Estado: "Nuevo",
            Descripcion: ""
        };
        $scope.item = item;
        $scope.settings.cmd = 'Nuevo';
        showForm();
    };



    $scope.saveItem = function () {
        if ($scope.settings.cmd == 'Guardar') {
            $scope.data.push($scope.item);

        }
        hideForm();
    };

    $scope.remove = function (item) {
        if (confirm('¿Está seguro?')) {
            if (item) {
                libroService.del(item.Id).then(function (item) {
                    $scope.loadData();
                    $rootScope.libros.splice($scope.data.indexOf(item), 1);
                    $scope.data.splice($scope.data.indexOf(item), 1);
                }, function (error) {

                })

            } else {
                $scope.data = $scope.data.forEach(
                  function (item) {
                      if (item.selected) {
                          libroService.del(item.Id).then(function (item) {
                              $scope.loadData();
                          }, function (error) { })
                          return !item.selected;
                      }
                  }
                );
                $scope.selectAll = false;
            }
        }
    };

    showForm = function () {
        angular.element('.tooltip').remove();
        formTpl.show();
    };

    hideForm = function () {
        formTpl.hide();
    };

    $scope.$on('$destroy', function () {
        hideForm();
    });

}]);
