﻿app.controller('GestionAutoresController', ['$scope', '$window', '$aside', 'autorService', 'paisService', '$rootScope', function ($scope, $window, $aside, autorService, paisService, $rootScope) {



    // settings
    $scope.settings = {
        singular: 'autor',
        plural: 'autores',
        cmd: 'Agregar'
    };


    paisService.all().then(function (response) {
        $scope.paises = response.data;
    });

    $scope.loadData = function (autor) {
        var usu = autorService.all().then(function (items) {
            $scope.data = items.data;

        });

    }

    $scope.loadData();

    // defining template
    var formTpl = $aside({
        scope: $scope,
        template: '/app/assets/tpl/productos/gestion-autores-form.html',
        show: false,
        placement: 'left',
        backdrop: false,
        animation: 'am-slide-left'
    });

    // methods
    $scope.checkAll = function () {
        angular.forEach($scope.data, function (item) {
            item.selected = !item.selected;
        });
    };

    $scope.editItem = function (item) {
        if (item) {
            item.editing = true;
            $scope.item = item;
            //$scope.item.pais = item.PaisDeOrigen;
            $scope.settings.cmd = 'Editar';
            showForm();
        }
    };

    $scope.viewItem = function (item) {
        if (item) {
            item.editing = false;
            $scope.item = item;
            //$scope.item.pais = $scope.paises;
            $scope.settings.cmd = 'Ver';
            showForm();
        }
    };


    $scope.cambiarPais = function (id) {
        //$scope.item.PaisDeOrigen = id;
    }

    $scope.autorPut = function () {
        
        autorService.put($scope.item).then(function (autor) {

            $rootScope.entitySaved();
            if ($scope.item.Estado == "Nuevo")
                $scope.data.push(autor.data);
            $rootScope.$broadcast('autor:nuevo', $scope.data);

        }, function (error) {

        }).finally(function () {

            loadData();

        });


    };


    $scope.createItem = function () {
        var item = {
            editing: true,
            Estado: "Nuevo",
            Descripcion: "",
            Pais: ""
        };
        $scope.item = item;
        $scope.settings.cmd = 'Nuevo';
        showForm();
    };



    $scope.saveItem = function () {
        if ($scope.settings.cmd == 'Guardar') {
            $scope.data.push($scope.item);

        }
        hideForm();
    };

    $scope.remove = function (item) {
        if (confirm('¿Está seguro?')) {
            if (item) {
                autorService.del(item.Id).then(function (item) {
                    $scope.loadData();
                    $rootScope.autores.splice($scope.data.indexOf(item), 1);
                    $scope.data.splice($scope.data.indexOf(item), 1);
                }, function (error) {

                })

            } else {
                $scope.data = $scope.data.forEach(
                  function (item) {
                      if (item.selected) {
                          autorService.del(item.Id).then(function (item) {
                              $scope.loadData();
                          }, function (error) { })
                          return !item.selected;
                      }
                  }
                );
                $scope.selectAll = false;
            }
        }
    };

    showForm = function () {
        angular.element('.tooltip').remove();
        formTpl.show();
    };

    hideForm = function () {
        formTpl.hide();
    };

    $scope.$on('$destroy', function () {
        hideForm();
    });

}]);
