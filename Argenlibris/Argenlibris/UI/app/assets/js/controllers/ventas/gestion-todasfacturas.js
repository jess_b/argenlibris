﻿app.controller('GestionTodasFacturasController', ['$scope', '$window', '$aside', 'facturasService', 'NDService' ,'$rootScope', function ($scope, $window, $aside, facturasService, NDService,  $rootScope) {



    // settings
    $scope.settings = {
        singular: 'factura',
        plural: 'facturas',
        cmd: 'Agregar'
    };



    $scope.loadData = function (factura) {
        var facturas = [];

        var usu = facturasService.all().then(function (items) {
            facturas = items.data;

            var usu = NDService.one(facturas).then(function (items) {
                $scope.data = items.data;


            });
        });



    }

    $scope.loadData();

    // defining template
    var formTpl = $aside({
        scope: $scope,
        template: '/app/assets/tpl/micuenta/gestion-facturas-form.html',
        show: false,
        placement: 'left',
        backdrop: false,
        animation: 'am-slide-left'
    });

    // methods
    $scope.checkAll = function () {
        angular.forEach($scope.data, function (item) {
            item.selected = !item.selected;
        });
    };

    //descargar a pdf
    $scope.editItem = function (item) {
        if (item) {
            item.editing = true;
            $scope.item = item;
            //$scope.settings.cmd = 'Editar';
            //showForm();
        }
    };

    $scope.viewItem = function (item) {
        if (item) {
            item.editing = false;
            $scope.item = item;
            $scope.settings.cmd = 'Ver';
            showForm();
        }
    };

    $scope.acreditar = function (item) {
        var ant;
        item.Acreditado = true;

        if (item) {
            ant = item.Total;
            $scope.guardarCambios = true;
            item.Total = item.Credito;
            NDService.put(item).then(function (response) {

         

            }, function (error) {
                item.Total = ant;
            }).finally(function () {
                item.Total = ant;
            

            });
        }
    };



    $scope.facturaPut = function () {

        facturasService.put($scope.item).then(function (factura) {

            $rootScope.entitySaved();
            if ($scope.item.Estado == "Nuevo")
                $scope.data.push(factura.data);
            $rootScope.$broadcast('factura:nuevo', $scope.data);

        }, function (error) {

        }).finally(function () {

            loadData();

        });


    };


    $scope.createItem = function () {
        var item = {
            editing: true,
            Estado: "Nuevo",
            Descripcion: ""
        };
        $scope.item = item;
        $scope.settings.cmd = 'Nuevo';
        showForm();
    };



    $scope.saveItem = function () {
        if ($scope.settings.cmd == 'Guardar') {
            $scope.data.push($scope.item);

        }
        hideForm();
    };



    showForm = function () {
        angular.element('.tooltip').remove();
        formTpl.show();
    };

    hideForm = function () {
        formTpl.hide();
    };

    $scope.$on('$destroy', function () {
        hideForm();
    });

}]);
