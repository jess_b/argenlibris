﻿app.controller('GestionInformeVentasController', ['$scope', '$window', '$aside', 'informeService', '$rootScope', 'NCService', function ($scope, $window, $aside, informeService, $rootScope, NCService) {


    $scope.showGraph = function () {

        var datos = [];
        var o = 0;
        $scope.libros.forEach(function (i) {

            var p = [i.Titulo, i.Total];
            o = o + 1;
            datos.push(p);

        });

        $scope.chart = c3.generate({
            bindto: '#chart',
            
            data: {
                type: 'pie',
                columns: datos
            }
        });
    }


    //// settings
    //$scope.settings = {
    //    singular: 'pedido',
    //    plural: 'pedidos',
    //    cmd: 'Agregar'
    //};


    $scope.posiciones = [];
    $scope.libros = [];

    $scope.loadData = function (fechadesde, fechahasta) {

        var dto = {};
        var libroTotal = 0;
        $scope.total = 0;
        $scope.posiciones = [];
        $scope.libros = [];

        if (fechadesde == undefined) {

            if ($scope.fechadesde == undefined) {
                fechadesde = "2015-01-01";
            } else {
                fechadesde = $scope.fechadesde;
            }
        }
        if (fechahasta == undefined) {

            if ($scope.fechahasta == undefined) {
                fechahasta = "2099-01-01";
            } else {
                fechahasta = $scope.fechahasta;
            }
        }

        if (fechadesde > fechahasta) {
            $scope.mensajeError = "La fecha desde debe ser menor a la fecha hasta";
        } else {
            $scope.mensajeError = "";
        }


        var usu = informeService.all(fechadesde, fechahasta).then(function (items) {
            $scope.total = items.data;

        });

        var usu = informeService.put(fechadesde, fechahasta).then(function (items) {
            var libro = {};
            var libroant = {};
            var a = 0;
            var subtot = 0;
            $scope.posiciones = items.data;

            $scope.posiciones.forEach(function (e) {
                libro = {};
                libroant = {};
                if ($scope.libros.length < 1) {

                    libro.Total = e.Cantidad * e.Libro.Precio;
                    libro.Titulo = e.Libro.Titulo;
                    libro.Id = e.Libro.Id;
                    
                    $scope.libros.push(libro);

                } else {

                    var found = false;

                    $scope.libros.forEach(function (l) {
                        if (l.Id == e.Libro.Id) {
                            a = $scope.libros.indexOf(l);
                            libroant = $scope.libros.splice(a,1);
                            found = true;
                        } 
                    });

                    if (found == false) {
                        libro.Total = e.Cantidad * e.Libro.Precio;
                        libro.Titulo = e.Libro.Titulo;
                        libro.Id = e.Libro.Id;
                        $scope.libros.push(libro);
                    } else {
                        subtot = e.Cantidad * e.Libro.Precio;
                        libroant[0].Total = libroant[0].Total + subtot;
                        $scope.libros.push(libroant[0]);
                    }
                  

                }
             
            
            });
               $scope.showGraph();
        });

    }

    $scope.loadData();

    $scope.getdatefrom = function (date) {
        $scope.fechadesde = date;
        $scope.loadData();
    }
    $scope.getdateto = function (date) {
        $scope.fechahasta = date;
        $scope.loadData();
    }

    //// defining template
    //var formTpl = $aside({
    //    scope: $scope,
    //    //template: '/app/assets/tpl/micuenta/gestion-pedidos-form.html',
    //    show: false,
    //    placement: 'left',
    //    backdrop: false,
    //    animation: 'am-slide-left'
    //});

    //// methods
    //$scope.checkAll = function () {
    //    angular.forEach($scope.data, function (item) {
    //        item.selected = !item.selected;
    //    });
    //};

    //$scope.Reclamar = function (item) {

    //    if (item) {
    //        item.Reclamado = true;
    //        informeService.put(item).then(function (response) {



    //        }, function (error) {

    //        }).finally(function () {

    //            $scope.loadData();

    //        });

    //    }
    //};


    //$scope.generarNC = function (item) {
    //    if (item) {

    //        item.Resuelto = true;

    //        informeService.put(item).then(function (response) {

    //            NCService.put(item).then(function (response) { });

    //        }, function (error) {

    //        }).finally(function () {

    //            $scope.loadData();

    //        });





    //    }
    //};

    //$scope.viewItem = function (item) {
    //    if (item) {
    //        item.editing = false;
    //        $scope.item = item;
    //        $scope.settings.cmd = 'Ver';
    //        showForm();
    //    }
    //};




    //$scope.pedidoPut = function () {

    //    informeService.put($scope.item).then(function (pedido) {

    //        $rootScope.entitySaved();
    //        if ($scope.item.Estado == "Nuevo")
    //            $scope.data.push(pedido.data);
    //        $rootScope.$broadcast('pedido:nuevo', $scope.data);

    //    }, function (error) {

    //    }).finally(function () {

    //        loadData();

    //    });


    //};


    //$scope.createItem = function () {
    //    var item = {
    //        editing: true,
    //        Estado: "Nuevo",
    //        Descripcion: ""
    //    };
    //    $scope.item = item;
    //    $scope.settings.cmd = 'Nuevo';
    //    showForm();
    //};



    //$scope.saveItem = function () {
    //    if ($scope.settings.cmd == 'Guardar') {
    //        $scope.data.push($scope.item);

    //    }
    //    hideForm();
    //};

    //$scope.remove = function (item) {
    //    if (confirm('¿Está seguro?')) {
    //        if (item) {
    //            informeService.del(item.Id).then(function (item) {
    //                $scope.loadData();
    //                $rootScope.pedidos.splice($scope.data.indexOf(item), 1);
    //                $scope.data.splice($scope.data.indexOf(item), 1);
    //            }, function (error) {

    //            })

    //        } else {
    //            $scope.data = $scope.data.forEach(
    //              function (item) {
    //                  if (item.selected) {
    //                      informeService.del(item.Id).then(function (item) {
    //                          $scope.loadData();
    //                      }, function (error) { })
    //                      return !item.selected;
    //                  }
    //              }
    //            );
    //            $scope.selectAll = false;
    //        }
    //    }
    //};

    //showForm = function () {
    //    angular.element('.tooltip').remove();
    //    formTpl.show();
    //};

    //hideForm = function () {
    //    formTpl.hide();
    //};

    //$scope.$on('$destroy', function () {
    //    hideForm();
    //});

}]);
