﻿app.controller('GestionRepresentantesController', ['chatService', '$scope', '$window', '$aside', 'representantesService', '$rootScope', 'NCService', function (chatService,$scope, $window, $aside, representantesService, $rootScope, NCService) {



    // settings
    $scope.settings = {
        singular: 'pedido',
        plural: 'pedidos',
        cmd: 'Agregar'
    };



    $scope.loadData = function (pedido) {
        var usu = representantesService.all().then(function (items) {
            $scope.data = items.data;

        });

        representantesService.allClientesVendedores().then(function (res) {
            $scope.clientes = res.data.Clientes;
            $scope.vendedores = res.data.Vendedores;

        });

    }

    $scope.loadData();

    // defining template
    var formTpl = $aside({
        scope: $scope,
        template: '/app/assets/tpl/ventas/gestion-representantes-form.html',
        show: false,
        placement: 'left',
        backdrop: false,
        animation: 'am-slide-left'
    });

    // defining template
    var chatTpl = $aside({
        scope: $scope,
        template: '/app/assets/tpl/partials/chat-modal.html',
        show: false,
        placement: 'left',
        backdrop: false,
        animation: 'am-slide-left'
    });

    // methods
    $scope.checkAll = function () {
        angular.forEach($scope.data, function (item) {
            item.selected = !item.selected;
        });
    };

    $scope.Reclamar = function (item) {

        if (item) {
            item.Reclamado = true;
            representantesService.put(item).then(function (response) {



            }, function (error) {

            }).finally(function () {

                $scope.loadData();

            });

        }
    };


  

    $scope.viewItem = function (item) {
        if (item) {
            item.editing = false;
            $scope.item = item;
            $scope.settings.cmd = 'Ver';
            showForm();
        }
    };




    $scope.respresentacionPut = function () {

        representantesService.put($scope.item).then(function (response) {

            $rootScope.entitySaved();
            if ($scope.item.Estado == "Nuevo")
                $scope.data.push(response.data);
           // $rootScope.$broadcast('pedido:nuevo', $scope.data);
            
        }, function (error) {

        }).finally(function () {

            loadData();

        });


    };

    $scope.verChat = false;
    $scope.toggleChat = function (item) {
        $scope.verChat = true;
        $rootScope.chat.invoke("UnirseCanalChat", function cb() {
            $rootScope.chatMensajes = [];
            chatService.all(item).then(function (resp) {
                $rootScope.chatMensajes = resp.data;
            });
        }, item);
        $rootScope.chatChannel = item;
        chatTpl.show();
    }
    $scope.createItem = function () {
        var item = {
            editing: true,
            Estado: "Nuevo",
            Descripcion: ""
        };
        $scope.item = item;
        $scope.settings.cmd = 'Nuevo';
        showForm();
    };



    $scope.saveItem = function () {
        if ($scope.settings.cmd == 'Guardar') {
            $scope.data.push($scope.item);

        }
        hideForm();
    };

    $scope.remove = function (item) {
        if (confirm('¿Está seguro?')) {
            if (item) {
                representantesService.del(item.Id).then(function (item) {
                    $scope.loadData();
                    $rootScope.pedidos.splice($scope.data.indexOf(item), 1);
                    $scope.data.splice($scope.data.indexOf(item), 1);
                }, function (error) {

                })

            } else {
                $scope.data = $scope.data.forEach(
                  function (item) {
                      if (item.selected) {
                          representantesService.del(item.Id).then(function (item) {
                              $scope.loadData();
                          }, function (error) { })
                          return !item.selected;
                      }
                  }
                );
                $scope.selectAll = false;
            }
        }
    };
  
    showForm = function () {
        angular.element('.tooltip').remove();
        formTpl.show();
    };

    hideForm = function () {
        formTpl.hide();
    };

    $scope.$on('$destroy', function () {
        hideForm();
    });

}]);
