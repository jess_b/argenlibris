﻿app.controller('GestionMisFacturasController', ['$scope', '$window', '$aside', 'misfacturasService', '$rootScope', function ($scope, $window, $aside, misfacturasService, $rootScope) {



    // settings
    $scope.settings = {
        singular: 'factura',
        plural: 'facturas',
        cmd: 'Agregar'
    };



    $scope.loadData = function (factura) {
        var usu = misfacturasService.all().then(function (items) {
            $scope.data = items.data;

        });

    }

    $scope.loadData();

    // defining template
    var formTpl = $aside({
        scope: $scope,
        template: '/app/assets/tpl/micuenta/gestion-facturas-form.html',
        show: false,
        placement: 'left',
        backdrop: false,
        animation: 'am-slide-left'
    });

    // methods
    $scope.checkAll = function () {
        angular.forEach($scope.data, function (item) {
            item.selected = !item.selected;
        });
    };

    //descargar a pdf
    $scope.editItem = function (item) {
        if (item) {
            item.editing = true;
            $scope.item = item;
            //$scope.settings.cmd = 'Editar';
            //showForm();
        }
    };

    $scope.viewItem = function (item) {
        if (item) {
            item.editing = false;
            $scope.item = item;
            $scope.settings.cmd = 'Ver';
            showForm();
        }
    };

    $scope.sendItem = function (item) {
        if (item) {

            facturaService.put(item).then(function (response) {

         

            }, function (error) {

            }).finally(function () {

            

            });
        }
    };



    $scope.facturaPut = function () {

        misfacturasService.put($scope.item).then(function (factura) {

            $rootScope.entitySaved();
            if ($scope.item.Estado == "Nuevo")
                $scope.data.push(factura.data);
            $rootScope.$broadcast('factura:nuevo', $scope.data);

        }, function (error) {

        }).finally(function () {

            loadData();

        });


    };


    $scope.createItem = function () {
        var item = {
            editing: true,
            Estado: "Nuevo",
            Descripcion: ""
        };
        $scope.item = item;
        $scope.settings.cmd = 'Nuevo';
        showForm();
    };



    $scope.saveItem = function () {
        if ($scope.settings.cmd == 'Guardar') {
            $scope.data.push($scope.item);

        }
        hideForm();
    };

    $scope.remove = function (item) {
        if (confirm('¿Está seguro?')) {
            if (item) {
                misfacturasService.del(item.Id).then(function (item) {
                    $scope.loadData();
                    $rootScope.facturas.splice($scope.data.indexOf(item), 1);
                    $scope.data.splice($scope.data.indexOf(item), 1);
                }, function (error) {

                })

            } else {
                $scope.data = $scope.data.forEach(
                  function (item) {
                      if (item.selected) {
                          misfacturasService.del(item.Id).then(function (item) {
                              $scope.loadData();
                          }, function (error) { })
                          return !item.selected;
                      }
                  }
                );
                $scope.selectAll = false;
            }
        }
    };

    showForm = function () {
        angular.element('.tooltip').remove();
        formTpl.show();
    };

    hideForm = function () {
        formTpl.hide();
    };

    $scope.$on('$destroy', function () {
        hideForm();
    });

}]);
