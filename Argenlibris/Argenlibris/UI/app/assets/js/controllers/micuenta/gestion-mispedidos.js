﻿app.controller('GestionMisPedidosController', ['$scope', '$window', '$aside', 'mispedidosService', '$rootScope', function ($scope, $window, $aside, mispedidosService, $rootScope) {



    // settings
    $scope.settings = {
        singular: 'pedido',
        plural: 'pedidos',
        cmd: 'Agregar'
    };



    $scope.loadData = function (pedido) {
        var usu = mispedidosService.all().then(function (items) {
            $scope.data = items.data;

        });

    }

    $scope.loadData();

    // defining template
    var formTpl = $aside({
        scope: $scope,
        template: '/app/assets/tpl/micuenta/gestion-pedidos-form.html',
        show: false,
        placement: 'left',
        backdrop: false,
        animation: 'am-slide-left'
    });

    // methods
    $scope.checkAll = function () {
        angular.forEach($scope.data, function (item) {
            item.selected = !item.selected;
        });
    };

    $scope.Reclamar = function (item) {

        if (item) {
            item.Reclamado = true;
            mispedidosService.put(item).then(function (response) {



            }, function (error) {

            }).finally(function () {

                $scope.loadData();

            });

        }
    };

    $scope.Recibir = function (item) {

        if (item) {
            item.Recibido = true;
            mispedidosService.put(item).then(function (response) {



            }, function (error) {

            }).finally(function () {

                $scope.loadData();

            });

        }
    };

    $scope.editItem = function (item) {
        if (item) {

            mispedidosService.put(item).then(function (response) {



            }, function (error) {

            }).finally(function () {

                $scope.loadData()   ;

            });

        }
    };

    $scope.viewItem = function (item) {
        if (item) {
            item.editing = false;
            $scope.item = item;
            $scope.settings.cmd = 'Ver';
            showForm();
        }
    };




    $scope.pedidoPut = function () {

        mispedidosService.put($scope.item).then(function (pedido) {

            $rootScope.entitySaved();
            if ($scope.item.Estado == "Nuevo")
                $scope.data.push(pedido.data);
            $rootScope.$broadcast('pedido:nuevo', $scope.data);

        }, function (error) {

        }).finally(function () {

            loadData();

        });


    };


    $scope.createItem = function () {
        var item = {
            editing: true,
            Estado: "Nuevo",
            Descripcion: ""
        };
        $scope.item = item;
        $scope.settings.cmd = 'Nuevo';
        showForm();
    };



    $scope.saveItem = function () {
        if ($scope.settings.cmd == 'Guardar') {
            $scope.data.push($scope.item);

        }
        hideForm();
    };

    $scope.remove = function (item) {
        if (confirm('¿Está seguro?')) {
            if (item) {
                mispedidosService.del(item.Id).then(function (item) {
                    $scope.loadData();
                    $rootScope.pedidos.splice($scope.data.indexOf(item), 1);
                    $scope.data.splice($scope.data.indexOf(item), 1);
                }, function (error) {

                })

            } else {
                $scope.data = $scope.data.forEach(
                  function (item) {
                      if (item.selected) {
                          mispedidosService.del(item.Id).then(function (item) {
                              $scope.loadData();
                          }, function (error) { })
                          return !item.selected;
                      }
                  }
                );
                $scope.selectAll = false;
            }
        }
    };

    showForm = function () {
        angular.element('.tooltip').remove();
        formTpl.show();
    };

    hideForm = function () {
        formTpl.hide();
    };

    $scope.$on('$destroy', function () {
        hideForm();
    });

}]);
