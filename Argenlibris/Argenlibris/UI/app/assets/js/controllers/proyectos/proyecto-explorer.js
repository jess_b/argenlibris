﻿//http://codepen.io/templarian/pen/VLKZLB
app.controller('ProyectoExplorerController', ['$scope','$rootScope','proyectoService',
function ($scope,$rootScope,proyectoService) {


   var saveProyect = function()
    {
        proyectoService.put($rootScope.selectedProyect).then(function (response)
        {
           // $rootScope.selectedProyect = response.data;
        });
    }



    $scope.tipoElemento = function (item) {
        if (item == undefined)
                return null;

        var isFolder = "<i class='md md-folder'> </i>" +item.Nombre;
             var isDiagram = item.Nombre ;
           //  var isFolderOpen = "<i class='md md-verified-user'></i>";
                                             if (item.isFolder)
                                                 return isFolder;
                                             return isDiagram;

                                         }

    $scope.editNode=function(node)
    {
        node.editing = false;
        saveProyect();
    }


     
      //$scope.menuOptions = [
      //    ['Crear Carpeta', function ($itemScope) {
      //        var nodeData = $itemScope.$modelValue;
      //        addFolder($itemScope);
      //        // $scope.player.gold -= $itemScope.item.cost;
      //    },  function ($itemScope) {
      //        return true;
      //    }],         
      //    ['Crear Diagrama', function ($itemScope) {
      //        addDiagram($itemScope);
      //       // $scope.player.gold += $itemScope.item.cost;
      //    }, function ($itemScope) {
      //        return true;//$itemScope.item.name.match(/Iron/) == null;
      //    }]
      //];


      $scope.remove = function (scope) {
          scope.remove();
          saveProyect();
      };

      $scope.toggle = function (scope) {
          scope.toggle();
      };

    

      $scope.newModel = function (scope) {
          var nodeData = $rootScope.selectedProyect;
          if (scope!=undefined)
              nodeData = scope.$modelValue;
          
          nodeData.Elementos.push({
              Nombre: "new diagram",
              editing: true,
              IsFolder: false
              
          });
          saveProyect();
      };

      $scope.openDiagram = function (item) {

          $rootScope.selectedDiagram = item;
          location.href = "/app/#/proyectos/diagrama";
          $rootScope.closeExplorer();
      };

      $scope.newSubItem = function (scope) {
          var nodeData = $rootScope.selectedProyect;
          if (scope != undefined)
              nodeData = scope.$modelValue;
          nodeData.Elementos.push({
              Nombre: "new folder",
              IsFolder: true,
              editing: true,
              Elementos: []
          });
          saveProyect();
      };

      $scope.collapseAll = function () {
          $scope.$broadcast('collapseAll');
      };

      $scope.expandAll = function () {
          $scope.$broadcast('expandAll');
      };


                      

      
  }
]);