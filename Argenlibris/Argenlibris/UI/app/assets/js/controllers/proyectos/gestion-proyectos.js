﻿app.controller('GestionProyectosController', ['$scope', '$window', '$aside','proyectoService','enumService','$rootScope', function ($scope, $window, $aside, proyectoService,enumService,$rootScope) {


    
    // settings
    $scope.settings = {
        singular: 'Proyecto',
        plural: 'Proyectos',
        cmd: 'Agregar'
    };


    $scope.loadData = function () {
        var usu = proyectoService.all().then(function (items) {
           $scope.data = items.data;
            


        });

    }

    $scope.loadData();

    

    // defining template
    var formTpl = $aside({
        scope: $scope,
        template: '/app/assets/tpl/proyectos/gestion-form.html',
        show: false,
        placement: 'left',
        backdrop: false,
        animation: 'am-slide-left'
    });

    // methods
    $scope.checkAll = function () {
        angular.forEach($scope.data, function (item) {
            item.selected = !item.selected;
        });
    };

    $scope.editItem = function (item) {
        if (item) {
            item.editing = true;
            $scope.item = item;
            $scope.settings.cmd = 'Editar';
            showForm();
        }
    };

    $scope.viewItem = function (item) {
        if (item) {
            item.editing = false;
            $scope.item = item;
            $scope.settings.cmd = 'Ver';
            showForm();
        }
    };

    
    $scope.estado=function(estado)
    {
        var r= enumService.htmlEstado(estado, "info");
        if (estado == "Finalizado")
            r = enumService.htmlEstado(estado, "success");

        return r;
    }

    $scope.proyectoPut=function()
    {
        
            proyectoService.put($scope.item).then(function (proyecto) {
                //if (proyecto.data!=undefined && proyecto.data!="")
                $rootScope.entitySaved();
                if ($scope.item.Estado == "Nuevo")
                    $scope.data.push(proyecto.data);
                $rootScope.$broadcast('proyecto:nuevo', $scope.data.length);
                
            }, function (error) {
               
            }).finally(function () {
               
                loadData();
                
            });

         
        };
    
 
    $scope.select=function(item)
    {
        $rootScope.$broadcast("proyecto:select", item);
    }
    $scope.createItem = function () {
        var item = {
            editing: true,
            Estado:"Nuevo"
        };
        $scope.item = item;
        $scope.settings.cmd = 'Nuevo';
        showForm();
    };

   

    $scope.saveItem = function () {
        if ($scope.settings.cmd == 'Guardar') {
            $scope.data.push($scope.item);
        
        }
        hideForm();
    };

    $scope.remove = function (item) {
        if (confirm('¿Está seguro?')) {
            if (item) {
                $scope.data.splice($scope.data.indexOf(item), 1);
            } else {
                $scope.data = $scope.data.filter(
                  function (item) {
                      return !item.selected;
                  }
                );
                $scope.selectAll = false;
            }
        }
    };

    showForm = function () {
        angular.element('.tooltip').remove();
        formTpl.show();
    };

    hideForm = function () {
        formTpl.hide();
    };

    $scope.$on('$destroy', function () {
        hideForm();
    });

}]);
