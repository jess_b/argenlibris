﻿app.controller('diagramaController', ['$scope', '$window', '$aside', 'proyectoService', 'diagramaService', '$rootScope','$http','$route',
    function ($scope, $window, $aside, proyectoService, diagramaService, $rootScope, $http,$route) {
    if ($rootScope.selectedDiagram == undefined) location.href = "/app/#/proyectos/gestion-proyectos";

    $rootScope.pageTitle = "Herramienta de modelado";


    //$scope.getDiagram = function () {
    //    dia.oneDiagram($rootS.selectedDiagram.Id).then(function (response)
    //    {
    //        $scope.modelJson = response.data;
    //        $rootScope.$broadcast("diagram:loaded", response.data);
    //    });
    //};

    
    $scope.load = function () {

        var id = $rootScope.selectedDiagram.Id;
        if (id != undefined && id !=null) {
            diagramaService.oneDiagram(id).then(function (response) {

                $window.graph.fromJSON(JSON.parse(response.data));

                //vm.graph.fromJSON(JSON.parse(jsonString))
      
            });
        }
    }

    $scope.load();
    $scope.save = function (graph)
    {
       
        var jsonString = JSON.stringify(graph)
        diagramaService.put(jsonString).then(function (response) {
            $rootScope.entitySaved();
        });
    }

}]);