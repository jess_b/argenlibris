﻿app.controller('GestionRespuestasController', ['$scope', '$window', '$aside', 'preguntaService', 'respuestaService', 'clienterespuestaService', '$rootScope', function ($scope, $window, $aside, preguntaService, respuestaService, clienterespuestaService, $rootScope) {

    $scope.preguntas = [];
    $scope.respuestas = [];


    $scope.showGraph = function (pregunta) {
        $scope.pp = pregunta.Descripcion;
        var datos = [];
        var votosTotales=0;
        $scope.respuestas.forEach(function (i) {

            //if (i.Pregunta.Id == pregunta.Id)
            if (i.respuesta.Pregunta.Id == pregunta.Id)
            {
                var p = [i.respuesta.Descripcion, i.votos];
                //var p = [i.Descripcion, i.votos];
                datos.push(p);
            }

        });

        $scope.chart = c3.generate({
            bindto: '#chart',
           
            data: {
                type: 'pie',
                columns: datos
            }
        });
    }



    $scope.$on("ok-votos", function ()
    {
        if ($scope.preguntas.length > 0)
            $scope.showGraph($scope.preguntas[0])
    });

    $scope.getdatefrom = function (date) {
        $scope.fechadesde = date;
        $scope.loadData();
    }
    $scope.getdateto = function (date) {
        $scope.fechahasta = date;
        $scope.loadData();
    }

    //$scope.loadData = function () {
    $scope.loadData = function (fechadesde, fechahasta) {
        if ($rootScope.encuestaElegida != undefined) {

            var encuesta = {};
            //var fechadesde;
            //var fechahasta;
            encuesta.Id = $rootScope.encuestaElegida.Id;

            if (fechadesde == undefined) {

                if ($scope.fechadesde == undefined) {
                    encuesta.fechadesde = "2015-01-01";
                } else {
                    encuesta.fechadesde = $scope.fechadesde;
                }
            }
            if (fechahasta == undefined) {

                if ($scope.fechahasta == undefined) {
                    encuesta.fechahasta = "2099-01-01";
                } else {
                    encuesta.fechahasta = $scope.fechahasta;
                }
            }

            if (fechadesde > fechahasta) {
                $scope.mensajeError = "La fecha desde debe ser menor a la fecha hasta";
            } else {
                $scope.mensajeError = "";
            }



            clienterespuestaService.all(encuesta).then(function (response) {

                $scope.respuestas = response.data;
                
                preguntaService.one($rootScope.encuestaElegida.Id).then(function (response) {

                    $scope.preguntas = response.data;
                   $scope.$emit("ok-votos");
     

                });
            });

            //respuestaService.one($rootScope.encuestaElegida.Id).then(function (response) {
            //    $scope.respuestas = response.data;
              

            //    $scope.respuestas.forEach(function (e) {
            //        clienterespuestaService.one(e.Id).then(function (response) {
            //            e.votos = response.data;
            //            $scope.$emit("ok-votos");
            //        });
            //    });

            //});


            //
        } else
            $window.location.href = "/app/#/marketing/gestion-encuestas";

    }

    $scope.loadData();



}]);
