﻿app.controller('GestionNewsletterController', ['$scope', '$window', '$aside', 'newsletterService', 'libroService', '$rootScope', function ($scope, $window, $aside, newsletterService, libroService, $rootScope) {

    $scope.librosNewsletter = [];

    libroService.all().then(function (response) {
        $scope.libros = response.data;
    });



    // settings
    $scope.settings = {
        singular: 'newsletter',
        plural: 'newsletters',
        cmd: 'Agregar'
    };

    $scope.savenewsletter = function (item) {
        if (item) {

            newsletterService.put(item).then(function (response) {
                $scope.data.push(item);

            }, function (error) {

            }).finally(function () {

                //$scope.loadData();

            });

        }
    };

    $scope.loadData = function (newsletter) {
        var usu = newsletterService.dos().then(function (items) {
            $scope.data = items.data;

        });

    }

    $scope.loadData();

    $scope.agregarLibro = function (libro) {

        var agregar = true;

        $scope.librosNewsletter.forEach(
            function (item) {
                if (item == libro) {
                    agregar = false;
                }
            });

        if (agregar == true) {
            $scope.librosNewsletter.push(libro);
        }
    }

    // defining template
    var formTpl = $aside({
        scope: $scope,
        mirespuesta: $scope.mirespuesta,
        template: '/app/assets/tpl/marketing/gestion-newsletters-form.html',
        show: false,
        placement: 'left',
        backdrop: false,
        animation: 'am-slide-left'
    });

    // methods
    $scope.checkAll = function () {
        angular.forEach($scope.data, function (item) {
            item.selected = !item.selected;
        });
    };

    $scope.sendItem = function (item) {

            newsletterService.send(item.Id).then(function (response) {

            });

    }

    $scope.editItem = function (item) {
        if (item) {
            // mando id de newsletter
            newsletterService.one(item.Id).then(function (response) {
                $scope.librosNewsletter = response.data.Libros;
            });

            item.editing = true;
            $scope.item = item;
            $scope.mirespuesta = "";
            $scope.settings.cmd = 'Editar';

            showForm();

        }
    };

    $scope.viewItem = function (item) {
        if (item) {
            item.editing = false;
            $scope.item = item;
            $scope.settings.cmd = 'Ver';
            showForm();
        }
    };




    $scope.newsletterPut = function () {

        $scope.item.Libros = $scope.librosNewsletter;

        newsletterService.put($scope.item).then(function (newsletter) {

            $rootScope.entitySaved();
            if ($scope.item.Estado == "Nuevo")
                $scope.data.push(newsletter.data);
            $rootScope.$broadcast('newsletter:nuevo', $scope.data);

        }, function (error) {

        }).finally(function () {

            loadData();

        });


    };


    $scope.createItem = function () {
        var item = {
            editing: true,
            Estado: "Nuevo",
            Descripcion: ""
        };
        $scope.item = item;
        $scope.librosNewsletter = [];
        $scope.settings.cmd = 'Nuevo';
        showForm();
    };



    $scope.saveItem = function () {
        if ($scope.settings.cmd == 'Guardar') {
            $scope.data.push($scope.item);

        }
        hideForm();
    };

    $scope.guardarCambios = function (item) {
        $scope.data = $scope.data.forEach(
                  function (item) {
                      newsletterService.put(item).then(function (response) {

                      }
                  );

                  });
    }


    $scope.remove = function (item) {
        if (confirm('¿Está seguro?')) {
            if (item) {
                newsletterService.del(item.Id).then(function (item) {
                    $scope.loadData();
                    $rootScope.newsletters.splice($scope.data.indexOf(item), 1);
                    $scope.data.splice($scope.data.indexOf(item), 1);
                }, function (error) {

                })

            } else {
                $scope.data = $scope.data.forEach(
                  function (item) {
                      if (item.selected) {
                          newsletterService.del(item.Id).then(function (item) {
                              $scope.loadData();
                          }, function (error) { })
                          return !item.selected;
                      }
                  }
                );
                $scope.selectAll = false;
            }
        }
    };

    showForm = function () {
        angular.element('.tooltip').remove();
        //$scope.librosNewsletter = [];
        formTpl.show();
    };

    hideForm = function () {
        formTpl.hide();
    };

    $scope.$on('$destroy', function () {
        hideForm();
    });

}]);
