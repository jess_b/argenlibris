﻿app.controller('GestionPreguntasController', ['$scope', '$window', '$aside', 'preguntaService', 'respuestaService', 'encuestaService', '$rootScope', function ($scope, $window, $aside, preguntaService, respuestaService, encuestaService, $rootScope) {


    encuestaService.all().then(function (response) {
        $scope.encuestas = response.data;
    });



    // settings
    $scope.settings = {
        singular: 'pregunta',
        plural: 'preguntas',
        cmd: 'Agregar'
    };

    $scope.savePregunta = function (item) {
        if (item) {

            preguntaService.put(item).then(function (response) {


            }, function (error) {

            }).finally(function () {

                $scope.loadData();

            });

        }
    };

    $scope.loadData = function (pregunta) {
        var usu = preguntaService.all().then(function (items) {
            $scope.data = items.data;

        });

    }

    $scope.loadData();

    // defining template
    var formTpl = $aside({
        scope: $scope,
        mirespuesta: $scope.mirespuesta,
        template: '/app/assets/tpl/marketing/gestion-preguntas-form.html',
        show: false,
        placement: 'left',
        backdrop: false,
        animation: 'am-slide-left'
    });

    // methods
    $scope.checkAll = function () {
        angular.forEach($scope.data, function (item) {
            item.selected = !item.selected;
        });
    };

    $scope.editItem = function (item) {
        if (item) {
            // mando id de pregunta
            respuestaService.misrespuestas(item.Id).then(function (response) {
                $scope.respuestas = response.data;
            });

            item.editing = true;
            $scope.item = item;
            $scope.mirespuesta = "";
            $scope.settings.cmd = 'Editar';
            showForm();
        }
    };

    $scope.viewItem = function (item) {
        if (item) {
            item.editing = false;
            $scope.item = item;
            $scope.settings.cmd = 'Ver';
            showForm();
        }
    };




    $scope.preguntaPut = function () {

        preguntaService.put($scope.item).then(function (pregunta) {

            $rootScope.entitySaved();
            if ($scope.item.Estado == "Nuevo")
                $scope.data.push(pregunta.data);
            $rootScope.$broadcast('pregunta:nuevo', $scope.data);

        }, function (error) {

        }).finally(function () {

            loadData();

        });


    };

    $scope.respuestaPut = function (respuesta) {
        var mirespuesta = {};
        mirespuesta.Pregunta = $scope.item;
        mirespuesta.Descripcion = respuesta;
       
        $scope.respuestas.push(mirespuesta);

        respuestaService.put(mirespuesta).then(function (r) {



        }, function (error) {

        }).finally(function () {

            loadData();

        });


    };

    $scope.createItem = function () {
        var item = {
            editing: true,
            Estado: "Nuevo",
            Descripcion: ""
        };
        item.creating = true;
        $scope.item = item;
        $scope.settings.cmd = 'Nuevo';
        showForm();
    };



    $scope.saveItem = function () {
        if ($scope.settings.cmd == 'Guardar') {
            $scope.data.push($scope.item);

        }
        hideForm();
    };

    $scope.guardarCambios = function (item) {
        $scope.data = $scope.data.forEach(
                  function (item) {
                      preguntaService.put(item).then(function (response) {

                      }
                  );

                  });
    }


    $scope.remove = function (item) {
        if (confirm('¿Está seguro?')) {
            if (item) {
                preguntaService.del(item.Id).then(function (item) {
                    $scope.loadData();
                    $rootScope.preguntas.splice($scope.data.indexOf(item), 1);
                    $scope.data.splice($scope.data.indexOf(item), 1);
                }, function (error) {

                })

            } else {
                $scope.data = $scope.data.forEach(
                  function (item) {
                      if (item.selected) {
                          preguntaService.del(item.Id).then(function (item) {
                              $scope.loadData();
                          }, function (error) { })
                          return !item.selected;
                      }
                  }
                );
                $scope.selectAll = false;
            }
        }
    };

    showForm = function () {
        angular.element('.tooltip').remove();
        formTpl.show();
    };

    hideForm = function () {
        formTpl.hide();
    };

    $scope.$on('$destroy', function () {
        hideForm();
    });

}]);
