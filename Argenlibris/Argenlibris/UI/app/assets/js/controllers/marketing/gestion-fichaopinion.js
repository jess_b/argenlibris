﻿app.controller('GestionFichaOpinionController', ['$scope', '$window', '$aside', 'fichaopinionService', 'preguntaService', 'respuestaService', '$rootScope', function ($scope, $window, $aside, fichaopinionService, preguntaService, respuestaService, $rootScope) {



    // settings
    $scope.settings = {
        singular: 'fichaopinion',
        plural: 'fichasopinion',
        cmd: 'Agregar'
    };



    $scope.loadData = function (fichaopinion) {
        var usu = fichaopinionService.dos().then(function (items) {
            $scope.data = items.data;

        });

    }

    $scope.loadData();

    // defining template
    var formTpl = $aside({
        scope: $scope,
        template: '/app/assets/tpl/marketing/gestion-fichaopinion-form.html',
        show: false,
        placement: 'left',
        backdrop: false,
        animation: 'am-slide-left'
    });

    var formTpl2 = $aside({
        scope: $scope,
        template: '/app/assets/tpl/marketing/gestion-preguntas-form.html',
        show: false,
        placement: 'left',
        backdrop: false,
        animation: 'am-slide-left'
    });

    var formTpl3 = $aside({
        scope: $scope,
        template: '/app/assets/tpl/marketing/gestion-preguntas-todas-form.html',
        show: false,
        placement: 'left',
        backdrop: false,
        animation: 'am-slide-left'

    });

    // methods
    $scope.checkAll = function () {
        angular.forEach($scope.data, function (item) {
            item.selected = !item.selected;
        });
    };

    $scope.editItem = function (item) {
        if (item) {
            item.editing = true;
            $scope.item = item;
            $scope.settings.cmd = 'Editar';
            showForm();
        }
    };

    $scope.addItem = function (item) {
        if (item) {
            item.editing = true;
            $scope.item = item;
            $scope.settings.cmd = 'Editar';
            angular.element('.tooltip').remove();
            $scope.limpiarForm();
            formTpl2.show();
        }
    };

    $scope.verMas = function (item) {
        if (item) {
            item.editing = true;
            $scope.item = item;
            $scope.settings.cmd = 'Editar';
            angular.element('.tooltip').remove();

            preguntaService.one(item.Id).then(function (response) {
                $scope.preguntas = response.data;
            });
            respuestaService.one(item.Id).then(function (response) {
                $scope.respuestas = response.data;
            });

            formTpl3.show();
        }
    }

    $scope.verGrafico = function (item) {

        $rootScope.encuestaElegida = item;

        $window.location.href = '/app/#/marketing/gestion-respuestas';

    };

    $scope.limpiarForm = function () {
        $scope.preguntas = [];
        $scope.respuestas = [];
    }

    $scope.agregarRespuesta = function (item) {

        var nuevaRespuesta = {};

        if (item) {
            nuevaRespuesta.Descripcion = item;
            $scope.respuestas.push(item);
        }
    };



    $scope.agregar = function () {

        var nuevaPregunta = {};
        var nuevaRespuesta = {};

        nuevaPregunta.Descripcion = $scope.item.pregunta;
        nuevaPregunta.Encuesta = $scope.item;

        preguntaService.put(nuevaPregunta).then(function (response) {
            nuevaPregunta = response.data;
            $scope.preguntas.push(nuevaPregunta);

            // guardo respuestas
            nuevaRespuesta.Pregunta = nuevaPregunta;
            nuevaRespuesta.Respuestas = $scope.respuestas;
            respuestaService.put(nuevaRespuesta).then(function (response) {
                $scope.limpiarForm();
            })

        })

    }


    $scope.fichaopinionPut = function () {
        
        if (Date.parse($scope.item.FechaDesde) > Date.parse($scope.item.FechaHasta)) {
            $scope.mensajeFecha = "Fecha Desde debe ser menor a Fecha Hasta";

        } else {

            $scope.mensajeFecha = "";

            fichaopinionService.put($scope.item).then(function (fichaopinion) {

                $rootScope.entitySaved();
                if ($scope.item.Estado == "Nuevo")
                    $scope.data.push(fichaopinion.data);
                $rootScope.$broadcast('fichaopinion:nuevo', $scope.data);

            }, function (error) {

            }).finally(function () {

                loadData();

            });
        }

    };


    $scope.createItem = function () {
        var item = {
            editing: true,
            Estado: "Nuevo",
            Descripcion: ""
        };
        $scope.item = item;
        $scope.settings.cmd = 'Nuevo';
        showForm();
    };



    $scope.saveItem = function () {
        if ($scope.settings.cmd == 'Guardar') {
            $scope.data.push($scope.item);

        }
        hideForm();
    };

    $scope.guardarCambios = function () {
        $scope.data = $scope.data.forEach(
                  function (item) {
                      fichaopinionService.put(item).then(function (response) {

                      }
                  );

                  });
    }


    $scope.remove = function (item) {
        if (confirm('¿Está seguro?')) {
            if (item) {
                fichaopinionService.del(item.Id).then(function (item) {
                    $scope.loadData();
                    $rootScope.fichasopinion.splice($scope.data.indexOf(item), 1);
                    $scope.data.splice($scope.data.indexOf(item), 1);
                }, function (error) {

                })

            } else {
                $scope.data = $scope.data.forEach(
                  function (item) {
                      if (item.selected) {
                          fichaopinionService.del(item.Id).then(function (item) {
                              $scope.loadData();
                          }, function (error) { })
                          return !item.selected;
                      }
                  }
                );
                $scope.selectAll = false;
            }
        }
    };

    showForm = function () {
        angular.element('.tooltip').remove();
        if ($scope.item.FechaDesde == undefined) {
            $scope.item.FechaDesde = new Date();
        }
        if ($scope.item.FechaHasta == undefined) {
            $scope.item.FechaHasta = new Date();
        }
        formTpl.show();
    };

    hideForm = function () {
        formTpl.hide();
    };

    $scope.$on('$destroy', function () {
        hideForm();
    });

}]);
