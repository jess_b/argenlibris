﻿
app.controller('GestionNovedadesController', ['fileUpload', '$scope', '$window', '$aside', 'NovedadService', '$rootScope', 'categoriaService', function (fileUpload, $scope, $window, $aside, NovedadService, $rootScope, categoriaService) {



    // settings
    $scope.settings = {
        singular: 'Novedad',
        plural: 'Novedades',
        cmd: 'Agregar'
    };


    categoriaService.all().then(function (response) {
        $scope.categorias = response.data;
    });

    $scope.loadData = function (Novedad) {
        var usu = NovedadService.one().then(function (items) {
            $scope.data = items.data;

        });

    }

    $scope.loadData();

    // defining template
    var formTpl = $aside({
        scope: $scope,
        template: '/app/assets/tpl/marketing/gestion-novedades-form.html',
        show: false,
        placement: 'left',
        backdrop: false,
        animation: 'am-slide-left'
    });

    // methods
    $scope.checkAll = function () {
        angular.forEach($scope.data, function (item) {
            item.selected = !item.selected;
        });
    };

    $scope.editItem = function (item) {
        if (item) {
            item.editing = true;
            $scope.item = item;
            $scope.settings.cmd = 'Editar';
            showForm();
        }
    };

    $scope.viewItem = function (item) {
        if (item) {
            item.editing = false;
            $scope.item = item;
            $scope.settings.cmd = 'Ver';
            showForm();
        }
    };




    $scope.NovedadPut = function () {

        var file = $scope.item.image;
        var uploadUrl = '/api/image';
        if (file != undefined) {
            var extn = $scope.item.Id + "." + file.name.split(".").pop()
            $scope.item.FileName = extn;
        }

        NovedadService.put($scope.item).then(function (Novedad) {

            $rootScope.entitySaved();
            if ($scope.item.Estado == "Nuevo")
                $scope.data.push(Novedad.data);
            $rootScope.$broadcast('Novedad:nuevo', $scope.data);
            fileUpload.uploadFileToUrl(file, uploadUrl, Novedad.data.Id);

        }, function (error) {

        }).finally(function () {

            loadData();

        });


    };


    $scope.sendItem = function (item) {

        NovedadService.send(item.Id).then(function (response) {

        });

    }


    $scope.createItem = function () {
        var item = {
            editing: true,
            Estado: "Nuevo",
            Descripcion: ""
        };
        $scope.item = item;
        $scope.settings.cmd = 'Nuevo';
        showForm();
    };



    $scope.saveItem = function () {
        if ($scope.settings.cmd == 'Guardar') {
            $scope.data.push($scope.item);

        }
        hideForm();
    };

    $scope.remove = function (item) {
        if (confirm('¿Está seguro?')) {
            if (item) {
                NovedadService.del(item.Id).then(function (item) {
                    $scope.loadData();
                    $rootScope.Novedades.splice($scope.data.indexOf(item), 1);
                    $scope.data.splice($scope.data.indexOf(item), 1);
                }, function (error) {

                })

            } else {
                $scope.data = $scope.data.forEach(
                  function (item) {
                      if (item.selected) {
                          NovedadService.del(item.Id).then(function (item) {
                              $scope.loadData();
                          }, function (error) { })
                          return !item.selected;
                      }
                  }
                );
                $scope.selectAll = false;
            }
        }
    };

    showForm = function () {
        angular.element('.tooltip').remove();
        formTpl.show();
    };

    hideForm = function () {
        formTpl.hide();
    };

    $scope.$on('$destroy', function () {
        hideForm();
    });

}]);
