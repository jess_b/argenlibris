/*jslint strict: true */

var app = angular.module('argenlibris', [
  'app.constants',

  'ngRoute',
  'ngAnimate',
  'ngSanitize',
  'ngPlaceholders',
  'ngTable',

  'angular-loading-bar',
  'ui.tree',
  'angulartics',
  'angulartics.google.analytics',

  'uiGmapgoogle-maps',
  'ui.select',
   

  //'gridshore.c3js.chart',
  'monospaced.elastic',     // resizable textarea
  'mgcrea.ngStrap',
  'jcs-autoValidate',
  'ngFileUpload',
  'textAngular',
  'fsm',                    // sticky header
  'smoothScroll',
  'LocalStorageModule',
  'angularPayments'
]);
app.run(['$rootScope', '$http', 'usuarioService', 'mispermisosService', function ($rootScope, $http, usuarioService, mispermisosService)
{
   
    $rootScope.logout = function () {

        $rootScope.logged_user = sessionStorage.username =  sessionStorage.userId = undefined;
        $rootScope.token = sessionStorage.token = undefined;
        $rootScope.user = undefined;
        $rootScope.mispermisos = undefined;
        
        //sessionStorage.userId = response.data.UsuarioId;
        //sessionStorage.token = response.data.Token;
        location.href = '/';

        
    };
    $rootScope.isLogged = function () {
        
        return !($rootScope.token === undefined);
        //return !($rootScope.token === undefined) && ($rootScope.userId === undefined);

    }

    var init = function()
    {

     

        $rootScope.userId = sessionStorage.userId;
        $rootScope.token = sessionStorage.token;
        $rootScope.idiomaActual = "4f486fe3-0a50-4ee8-9f32-a534017c9a11";
        if ($rootScope.isLogged) {
            
            usuarioService.one($rootScope.userId)
            .then(function (data, status, headers, config) {
                $rootScope.user = data.data;
            }).catch(function (data, status, headers, config) {
                $rootScope.userId = sessionStorage.userId = undefined;//$window.sessionStorage.username;
                $rootScope.token = sessionStorage.token = undefined;
                $rootScope.user = undefined;
                $rootScope.logout();
            });

            mispermisosService.all($rootScope.userId).then(function (response) {
                $rootScope.mispermisos = response.data;
            });


        }
       

       

        
        
    }
    
    
    init();
    
    }]);
