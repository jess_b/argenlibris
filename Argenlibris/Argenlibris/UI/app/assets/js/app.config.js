// routes

//Implementing Authentication in Angular Applications
//http://www.sitepoint.com/implementing-authentication-angular-applications/

app.config(['$routeProvider', 'USER_ROLES', function ($routeProvider, USER_ROLES) {
    


    $routeProvider.when('/', {
        templateUrl: 'assets/tpl/dashboard.html',
      
    }).when('/:folder/:tpl', {
        templateUrl: function (attr) {
            return 'assets/tpl/' + attr.folder + '/' + attr.tpl + '.html';
      
        }
    }).when('/:tpl', {
        templateUrl: function (attr) {
            return 'assets/tpl/' + attr.tpl + '.html';
      
        }
    }).otherwise({ redirectTo: '/app' });
}])

// google maps
.config(['uiGmapGoogleMapApiProvider', function (uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
        //    key: 'your api key',
        v: '3.17',
        libraries: 'weather,geometry,visualization'
    });
}])

// loading bar settings
.config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
    cfpLoadingBarProvider.latencyThreshold = 300;
}])

// defaults for date picker
.config(['$datepickerProvider', function ($datepickerProvider) {
    angular.extend($datepickerProvider.defaults, {
        dateFormat: 'dd/MM/yyyy',
        iconLeft: 'md md-chevron-left',
        iconRight: 'md md-chevron-right',
        autoclose: true
    });
}])

// defaults for date picker
.config(['$timepickerProvider', function ($timepickerProvider) {
    angular.extend($timepickerProvider.defaults, {
        timeFormat: 'HH:mm',
        iconUp: 'md md-expand-less',
        iconDown: 'md md-expand-more',
        hourStep: 1,
        minuteStep: 1,
        arrowBehavior: 'picker',
        modelTimeFormat: 'HH:mm'
    });
}])

// disable nganimate with adding class
.config(['$animateProvider', function ($animateProvider) {
    $animateProvider.classNameFilter(/^(?:(?!ng-animate-disabled).)*$/);
}])


    .factory('authHttpResponseInterceptor', ['$q', '$location', '$rootScope','$window', function ($q, $location, $rootScope,$window) {
        return {
            // On request success
            request: function (config) {
                 //console.log(config); // Contains the data about the request before it is sent.
                if ($rootScope.isLogged()) {
                    config.headers['X-Token'] = $rootScope.token;
                
                }
                // Return the config or wrap it in a promise if blank.
                return config || $q.when(config);
            },

            // On request failure
            requestError: function (rejection) {
                
                return $q.reject(rejection);
            },
            response: function (response) {
                
                
                $rootScope.manageErrors(response.status);
                
                return response || $q.when(response);
            },
            responseError: function (rejection) {
                
                $rootScope.manageErrors(rejection.status);
                if (rejection.status == 403)
                    $window.location.href = "/app/#/forbidden";
                
                return $q.reject(rejection);
            }
        }
    }])

    //agregop un interceptor para capturar responses
.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('authHttpResponseInterceptor');

}])



//.config(function ($httpProvider) {
//    $httpProvider.defaults.transformRequest = function (data) {
//        if (data === undefined) {
//            return data;
//        }
//        return $.param(data);
//    }

//})
.config(function ($httpProvider) {
    //$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'; //convierto el contneido a formencoded
    //$httpProvider.defaults.headers.post['Accept'] = '*/*';
    ////$httpProvider.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded'; //convierto el contneido a formencoded
    //$httpProvider.defaults.headers.put['Accept'] = '*/*';

})


// set constants
.run(['$rootScope', 'APP', '$http', '$alert', '$timeout', function ($rootScope, APP, $http, $alert, $timeout) {
    $rootScope.APP = APP;
    
   if ($rootScope.isLogged()) {
        //do something...
    } else {
       //$rootScope.logout();
    }

    // register listener to watch route changes
    $rootScope.$on("$routeChangeStart", function (event, next, current) {
     //ver como funca esto
    });


    


   
    var entitySavedMsg = $alert({
        title:'Guardado ',
        content: 'Se completo la solicitud correctamente!',
        placement: 'top-right',
        type: 'theme',
        container: '.alert-container-top-right',
        show: false,
        animation: 'mat-grow-top-right'
    });

    var error400 = $alert({
        title: 'Error 400',
        content: 'Bad request',
        placement: 'top-right',
        type: 'theme',
        container: '.alert-container-top-right',
        show: false,
        animation: 'mat-grow-top-right'
    });
    var error403 = $alert({
        title: 'Error 403',
        content: 'No tiene acceso al recurso solicitado',
        placement: 'top-right',
        type: 'theme',
        container: '.alert-container-top-right',
        show: false,
        animation: 'mat-grow-top-right'
    });

    var error404 = $alert({
        title: 'Error 404',
        content: 'El recurso no est� disponible',
        placement: 'top-right',
        type: 'theme',
        container: '.alert-container-top-right',
        show: false,
        animation: 'mat-grow-top-right'
    });
    var error500 = $alert({
        title: 'Error 500!',
        content: 'Error interno del servidor',
        placement: 'top-right',
        type: 'theme',
        container: '.alert-container-top-right',
        show: false,
        animation: 'mat-grow-top-right'
    });

    var error501 = $alert({
        title: 'Error',
        content: 'El valor introducido ya existe',
        placement: 'top-right',
        type: 'theme',
        container: '.alert-container-top-right',
        show: false,
        animation: 'mat-grow-top-right'
    });


    var error415 = $alert({
        title: 'Error 415!',
        content: 'Unsupported media type',
        placement: 'top-right',
        type: 'theme',
        container: '.alert-container-top-right',
        show: false,
        animation: 'mat-grow-top-right'
    });

    $rootScope.entitySaved = function()
    {
 entitySavedMsg.show();
 $timeout(function () { entitySavedMsg.hide(); }, 3000);
       
    }
    $rootScope.manageErrors = function (status) {
        if (status == 401) {
            $rootScope.logout();
        }

        if (status == 415) {
            error415.show();
            $timeout(function () { error415.hide(); }, 3000);
        }
        if (status == 403) {
            error403.show();
            $timeout(function () { error403.hide(); }, 3000);


        }
        if (status == 404) {
            error404.show();
            $timeout(function () { error404.hide(); }, 3000);

        }

        if (status == 500) {
            error500.show();
            $timeout(function () { error500.hide(); }, 3000);

        }
        if (status == 400) {
            error400.show();
            $timeout(function () { error400.hide(); }, 3000);

        }
        if (status == 501) {
            error501.show();
            $timeout(function () { error501.hide(); }, 3000);

        }
    };





}]);

