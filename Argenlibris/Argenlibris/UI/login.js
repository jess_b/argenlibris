﻿var app = angular.module('login', ['ngCookies']);

app.factory('authHttpResponseInterceptor', ['$q', '$location', '$rootScope', function ($q, $location, $rootScope) {
    return {
        // On request success
        request: function (config) {
            $rootScope.ErrorValidador = false;
            //console.log(config); // Contains the data about the request before it is sent.
            if (sessionStorage.userId!="undefined") {
                config.headers['X-Token'] = sessionStorage.token;
                
            }
            // Return the config or wrap it in a promise if blank.
            return config || $q.when(config);
        },

        // On request failure
        requestError: function (rejection) {
                
            return $q.reject(rejection);
        },
        response: function (response) {
                
                
           // $rootScope.manageErrors(response.status);
                
            return response || $q.when(response);
        },
        responseError: function (rejection) {
                
           // $rootScope.manageErrors(rejection.status);
            if (rejection.status == 520) {
                var p = "";
                rejection.data.forEach(function (e) {
                    p = p + e.Clave + ": " + e.Valor + ", ";
                });
                $rootScope.ErrorValidador = true;
                $rootScope.error_validador = "Errores de validacion: " + p;
                //alert("Errores de validacion: " + p);
            } else {
                $rootScope.ErrorValidador = false;
            }

            return $q.reject(rejection);
        }
    }
}])

 //agregop un interceptor para capturar responses
.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('authHttpResponseInterceptor');

}])

app.controller('registroController', function ($scope, $http, $rootScope, $window) {

    $scope.errorRegistro = false;
    $scope.mensajeErrorRegistro = "Complete los campos obligatorios";
    $scope.leerCondiciones = false;
    $scope.usuario = {};
    $scope.categoriacliente = {};
    $scope.usuario.apellido = "";
   



    $scope.activar = function () {
        var token = QueryString.activar;
        if (token != undefined)
        { var res = $http.post("/api/usuario/" + token ); }
    }


    $scope.validarUsuario = function(){
        var usuario = {};
        usuario.NombreUsuario = $scope.usuario.nombreusuario;

            var res = $http.post("/api/usuariodos/", usuario);
            res.then(function (response) {

                if (response.data == null) {

                    var n = $scope.usuario.nombreusuario.length;

                    if (n > 255) {

                        $scope.mensajeErrorRegistro = "Campo Nombre de Usuario: Longitud Maxima de 255 caracteres";
                        $scope.errorRegistro = true;

                    } else {

                        //usuario aun no creado con el mismo nombre de usuario
                        $scope.mensajeErrorRegistro = "";
                        $scope.errorRegistro = false;

                    }
                    
                } else {
                    //encontró usuario con el mismo nombre de usuario
                    if ($scope.errorRegistro == false) {

                            $scope.mensajeErrorRegistro = "Ya existe el nombre de usuario";
                            $scope.errorRegistro = true;
                        
                    }
                }


            })
             .catch(function (resp, status) {

                     $scope.mensajeErrorRegistro = "Error nombre de usuario";
                     $scope.errorRegistro = true;
                 

             });

       
    }
  
    $scope.validarMail = function () {
        var usuario = {};
        usuario.Email = $scope.usuario.email;
    

            var res = $http.post("/api/usuariodos/", usuario);
            res.then(function (response) {

                if (response.data == null) {

                    var n = $scope.usuario.email.length;

                    if (n > 255) {

                        $scope.mensajeErrorRegistro = "Campo Email: Longitud Maxima de 255 caracteres";
                        $scope.errorRegistro = true;

                    } else {
                        //usuario aun no creado con el mismo email
                        $scope.mensajeErrorRegistro = "";
                        $scope.errorRegistro = false;
                    }
                } else {

                    if ($scope.errorRegistro == false) {
                        //encontró usuario con el mismo email
                        $scope.mensajeErrorRegistro = "El Email ya está registrado";
                        $scope.errorRegistro = true;
                    }
                }


            })
             .catch(function (resp, status) {

                 $scope.mensajeErrorRegistro = "Error mail";
                 $scope.errorRegistro = true;

             });

        

    }

    $scope.validarEdad = function () {
        var edad = 0;

        edad = $scope.getAge($scope.fechanacimiento);


        if (edad < 18) {

                if ($scope.errorRegistro == false) {

                    $scope.errorRegistro = true;
                    $scope.mensajeErrorRegistro = "Debe ser mayor a 18 años";

                }

            } else {
                $scope.errorRegistro = false;
                $scope.mensajeErrorRegistro = "";
            }

        

    }

    $scope.getAge = function (dateString) {

        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }

    $scope.activar();

    $scope.validarRegistro = function () {

        if ($scope.errorRegistro == false) {

            if ($scope.usuario.nombre.length > 255) {

                $scope.mensajeErrorRegistro = "Campo Nombre: Longitud Maxima de 255 caracteres";
                $scope.errorRegistro = true;

            } else if ($scope.usuario.apellido.length > 255) {

                $scope.mensajeErrorRegistro = "Campo Apellido: Longitud Maxima de 255 caracteres";
                $scope.errorRegistro = true;

            } else if ($scope.usuario.calle.length > 255) {

                $scope.mensajeErrorRegistro = "Campo Dirección - Calle: Longitud Maxima de 255 caracteres";
                $scope.errorRegistro = true;


            } else if ($scope.usuario.numero.length > 255) {

                $scope.mensajeErrorRegistro = "Campo Dirección - Numero: Longitud Maxima de 255 caracteres";
                $scope.errorRegistro = true;

            } else if ($scope.usuario.Password.length > 255) {

                $scope.mensajeErrorRegistro = "Campo Password: Longitud Maxima de 255 caracteres";
                $scope.errorRegistro = true;

            }

        }



    }

    $scope.GuardarRegistro = function()
    {
        $scope.usuario.direccion = $scope.usuario.calle + ' ' +  $scope.usuario.numero
        $scope.usuario.FechaNacimiento = $("#fechanacimiento").val();

        $scope.validarMail();
        $scope.validarEdad();
        $scope.validarUsuario();
        $scope.validarRegistro();

        if ($scope.errorRegistro == false) {


            var res = $http.put("/api/usuario/", $scope.usuario);
            res.then(function (response) {


                console.log(response.data);
                $scope.error_message = "";
                $rootScope.isError = false;
                $rootScope.token = sessionStorage.token = response.data.Token;
                $rootScope.userId = sessionStorage.userId = response.data.UsuarioId;
                $rootScope.registro = !$rootScope.registro;
                $rootScope.info_message = "Se ha enviado un mail para activar la cuenta";
                $rootScope.isInfo = true;
                //$window.location.href = '/app';

                $scope.categorias.forEach(function (e) {
                    if (e.Eleccion) {
                        $scope.categoriacliente.idcategoria = e.Id;
                        $scope.categoriacliente.idcliente = response.data.Id;
                        var res = $http.put("/api/categoriacliente/", $scope.categoriacliente);
                        res.then(function (response) {

                        })
                         .catch(function (resp, status) {

                         });

                    }

                });

            })

             .catch(function (resp, status) {
                 $rootScope.isError = true;
                 delete sessionStorage.Token;
                 delete sessionStorage.UsuarioId;

                 if (resp.status == 401)
                     $scope.error_message = "usuario o contraseña incorrecto!";
                 else
                     $scope.error_message = "error del sistema";


             });


        } else {

            $scope.errorRegistro = true;

        }


    }

    $scope.esconderRegistro = function()
    {
        $rootScope.registro = false;
        $scope.leerCondiciones = false;
        
    }

    $scope.verCondiciones = function () {
        $scope.leerCondiciones = true;
    }

    $scope.Agree = function () {
        $scope.error_condiciones = "";
    }

    $scope.Desagree = function () {
        $scope.error_condiciones = "Debe aceptar los terminos y condiciones para continuar";

    }

    //Agree
    $('#Button1').on('click', function () {
        //$scope.error_condiciones = " ";
        var iSelector = $('#signupform').find("i");
        if (iSelector.hasClass('form-control-feedback fv-icon-no-label glyphicon glyphicon-remove')) {
            iSelector.removeClass('form-control-feedback fv-icon-no-label glyphicon glyphicon-remove');
            iSelector.addClass('form-control-feedback fv-icon-no-label glyphicon glyphicon-ok');
        }
        $scope.leerCondiciones = !$scope.leerCondiciones;

    });

    //Desagree
    $('#Button2').on('click', function () {
        
        var iSelector = $('#signupform').find("i");
        if (iSelector.hasClass('form-control-feedback fv-icon-no-label glyphicon glyphicon-ok')) {
            iSelector.removeClass('form-control-feedback fv-icon-no-label glyphicon glyphicon-ok');
            iSelector.addClass('form-control-feedback fv-icon-no-label glyphicon glyphicon-remove');
        }
        $scope.leerCondiciones = !$scope.leerCondiciones;

        //$scope.error_condiciones = "Debe aceptar los terminos y condiciones para continuar";
    });

});

app.controller('loginController', function (chatService,representantesService,backendHubProxy, $scope, $http, $rootScope, $window, $cookies) {

    $scope.preguntas = [];
    $scope.respuestas = [];
    $scope.misEncuestas = [];
    $scope.misRespuestas = [];
    $scope.libroSeleccionado = "";
    $scope.PuedeComentar = false;
    $scope.PuedePuntuar = false;
    $scope.estaPuntuacion = "";
    $scope.mostrarNC = false;
    $scope.contacto = "";

    $scope.sumNC = 0;
   
        
     $http.post("/api/novedad/").then(function (response) {
            $scope.novedades = response.data;
     })


    $http.post("/api/categoria/").then(function (response) {
        $scope.categorias = response.data;
    })

    $http.post("/api/tarjeta/").then(function (response) {
        $scope.tarjetas = response.data;
    })

    $http.get("/api/encuesta/").then(function (response) {
        $scope.misEncuestas = response.data;

        $http.get("/api/fichaopinion/").then(function (response) {
            // agrego las fichas de opinion a las encuestas a completar
            response.data.forEach(function (e) {
                $scope.misEncuestas.push(e);
            });
        })

    })

    $scope.enviarMailContacto = function () {
        //var contacto = "";
        //contacto = $scope.contacto;
        $scope.mensajeContacto = "";


        $http.put("/api/contacto/", $scope.contacto).then(function (response) {
            $scope.contacto = "";
            $scope.mensajeContacto = "Mensaje enviado";
        })

    }
   
    $scope.enviarMailNovedad = function () {
        var categoriaVisitante = {};
        var categoria = [];
        $scope.mensajeCategoriaVisitante = "";
        $scope.categorias.forEach(function (e) {
            if (e.Eleccion) {
                categoria.push(e);
            }
        });

        categoriaVisitante.Mail = $scope.novedad.mail;
        categoriaVisitante.Categorias = categoria;

        var res = $http.put("/api/categoriavisitante/", categoriaVisitante);
        res.then(function (response) {
            $scope.novedad.mail = "";
            $scope.mensajeCategoriaVisitante = "El registro fue enviado";
        })
         .catch(function (resp, status) {
             $scope.mensajeCategoriaVisitante = "Este mail ya se encuentra registrado";
         });

        $http.put("/api/contacto/", $scope.contacto).then(function (response) {
            $scope.contacto = "";
            $scope.mensajeContacto = "Mensaje enviado";
        })

    }

    $scope.seleccionarEncuesta = function (item) {
        $rootScope.mostrarEncuestasDetalle = true;
        $rootScope.mostrarEncuestas = false;
        $scope.encuesta = item;
       
        $http.get("/api/pregunta/" + item.Id).then(function (response) {
            $scope.preguntas = response.data;
        })

        $http.get("/api/respuesta/" + item.Id).then(function (response) {
            $scope.respuestas = response.data;
        })
    }

    $scope.cancelarEncuesta = function () {
        $rootScope.mostrarEncuestasDetalle = false;
        $rootScope.mostrarEncuestas = true;
        $scope.preguntas = [];
        $scope.respuestas = [];
    }

    $scope.seleccionarRepuesta = function (item) {
        // si ya se contestó esa pregunta con otra respuesta
        // quito de la coleccion la respuesta anterior
        var a;
        $scope.misRespuestas.forEach(function (e) {
            if (e.Pregunta.Id == item.Pregunta.Id) {
                a = $scope.misRespuestas.indexOf(e);
                $scope.misRespuestas.splice(a, 1);
            }
        });
        $scope.misRespuestas.push(item);
    }

    $scope.enviarEncuesta = function () {
        var a;
        //Actualizar clienteRespuesta
        $scope.misRespuestas.forEach(function (e) {
            $http.put("/api/clienterespuesta/" + e.Id).then(function (response) {
                //
            })
        });

        //Volver a actualizar encuestas pendientes del cliente
        a = $scope.misEncuestas.indexOf($scope.encuesta);
        $scope.misEncuestas.splice(a, 1);
        $scope.encuesta = {};

        //Volver a la parte de encuestas
        $rootScope.mostrarEncuestasDetalle = false;
        $rootScope.mostrarEncuestas = true;

    }

    $scope.seleccionarlibro = function (producto) {
        $scope.libroSeleccionado = producto;
        


        if ($rootScope.isLogged) {
            $scope.PuedeComentar = true;
            $scope.PuedePuntuar = true;
        }

        $http.post("/api/comentario/" + $scope.libroSeleccionado.Id).then(function (response) {
            $scope.comentarios = response.data;
        })

        $http.post("/api/puntuacion/" + $scope.libroSeleccionado.Id).then(function (response) {
            $scope.estaPuntuacion = response.data;
            if ($scope.estaPuntuacion != "") {
                $scope.media = $scope.promedio();
                $scope.estaPuntuacion.forEach(function (e) {
                    if (e.Usuario.Id == $rootScope.user.Id)
                        $scope.PuedePuntuar = false;
                });

            }
        })
       

    }
    $scope.verComparacion = false;
    $scope.comparar = function ()
    {
        $scope.verComparacion = true;
    }
    
    $scope.comentar = function () {
        var comentario = {};
        comentario.Libro = $scope.libroSeleccionado;
        comentario.Comentario = $scope.MiComentario;

        $http.put("/api/comentario", comentario).then(function (response) {
            //$scope.PuedeComentar = false;
            $scope.comentarios.push(response.data);
            $scope.MiComentario = undefined;
        })

    }
    $scope.media = 0;
    $scope.promedio = function () {

        var c = 0;
        var count = 0;
        if ($scope.estaPuntuacion != "") {
       
            $scope.estaPuntuacion.forEach(function (e) {
                count += 1;
                c += e.Puntuacion;
            })
       
            return (c / count);

        }
    }

    $scope.votar = function (i) {
        var puntuacion = {};
        puntuacion.Puntuacion = i;
        puntuacion.Libro = $scope.libroSeleccionado;

        if ($scope.PuedePuntuar) {
            $http.put("/api/puntuacion", puntuacion).then(function (response) {
                $scope.PuedePuntuar = false;
                $scope.estaPuntuacion.push(response.data);
                //$scope.estaPuntuacion = response.data;
                $scope.media = $scope.promedio();
            })
        }
        
    }

    $scope.rating = 5;
    $scope.rateFunction = function (rating) {
        alert('Rating selected - ' + rating);

    };




    $scope.favoritos = [];
    $scope.carrito = [];
    $scope.verChat = false;
    $scope.toggleChat = function ()
    {
        $scope.verChat = true;
    }

    $scope.toggleNovedad = function () {
        $rootScope.mostrarCarrito = false;
        $rootScope.registro = false;
        $rootScope.mostrarCatalogo = false;
        $rootScope.recuperar = false;
        $rootScope.mostrarLogin = false;
        $rootScope.mostrarFavoritos = false;
        $rootScope.mostrarFaq = false;
        $rootScope.mostrarEncuestas = false;
        $rootScope.mostrarEncuestasDetalle = false;
        $rootScope.mostrarInstitucional = false;
        $rootScope.mostrarContacto = false;
        $rootScope.mostrarNovedades = true;
    }

    
    $scope.toggleInstitucional = function () {
        $rootScope.mostrarCarrito = false;
        $rootScope.registro = false;
        $rootScope.mostrarCatalogo = false;
        $rootScope.recuperar = false;
        $rootScope.mostrarLogin = false;
        $rootScope.mostrarFavoritos = false;
        $rootScope.mostrarFaq = false;
        $rootScope.mostrarEncuestas = false;
        $rootScope.mostrarEncuestasDetalle = false;
        $rootScope.mostrarInstitucional = true;
        $rootScope.mostrarContacto = false;
        $rootScope.mostrarNovedades = false;
    }

    $scope.toggleCarrito = function () {
        $rootScope.mostrarCarrito = true;
        $rootScope.registro = false;
        $rootScope.mostrarCatalogo = false;
        $rootScope.recuperar = false;
        $rootScope.mostrarLogin = false;
        $rootScope.mostrarFavoritos = false;
        $rootScope.mostrarFaq = false;
        $rootScope.mostrarEncuestas = false;
        $rootScope.mostrarEncuestasDetalle = false;
        $rootScope.mostrarInstitucional = false;
        $rootScope.mostrarContacto = false;
        $rootScope.mostrarNovedades = false;
    }

    $scope.toggleNC = function () {
        
        $scope.restoTarjeta = 0;
        $scope.restoTarjeta = $scope.total - $scope.totalNC;
        $scope.totalNC = 0;
        $scope.restoTarjeta = 0;
        $scope.sumNC = 0;

        if ($scope.mostrarNC) {
            $scope.mostrarNC = false;
            $scope.deshabilitarNC = false;
            $scope.notasdecredito = [];
            
        }
        else {
            $scope.mostrarNC = true;
            $http.get("/api/NC/").then(function (response) {
                $scope.notasdecredito = response.data;

            })
        }
        

        //$scope.mostrarNC = true;
        //$http.post("/api/NC/").then(function (response) {
        //    $scope.notasdecredito = response.data;

        //})
    }
    $scope.loadProductos = function()
    {
        $http.post("/api/libro").then(function (response) {
            $scope.productos = response.data;
            //$http.get("/api/historico").then(function (resp) {
            //    $scope.historico = resp.data;
            //});
        });
    }

    $scope.loadFavoritos = function () {
        $http.get("/api/libro").then(function (resp) {
            $scope.historico = resp.data;
        });
    }

   
    $scope.loadFavoritos();
    

    
        
 

    $scope.agregarLeido = function (producto) {

        var clienteId = {}
        clienteId.ClienteId = $rootScope.user.Id;
        var esta = false;
        producto.Leidos.forEach(function (e) {
            if (e.ClienteId == clienteId.ClienteId)
                esta = true;
        });

        if (!esta) {
            producto.Leidos.push(clienteId);
            $http.put("/api/libro/", producto).then(function (resp) {

                $scope.historico.Leidos.push(resp.data);
                productos.forEach(function (e) {
                    if (e.Id == resp.data.Id)
                        e = resp;

                });
            });
        }
      


    }
    $scope.agregarFavorito = function (producto) {

        var clienteId = {}
        clienteId.ClienteId = $rootScope.user.Id;
        var esta = false;
        producto.Favoritos.forEach(function (e) {
            if (e.ClienteId == clienteId.ClienteId)
                esta = true;
        });

        if (!esta) {
            producto.Favoritos.push(clienteId);
            $http.put("/api/libro/", producto).then(function (resp) {
                $scope.historico.Favoritos.push(resp.data);
                productos.forEach(function (e) {
                    if (e.Id == resp.data.Id)
                        e = resp;

                });
            });
        }


  
    }

    $scope.agregarCarrito = function (producto) {
        //producto.cantidad = 1;
        var exist = false;
        if (producto.Disponibilidad > 0) {
            producto.Disponibilidad -= 1;
        $scope.carrito.forEach(function (e) {
            if (e.Id == producto.Id) {
                e.cantidad = e.cantidad + 1;
                exist = true;
            }
        });

        if (!exist) {
            producto.cantidad = 1;
            $scope.carrito.push(producto);
        }
        }
        else { }

        if ($scope.mostrarNC) {
            $scope.mostrarNC = false;
            $scope.notasdecredito = [];
            $scope.totalNC = 0;
            $scope.restoTarjeta = $scope.total;
            $scope.deshabilitarNC = false;
        }

        $scope.totalizar();
    }

    $scope.editNC = function (item) {

        $scope.sumNC = $scope.sumNC + item.MontoADebitar;
        $scope.totalNC = $scope.totalNC + item.MontoADebitar;
        $scope.restoTarjeta = $scope.total - $scope.totalNC;

        if ($scope.total == $scope.totalNC) {
            //actualizar NC con valor 0 y descripcion factura
            $scope.deshabilitarNC = true;
        }
        else if ($scope.total < $scope.totalNC) { //500 < 100(nc)
            //actualizar NC con valor RESTANTE y descripcion factura
            $scope.totalNC = $scope.total; // total NC = 100
            $scope.deshabilitarNC = true;
            $scope.restoTarjeta = 0;

        }
        else if ($scope.total > $scope.totalNC) { // 250 > 100(nc)
            //actualizar NC con valor 0 y descripcion factura
            $scope.restoTarjeta = $scope.total - $scope.totalNC ;//resto = 250 - 100
            //$scope.deshabilitarNC = true;
        }      
     


    };

    $scope.comprar = function () {
        var pedido = {};
        var montoObservacion = "";

        pedido.cliente = $rootScope.user;
        pedido.direccion = $scope.calle + ' ' + $scope.numero;

        $scope.calle = "";
        $scope.numero = "";
        pedido.posiciones = $scope.carrito;
        
        pedido.total = $scope.total;

        var AuxCompra = 0;
        var AuxNC = 0;
        AuxCompra = $scope.total;
        AuxNC = $scope.sumNC;
        var resto = 0;
        var flag = "";

        var misnotasdecredito = [];

        //$http.put("/api/mispedidos", pedido).then(function (response) {

        if ($scope.mostrarNC == true) {
          
            //le saco el filter y hago un foreach 
            $scope.notasdecredito.forEach(
              function (item) {

                  if (item.selected) {

                      if (AuxCompra >= AuxNC) {
                          //
                          AuxCompra = AuxCompra - item.MontoADebitar;
                          AuxNC = AuxNC - item.MontoADebitar;
                          item.MontoADebitar = 0;
                          pedido.total = AuxCompra - item.MontoADebitar;
                      }
                      else if (AuxCompra < AuxNC) { // 50 < 100

                          if (AuxNC > item.MontoADebitar) { //300 (tot nc) > 200 (nc)
                              AuxNC = AuxNC - item.MontoADebitar;
                              AuxCompra = AuxCompra - item.MontoADebitar;
                              item.MontoADebitar = 0;
                          } else {
                              //resto de la tarjeta 
                              resto = item.MontoADebitar;
                              item.MontoADebitar = item.MontoADebitar - AuxCompra;
                              if (resto == item.MontoADebitar) {
                                  flag = "x";
                              }
                              AuxNC = 0;
                              AuxCompra = 0;
                              pedido.total = 0;
                          }

                      }


                      //item.Observacion = item.Observacion + "Se compensó $" + resto + ". ";
                      if (flag != "x") {
                        misnotasdecredito.push(item);
                      }
                      

                  }
              }
            );

           
            $http.post("/api/NC/", misnotasdecredito).then(function (response) {
                $scope.mostrarNC = false;

                var ncs = "";
               
                // Notas de crédito
                response.data.forEach(function (e) {

                    montoObservacion = e.Total - e.MontoADebitar;

                    if (ncs == undefined) {
                        ncs = "$" + montoObservacion + " Se compensó con NC: " + e.Numero + ". ";
                    } else {
                        ncs = ncs + "$" + montoObservacion + " Se compensó con NC: " + e.Numero + ". ";
                    }
                });

                // actualizo precio del pedido y observacion
                pedido.ObservacionContable = ncs;
                $http.put("/api/mispedidos/", pedido).then(function (response) {


                })
            })

        } 

        if ($scope.mostrarNC == false) {
            // actualizo precio del pedido
            $http.put("/api/mispedidos/", pedido).then(function (response) {


            })
        }




        $scope.sumNC = 0;


    //}
       
    
    try {
   
        $scope.carrito = JSON.parse($cookies.carrito);
        $scope.deshabilitarNC = false;

        $scope.total = 0;
        $scope.totalNC = 0;
        $scope.restoTarjeta = 0;
        $scope.carrito.forEach(function (e) {
            $scope.total = $scope.total + (e.cantidad * e.Precio);

        });

        $scope.quitarCarrito();
       
    } catch (e) {
        $cookies.carrito = "[]";
        $scope.carrito = [];

    }
   
    }

    $scope.quitarCarrito = function (producto) {
        var exist = false;
       
       
            $scope.carrito.forEach(function (e) {
                if (e.Id == producto.Id) 
                {
                    producto.Disponibilidad += 1;
                    if (e.cantidad > 1)
                    {
                        e.cantidad = e.cantidad - 1;
                 
                    }
                    else{
                        var idx = $scope.carrito.indexOf(e)
                        $scope.carrito.splice(idx,1);
                    }
               
                 

                }
            });

           
            if ($scope.mostrarNC) {
                $scope.mostrarNC = false;
                $scope.notasdecredito = [];
                $scope.totalNC = 0;
                $scope.restoTarjeta = $scope.total;
                $scope.deshabilitarNC = false;

            }

        $scope.totalizar();
    }

    //$scope.total = 0;
    $scope.totalizar = function ()

    { $cookies.carrito = [];
        $scope.total = 0;
        $scope.carrito.forEach(function (e) {
            $scope.total = $scope.total+ (e.cantidad * e.Precio);
            
        });
       
        $cookies.carrito = JSON.stringify($scope.carrito);
       
    }



    $scope.quitarFavorito = function (producto) {
        var exist = false;
       
          
            $scope.favoritos.forEach(function (e) {
                if (e.Id == producto.Id) {
                 
                    exist = true;
                }
            });

            if (exist) {
                producto.favorito = false;
                var index = $scope.favoritos.indexOf(producto);
                $scope.favoritos.splice(index, 1);

            }
            $scope.totalizar();
    }

    $scope.loadProductos();
    $scope.toggleCatalogo = function () {
        $rootScope.mostrarCarrito = false;
        $rootScope.registro = false;
        $rootScope.mostrarCatalogo = true;
        $rootScope.recuperar = false;
        $rootScope.mostrarLogin = false;
        $rootScope.mostrarFavoritos = false;
        $rootScope.mostrarFaq = false;
        $rootScope.mostrarEncuestas = false;
        $rootScope.mostrarEncuestasDetalle = false;
        $rootScope.mostrarInstitucional = false;
        $rootScope.mostrarContacto = false;
        $rootScope.mostrarNovedades = false;
        $scope.mostrarNC = false;
        $scope.totalNC = 0;
        $scope.restoTarjeta = 0;
    }

    $scope.Registrarse = "Registrarse";
    $scope.OlvidePassword = "Olvidaste el password?";
    $scope.Ingresar = "Ingresar";
    $scope.RecordarPassword = "Recordar Password?";
    $scope.Login = "Login";
    $scope.OlvideContrasenia = "Olvide mi Contraseña";



    //que se ve en la pantalla de inicio al principio?
    $rootScope.isInfo = false;
    $rootScope.registro = false;
    $rootScope.recuperar = false;
    $rootScope.mostrarCatalogo = true;
    $rootScope.mostrarLogin = false;
    $rootScope.mostrarCarrito = false;
    $rootScope.mostrarFavoritos = false;
    $rootScope.mostrarFaq = false;

    $scope.lockLogin = false;


    $scope.NombreUsuario = "jess";
    $scope.PasswordNueva = "luna";

    $scope.registrarse = function()
    {
        $rootScope.registro = true;
        $rootScope.mostrarLogin = false;
        //$scope.errorRegistro = false;
        //$scope.mensajeErrorRegistro = "Complete los campos obligatorios";
    }

    $scope.toggleFavoritos = function () {
        $rootScope.isInfo = false;
        $rootScope.registro = false;
        $rootScope.recuperar = false;
        $rootScope.mostrarCatalogo = false;
        $rootScope.mostrarCarrito = false;
        $rootScope.mostrarLogin = false;
        $rootScope.mostrarFavoritos = true;
        $rootScope.mostrarFaq = false;
        $rootScope.mostrarEncuestas = false;
        $rootScope.mostrarEncuestasDetalle = false;
        $rootScope.mostrarInstitucional = false;
        $rootScope.mostrarContacto = false;
        $rootScope.mostrarNovedades = false;
    }

    $scope.toggleFaq = function () {
        $rootScope.isInfo = false;
        $rootScope.registro = false;
        $rootScope.recuperar = false;
        $rootScope.mostrarCatalogo = false;
        $rootScope.mostrarCarrito = false;
        $rootScope.mostrarLogin = false;
        $rootScope.mostrarFavoritos = false;
        $rootScope.mostrarFaq = true;
        $rootScope.mostrarEncuestas = false;
        $rootScope.mostrarEncuestasDetalle = false;
        $rootScope.mostrarInstitucional = false;
        $rootScope.mostrarContacto = false;
        $rootScope.mostrarNovedades = false;
    }

    $scope.toggleContacto = function () {
        $rootScope.isInfo = false;
        $rootScope.registro = false;
        $rootScope.recuperar = false;
        $rootScope.mostrarCatalogo = false;
        $rootScope.mostrarCarrito = false;
        $rootScope.mostrarLogin = false;
        $rootScope.mostrarFavoritos = false;
        $rootScope.mostrarFaq = false;
        $rootScope.mostrarEncuestas = false;
        $rootScope.mostrarEncuestasDetalle = false;
        $rootScope.mostrarInstitucional = false;
        $rootScope.mostrarContacto = true;
        $rootScope.mostrarNovedades = false;
    }

    $scope.toggleEncuestas = function () {
        $rootScope.isInfo = false;
        $rootScope.registro = false;
        $rootScope.recuperar = false;
        $rootScope.mostrarCatalogo = false;
        $rootScope.mostrarCarrito = false;
        $rootScope.mostrarLogin = false;
        $rootScope.mostrarFavoritos = false;
        $rootScope.mostrarFaq = false;
        $rootScope.mostrarEncuestas = true;
        $rootScope.mostrarEncuestasDetalle = false;
        $rootScope.mostrarInstitucional = false;
        $rootScope.mostrarContacto = false;
        $rootScope.mostrarNovedades = false;
    }

    $scope.toggleLogin = function () {
        $rootScope.isInfo = false;
        $rootScope.registro = false;
        $rootScope.recuperar = false;
        $rootScope.mostrarCatalogo = false;
        $rootScope.mostrarCarrito = false;
        $rootScope.mostrarLogin = true;
        $rootScope.mostrarFavoritos = false;
        $rootScope.mostrarFaq = false;
        $rootScope.mostrarEncuestas = false;
        $rootScope.mostrarEncuestasDetalle = false;
        $rootScope.mostrarInstitucional = false;
        $rootScope.mostrarContacto = false;
        $rootScope.mostrarNovedades = false;
    }
    $scope.recuperarse = function () {
        $rootScope.recuperar = !$rootScope.recuperar;
        
    }

   
    $scope.logout = function () {

        $rootScope.userId = sessionStorage.userId = undefined;//$window.sessionStorage.username;
        $rootScope.token = sessionStorage.token = undefined;
        $rootScope.representanteId = sessionStorage.representanteId = undefined;
        $rootScope.user = undefined;
        $rootScope.isLogged = false;
    }

  
    $scope.cargarUsuario = function()
    {
      
        if (sessionStorage.userId != 'undefined') {
            $http.get("api/usuario/" + sessionStorage.userId).then(function (data, status, headers, config) {
                $rootScope.user = data.data;
                $rootScope.isLogged = true;

               
                representantesService.one(sessionStorage.userId).then(function (resp) {
                    $rootScope.representanteId = resp.data.Id;
                    $rootScope.chat.invoke("UnirseCanalChat", function cb() { }, $rootScope.representanteId);
                    chatService.all($rootScope.representanteId).then(function (resp) {
                        $scope.chatMensajes = resp.data;
                    });
                });

                


            }).catch(function (data, status, headers, config) {
                $rootScope.userId = sessionStorage.userId = undefined;//$window.sessionStorage.username;
                $rootScope.token = sessionStorage.token = undefined;
                $rootScope.user = undefined;

            });
        } else {
            $rootScope.isLogged = false;
            sessionStorage.userId = undefined;
        }
    }
    $scope.cargarUsuario();
    
    $scope.login = function () {
        $scope.lockLogin = true;
        var res = $http.post("/api/auth/", {NombreUsuario: $scope.NombreUsuario, PasswordNueva: $scope.PasswordNueva });

        

        res.then(function (response) {

            //$scope.traerEncuestas();

            console.log(response.data);
            $scope.error_message = "";
            $rootScope.token = sessionStorage.token = response.data.Token;
            $rootScope.userId = sessionStorage.userId = response.data.UsuarioId;
            $rootScope.isLogged = true;
            $rootScope.isError = false;

           

            representantesService.one($rootScope.userId).then(function (resp) {
                $rootScope.representanteId =  resp.data.Id;
                $rootScope.chat.invoke("UnirseCanalChat", function cb() { }, $rootScope.representanteId);
            });

            
            $window.location.href = '/app';
         
            
        })
        
         .catch(function (resp, status) {
             $rootScope.isError = true;
            delete sessionStorage.Token;
            delete sessionStorage.UsuarioId;

            switch (resp.status) {
                case 401:
                    $scope.error_message = "usuario o contraseña incorrecto!";
                    break;
                case 404:
                    $scope.error_message = "El usuario no está activo";
                    break;
                default:
                    $scope.error_message = "error del sistema";
            }

            //if (resp.status == 401)
            // $scope.error_message = "usuario o contraseña incorrecto!"; 
            //else
            // $scope.error_message = "error del sistema";
        
            $scope.lockLogin = false;
        });
        /*

         */

    };

    


    $scope.chatMensajes = [];
    $rootScope.chat = backendHubProxy("ChatHub");
    $rootScope.chat.on("HayUnNuevoMensaje", function (message, usuario,fecha) {
        var msg = {};
        msg.Mensaje = message;
        msg.NombreUsuario = usuario;
        msg.FechaMensaje = fecha;
        $scope.chatMensajes.push(msg);
        var pre = jQuery("#output");
        pre.scrollTop(pre.prop("scrollHeight"));
    });


    $scope.enviarMensaje = function (mensaje)
    {
        var msg = {};
        msg.Mensaje = mensaje;
        msg.NombreUsuario = $rootScope.user.NombreUsuario;
        msg.IdRepresentante = $rootScope.representanteId;
        chatService.put(msg);
       // $rootScope.chat.invoke("EnviarMensaje", function cb() { }, mensaje, $rootScope.user.NombreUsuario, $rootScope.representanteId);
    }


   // $rootScope.chat.invoke("UnirseCanalChat", function cb() { }, "chatChannel");

});

app.controller('recuperoController', function ($scope, $http, $rootScope, $window) {

    $scope.usuario = {};
    $scope.Recuperar = "Recuperar";

    $scope.EnviarMail = function ()
    {
        var res = $http.put("/api/auth/",  $scope.usuario );
        res.then(function (response) {

            console.log(response.data);
            $rootScope.recuperar = false;
            $scope.error_message = "";
            $rootScope.isInfo = true;
            $rootScope.info_message = "Se le ha enviado un mail con su nueva contraseña";
            $rootScope.isError = false;

        })

         .catch(function (resp, status) {
             $rootScope.isError = true;
             delete sessionStorage.Token;
             delete sessionStorage.UsuarioId;
             
             if (resp.status == 404)
                 $rootScope.error_message = "Mail incorrecto";
             else
                 $rootScope.error_message = "error del sistema";

             
         });

    }
    
    $scope.esconderRecupero = function ()
    {
        $rootScope.recuperar = false;
    }
});