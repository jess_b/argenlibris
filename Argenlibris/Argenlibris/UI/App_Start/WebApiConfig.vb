﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web.Http
Imports BLL

Imports Argenlibris.NHibernate

Public Module WebApiConfig
    Public Sub Register(ByVal config As HttpConfiguration)
        ' Web API configuration and services

        ' Web API routes
        config.MapHttpAttributeRoutes()

        config.Routes.MapHttpRoute(
            name:="DefaultApi",
            routeTemplate:="api/{controller}/{id}",
            defaults:=New With {.id = RouteParameter.Optional}
        )

        config.Routes.MapHttpRoute("WithExtension", "api/{controller}/{action}/{id}", _
                New With {.id = UrlParameter.Optional})



        '        config.Routes.MapHttpRoute( _
        'name:="DefaultApi", _
        'routeTemplate:="api/{controller}/{action}/{id}", _
        'defaults:=New With {.id = RouteParameter.Optional} _
        ')


        config.Filters.Add(New AuthorizeAttribute())
        config.Filters.Add(New RolesValidosAttribute())
        config.Filters.Add(New ExcepcionDeNegocioFilter())
        config.MessageHandlers.Add(New TokenAuthenticationHandler())
        'config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = New CustomContractResolver()
        config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(New Newtonsoft.Json.Converters.StringEnumConverter())
        config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = New NHibernateContractResolver()


    End Sub
End Module
