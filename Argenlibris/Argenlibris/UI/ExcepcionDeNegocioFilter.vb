﻿Imports System.Web.Http.Filters
Imports BLL
Imports System.Net.Http
Imports System.Net.Http.Formatting

Public Class ExcepcionDeNegocioFilter
    Inherits ExceptionFilterAttribute
    Implements System.Web.Mvc.IExceptionFilter


    Public Overrides Sub OnException(actionExecutedContext As HttpActionExecutedContext)

        actionExecutedContext.Response = New HttpResponseMessage()
        If TypeOf actionExecutedContext.Exception Is ExcepcionDeNegocio Then
            actionExecutedContext.Response.StatusCode = 520
            Dim e As ExcepcionDeNegocio
            e = actionExecutedContext.Exception

            Dim t As Type


            actionExecutedContext.Response.Content = New ObjectContent(GetType(IList(Of ErrorNegocio)), e.Errores, New JsonMediaTypeFormatter())

        Else
            actionExecutedContext.Response.StatusCode = 520
            Dim e As Exception
            e = actionExecutedContext.Exception

            Dim t As Type
            actionExecutedContext.Response.Content = New StringContent("eerror")

        End If


        MyBase.OnException(actionExecutedContext)


    End Sub


    Public Sub OnException1(filterContext As ExceptionContext) Implements Mvc.IExceptionFilter.OnException


        filterContext.ExceptionHandled = True
        filterContext.HttpContext.Response.Clear()
        filterContext.HttpContext.Response.StatusCode = 303
        filterContext.HttpContext.Response.TrySkipIisCustomErrors = True

    End Sub
End Class
