﻿Imports System.Net.Http
Imports Argenlibris.Seguridad
Imports System.Security.Principal
Imports System.Threading
Imports System.Net
Imports BLL

Public Class TokenAuthenticationHandler
    Inherits DelegatingHandler

  
    Protected Overrides Function SendAsync(request As HttpRequestMessage, cancellationToken As Threading.CancellationToken) As Threading.Tasks.Task(Of HttpResponseMessage)
        Const token_header As String = "X-Token"

        If (request.Headers.Contains(token_header)) Then
            Dim strToken As String
            strToken = request.Headers().GetValues("X-Token").First

            Dim token As TokenSeguridad
            Try
                token = ManejadorToken.Autorizar(strToken)

                Dim principal As IPrincipal

                principal = New ArgenlibrisPrincipal(token.Id)

                Thread.CurrentPrincipal = principal
                HttpContext.Current.User = principal


            Catch ex As Exception

                Dim responseMessage As HttpResponseMessage = request.CreateResponse(Of String)(HttpStatusCode.Forbidden, "Token not valid")

                Return Threading.Tasks.Task(Of HttpResponseMessage).Factory.StartNew(Function() responseMessage)
            End Try
        End If
        'Thread.CurrentThread.CurrentCulture = new CultureInfo("es-AR");
        'Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
        Return MyBase.SendAsync(request, cancellationToken)
    End Function

End Class
