﻿Imports Microsoft.AspNet.SignalR

Public Class ChatHub
    Inherits Hub


    Public Sub UnirseCanalChat(roomName As String, a As String, b As String)

        Groups.Add(Context.ConnectionId, roomName)
        '   Clients.Group(roomName).HayUnNuevoMensaje(Context.User.Identity.Name + " se conecto.")

    End Sub
    Public Sub EnviarMensaje(mensaje As String, usuario As String, idRepresentacion As String)
        If Not idRepresentacion Is Nothing Then
            Clients.Group(idRepresentacion).HayUnNuevoMensaje(mensaje, usuario, DateTime.Now.ToString)
        End If
    End Sub


End Class
