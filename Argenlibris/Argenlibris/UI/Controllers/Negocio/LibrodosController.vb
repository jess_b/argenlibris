﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class LibrodosController
        Inherits ApiController

        Dim bllLibroNewsletter As ILibroNewsletterBLL
        Dim bll As IBLL(Of Libro)
        Dim bllEditorial As IEditorialBLL
        Dim bllAutor As IAutorBLL
        Dim bllCategoria As ICategoriaBLL
        Dim bllCategoriaCliente As ICategoriaClienteBLL
        Dim bllEmail As EmailBLL

        Public Sub New(bllLibro As IBLL(Of Libro), blleditorial_ As IEditorialBLL, bllautor_ As IAutorBLL, bllCategoria_ As ICategoriaBLL, bllLibroNewsletter_ As ILibroNewsletterBLL, bllCategoriaCliente_ As ICategoriaClienteBLL, bllEmail_ As EmailBLL)
            bll = bllLibro
            bllEditorial = blleditorial_
            bllAutor = bllautor_
            bllCategoria = bllCategoria_
            bllLibroNewsletter = bllLibroNewsletter_
            bllCategoriaCliente = bllCategoriaCliente_
            bllEmail = bllEmail_
        End Sub



        <HttpPost>
        <RolesValidos(EnumPermiso.ProductosPuedeGestionar)>
        Public Function ConsultarTodos() As IHttpActionResult
            Return Ok(AutoMapper.Mapper.Map(Of IList(Of Libro))(bll.GetAll().ToList))
        End Function



    End Class

End Namespace