﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class NovedadController
        Inherits ApiController

        Dim bll As IBLL(Of Novedad)
        'Dim bllCategoria As IBLL(Of Categoria)
        Dim bllCatUsuario As IBLL(Of CategoriaCliente)
        Dim bllVisitante As CategoriaVisitanteBLL
        Dim bllEmail As EmailBLL

        Public Sub New(bllNovedad As IBLL(Of Novedad), bllCatUsuario_ As IBLL(Of CategoriaCliente), bllVisitante_ As CategoriaVisitanteBLL, bllEmail_ As EmailBLL)
            bll = bllNovedad
            bllCatUsuario = bllCatUsuario_
            bllVisitante = bllVisitante_
            bllEmail = bllEmail_
        End Sub


        <HttpGet>
        <RolesValidos(EnumPermiso.ProductosPuedeGestionar)>
        Public Function Consultar() As IHttpActionResult
            'Return Ok(bll.GetAll().ToList)
            Return Ok(AutoMapper.Mapper.Map(Of IList(Of Novedad))(bll.GetAll().ToList))
        End Function

        <AllowAnonymous>
        <HttpPost>
        Public Function ConsultarTodos() As IHttpActionResult
            Dim _id As Guid
            Try
                _id = Guid.Parse(HttpContext.Current.User.Identity.Name)
            Catch ex As Exception

            End Try

            If _id.Equals(Guid.Empty) Then
                ' traigo todas las novedades
                'Return Ok(bll.GetAll().ToList)
                Return Ok(AutoMapper.Mapper.Map(Of IList(Of Novedad))(bll.GetAll().ToList))
            Else

                Dim _catUsu As List(Of CategoriaCliente)
                _catUsu = bllCatUsuario.GetAll().Where(Function(c) c.Cliente.Id.Equals(_id)).ToList()

                Dim _aux As New List(Of Novedad)
                _aux = bll.GetAll().ToList()

                Dim _nov As New List(Of Novedad)
                For Each nov_ As Novedad In _aux
                    For Each cat_ As CategoriaCliente In _catUsu
                        If nov_.Categoria.Id = cat_.Categoria.Id Then
                            _nov.Add(nov_)
                        End If
                    Next
                Next

                'Return Ok(_nov)
                Return Ok(AutoMapper.Mapper.Map(Of IList(Of Novedad))(_nov))


            End If

        End Function

        <HttpPut()>
        Public Function guardar(novedad_ As Novedad) As IHttpActionResult

            'If validar(novedad_) Then

            bll.SaveOrUpdate(novedad_)
            Return Ok(novedad_)

            'Else

            'Return BadRequest()

            'End If

        End Function

        'Private Function validar(dto As Novedad) As Boolean

        '    If dto.Id.Equals(Guid.Empty) Then

        '        Dim _lista As New List(Of Novedad)

        '        _lista = bll.GetAll().ToList()

        '        For Each item As Novedad In _lista
        '            If item.Titulo = dto.Titulo Then
        '                Return False
        '            End If
        '        Next

        '    End If

        '    Return True

        'End Function


        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            bll.Delete(id)
            Return Ok()

        End Function

        <HttpHead>
        Public Sub Enviar(id As Guid)

            'Dim _ruta As String
            'Dim _rutaImagen As String
            Dim _novedad As New Novedad
            Dim _libroNewsletter As New List(Of LibroNewsletter)
            Dim _visitantes As New List(Of CategoriaVisitante)

            _novedad = bll.Obtain(id)
            _novedad.Enviado = True
            bll.SaveOrUpdate(_novedad)




            '_visitantes = bllVisitante.GetAll().Where(Function(v) v.Categoria.Id = _novedad.Categoria.Id).ToList

            ' ''_ruta = HttpContext.Current.Server.MapPath("/Newsletter.html")
            ' ''_rutaImagen = Request.RequestUri.Authority & "/images" 'HttpContext.Current.Request.Url.ToString("/images")

            'bllEmail.EnviarMailNovedades(_visitantes, _novedad)

        End Sub

    End Class

End Namespace