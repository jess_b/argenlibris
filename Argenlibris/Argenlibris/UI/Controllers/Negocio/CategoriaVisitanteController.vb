﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class CategoriaVisitanteController
        Inherits ApiController

        Dim bll As ICategoriaVisitanteBLL
        Dim bllCategoria As ICategoriaBLL
        Dim bllMail As IEmailBLL

        Public Sub New(bllcategoriavisitante_ As IBLL(Of CategoriaVisitante), bllCategoria_ As CategoriaBLL, bllMail_ As EmailBLL)
            bll = bllcategoriavisitante_
            bllCategoria = bllCategoria_
            bllMail = bllMail_
        End Sub

        '<HttpGet>
        'Public Function ConsultarPorCliente(id As Guid) As IHttpActionResult

        '    Return Ok(AutoMapper.Mapper.Map(Of IList(Of CategoriaCliente))(bll.GetAll().Where(Function(c) c.Cliente.Id.Equals(id)).ToList))

        'End Function

        <HttpPost>
        Public Function ConsultarTodos() As IHttpActionResult

            Return Ok(AutoMapper.Mapper.Map(Of IList(Of CategoriaCliente))(bll.GetAll().ToList))

        End Function

        <HttpPut()>
        <AllowAnonymous>
        Public Function guardar(categoriaVisitante_ As CategoriaVisitanteDTO) As IHttpActionResult


            Dim _categoriaVisitante As New CategoriaVisitante
            Try
                _categoriaVisitante = bll.GetAll().Where(Function(m) m.Mail = categoriaVisitante_.Mail).FirstOrDefault
            Catch ex As Exception

            End Try


            If _categoriaVisitante Is Nothing Then

                'bllMail.EnviarMailDeRegistroNovedad(categoriaVisitante_.Mail)

                Dim _cv As CategoriaVisitante

                For Each _categoria As Categoria In categoriaVisitante_.Categorias

                    _cv = New CategoriaVisitante
                    _cv.Categoria = _categoria
                    _cv.Mail = categoriaVisitante_.Mail

                    bll.SaveOrUpdate(_cv)
                Next

                Return Ok()
            Else
                Return BadRequest()
            End If


        End Function

        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            bll.Delete(id)
            Return Ok(id)

        End Function

    End Class
End Namespace