﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Public Class DocumentosController
    Inherits ApiController

    Dim bllFactura As IDocumentoBLL


    Public Sub New(bllFactura_ As IDocumentoBLL)
        bllFactura = bllFactura_

    End Sub

    ' GET api/<controller>
    <HttpHead>
    Public Function TraerTotalesPorFecha(dto As TotalDocumentosDTO) As IHttpActionResult

        Dim _documentos As New List(Of DocumentoCabecera)
        Dim _totales As New Integer
        Dim _fechaDesde As New Nullable(Of DateTime)
        Dim _fechaHasta As New Nullable(Of DateTime)

        _fechaDesde = dto.FechaDesde
        _fechaHasta = dto.FechaHasta

        If _fechaHasta.HasValue Then
            _documentos = bllFactura.GetAll().Where(Function(d) d.Fecha.Date <= dto.FechaHasta).ToList()
        Else
            _documentos = bllFactura.GetAll().ToList()
        End If

        'totalizar
        For Each _documento As DocumentoCabecera In _documentos

            If _documento.TipoDocumento = EnumTipoDocumento.Factura Then
                _totales = _totales + _documento.Total
            ElseIf _documento.TipoDocumento = EnumTipoDocumento.NotaDeCredito Then
                _totales = _totales - _documento.Total
            End If

        Next

        Return Ok(_totales)


    End Function

    '' GET api/<controller>/5
    'Public Function GetValue(ByVal id As Integer) As String
    '    Return "value"
    'End Function

    ' POST api/<controller>
    Public Function PostValue() As IHttpActionResult

        Dim _ctacteDTO As New CuentaCorrienteDTO
        Dim _documentos As New List(Of DocumentoCabecera)
        Dim _id As Guid = Guid.Parse(HttpContext.Current.User.Identity.Name)
        Dim _total As Double

        _documentos = AutoMapper.Mapper.Map(Of IList(Of DocumentoCabecera))(bllFactura.GetAll().Where(Function(o) o.Cliente.Id.Equals(_id)).ToList())

        For Each _documento As DocumentoCabecera In _documentos
            If _documento.TipoDocumento = EnumTipoDocumento.NotaDeCredito Then
                _documento.Total = _documento.Total * -1
            End If
            _total = _total + _documento.Total
        Next
        _ctacteDTO.Documentos = _documentos
        _ctacteDTO.Total = _total
        Return Ok(_ctacteDTO)

    End Function



    ' PUT api/<controller>/5
    Public Sub PutValue(ByVal id As Integer, <FromBody()> ByVal value As String)

    End Sub

    ' DELETE api/<controller>/5
    Public Sub DeleteValue(ByVal id As Integer)

    End Sub
End Class
