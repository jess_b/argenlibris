﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Public Class RemitoController
    Inherits ApiController

    Dim bll As IRemitoBLL

    Public Sub New(bllRemito_ As IRemitoBLL)
        Me.bll = bllRemito_
    End Sub

    ' GET api/<controller>
    Public Function GetValues() As IEnumerable(Of String)
        Return New String() {"value1", "value2"}
    End Function

    ' GET api/<controller>/5
    Public Function GetValue(ByVal id As Integer) As String
        Return "value"
    End Function

    ' POST api/<controller>
    <RolesValidos(EnumPermiso.AlmacenesPuedeGestionar)>
    Public Function PostValue() As IHttpActionResult

        Return Ok(AutoMapper.Mapper.Map(Of IList(Of RemitoCabecera))(bll.GetAll().ToList))

    End Function

    ' PUT api/<controller>/5
    Public Sub PutValue(ByVal id As Integer, <FromBody()> ByVal value As String)

    End Sub

    ' DELETE api/<controller>/5
    Public Sub DeleteValue(ByVal id As Integer)

    End Sub
End Class
