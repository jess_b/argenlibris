﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Public Class facturaController
    Inherits ApiController

    Dim bll As IDocumentoBLL

    Public Sub New(bllFactura_ As IDocumentoBLL)
        Me.bll = bllFactura_
    End Sub

    '' GET api/<controller>
    'Public Function GetValues() As IEnumerable(Of String)
    '    Return New String() {"value1", "value2"}
    'End Function
    Public Function GetValues() As IHttpActionResult
        Return Ok(AutoMapper.Mapper.Map(Of IList(Of DocumentoCabecera))(bll.GetAll().Where(Function(p) p.TipoDocumento.ToString.Contains(EnumTipoDocumento.Factura.ToString)).ToList()))
    End Function

    ' GET api/<controller>/5
    Public Function GetValue(ByVal id As Integer) As String
        Return "value"
    End Function

    ' POST api/<controller>
    <AllowAnonymous>
    Public Function PostValue() As IHttpActionResult

        Dim _id As Guid = Guid.Parse(HttpContext.Current.User.Identity.Name)

        Return Ok(AutoMapper.Mapper.Map(Of IList(Of DocumentoCabecera))(bll.GetAll().Where(Function(o) o.Cliente.Id.Equals(_id)).Where(Function(p) p.TipoDocumento.ToString.Contains(EnumTipoDocumento.Factura.ToString)).ToList()))

    End Function
    ' PUT api/<controller>/5
    Public Sub PutValue(ByVal id As Integer, <FromBody()> ByVal value As String)

    End Sub

    ' DELETE api/<controller>/5
    Public Sub DeleteValue(ByVal id As Integer)

    End Sub
End Class
