﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class PedidoController
        Inherits ApiController

        Dim bll As IPedidoBLL
        Dim bllFactura As IDocumentoBLL
        Dim bllRemito As IRemitoBLL
        Dim bllEmail As IEmailBLL

        Public Sub New(bll As IPedidoBLL, bllFactura_ As IDocumentoBLL, bllRemito_ As IRemitoBLL, bllEmail_ As IEmailBLL)
            Me.bll = bll
            bllFactura = bllFactura_
            bllRemito = bllRemito_
            bllEmail = bllEmail_
        End Sub


   

        ' POST api/<controller>
        <RolesValidos(EnumPermiso.AlmacenesPuedeGestionar)>
        Public Function PostValue() As IHttpActionResult

            Return Ok(AutoMapper.Mapper.Map(Of IList(Of PedidoCabecera))(bll.GetAll().Where(Function(o) o.Despachado = False).ToList))

        End Function

        <HttpGet>
        <RolesValidos(EnumPermiso.VentasPuedeGestionar)>
        Public Function ObtenerReclamados() As IHttpActionResult

            Dim lista As IList = AutoMapper.Mapper.Map(Of IList(Of PedidoCabecera))(bll.GetAll().Where(Function(o) o.Reclamado = True).ToList)


            Return Ok(lista)
        End Function

        ' PUT api/<controller>/5
        <AllowAnonymous>
        Public Sub PutValue(pedido_ As PedidoDTO)

            Dim _pedido As New PedidoCabecera

            If Not pedido_.Id.Equals(Guid.Empty) And pedido_.Despachado Then
                _pedido = bll.Obtain(pedido_.Id)
                _pedido.Despachado = pedido_.Despachado
                bll.SaveOrUpdate(_pedido)

                CrearFactura(_pedido)
                CrearRemito(_pedido)

            End If

        End Sub

        Private Sub CrearFactura(pedido_ As PedidoCabecera)

            Dim _factura As New DocumentoCabecera
            _factura.Cliente = pedido_.Cliente
            _factura.Direccion = pedido_.Direccion
            _factura.Fecha = DateTime.Now
            _factura.LetraDocumento = "A"
            _factura.TipoDocumento = EnumTipoDocumento.Factura
            _factura.Total = pedido_.Total
            _factura.Observacion = pedido_.ObservacionContable

            Dim _facturaPosicion As DocumentoPosicion
            For Each _posicion As PedidoPosicion In pedido_.Posiciones
                _facturaPosicion = New DocumentoPosicion
                _facturaPosicion.Cantidad = _posicion.Cantidad
                _facturaPosicion.Libro = _posicion.Libro
                _facturaPosicion.Precio = _posicion.Precio
                _factura.Posiciones.Add(_facturaPosicion)
            Next

            bllFactura.SaveOrUpdate(_factura)
            'bllFactura.GuardarEnPDF(_factura)
            'bllEmail.EnviarMailDeFactura(_factura.Cliente, _factura)
        End Sub
        Private Sub CrearRemito(pedido_ As PedidoCabecera)

            Dim _remito As New RemitoCabecera
            _remito.Cliente = pedido_.Cliente
            _remito.Direccion = pedido_.Direccion
            _remito.Fecha = DateTime.Now

            Dim _remitoPosicion As RemitoPosicion
            For Each _posicion As PedidoPosicion In pedido_.Posiciones
                _remitoPosicion = New RemitoPosicion
                _remitoPosicion.Cantidad = _posicion.Cantidad
                _remitoPosicion.Libro = _posicion.Libro
                _remito.Posiciones.Add(_remitoPosicion)
            Next

            bllRemito.SaveOrUpdate(_remito)

        End Sub
        ' DELETE api/<controller>/5
        Public Sub DeleteValue(ByVal id As Integer)

        End Sub
    End Class
End Namespace
