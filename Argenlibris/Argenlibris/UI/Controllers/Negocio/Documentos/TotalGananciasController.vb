﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers
    Public Class TotalGananciasController
        Inherits ApiController

        Dim bll As IBLL(Of DocumentoCabecera)
        Dim bllPosicion As DocumentoPosicionBLL
        Dim bllLibro As IBLL(Of Libro)

        Public Sub New(bll_ As IBLL(Of DocumentoCabecera), bllPosicion_ As DocumentoPosicionBLL, bllLibro_ As IBLL(Of Libro))
            bll = bll_
            bllPosicion = bllPosicion_
            bllLibro = bllLibro_
        End Sub

        <HttpPost>
        <RolesValidos(EnumPermiso.VentasPuedeGestionar)>
        Public Function TraerTotalesPorFecha(dto As TotalDocumentosDTO) As IHttpActionResult

            Dim _documentos As New List(Of DocumentoCabecera)
            Dim _totales As New Integer
            Dim _count As New Integer

            _documentos = bll.GetAll().Where(Function(o) o.Fecha.Date >= dto.FechaDesde) _
            .Where(Function(p) p.Fecha.Date <= dto.FechaHasta).ToList() '.OrderByDescending(Function(h) h.Fecha)

            Dim ganancias As New List(Of Ganancia)
          
            For Each d As DocumentoCabecera In _documentos

                Dim g As Ganancia = ganancias.AsQueryable.Where(Function(o) o.Mes.Equals(d.Fecha.Month)).Where(Function(o) o.Año.Equals(d.Fecha.Year)).FirstOrDefault
                If (Not IsNothing(g)) Then
                    _count = getGanancia(d)
                    g.Total = g.Total + _count

                Else
                    g = New Ganancia
                    g.Mes = d.Fecha.Month
                    g.Año = d.Fecha.Year
                    _count = getGanancia(d)
                    g.Total = _count


                    g.Periodo = CInt(g.Mes.ToString + g.Año.ToString)

                    ganancias.Add(g)
                End If

            Next



            Return Ok(ganancias)


        End Function

        Private Function getGanancia(doc_ As DocumentoCabecera) As Integer

            Dim _gcia As Integer

            For Each _pos As DocumentoPosicion In doc_.Posiciones
                _gcia += (_pos.Libro.Precio - _pos.Libro.Costo) * _pos.Cantidad
            Next

            If doc_.TipoDocumento = EnumTipoDocumento.NotaDeCredito Then
                _gcia = _gcia * -1
            End If

            Return _gcia

        End Function

        <HttpPut>
        Public Function Obtener(dto As TotalDocumentosDTO) As IHttpActionResult

            Dim _documentos As New List(Of DocumentoCabecera)
            Dim _totales As New Integer
            Dim _libros As New List(Of Libro)
            Dim _agregar As Boolean = True
            'Dim _posicion As DocumentoPosicion
            Dim _posiciones As New List(Of DocumentoPosicion)

            _documentos = bll.GetAll().Where(Function(o) o.Fecha.Date >= dto.FechaDesde).Where(Function(p) p.Fecha.Date <= dto.FechaHasta).ToList()


            'totalizar
            For Each _documento As DocumentoCabecera In _documentos

                If _documento.TipoDocumento = EnumTipoDocumento.Factura Then
                    '_posicion = New DocumentoPosicion
                    For Each _posicion As DocumentoPosicion In _documento.Posiciones
                        _posiciones.Add(_posicion)
                    Next



                    '_totales = _totales + _documento.Total
                    'ElseIf _documento.TipoDocumento = EnumTipoDocumento.NotaDeCredito Then
                    '    _totales = _totales - _documento.Total
                End If

            Next

            Return Ok(AutoMapper.Mapper.Map(Of List(Of DocumentoPosicion))(_posiciones))
        End Function

        '<RolesValidos(EnumPermiso.BitacoraPuedeGestionar)>

        '<HttpPut>
        '<AllowAnonymous>
        'Public Function consultar(dto As ConsultarBitacoraDTO) As IHttpActionResult

        '    'Return Ok(bll.GetAll().Where(Function(o) o.Fecha.Equals(dto.Fecha)).Where(Function(p) p.TipoDeMensaje.Equals(dto.TipoDeMensaje)).FirstOrDefault())

        '    Return Ok(bll.GetAll.Where(Function(u) u.Fecha.Equals(dto.Fecha)).ToList())


        'End Function
    End Class
End Namespace