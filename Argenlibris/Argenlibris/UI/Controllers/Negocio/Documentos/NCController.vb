﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Public Class NCController
    Inherits ApiController

    Dim bllDocumento As IDocumentoBLL

    Public Sub New(bllDocumento_ As IDocumentoBLL)
        bllDocumento = bllDocumento_
    End Sub

    Public Function GetValue() As IHttpActionResult

        Dim _notaDeCredito As New List(Of DocumentoCabecera)

        Dim _id As Guid = Guid.Parse(HttpContext.Current.User.Identity.Name)

        _notaDeCredito = AutoMapper.Mapper.Map(Of IList(Of DocumentoCabecera))(bllDocumento.GetAll().Where(Function(o) o.Cliente.Id.Equals(_id)).Where(Function(p) p.TipoDocumento.ToString.Contains(EnumTipoDocumento.NotaDeCredito.ToString)).Where(Function(f) f.MontoADebitar > 0).ToList())

        Return Ok(_notaDeCredito)

    End Function

    ' POST api/<controller>


    '<HttpHead>
    'Public Function PostMultipleValue(lista As IList(Of DocumentoCabecera)) As IHttpActionResult
    '    For Each item In lista

    '        AutoMapper.Mapper.Map(Of DocumentoCabecera)(bllDocumento.SaveOrUpdate(item))
    '    Next
    '    Return Ok(lista)
    'End Function

    <HttpPost>
    Public Function PostMultipleValue(lista As IList(Of DocumentoCabecera)) As IHttpActionResult
        For Each item In lista

            AutoMapper.Mapper.Map(Of DocumentoCabecera)(bllDocumento.SaveOrUpdate(item))
        Next
        Return Ok(lista)
    End Function

    'Public Function PostValue(nc_ As DocumentoCabecera) As IHttpActionResult

    '    Return Ok(AutoMapper.Mapper.Map(Of DocumentoCabecera)(bllDocumento.SaveOrUpdate(nc_)))

    '    'Dim _notaDeCredito As New List(Of DocumentoCabecera)

    '    'Dim _id As Guid = Guid.Parse(HttpContext.Current.User.Identity.Name)

    '    '_notaDeCredito = AutoMapper.Mapper.Map(Of IList(Of DocumentoCabecera))(bllDocumento.GetAll().Where(Function(o) o.Cliente.Id.Equals(_id)).Where(Function(p) p.TipoDocumento.ToString.Contains(EnumTipoDocumento.NotaDeCredito.ToString)).ToList())

    '    'Return Ok(_notaDeCredito)

    'End Function

    ' PUT api/<controller>/5
    Public Function PutValue(pedido_ As PedidoCabecera) As IHttpActionResult

        Dim _notaDeCredito As New DocumentoCabecera
        _notaDeCredito.Fecha = DateTime.Now
        _notaDeCredito.Total = pedido_.Total
        _notaDeCredito.MontoADebitar = pedido_.Total
        _notaDeCredito.Cliente = pedido_.Cliente
        _notaDeCredito.Direccion = pedido_.Direccion
        _notaDeCredito.TipoDocumento = EnumTipoDocumento.NotaDeCredito
        _notaDeCredito.Observacion = "Factura que lo originó: " & pedido_.Numero & ". "

        Dim _posicion As DocumentoPosicion
        For Each posicion As PedidoPosicion In pedido_.Posiciones
            _posicion = New DocumentoPosicion
            _posicion.Cantidad = posicion.Cantidad
            _posicion.Libro = posicion.Libro
            _notaDeCredito.Posiciones.Add(_posicion)
        Next

        Return Ok(bllDocumento.SaveOrUpdate(_notaDeCredito))

    End Function

    ' DELETE api/<controller>/5
    Public Sub DeleteValue(ByVal id As Integer)

    End Sub
End Class
