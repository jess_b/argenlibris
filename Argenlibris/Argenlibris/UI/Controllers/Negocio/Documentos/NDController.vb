﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Public Class NDController
    Inherits ApiController

    Dim bllDocumento As IDocumentoBLL

    Public Sub New(bllDocumento_ As IDocumentoBLL)
        bllDocumento = bllDocumento_
    End Sub

    Public Function GetValue() As IHttpActionResult

        Dim _notaDeDebito As New List(Of DocumentoCabecera)

        Dim _id As Guid = Guid.Parse(HttpContext.Current.User.Identity.Name)

        _notaDeDebito = AutoMapper.Mapper.Map(Of IList(Of DocumentoCabecera))(bllDocumento.GetAll().Where(Function(o) o.Cliente.Id.Equals(_id)).Where(Function(p) p.TipoDocumento.ToString.Contains(EnumTipoDocumento.notaDeDebito.ToString)).Where(Function(f) f.MontoADebitar > 0).ToList())

        Return Ok(_notaDeDebito)

    End Function

    ' POST api/<controller>


    '<HttpHead>
    'Public Function PostMultipleValue(lista As IList(Of DocumentoCabecera)) As IHttpActionResult
    '    For Each item In lista

    '        AutoMapper.Mapper.Map(Of DocumentoCabecera)(bllDocumento.SaveOrUpdate(item))
    '    Next
    '    Return Ok(lista)
    'End Function

    <HttpPost>
    Public Function PostMultipleValue(lista As IList(Of DocumentoCabecera)) As IHttpActionResult
        Dim _DTO As NCDTO
        Dim _listaDTO As New List(Of NCDTO)
        Dim _notaDeDebito As New List(Of DocumentoCabecera)

        _notaDeDebito = AutoMapper.Mapper.Map(Of IList(Of DocumentoCabecera))(bllDocumento.GetAll().Where(Function(p) p.TipoDocumento.ToString.Contains(EnumTipoDocumento.NotaDeDebito.ToString)).ToList())


        For Each item In lista

            _DTO = New NCDTO
            _DTO.Id = item.Id
            _DTO.Total = item.Total
            _DTO.Cliente = item.Cliente
            _DTO.Direccion = item.Direccion
            _DTO.Fecha = item.Fecha
            _DTO.Numero = item.Numero
            _DTO.Observacion = item.Observacion
            _DTO.Acreditado = False
            _DTO.Posiciones = item.Posiciones

            For Each _nd As DocumentoCabecera In _notaDeDebito

                If item.Numero = _nd.LetraDocumento Then
                    _DTO.Credito = _nd.Total
                    _DTO.Acreditado = True
                End If

            Next

            _listaDTO.Add(_DTO)

        Next
        Return Ok(_listaDTO)
    End Function

    'Public Function PostValue(nc_ As DocumentoCabecera) As IHttpActionResult

    '    Return Ok(AutoMapper.Mapper.Map(Of DocumentoCabecera)(bllDocumento.SaveOrUpdate(nc_)))

    '    'Dim _notaDeDebito As New List(Of DocumentoCabecera)

    '    'Dim _id As Guid = Guid.Parse(HttpContext.Current.User.Identity.Name)

    '    '_notaDeDebito = AutoMapper.Mapper.Map(Of IList(Of DocumentoCabecera))(bllDocumento.GetAll().Where(Function(o) o.Cliente.Id.Equals(_id)).Where(Function(p) p.TipoDocumento.ToString.Contains(EnumTipoDocumento.notaDeDebito.ToString)).ToList())

    '    'Return Ok(_notaDeDebito)

    'End Function

    ' PUT api/<controller>/5
    Public Function PutValue(nc_ As NCDTO) As IHttpActionResult

        Dim _notaDeDebito As New DocumentoCabecera
        _notaDeDebito.Fecha = DateTime.Now
        _notaDeDebito.Total = nc_.Credito
        _notaDeDebito.Cliente = nc_.Cliente
        _notaDeDebito.Direccion = nc_.Direccion
        _notaDeDebito.LetraDocumento = nc_.Numero
        _notaDeDebito.TipoDocumento = EnumTipoDocumento.NotaDeDebito
        _notaDeDebito.Observacion = "Factura que lo originó: " + nc_.Numero.ToString


        Return Ok(bllDocumento.SaveOrUpdate(_notaDeDebito))

    End Function

    ' DELETE api/<controller>/5
    Public Sub DeleteValue(ByVal id As Integer)

    End Sub
End Class
