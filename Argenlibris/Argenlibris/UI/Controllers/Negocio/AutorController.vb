﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class AutorController
        Inherits ApiController

        Dim bll As IAutorBLL
        Dim bllpais As IPaisBLL
        Dim bllLibro As ILibroBLL

        Public Sub New(bllautor As IAutorBLL, bllpais_ As IPaisBLL, bllLibro_ As ILibroBLL)
            bll = bllautor
            bllpais = bllpais_
            bllLibro = bllLibro_
        End Sub

        <HttpPost>
         <RolesValidos(EnumPermiso.ProductosPuedeGestionar)>
        Public Function ConsultarTodos() As IHttpActionResult
            Return Ok(AutoMapper.Mapper.Map(Of IList(Of Autor))(bll.GetAll().ToList))
            'Return Ok(bll.GetAll().ToList)
        End Function

        <HttpPut()>
        Public Function guardar(autor As Autor) As IHttpActionResult

            'If validar(autor) Then

            bll.SaveOrUpdate(autor)
            Return Ok(autor)

            'Else

            'Return BadRequest()

            'End If

        End Function

        'Private Function validar(autor As Autor) As Boolean

        '    Dim _lista As New List(Of Autor)

        '    If autor.Id.Equals(Guid.Empty) Then


        '        _lista = bll.GetAll().ToList()

        '        For Each item As Autor In _lista
        '            If item.Nombre = autor.Nombre And item.Apellido = autor.Apellido Then
        '                Return False
        '            End If
        '        Next

        '    End If

        '    Return True

        'End Function

        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            Dim _libros As List(Of Libro)
            _libros = bllLibro.GetAll().Where(Function(l) l.Autor.Id.Equals(id)).ToList()
            If _libros.Count > 0 Then
                Return BadRequest()
            Else
                bll.Delete(id)
                Return Ok()
            End If

        End Function

    End Class
End Namespace