﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class ClienteRespuestaController
        Inherits ApiController

        Dim bll As IBLL(Of ClienteRespuesta)
        Dim bllUsuario As IBLL(Of Usuario)
        Dim bllRespuesta As IBLL(Of Respuesta)
        Dim bllPregunta As IBLL(Of Pregunta)



        Public Sub New(bllClienteRespuesta As IBLL(Of ClienteRespuesta), bllUsuario_ As IBLL(Of Usuario), bllRespuesta_ As IBLL(Of Respuesta), bllPregunta_ As IBLL(Of Pregunta))
            bll = bllClienteRespuesta
            bllUsuario = bllUsuario_
            bllRespuesta = bllRespuesta_
            bllPregunta = bllPregunta_
            'bllEncuesta = bllEncuesta_
        End Sub


        '<HttpPost>
        'Public Function ConsultarTodos() As IHttpActionResult
        '    Return Ok(bll.GetAll().ToList)
        'End Function

        <HttpPost>
        Public Function ConsultarTodos(dto As Encuesta) As IHttpActionResult

            Dim _preguntas As New List(Of Pregunta)
            Dim _respuestas As New List(Of Respuesta)
            Dim _ClienteRespuesta As New List(Of ClienteRespuesta)
            Dim _ClienteRespuestaDTO As New ClienteRespuestaDTO

            _preguntas = bllPregunta.GetAll().Where(Function(p) p.Encuesta.Id.Equals(dto.Id)).ToList()
            _respuestas = bllRespuesta.GetAll().Where(Function(r) r.Pregunta.Encuesta.Id.Equals(dto.Id)).ToList()
            '_ClienteRespuesta = bll.GetAll().Where(Function(c) c.Respuesta.Pregunta.Encuesta.Id.Equals(dto.Id)).ToList().OrderBy(Function(o) o.Respuesta)

            Dim _count As New Integer
            Dim _items As New List(Of ClienteRespuestaDTO)
            Dim _item As ClienteRespuestaDTO

            For Each r As Respuesta In _respuestas

                '_ClienteRespuesta = bll.GetAll().Where(Function(c) c.Respuesta.Id.Equals(r.Id)).ToList()
                '_ClienteRespuesta = bll.GetAll().Where(Function(c) c.Respuesta.Id.Equals(r.Id)).Where(Function(f) f.Fecha <= dto.FechaHasta).Where(Function(f) f.Fecha >= dto.FechaDesde).ToList()
                _ClienteRespuesta = bll.GetAll().Where(Function(c) c.Respuesta.Id.Equals(r.Id)).Where(Function(f) f.Fecha.Date <= dto.FechaHasta.Date).Where(Function(f) f.Fecha.Date >= dto.FechaDesde.Date).ToList()
                For Each _cr As ClienteRespuesta In _ClienteRespuesta
                    _count = _count + 1
                Next

                _item = New ClienteRespuestaDTO
                _item.votos = _count
                _item.respuesta = r
                _items.Add(_item)
                _count = 0

            Next


            Return Ok(_items)



        End Function

        <HttpPut()>
        Public Function guardar(id As Guid) As IHttpActionResult

            Dim _ClienteRespuesta As New ClienteRespuesta
            Dim _id As Guid = Guid.Parse(HttpContext.Current.User.Identity.Name)
            _ClienteRespuesta.Respuesta = bllRespuesta.Obtain(id)
            _ClienteRespuesta.Cliente = bllUsuario.Obtain(_id)
            _ClienteRespuesta.Fecha = Date.Now

            bll.SaveOrUpdate(_ClienteRespuesta)
            Return Ok(_ClienteRespuesta)

        End Function

        <HttpGet>
        Public Function TraerPorRespuesta(id As Guid) As IHttpActionResult

            Dim _ClienteRespuesta As New List(Of ClienteRespuesta)
            _ClienteRespuesta = bll.GetAll().Where(Function(c) c.Respuesta.Id.Equals(id)).ToList()
            Dim _cantidadDeVotos As Integer
            _cantidadDeVotos = _ClienteRespuesta.Count
            Return Ok(_cantidadDeVotos)

        End Function

        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            Dim _ClienteRespuesta As New ClienteRespuesta
            _ClienteRespuesta = bll.Obtain(id)
            bll.Delete(_ClienteRespuesta.Id)
            Return Ok()

        End Function

    End Class

End Namespace