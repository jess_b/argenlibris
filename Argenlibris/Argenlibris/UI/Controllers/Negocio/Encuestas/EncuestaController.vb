﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class EncuestaController
        Inherits ApiController

        Dim bll As IEncuestaBLL
        Dim bllPregunta As IPreguntaBLL
        Dim bllRespuesta As IRespuestaBLL
        Dim bllClienteRespuesta As IClienteRespuestaBLL

        Public Sub New(bllencuesta_ As IEncuestaBLL, bllPregunta_ As IPreguntaBLL, bllRespuesta_ As IRespuestaBLL, bllClienteRespuesta_ As IClienteRespuestaBLL)
            bll = bllencuesta_
            bllPregunta = bllPregunta_
            bllRespuesta = bllRespuesta_
            bllClienteRespuesta = bllClienteRespuesta_
        End Sub

        <HttpPost>
        Public Function ConsultarTodos() As IHttpActionResult
            Return Ok(AutoMapper.Mapper.Map(Of IList(Of Encuesta))(bll.GetAll().Where(Function(e) e.FechaHasta <= Date.Now).ToList))
            'Return Ok(AutoMapper.Mapper.Map(Of IList(Of Encuesta))(bll.GetAll().ToList))
        End Function

        <HttpPut()>
        Public Function guardar(encuesta As Encuesta) As IHttpActionResult

            'If validar(encuesta) Then

            bll.SaveOrUpdate(encuesta)
            Return Ok(encuesta)

            'Else

            'Return BadRequest()

            'End If

        End Function

        'Private Function validar(dto As Encuesta) As Boolean
        '    If dto.Id.Equals(Guid.Empty) Then

        '        Dim _lista As New List(Of Encuesta)

        '        _lista = bll.GetAll().ToList()

        '        For Each item As Encuesta In _lista
        '            If item.Descripcion = dto.Descripcion Then
        '                Return False
        '            End If
        '        Next
        '    End If

        '    Return True

        'End Function

        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            'borro las respuestas de esas preguntas de esa encuesta
            Dim _respuestas As New List(Of Respuesta)
            _respuestas = bllRespuesta.GetAll().Where(Function(r) r.Pregunta.Encuesta.Id.Equals(id)).ToList()
            For Each _respuesta As Respuesta In _respuestas
                bllRespuesta.Delete(_respuesta.Id)
            Next

            'borro las preguntas de esa encuesta
            Dim _preguntas As New List(Of Pregunta)
            _preguntas = bllPregunta.GetAll().Where(Function(p) p.Encuesta.Id.Equals(id)).ToList()
            For Each _pregunta As Pregunta In _preguntas
                bllPregunta.Delete(_pregunta.Id)
            Next

            'borro las encuestas
            bll.Delete(id)
            Return Ok()

        End Function

        <HttpGet>
        Public Function ConsultarPorCliente() As IHttpActionResult

            Dim _id As Guid = Guid.Parse(HttpContext.Current.User.Identity.Name)

            Dim _respuestas As New List(Of ClienteRespuesta)
            _respuestas = bllClienteRespuesta.GetAll().Where(Function(r) r.Cliente.Id.Equals(_id)).ToList()

            Dim _encuestas As New List(Of Encuesta)
            Dim _misEncuestas As New List(Of Encuesta)
            Dim _agregar As Boolean = True
            _encuestas = bll.GetAll().Where(Function(e) e.FechaHasta.Date >= Date.Now).ToList()

            For Each _encuesta As Encuesta In _encuestas

                _agregar = True
                If _encuesta.FechaHasta >= Date.Now.Date Then
                    For Each _respuesta As ClienteRespuesta In _respuestas
                        If _respuesta.Respuesta.Pregunta.Encuesta.Id = _encuesta.Id Then
                            _agregar = False
                        End If
                    Next
                    If _agregar = True Then
                        _misEncuestas.Add(_encuesta)
                    End If
                End If

            Next


            Return Ok(AutoMapper.Mapper.Map(Of List(Of Encuesta))(_misEncuestas))

        End Function

    End Class
End Namespace