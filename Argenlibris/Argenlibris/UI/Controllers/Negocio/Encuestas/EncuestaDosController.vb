﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class EncuestaDosController
        Inherits ApiController

        Dim bll As IEncuestaBLL
        Dim bllPregunta As IPreguntaBLL
        Dim bllRespuesta As IRespuestaBLL
        Dim bllClienteRespuesta As IClienteRespuestaBLL

        Public Sub New(bllencuesta_ As IEncuestaBLL, bllPregunta_ As IPreguntaBLL, bllRespuesta_ As IRespuestaBLL, bllClienteRespuesta_ As IClienteRespuestaBLL)
            bll = bllencuesta_
            bllPregunta = bllPregunta_
            bllRespuesta = bllRespuesta_
            bllClienteRespuesta = bllClienteRespuesta_
        End Sub

        <HttpPost>
        <RolesValidos(EnumPermiso.MarketingPuedeGestionar)>
        Public Function ConsultarTodos() As IHttpActionResult
            Return Ok(AutoMapper.Mapper.Map(Of IList(Of Encuesta))(bll.GetAll().ToList))
        End Function


    End Class
End Namespace