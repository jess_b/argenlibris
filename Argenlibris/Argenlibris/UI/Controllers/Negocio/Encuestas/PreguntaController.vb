﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class PreguntaController
        Inherits ApiController

        Dim bll As IPreguntaBLL

        Public Sub New(bllPregunta_ As IPreguntaBLL)
            bll = bllPregunta_
        End Sub

        <HttpPost>
        Public Function ConsultarTodos() As IHttpActionResult
            Return Ok(AutoMapper.Mapper.Map(Of IList(Of Pregunta))(bll.GetAll().ToList))
            'Return Ok(bll.GetAll().ToList)
        End Function

        <HttpPut()>
        Public Function guardar(Pregunta As Pregunta) As IHttpActionResult
            bll.SaveOrUpdate(Pregunta)
            Return Ok(Pregunta)

        End Function

        <HttpGet>
        Public Function TraerPorEncuesta(id As Guid) As IHttpActionResult
            Return Ok(AutoMapper.Mapper.Map(Of IList(Of Pregunta))(bll.GetAll().Where(Function(p) p.Encuesta.Id.Equals(id)).ToList()))

        End Function

        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            bll.Delete(id)
            Return Ok()

        End Function

    End Class
End Namespace