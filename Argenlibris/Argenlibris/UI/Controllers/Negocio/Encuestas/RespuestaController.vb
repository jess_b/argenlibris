﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class RespuestaController
        Inherits ApiController

        Dim bll As IRespuestaBLL
       
        Public Sub New(bllRespuesta_ As IRespuestaBLL)
            bll = bllRespuesta_

        End Sub

        <HttpPost>
        Public Function ConsultarTodos(pregunta As Pregunta) As IHttpActionResult

            Dim _respuestas As New List(Of Respuesta)
            _respuestas = AutoMapper.Mapper.Map(Of IList(Of Respuesta))(bll.GetAll().Where(Function(u) u.Pregunta.Id.Equals(pregunta.Id)).ToList())
            Return Ok(_respuestas)

        End Function

        <HttpGet>
        Public Function TraerPorEncuesta(id As Guid) As IHttpActionResult

            Dim _respuestas As New List(Of Respuesta)

            _respuestas = AutoMapper.Mapper.Map(Of IList(Of Respuesta))(bll.GetAll().Where(Function(u) u.Pregunta.Encuesta.Id.Equals(id)).ToList())

            Return Ok(_respuestas)

        End Function

        <HttpPut()>
        Public Function guardar(Respuesta As RespuestaDTO) As IHttpActionResult

            Dim _respuesta As Respuesta

            For Each s As String In Respuesta.Respuestas
                _respuesta = New Respuesta
                _respuesta.Descripcion = s
                _respuesta.Pregunta = Respuesta.Pregunta
                bll.SaveOrUpdate(_respuesta)
            Next

            'Dim _preguntaRespuesta As New PreguntaRespuesta
            '_preguntaRespuesta.Pregunta = Respuesta.Pregunta
            '_preguntaRespuesta.Respuesta = _respuesta

            'bllPreguntaRespuesta.SaveOrUpdate(_preguntaRespuesta)

            Return Ok()

        End Function

        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            'Dim _preguntaRespuesta As New PreguntaRespuesta
            '_preguntaRespuesta = bllPreguntaRespuesta.GetAll().Where(Function(p) p.Respuesta.Id.Equals(id)).First
            'bllPreguntaRespuesta.Delete(_preguntaRespuesta)

            bll.Delete(id)
            Return Ok()

        End Function

    End Class
End Namespace