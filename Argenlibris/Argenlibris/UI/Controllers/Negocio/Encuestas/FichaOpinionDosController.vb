﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class FichaOpinionDosController
        Inherits ApiController

        Dim bll As IFichaOpinionBLL
        Dim bllPregunta As IPreguntaBLL
        Dim bllRespuesta As IRespuestaBLL
        Dim bllClienteRespuesta As IClienteRespuestaBLL

        Public Sub New(bll_ As IFichaOpinionBLL, bllPregunta_ As IPreguntaBLL, bllRespuesta_ As IRespuestaBLL, bllClienteRespuesta_ As IClienteRespuestaBLL)
            bll = bll_
            bllPregunta = bllPregunta_
            bllRespuesta = bllRespuesta_
            bllClienteRespuesta = bllClienteRespuesta_
        End Sub

        <HttpPost>
        <RolesValidos(EnumPermiso.MarketingPuedeGestionar)>
        Public Function ConsultarTodos() As IHttpActionResult

            Return Ok(AutoMapper.Mapper.Map(Of IList(Of FichaDeOpinion))(bll.GetAll().ToList))

        End Function


    End Class
End Namespace