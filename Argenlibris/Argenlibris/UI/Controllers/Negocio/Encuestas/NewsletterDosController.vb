﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class NewsletterDosController
        Inherits ApiController

        Dim bll As INewsletterBLL
        Dim bllLibroNewsletter As ILibroNewsletterBLL
        Dim bllMail As EmailBLL
        Dim bllUsuario As IUsuarioBLL


        Public Sub New(bllnewsletter As INewsletterBLL, bllLibroNewsletter_ As ILibroNewsletterBLL, bllMail_ As IEmailBLL, bllUsuario_ As IUsuarioBLL)
            bll = bllnewsletter
            bllLibroNewsletter = bllLibroNewsletter_
            bllMail = bllMail_
            bllUsuario = bllUsuario_
        End Sub


        <HttpPost>
        <RolesValidos(EnumPermiso.MarketingPuedeGestionar)>
        Public Function ConsultarTodos() As IHttpActionResult

            Return Ok(AutoMapper.Mapper.Map(Of IList(Of Newsletter))(bll.GetAll().ToList))

        End Function




    End Class
End Namespace