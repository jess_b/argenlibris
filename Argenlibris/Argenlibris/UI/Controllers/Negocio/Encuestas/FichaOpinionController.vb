﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class FichaOpinionController
        Inherits ApiController

        Dim bll As IFichaOpinionBLL
        Dim bllPregunta As IPreguntaBLL
        Dim bllRespuesta As IRespuestaBLL
        Dim bllClienteRespuesta As IClienteRespuestaBLL

        Public Sub New(bll_ As IFichaOpinionBLL, bllPregunta_ As IPreguntaBLL, bllRespuesta_ As IRespuestaBLL, bllClienteRespuesta_ As IClienteRespuestaBLL)
            bll = bll_
            bllPregunta = bllPregunta_
            bllRespuesta = bllRespuesta_
            bllClienteRespuesta = bllClienteRespuesta_
        End Sub

        <HttpPost>
        <AllowAnonymous>
        Public Function ConsultarTodos() As IHttpActionResult

            Return Ok(AutoMapper.Mapper.Map(Of IList(Of FichaDeOpinion))(bll.GetAll().Where(Function(e) e.FechaHasta <= Date.Now).ToList))

        End Function

        <HttpPut()>
        Public Function guardar(FichaDeOpinion As FichaDeOpinion) As IHttpActionResult

            'If validar(FichaDeOpinion) Then

            bll.SaveOrUpdate(FichaDeOpinion)
            Return Ok(FichaDeOpinion)

            'Else

            'Return BadRequest()

            'End If


        End Function

        'Private Function validar(dto As FichaDeOpinion) As Boolean
        '    If dto.Id.Equals(Guid.Empty) Then

        '        Dim _lista As New List(Of FichaDeOpinion)

        '        _lista = bll.GetAll().ToList()

        '        For Each item As FichaDeOpinion In _lista
        '            If item.Descripcion = dto.Descripcion Then
        '                Return False
        '            End If
        '        Next
        '    End If

        '    Return True

        'End Function

        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            'Dim _autor As New Autor
            '_autor = bll.Obtain(id)
            bll.Delete(id)
            Return Ok()

        End Function

        <HttpGet>
        Public Function ConsultarPorCliente() As IHttpActionResult

            Dim _id As Guid = Guid.Parse(HttpContext.Current.User.Identity.Name)

            Dim _respuestas As New List(Of ClienteRespuesta)
            _respuestas = bllClienteRespuesta.GetAll().Where(Function(r) r.Cliente.Id.Equals(_id)).ToList()

            Dim _fichasDeOpinion As New List(Of FichaDeOpinion)
            Dim _misFichaDeOpinion As New List(Of FichaDeOpinion)
            Dim _agregar As Boolean = True
            _fichasDeOpinion = bll.GetAll().Where(Function(e) e.FechaHasta.Date >= Date.Now).ToList()

            For Each _fichaDeOpinion As FichaDeOpinion In _fichasDeOpinion
                _agregar = True
                If _fichaDeOpinion.FechaHasta >= Date.Now.Date Then
                    For Each _respuesta As ClienteRespuesta In _respuestas
                        If _respuesta.Respuesta.Pregunta.Encuesta.Id = _fichaDeOpinion.Id Then
                            _agregar = False
                        End If
                    Next
                    If _agregar = True Then
                        _misFichaDeOpinion.Add(_fichaDeOpinion)
                    End If
                End If
            Next


            Return Ok(AutoMapper.Mapper.Map(Of List(Of FichaDeOpinion))(_misFichaDeOpinion))

        End Function

    End Class
End Namespace