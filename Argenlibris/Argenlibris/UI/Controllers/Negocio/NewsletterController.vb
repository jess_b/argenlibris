﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class NewsletterController
        Inherits ApiController

        Dim bll As INewsletterBLL
        Dim bllLibroNewsletter As ILibroNewsletterBLL
        Dim bllMail As EmailBLL
        Dim bllUsuario As IUsuarioBLL


        Public Sub New(bllnewsletter As INewsletterBLL, bllLibroNewsletter_ As ILibroNewsletterBLL, bllMail_ As IEmailBLL, bllUsuario_ As IUsuarioBLL)
            bll = bllnewsletter
            bllLibroNewsletter = bllLibroNewsletter_
            bllMail = bllMail_
            bllUsuario = bllUsuario_
        End Sub

        <HttpGet>
        Public Function VerNewsletter(id As Guid) As IHttpActionResult

            Dim _newsletterDTO As New CrearNewsletterDTO
            Dim _newsletter As Newsletter = bll.Obtain(id)
            Dim _librosNewsletter As IList(Of LibroNewsletter)

            _librosNewsletter = (AutoMapper.Mapper.Map(Of IList(Of LibroNewsletter))(bllLibroNewsletter.GetAll.Where(Function(n) n.Newsletter.Id.Equals(id)).ToList()))

            _newsletterDTO.Id = _newsletter.Id
            _newsletterDTO.Nombre = _newsletter.Nombre
            _newsletterDTO.Asunto = _newsletter.Asunto
            _newsletterDTO.Cuerpo = _newsletter.Cuerpo

            For Each _libroNewsletter As LibroNewsletter In _librosNewsletter
                _newsletterDTO.Libros.Add(_libroNewsletter.Libro)
            Next

            Return Ok(_newsletterDTO)

        End Function

        <HttpPost>
        Public Function ConsultarTodos() As IHttpActionResult

            Return Ok(AutoMapper.Mapper.Map(Of IList(Of Newsletter))(bll.GetAll().ToList))

        End Function

        <HttpPut()>
        Public Function guardar(newsletter As CrearNewsletterDTO) As IHttpActionResult

            'If validar(newsletter) Then

            ' Guardo el newsletter
            Dim _newsletter As New Newsletter
            _newsletter.Fecha = Date.Now
            _newsletter.Asunto = newsletter.Asunto
            _newsletter.Cuerpo = newsletter.Cuerpo
            _newsletter.Nombre = newsletter.Nombre
            _newsletter.Id = newsletter.Id

            bll.SaveOrUpdate(_newsletter)

            'Guardo la relacion Muchos a Muchos de Libro-Newsletter
            Dim _librosNewsletter As IList(Of LibroNewsletter)
            _librosNewsletter = (AutoMapper.Mapper.Map(Of IList(Of LibroNewsletter))(bllLibroNewsletter.GetAll.Where(Function(n) n.Newsletter.Id.Equals(_newsletter.Id)).ToList()))


            Dim _libroNewsletter As LibroNewsletter
            Dim _agregar As Boolean = True

            For Each _libro As Libro In newsletter.Libros

                _agregar = True

                For Each ln As LibroNewsletter In _librosNewsletter
                    If ln.Libro.Id = _libro.Id Then
                        _agregar = False
                    End If
                Next

                If _agregar Then
                    _libroNewsletter = New LibroNewsletter
                    _libroNewsletter.Libro = _libro
                    _libroNewsletter.Newsletter = _newsletter
                    bllLibroNewsletter.SaveOrUpdate(_libroNewsletter)
                End If
            Next

            Return Ok(_newsletter)

            'Else

            'Return BadRequest()

            'End If


        End Function

        'Private Function validar(dto As CrearNewsletterDTO) As Boolean

        '    If dto.Id.Equals(Guid.Empty) Then

        '        Dim _lista As New List(Of Newsletter)

        '        _lista = bll.GetAll().ToList()

        '        For Each item As Newsletter In _lista
        '            If item.Nombre = dto.Nombre Then
        '                Return False
        '            End If
        '        Next

        '    End If

        '    Return True

        'End Function


        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            bll.Delete(id)
            Return Ok()

        End Function

        <HttpHead>
        Public Sub Enviar(id As Guid)

            Dim _ruta As String
            Dim _rutaImagen As String
            Dim _newsletter As New Newsletter
            Dim _libroNewsletter As New List(Of LibroNewsletter)
            Dim _libros As New List(Of Libro)
            Dim _usuarios As New List(Of Usuario)

            _newsletter = bll.Obtain(id)
            _newsletter.Enviado = True
            bll.SaveOrUpdate(_newsletter)


            _libroNewsletter = bllLibroNewsletter.GetAll().Where(Function(n) n.Newsletter.Id.Equals(id)).ToList


            For Each ln As LibroNewsletter In _libroNewsletter
                _libros.Add(ln.Libro)
            Next


            '_usuarios = bllUsuario.GetAll().Where(Function(u) u.Activo.Equals(True)).ToList

            '_ruta = HttpContext.Current.Server.MapPath("/Newsletter.html")
            '_rutaImagen = Request.RequestUri.Authority & "/images" 'HttpContext.Current.Request.Url.ToString("/images")

            'bllMail.EnviarNewsletter(_usuarios, _libros, _newsletter, _ruta, _rutaImagen)

        End Sub



    End Class
End Namespace