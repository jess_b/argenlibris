﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class EditorialController
        Inherits ApiController

        Dim bll As IBLL(Of Editorial)
        Dim bllLibro As ILibroBLL

        Public Sub New(bllEditorial As IBLL(Of Editorial), bllLibro_ As ILibroBLL)
            bll = bllEditorial
            bllLibro = bllLibro_
        End Sub

        <HttpPost>
        <RolesValidos(EnumPermiso.ProductosPuedeGestionar)>
        Public Function ConsultarTodos() As IHttpActionResult
            Return Ok(bll.GetAll().ToList)
        End Function

        <HttpPut()>
        Public Function guardar(dto As ConsultarBasicoDTO) As IHttpActionResult

            'If validar(dto) Then

            Dim _editorial As New Editorial
            _editorial.Id = dto.id
            _editorial.Descripcion = dto.Descripcion
            bll.SaveOrUpdate(_editorial)
            Return Ok(_editorial)

            'Else

            'Return BadRequest()

            'End If


        End Function

        'Private Function validar(dto As ConsultarBasicoDTO) As Boolean

        '    Dim _lista As New List(Of Editorial)

        '        _lista = bll.GetAll().ToList()

        '        For Each item As Editorial In _lista
        '            If item.Descripcion = dto.Descripcion Then
        '                Return False
        '            End If
        '        Next

        '    Return True

        'End Function


        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            Dim _libros As List(Of Libro)
            _libros = bllLibro.GetAll().Where(Function(l) l.Editorial.Id.Equals(id)).ToList()
            If _libros.Count > 0 Then
                Return BadRequest()
            Else
                bll.Delete(id)
                Return Ok()
            End If

        End Function

    End Class
End Namespace