﻿Imports System.Net
Imports System.Web.Http
Imports Argenlibris.Domain
Imports BLL
Imports Microsoft.AspNet.SignalR

Public Class ChatController
    Inherits ApiController

    Dim bll As IChatMessageBLL

    Public Sub New(bll_ As IChatMessageBLL)
        bll = bll_

    End Sub

    ' GET api/<controller>
    Public Function GetValues(id As Guid) As IHttpActionResult
        Return Ok(bll.GetAll().Where(Function(o) o.IdRepresentante.Equals(id)).ToList)
    End Function


    ' PUT api/<controller>/5
    Public Sub PutValue(<FromBody()> ByVal value As ChatMessage)
        value.FechaMensaje = DateTime.Now
        bll.SaveOrUpdate(value)
        GlobalHost.ConnectionManager.GetHubContext(Of ChatHub).Clients.Group(value.IdRepresentante.ToString).HayUnNuevoMensaje(value.Mensaje, value.NombreUsuario, value.FechaMensaje.ToString)


        'hub.Clients.Group(value.IdRepresentante.ToString).HayUnNuevoMensaje(value.Mensaje, value.NombreUsuario, value.FechaMensaje.ToString)



    End Sub


End Class
