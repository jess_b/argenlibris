﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class CategoriaClienteController
        Inherits ApiController

        Dim bll As ICategoriaClienteBLL
        Dim bllCategoria As ICategoriaBLL
        Dim bllUsuario As IUsuarioBLL

        Public Sub New(bllcategoriacliente_ As IBLL(Of CategoriaCliente), bllUsuario_ As IUsuarioBLL, bllCategoria_ As CategoriaBLL)
            bll = bllcategoriacliente_
            bllUsuario = bllUsuario_
            bllcategoria = bllCategoria_
        End Sub

        <HttpGet>
        Public Function ConsultarPorCliente(id As Guid) As IHttpActionResult

            Return Ok(AutoMapper.Mapper.Map(Of IList(Of CategoriaCliente))(bll.GetAll().Where(Function(c) c.Cliente.Id.Equals(id)).ToList))

        End Function

        <HttpPost>
        Public Function ConsultarTodos() As IHttpActionResult

            Return Ok(AutoMapper.Mapper.Map(Of IList(Of CategoriaCliente))(bll.GetAll().ToList))

        End Function

        <HttpPut()>
        <AllowAnonymous>
        Public Function guardar(categoriaCliente_ As CrearCategoriaClienteDTO) As CategoriaCliente

            Dim _categoria As Categoria = bllCategoria.Obtain(categoriaCliente_.idcategoria)
            Dim _usuario As Usuario = bllUsuario.Obtain(categoriaCliente_.idcliente)
            Dim _categoriaCliente As New CategoriaCliente

            _categoriaCliente.Cliente = _usuario
            _categoriaCliente.Categoria = _categoria


            Return (bll.SaveOrUpdate(_categoriaCliente))


        End Function

        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            bll.Delete(id)
            Return Ok(id)

        End Function

    End Class
End Namespace