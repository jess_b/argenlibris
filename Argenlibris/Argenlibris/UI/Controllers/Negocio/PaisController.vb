﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class PaisController
        Inherits ApiController

        Dim bll As IBLL(Of Pais)
        Dim bllAutor As IBLL(Of Autor)

        Public Sub New(bllpais As IBLL(Of Pais), bllAutor_ As IBLL(Of Autor))
            bll = bllpais
            bllAutor = bllAutor_
        End Sub

        <HttpPost>
        <RolesValidos(EnumPermiso.ProductosPuedeGestionar)>
        Public Function ConsultarTodos() As IHttpActionResult
            Return Ok(bll.GetAll().ToList)
        End Function

        <HttpPut()>
        Public Function guardar(dto As ConsultarBasicoDTO) As IHttpActionResult

            'If validar(dto) Then

            Dim _pais As New Pais
            _pais.Id = dto.id
            _pais.Descripcion = dto.Descripcion
            bll.SaveOrUpdate(_pais)
            Return Ok(_pais)

            'Else

            'Return BadRequest()

            'End If


        End Function

        'Private Function validar(dto As ConsultarBasicoDTO) As Boolean

        '    Dim _lista As New List(Of Pais)

        '        _lista = bll.GetAll().ToList()

        '        For Each item As Pais In _lista
        '            If item.Descripcion = dto.Descripcion Then
        '                Return False
        '            End If
        '        Next

        '    Return True

        'End Function

        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            Dim _autores As List(Of Autor)
            _autores = bllAutor.GetAll().Where(Function(a) a.PaisDeOrigen.Id.Equals(id)).ToList()
            If _autores.Count > 0 Then
                Return BadRequest()
            Else
                bll.Delete(id)
                Return Ok()
            End If

        End Function

    End Class

End Namespace