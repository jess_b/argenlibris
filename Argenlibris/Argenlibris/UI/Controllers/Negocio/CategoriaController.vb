﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class categoriaController
        Inherits ApiController

        Dim bll As IBLL(Of Categoria)
        Dim bllLibro As ILibroBLL

        Public Sub New(bllcategoria As IBLL(Of Categoria), bllLibro_ As ILibroBLL)
            bll = bllcategoria
            bllLibro = bllLibro_
        End Sub

        <HttpGet>
        <RolesValidos(EnumPermiso.ProductosPuedeGestionar)>
        Public Function Consultar() As IHttpActionResult
            Return Ok(bll.GetAll().ToList)
        End Function

        <HttpPost>
        <AllowAnonymous>
        Public Function ConsultarTodos() As IHttpActionResult
            Return Ok(bll.GetAll().ToList)
        End Function

        <HttpPut()>
        Public Function guardar(dto As ConsultarBasicoDTO) As IHttpActionResult

            'If validar(dto) Then

            Dim _categoria As New Categoria
            _categoria.Id = dto.id
            _categoria.Descripcion = dto.Descripcion
            bll.SaveOrUpdate(_categoria)
            Return Ok(_categoria)

            'Else

            'Return BadRequest()

            'End If

        End Function

        'Private Function validar(dto As ConsultarBasicoDTO) As Boolean

        '    Dim _lista As New List(Of Categoria)


        '    _lista = bll.GetAll().ToList()

        '    For Each item As Categoria In _lista
        '        If item.Descripcion = dto.Descripcion Then
        '            Return False
        '        End If
        '    Next

        '    Return True

        'End Function

        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            Dim _libros As List(Of Libro)

            _libros = bllLibro.GetAll().Where(Function(l) l.Categoria.Id.Equals(id)).ToList()

            If _libros.Count > 0 Then
                Return BadRequest()
            Else
                bll.Delete(id)
                Return Ok()
            End If

        End Function

    End Class
End Namespace