﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class CategoriaClientedosController
        Inherits ApiController

        Dim bll As ICategoriaClienteBLL
        Dim bllCategoria As ICategoriaBLL
        Dim bllUsuario As IUsuarioBLL

        Public Sub New(bllcategoriacliente_ As IBLL(Of CategoriaCliente), bllUsuario_ As IUsuarioBLL, bllCategoria_ As CategoriaBLL)
            bll = bllcategoriacliente_
            bllUsuario = bllUsuario_
            bllcategoria = bllCategoria_
        End Sub

        <HttpGet>
        Public Function ConsultarPorCliente(id As Guid) As IHttpActionResult

            Return Ok(AutoMapper.Mapper.Map(Of IList(Of CategoriaCliente))(bll.GetAll().Where(Function(c) c.Cliente.Id.Equals(id)).ToList))

        End Function

        <HttpPost>
        Public Function Actualizar(cat As List(Of CrearCategoriaClienteDTO)) As IHttpActionResult

            Dim _idCliente As Guid = Guid.Parse(HttpContext.Current.User.Identity.Name)

            Dim _catCliente As List(Of CategoriaCliente) = bll.GetAll().Where(Function(c) c.Cliente.Id.Equals(_idCliente)).ToList()

            Dim _newCatCliente As New CategoriaCliente

            Dim _existeBD As Boolean = False

            For Each cc_ As CrearCategoriaClienteDTO In cat

                _existeBD = False

                ' si seleccionó o dejó la opcion seleccionada
                If cc_.Eleccion Then

                    For Each catCliente_ As CategoriaCliente In _catCliente

                        If catCliente_.Categoria.Id = cc_.idcategoria And catCliente_.Cliente.Id = cc_.idcliente Then
                            _existeBD = True
                        End If

                    Next

                    ' Si existia en la BD y checkbox sigue seleccionado no actualizar
                    ' Si no existia en la BD y checkbox seleccionado, Actualizar
                    If _existeBD = False Then

                        _newCatCliente.Cliente = bllUsuario.GetAll().Where(Function(c) c.Id.Equals(cc_.idcliente)).First
                        _newCatCliente.Categoria = bllCategoria.GetAll().Where(Function(cc) cc.Id.Equals(cc_.idcategoria)).First
                        bll.SaveOrUpdate(_newCatCliente)

                    End If

                Else

                    'si deseleccionó o dejó la opción deseleccionada

                    For Each catCliente_ As CategoriaCliente In _catCliente

                        If catCliente_.Categoria.Id = cc_.idcategoria And catCliente_.Cliente.Id = cc_.idcliente Then

                            'si estaba en la BD y deseleccionó la opción hay q borrar de la BD la CC
                                bll.Delete(catCliente_.Id)

                        End If

                    Next


                End If

            Next

            Return Ok()

        End Function

        <HttpPut()>
        <AllowAnonymous>
        Public Function guardar(categoriaCliente_ As CrearCategoriaClienteDTO) As CategoriaCliente


        End Function

        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            bll.Delete(id)
            Return Ok(id)

        End Function

    End Class
End Namespace