﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class TarjetaController
        Inherits ApiController

        Dim bll As ITarjetaBLL
        Dim bllUsuario As IUsuarioBLL

        Public Sub New(bllTarjeta_ As ITarjetaBLL, bllUsuario_ As IUsuarioBLL)
            bll = bllTarjeta_
            bllUsuario = bllUsuario_
        End Sub

        <HttpPost>
        Public Function ConsultarTodos() As IHttpActionResult

            Dim _id As Guid = Guid.Parse(HttpContext.Current.User.Identity.Name)
            Dim _tarjetas As New List(Of Tarjeta)
            _tarjetas = (AutoMapper.Mapper.Map(Of IList(Of Tarjeta))(bll.GetAll().Where(Function(o) o.Cliente.Id.Equals(_id)).ToList))
            'desencriptar
            Dim _encriptador As Encriptador = Encriptador.GetInstance
            For Each _tarjeta As Tarjeta In _tarjetas
                _tarjeta.NumeroTarjeta = _encriptador.Desencriptar(_tarjeta.NumeroTarjeta)
            Next
            Return Ok(_tarjetas)

        End Function

        <HttpPut()>
        Public Function guardar(tarjeta_ As Tarjeta) As Tarjeta

            Dim _id As Guid = Guid.Parse(HttpContext.Current.User.Identity.Name)
            Dim _usuario As New Usuario

            _usuario = bllUsuario.Obtain(_id)

            tarjeta_.Cliente = _usuario

            'encriptar
            Dim _encriptador As Encriptador = Encriptador.GetInstance
            tarjeta_.NumeroTarjeta = _encriptador.Encriptar(tarjeta_.NumeroTarjeta)

            bll.SaveOrUpdate(tarjeta_)
            Return tarjeta_

        End Function

        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            bll.Delete(id)
            Return Ok()

        End Function

    End Class
End Namespace