﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class ComentarioController
        Inherits ApiController

        Dim bll As IBLL(Of Comentario)
        Dim bllUsuario As IUsuarioBLL

        Public Sub New(bllComentario As IBLL(Of Comentario), bllUsuario_ As IUsuarioBLL)
            bll = bllComentario
            bllUsuario = bllUsuario_
        End Sub

        <HttpPost>
        <AllowAnonymous>
        Public Function ConsultarTodos(id As Guid) As IHttpActionResult

            Return Ok(AutoMapper.Mapper.Map(Of IList(Of Comentario))(bll.GetAll().Where(Function(o) o.Libro.Id.Equals(id)).ToList()))

        End Function

        <HttpPut()>
        <AllowAnonymous>
        Public Function guardar(comentario As Comentario) As IHttpActionResult

            Dim _id As Guid = Guid.Parse(HttpContext.Current.User.Identity.Name)
            Dim _usuario As Usuario = bllUsuario.Obtain(_id)
            comentario.Nombre = _usuario.NombreUsuario
            comentario.Fecha = DateTime.Now
            Return Ok(bll.SaveOrUpdate(comentario))

        End Function

        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            Dim _comentario As New Comentario
            _comentario = bll.Obtain(id)
            bll.Delete(_comentario.Id)
            Return Ok()

        End Function

    End Class
End Namespace