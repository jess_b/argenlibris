﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class PuntuacionController
        Inherits ApiController

        Dim bll As IBLL(Of Puntuacion)
        Dim bllUsuario As IUsuarioBLL
        Public Sub New(bllPuntuacion As IBLL(Of Puntuacion), bllUsuario_ As IUsuarioBLL)
            bll = bllPuntuacion
            bllUsuario = bllUsuario_
        End Sub

        <HttpPost>
        <AllowAnonymous()>
        Public Function ConsultarTodos(id As Guid) As IHttpActionResult
            Return Ok(AutoMapper.Mapper.Map(Of IList(Of Puntuacion))(bll.GetAll().Where(Function(o) o.Libro.Id.Equals(id)).ToList()))
        End Function

        <HttpPut()>
        <AllowAnonymous()>
        Public Function Guardar(puntuacion As Puntuacion) As IHttpActionResult

            Dim _id As Guid = Guid.Parse(HttpContext.Current.User.Identity.Name)
            Dim _usuario As Usuario = bllUsuario.Obtain(_id)
            puntuacion.Usuario = _usuario

            Return Ok(bll.SaveOrUpdate(puntuacion))

        End Function

        <HttpGet()>
        <AllowAnonymous()>
        Public Function Obtener(id As Guid) As IHttpActionResult

            Return Ok(AutoMapper.Mapper.Map(Of Puntuacion)(bll.GetAll().Where(Function(o) o.Libro.Id = id).FirstOrDefault))
        End Function

        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            Dim _puntuacion As New Puntuacion
            _puntuacion = bll.Obtain(id)
            bll.Delete(_puntuacion.Id)
            Return Ok()

        End Function

    End Class
End Namespace