﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class MisPedidosController
        Inherits ApiController

        Dim bll As IPedidoBLL
        Dim bllLibro As ILibroBLL
        Dim bllUsuario As IUsuarioBLL

        Public Sub New(bll As IPedidoBLL, blllibro_ As ILibroBLL, bllusuario_ As IUsuarioBLL)
            Me.bll = bll
            bllLibro = blllibro_
            bllUsuario = bllusuario_
        End Sub

        ' GET api/<controller>
        Public Function GetValues() As IEnumerable(Of String)
            Return New String() {"value1", "value2"}
        End Function

        ' GET api/<controller>/5
        Public Function GetValue(ByVal id As Integer) As String
            Return "value"
        End Function

        ' POST api/<controller>
        <AllowAnonymous>
        Public Function PostValue() As IHttpActionResult

            Dim _id As Guid = Guid.Parse(HttpContext.Current.User.Identity.Name)

            Return Ok(AutoMapper.Mapper.Map(Of IList(Of PedidoCabecera))(bll.GetAll().Where(Function(o) o.Cliente.Id.Equals(_id)).ToList()))

        End Function


        ' PUT api/<controller>/5
        Public Sub PutValue(pedido_ As PedidoDTO)

            If pedido_.Id.Equals(Guid.Empty) Then
                crearNuevoPedido(pedido_)
            Else
                modificarPedido(pedido_)
            End If

        End Sub

        Private Sub crearNuevoPedido(pedido_ As PedidoDTO)

            Dim _pedido As New PedidoCabecera
            _pedido.Cliente = pedido_.Cliente
            _pedido.Direccion = pedido_.Direccion
            _pedido.Total = pedido_.Total
            _pedido.Fecha = DateTime.Now
            _pedido.Despachado = False
            _pedido.ObservacionContable = pedido_.ObservacionContable

            Dim _posicion As PedidoPosicion
            For Each posicion As PedidoPosicionDTO In pedido_.Posiciones
                _posicion = New PedidoPosicion
                _posicion.Cantidad = posicion.Cantidad
                _posicion.Libro = bllLibro.Obtain(posicion.Id)
                _posicion.Libro.Disponibilidad -= _posicion.Cantidad

                _posicion.Precio = _posicion.Libro.Precio

                _pedido.Posiciones.Add(_posicion)
            Next

            bll.SaveOrUpdate(_pedido)

        End Sub

        Private Sub modificarPedido(pedido_ As PedidoDTO)

            Dim _pedido As New PedidoCabecera
            _pedido = bll.Obtain(pedido_.Id)
            If pedido_.Recibido Then
                _pedido.Recibido = pedido_.Recibido
            End If

            If pedido_.Reclamado Then
                _pedido.Reclamado = pedido_.Reclamado
            End If

            If pedido_.Resuelto Then
                _pedido.Resuelto = pedido_.Resuelto
            End If
            bll.SaveOrUpdate(_pedido)

        End Sub

        ' DELETE api/<controller>/5
        Public Sub DeleteValue(ByVal id As Integer)

        End Sub
    End Class
End Namespace
