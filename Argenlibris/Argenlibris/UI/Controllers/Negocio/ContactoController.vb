﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class ContactoController
        Inherits ApiController

        Dim bll As IBLL(Of Contacto)
        Dim bllEmail As IEmailBLL

        Public Sub New(bllContacto As IBLL(Of Contacto), bllEmail_ As IEmailBLL)
            bll = bllContacto
            bllEmail = bllEmail_
        End Sub

        <HttpPost>
        Public Function ConsultarTodos() As IHttpActionResult
            Return Ok()
            'Return Ok(bll.GetAll().ToList)
        End Function

        <AllowAnonymous>
        <HttpPut()>
        Public Function Enviar(contacto_ As Contacto) As IHttpActionResult

            'Dim _Contacto As New Contacto
            '_Contacto.Id = dto.id
            '_Contacto.Descripcion = dto.Descripcion
            'bll.SaveOrUpdate(_Contacto)
            'Return bll.Obtain(_Contacto.Id)
            'bllEmail.EnviarMailContacto(contacto_)
            Return Ok()
        End Function

        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            'Dim _autores As List(Of Autor)
            '_autores = bllAutor.GetAll().Where(Function(a) a.ContactoDeOrigen.Id.Equals(id)).ToList()
            'If _autores.Count > 0 Then
            '    Return BadRequest()
            'Else
            '    bll.Delete(id)
            '    Return Ok()
            'End If
            Return Ok()
        End Function

    End Class

End Namespace