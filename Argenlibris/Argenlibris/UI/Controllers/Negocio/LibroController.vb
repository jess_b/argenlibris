﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers.Negocio
    Public Class LibroController
        Inherits ApiController

        Dim bllLibroNewsletter As ILibroNewsletterBLL
        Dim bll As IBLL(Of Libro)
        Dim bllEditorial As IEditorialBLL
        Dim bllAutor As IAutorBLL
        Dim bllCategoria As ICategoriaBLL
        Dim bllCategoriaCliente As ICategoriaClienteBLL
        Dim bllEmail As EmailBLL

        Public Sub New(bllLibro As IBLL(Of Libro), blleditorial_ As IEditorialBLL, bllautor_ As IAutorBLL, bllCategoria_ As ICategoriaBLL, bllLibroNewsletter_ As ILibroNewsletterBLL, bllCategoriaCliente_ As ICategoriaClienteBLL, bllEmail_ As EmailBLL)
            bll = bllLibro
            bllEditorial = blleditorial_
            bllAutor = bllautor_
            bllCategoria = bllCategoria_
            bllLibroNewsletter = bllLibroNewsletter_
            bllCategoriaCliente = bllCategoriaCliente_
            bllEmail = bllEmail_
        End Sub



        <HttpGet>
        Public Function ConsultarFavoritosLeidos() As IHttpActionResult
            Dim HistoricoClienteDTO As New HistoricoClienteDTO


            Dim lista As IList
            Dim idCliente As Guid = Guid.Parse(HttpContext.Current.User.Identity.Name)

            HistoricoClienteDTO.Favoritos = AutoMapper.Mapper.Map(Of List(Of Libro))(bll.GetAll().Where(Function(o) o.Favoritos.Where(Function(e) e.ClienteId = idCliente).Count = 1).ToList)
            HistoricoClienteDTO.Leidos = AutoMapper.Mapper.Map(Of List(Of Libro))(bll.GetAll().Where(Function(o) o.Leidos.Where(Function(e) e.ClienteId = idCliente).Count = 1).ToList)


            Return Ok(HistoricoClienteDTO)
            ' Return Ok(bll.GetAll().ToList)
        End Function


        <HttpPost>
        <AllowAnonymous>
        Public Function ConsultarTodos() As IHttpActionResult
            Return Ok(AutoMapper.Mapper.Map(Of IList(Of Libro))(bll.GetAll().ToList))
            ' Return Ok(bll.GetAll().ToList)
        End Function

        <HttpPut()>
        Public Function Guardar(libro As Libro) As IHttpActionResult

            'If validar(libro) Then

            If libro.Id.Equals(Guid.Empty) Then
                libro.FechaCreacion = Date.Now
            End If

            bll.SaveOrUpdate(libro)

            Return Ok(libro)

            'Else

            'Return BadRequest()

            'End If


        End Function

        'Private Function validar(libro As Libro) As Boolean

        '    Dim _lista As New List(Of Libro)

        '    If libro.Id.Equals(Guid.Empty) Then


        '        _lista = bll.GetAll().ToList()

        '        For Each item As Libro In _lista
        '            If item.Titulo = libro.Titulo Then
        '                Return False
        '            ElseIf item.ISBN = libro.ISBN Then
        '                Return False
        '            End If
        '        Next

        '    End If

        '    Return True

        'End Function

        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            Dim _librosNewsletter As New List(Of LibroNewsletter)
            _librosNewsletter = bllLibroNewsletter.GetAll().Where(Function(p) p.Libro.Id.Equals(id)).ToList()
            For Each ln As LibroNewsletter In _librosNewsletter
                bllLibroNewsletter.Delete(ln.Id)
            Next

            bll.Delete(id)
            Return Ok()

        End Function

        <HttpHead>
        Public Sub Enviar(id As Guid)

            Dim _libro As New Libro
            Dim _categoriasClientes As New List(Of CategoriaCliente)
            Dim _usuarios As New List(Of Usuario)
            Dim _ruta As String

            _libro = bll.Obtain(id)
            _libro.enviado = True

            _categoriasClientes = bllCategoriaCliente.GetAll().Where(Function(c) c.Categoria.Id.Equals(_libro.Categoria.Id)).ToList
            For Each cc As CategoriaCliente In _categoriasClientes
                _usuarios.Add(cc.Cliente)
            Next

            _ruta = HttpContext.Current.Server.MapPath("/novedades.html")
            bllEmail.EnviarNovedad(_usuarios, _libro, _ruta)

            bll.SaveOrUpdate(_libro)

        End Sub


    End Class
End Namespace