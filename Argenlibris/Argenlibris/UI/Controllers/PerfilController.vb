﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad
Imports AutoMapper


Namespace Controllers
    Public Class PerfilController
        Inherits ApiController

        Dim bll As IUsuarioBLL
        Dim bllRol As IRolBLL
        Dim bllPermiso As IPermisoBLL
        Dim bllEmail As IEmailBLL

        Public Sub New(bllUsuario As IUsuarioBLL, _bllRol As IRolBLL, _bllPermiso As IPermisoBLL, email_ As EmailBLL)
            bllPermiso = _bllPermiso
            bll = bllUsuario
            bllRol = _bllRol
            bllEmail = email_
        End Sub

        '<HttpPost()>
        'Public Function consultar(dto As ConsultarUsuarioDTO) As IHttpActionResult 'IQueryable(Of Usuario)

        '    Return Ok(bll.GetAll.Where(Function(u) u.NombreUsuario.Equals(dto.Nombre)).ToList())


        'End Function

        <HttpPut()>
        <AllowAnonymous>
        Public Function guardar(dto As ConsultarPerfilDTO) As IHttpActionResult 'As Usuario

            Dim _usuario As New Usuario
            _usuario = Mapper.Map(Of Usuario)(dto)
            _usuario.Activo = True
            bll.SaveOrUpdate(_usuario)
            Return Ok(_usuario)

        End Function

      
        '<HttpGet()>
        ' <ActionName("activar")>
        <HttpPost()>
        <AllowAnonymous>
        Public Function ActivarMethod(id As Guid) As IHttpActionResult 'Usuario


            Dim _usuario As Usuario
            _usuario = bll.Obtain(id)

            If Not _usuario Is Nothing Then
                _usuario.Activo = True
                bll.SaveOrUpdate(_usuario)
            End If

            Return Ok(Mapper.Map(Of Usuario)(_usuario))

        End Function
        '<Authorize>
        <HttpGet()>
        Public Function Obtener(id As Guid) As IHttpActionResult 'Usuario


            Dim _usuario As Usuario
            _usuario = bll.Obtain(id)
            Return Ok(Mapper.Map(Of Usuario)(_usuario))

        End Function

        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            Dim _usuario As Usuario
            _usuario = bll.Obtain(id)
            bll.Delete(_usuario.Id)
            Return Ok()

        End Function

    End Class
End Namespace