﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers
    Public Class PermisoController
        Inherits ApiController
        Dim bll As IBLL(Of Permiso)


        Public Sub New(bllrol As IBLL(Of Permiso))
            bll = bllrol
        End Sub

        
        <HttpPost>
        Public Function ConsultarTodos() As IHttpActionResult
            Return Ok(bll.GetAll().ToList)
        End Function


        <HttpPut()>
        Public Function guardar(dto As ConsultarPermisoDTO) As Permiso
            Dim _permiso As New Permiso
            _permiso.Nombre = dto.Nombre
            bll.SaveOrUpdate(_permiso)
            Return bll.Obtain(_permiso.Id)
        End Function

        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            Dim _permiso As Permiso
            _permiso = bll.Obtain(id)
            bll.Delete(_permiso.Id)
            Return Ok()

        End Function

    End Class
End Namespace