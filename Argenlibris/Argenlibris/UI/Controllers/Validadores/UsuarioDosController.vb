﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad
Imports AutoMapper


Namespace Controllers
    Public Class UsuarioDosController
        Inherits ApiController

        Dim bll As IUsuarioBLL
        Dim bllRol As IRolBLL
        Dim bllPermiso As IPermisoBLL
        Dim bllUsuarioRol As IUsuarioRolBLL
        Dim bllEmail As IEmailBLL

        Public Sub New(bllUsuario As IUsuarioBLL, _bllRol As IRolBLL, _bllPermiso As IPermisoBLL, _bllUsuarioRol As IUsuarioRolBLL, email_ As EmailBLL)
            bllPermiso = _bllPermiso
            bll = bllUsuario
            bllRol = _bllRol
            bllUsuarioRol = _bllUsuarioRol
            bllEmail = email_
        End Sub


        '<Route("nombre")>
        <HttpPost()>
        <AllowAnonymous>
        Public Function consultar(<FromBody> dto As ConsultarUsuarioDTO) As IHttpActionResult 'IQueryable(Of Usuario)

            If Not dto.NombreUsuario Is Nothing Then
                'busco usuario por nombre de usuario
                Return Ok(bll.GetAll.Where(Function(u) u.NombreUsuario.Equals(dto.NombreUsuario)).FirstOrDefault)

            ElseIf Not dto.Email Is Nothing Then
                'busco usuario por email
                Return Ok(bll.GetAll.Where(Function(u) u.Email.Equals(dto.Email)).FirstOrDefault)

            End If

            Return BadRequest()

        End Function

        <HttpPut()>
          <AllowAnonymous>
        Public Function guardar(dto As ConsultarUsuarioDTO) As IHttpActionResult 'As Usuario

            'Cuando actualizo el perfil
            If Not dto.Id.Equals(Guid.Empty) Then
                Dim _usuario As New Usuario
                _usuario = Mapper.Map(Of Usuario)(dto)
                _usuario.Activo = True
                _usuario.Direccion = dto.Direccion
                bll.CambiarPerfil(_usuario)
                'bll.SaveOrUpdate(_usuario)
                Return Ok(_usuario)
            Else
                'cuando registro un usuario
                Return registrarusuario(dto)
            End If



            Return Nothing


        End Function

        Private Function registrarusuario(dto As ConsultarUsuarioDTO) As IHttpActionResult


            'no se pueden repetir nombre de usuarios
            Dim _usuario As New Usuario

            If bll.GetAll.Where(Function(u) u.NombreUsuario.Equals(dto.NombreUsuario)).Count < 1 Then
                '_usuario.Roles.Add(_rol)
                _usuario.NombreUsuario = dto.NombreUsuario
                _usuario.Nombre = dto.Nombre
                _usuario.Apellido = dto.Apellido
                _usuario.Password = dto.Password
                _usuario.Email = dto.Email
                _usuario.Activo = False
                _usuario.FechaNacimiento = dto.FechaNacimiento
                _usuario.Direccion = dto.Direccion
                bll.SaveOrUpdate(_usuario)

                'bllEmail.EnviarMailDeRegistro(_usuario, HttpContext.Current.Request.UrlReferrer.ToString)

                Return Ok(_usuario)

            End If

        End Function

        '<HttpGet()>
        ' <ActionName("activar")>
        <HttpPost()>
        <ActionName("activar")>
        <AllowAnonymous>
        Public Function ActivarMethod(id As Guid) As IHttpActionResult 'Usuario


            Dim _usuario As Usuario
            _usuario = bll.Obtain(id)

            If Not _usuario Is Nothing Then
                _usuario.Activo = True
                bll.SaveOrUpdate(_usuario)
            End If

            Return Ok(Mapper.Map(Of Usuario)(_usuario))

        End Function

        '<Authorize>
        <HttpGet()>
          <AllowAnonymous>
        Public Function Obtener(id As Guid) As IHttpActionResult 'Usuario


            Dim _usuario As Usuario
            _usuario = bll.Obtain(id)
            Return Ok(Mapper.Map(Of Usuario)(_usuario))

        End Function

        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            Dim _usuario As Usuario
            _usuario = bll.Obtain(id)
            bll.Delete(_usuario.Id)
            Return Ok()

        End Function

    End Class
End Namespace