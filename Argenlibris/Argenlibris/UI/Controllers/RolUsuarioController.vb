﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad
Imports AutoMapper


Namespace Controllers
    Public Class RolUsuarioController
        Inherits ApiController


        Dim bllRol As IRolBLL
        Dim bllUsuarioRol As IUsuarioRolBLL


        Public Sub New(_bllRol As IRolBLL, _bllUsuarioRol As IUsuarioRolBLL)

            bllRol = _bllRol
            bllUsuarioRol = _bllUsuarioRol

        End Sub



        <HttpPost()>
        Public Function consultar() As IHttpActionResult 'IQueryable(Of Usuario)

            Return Ok(bllRol.GetAll().ToList)


        End Function

        <HttpPut()>
          <AllowAnonymous>
        Public Function guardar(usuarioRol As UsuarioRol) As IHttpActionResult 'As Usuario

            Dim _agregar As Boolean = True

            Dim _usuarioRolAnt As New List(Of UsuarioRol)
            _usuarioRolAnt = bllUsuarioRol.GetAll().Where(Function(r) r.Usuario.Id.Equals(usuarioRol.Usuario.Id)).ToList

            For Each ur As UsuarioRol In _usuarioRolAnt

                If ur.Rol.Id = usuarioRol.Rol.Id Then
                    _agregar = False
                End If

            Next

            If _agregar = True Then
                bllUsuarioRol.SaveOrUpdate(usuarioRol)
            End If
            Return Ok()

            'For Each ur As UsuarioRol In dto.UsuarioRol
            '    bllUsuarioRol.SaveOrUpdate(ur)
            'Next
            'Return Ok()


        End Function


        <HttpGet()>
          <AllowAnonymous>
        Public Function Obtener(id As Guid) As IHttpActionResult 'Usuario

            Return Ok(Mapper.Map(Of UsuarioRol)(bllUsuarioRol.Obtain(id)))

        End Function

        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            bllUsuarioRol.Delete(id)
            Return Ok()

        End Function

    End Class
End Namespace