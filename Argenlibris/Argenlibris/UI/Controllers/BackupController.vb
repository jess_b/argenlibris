﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports System.IO
Imports Argenlibris.Domain
Imports NHibernate
Imports NHibernate.Context

Public Class BackupController
    Inherits ApiController
    Dim bll As IBackupBLL

    Dim session As ISession
    Public Sub New(bll_ As IBackupBLL)
        bll = bll_

    End Sub


    ' GET api/<controller>
    <RolesValidos(EnumPermiso.BitacoraPuedeGestionar)>
    Public Function GetValues() As IHttpActionResult
        Return Ok(bll.GetAll().ToList)
    End Function

    ' GET api/<controller>/5
    Public Function GetValue(ByVal id As Guid) As IHttpActionResult
        Dim s As ISessionFactory = GlobalConfiguration.Configuration.DependencyResolver.GetService(GetType(ISessionFactory))

        s.GetCurrentSession.Transaction.Commit()
        bll.restoreBackup(id)
       

        Return Ok(id)
    End Function



    ' PUT api/<controller>/5
    Public Function PutValue() As IHttpActionResult
        'para hacer un backup


        Dim s As ISessionFactory = GlobalConfiguration.Configuration.DependencyResolver.GetService(GetType(ISessionFactory))

        s.GetCurrentSession.Transaction.Commit()

        Try



            Dim file As String = bll.doBackup()
            s.OpenSession()

            s.GetCurrentSession.Transaction.Begin()

            Dim bup As New BackupFile
            bup.Fecha = Date.Now
            bup.Nombre = file

            bll.SaveOrUpdate(bup)


            Return Ok(bup)
        Catch e As Exception
            Return InternalServerError()
        End Try

    End Function

    ' DELETE api/<controller>/5
    Public Sub DeleteValue(ByVal id As Integer)

    End Sub
End Class
