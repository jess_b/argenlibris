﻿Imports System.Net
Imports System.Web.Http
Imports Argenlibris.Domain
Imports BLL

Public Class TraductordosController
    Inherits ApiController

    Dim bll As IBLL(Of Palabra)
    Public Sub New(bllTraductor As IBLL(Of Palabra))
        bll = bllTraductor
    End Sub

    <RolesValidos(EnumPermiso.IdiomaPuedeGestionar)>
    Public Function GetValues(id As Guid) As IHttpActionResult
        Return Ok(bll.GetAll().Where(Function(o) o.IdIdioma.Equals(id)).ToList)

    End Function

    <AllowAnonymous>
    Public Function PostValue(ByVal id As Guid, <FromBody()> ByVal value As List(Of String)) As List(Of String)

        Dim _listaTraducida As New List(Of String)
        Dim _lista As IQueryable(Of Palabra)
        _lista = bll.GetAll().Where(Function(u) u.IdIdioma.Equals(id))

        For Each _palabra As String In value
            Dim t As Palabra

            t = _lista.Where(Function(o) o.Clave.Equals(_palabra)).FirstOrDefault
            If t Is Nothing Then
                _listaTraducida.Add(_palabra)
            Else
                _listaTraducida.Add(t.Valor)
            End If
        Next
        Return _listaTraducida

    End Function



End Class
