﻿Imports System.Net
Imports System.Web.Http
Imports Argenlibris.Domain
Imports BLL

Public Class TraductorController
    Inherits ApiController

    Dim bll As IBLL(Of Palabra)
    Public Sub New(bllTraductor As IBLL(Of Palabra))
        bll = bllTraductor
    End Sub

    ' GET api/<controller>
    Public Function GetValues(id As Guid) As IHttpActionResult
        Return Ok(bll.GetAll().Where(Function(o) o.IdIdioma.Equals(id)).ToList)

    End Function


    ' POST api/<controller>
    <RolesValidos(EnumPermiso.IdiomaPuedeGestionar)>
    Public Function PostValue(ByVal id As Guid, <FromBody()> ByVal value As List(Of String)) As List(Of String)

        Dim _listaTraducida As New List(Of String)
        Dim _lista As IQueryable(Of Palabra)
        _lista = bll.GetAll().Where(Function(u) u.IdIdioma.Equals(id))

        For Each _palabra As String In value
            Dim t As Palabra

            t = _lista.Where(Function(o) o.Clave.Equals(_palabra)).FirstOrDefault
            If t Is Nothing Then
                _listaTraducida.Add(_palabra)
            Else
                _listaTraducida.Add(t.Valor)
            End If
        Next
        Return _listaTraducida

    End Function

    ' PUT api/<controller>/5
    <RolesValidos(EnumPermiso.BitacoraPuedeGestionar)>
    Public Sub PutValue(<FromBody()> ByVal value As List(Of Palabra))
        For Each p As Palabra In value
            bll.SaveOrUpdate(p)

        Next
    End Sub

    ' DELETE api/<controller>/5
    Public Sub DeleteValue(ByVal id As Integer)

    End Sub
End Class
