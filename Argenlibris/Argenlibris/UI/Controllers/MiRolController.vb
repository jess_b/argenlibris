﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers
    Public Class MiRolController
        Inherits ApiController

        Dim bll As IBLL(Of Rol)
        Dim bllUsuarioRol As IUsuarioRolBLL

        Public Sub New(bllrol As IBLL(Of Rol), bllUsuarioRol_ As IUsuarioRolBLL)
            bll = bllrol
            bllUsuarioRol = bllUsuarioRol_
        End Sub

        <HttpPost>
        Public Function Consultar() As IHttpActionResult

            'rol actual
            Dim _id As Guid = Guid.Parse(HttpContext.Current.User.Identity.Name)

            Dim _UsuarioRol As New List(Of UsuarioRol)
            _UsuarioRol = (AutoMapper.Mapper.Map(Of IList(Of UsuarioRol))(bllUsuarioRol.GetAll().Where(Function(o) o.Usuario.Id.Equals(_id)).ToList()))

            Dim _Roles As New List(Of Rol)
            For Each _ur As UsuarioRol In _UsuarioRol
                _Roles.Add(_ur.Rol)
            Next

            Return Ok(_Roles)
            'Return Ok(bll.GetAll().ToList)
        End Function

        <HttpGet>
        Public Function Consultar(id As Guid) As IHttpActionResult

            Dim _id As Guid

            _id = id
            'rol que pregunto
            Dim _UsuarioRol As New List(Of UsuarioRol)
            _UsuarioRol = (AutoMapper.Mapper.Map(Of IList(Of UsuarioRol))(bllUsuarioRol.GetAll().Where(Function(o) o.Usuario.Id.Equals(_id)).ToList()))

            Dim _Roles As New List(Of Rol)
            For Each _ur As UsuarioRol In _UsuarioRol
                _Roles.Add(_ur.Rol)
            Next

            Return Ok(_Roles)
            'Return Ok(bll.GetAll().ToList)
        End Function

        <HttpPut()>
        <AllowAnonymous>
        Public Function guardar(rol_ As Rol) As Rol
            'Public Function guardar(dto As ConsultarRolDTO) As Rol
            'Dim _rol As New Rol
            '_rol.Nombre = dto.Nombre
            'bll.SaveOrUpdate(_rol)
            'Return bll.Obtain(_rol.Id)
            Return (bll.SaveOrUpdate(rol_))
        End Function

        <HttpDelete>
        <AllowAnonymous>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            Dim _rol As Rol
            _rol = bll.Obtain(id)
            bll.Delete(_rol.Id)
            Return Ok()

        End Function

    End Class

End Namespace