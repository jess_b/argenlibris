﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO

Namespace Controllers
    Public Class idiomaController
        Inherits ApiController

        Dim bll As IBLL(Of Idioma)
        Dim bllPalabra As IPalabraBLL
        Public Sub New(bllIdioma As IBLL(Of Idioma), bllPalabra_ As IPalabraBLL)
            bll = bllIdioma
            bllPalabra = bllpalabra_
        End Sub

        <HttpGet>
        <AllowAnonymous>
        Public Function Obtain() As IHttpActionResult
            Return Ok(bll.GetAll().ToList)
        End Function

        <HttpPost>
        <RolesValidos(EnumPermiso.IdiomaPuedeGestionar)>
        Public Function Consultar() As IHttpActionResult
            Return Ok(bll.GetAll().ToList)
        End Function


        <HttpPut>
        <RolesValidos(EnumPermiso.IdiomaPuedeGestionar)>
        Public Function Crear(dto As IdiomaDTO) As IHttpActionResult

            If validar(dto)Then

                Dim _idioma As New Idioma
                _idioma.Codigo = dto.codigo
                _idioma.Descripcion = dto.descripcion
                bll.SaveOrUpdate(_idioma)

                Dim _idiomaDefault As Idioma = bll.GetAll().FirstOrDefault

                Dim _listaPalabras As New List(Of Palabra)
                _listaPalabras = bllPalabra.GetAll().Where(Function(o) o.IdIdioma.Equals(_idiomaDefault.Id)).ToList

                'Se cargan las palabras para el idioma nuevo, a partir del idioma default
                Dim _palabraNueva As Palabra
                For Each _palabra As Palabra In _listaPalabras
                    _palabraNueva = New Palabra
                    _palabraNueva.Valor = _palabra.Valor
                    _palabraNueva.Clave = _palabra.Clave
                    _palabraNueva.IdIdioma = _idioma.Id
                    bllPalabra.SaveOrUpdate(_palabraNueva)
                Next



                Return Ok(_idioma)

            Else
                Return BadRequest()
            End If

        End Function


        Private Function validar(dto As IdiomaDTO) As Boolean

            Dim _lista As New List(Of Idioma)

            If dto.id.Equals(Guid.Empty) Then


                _lista = bll.GetAll().ToList()

                For Each item As Idioma In _lista
                    If item.Codigo = dto.codigo Then
                        Return False
                    ElseIf item.Descripcion = dto.descripcion Then
                        Return False
                    End If
                Next

            End If

            Return True

        End Function

        <HttpDelete>
        <AllowAnonymous>
        Public Function Eliminar(id As Guid) As IHttpActionResult
            Dim _idioma As New Idioma
            _idioma = bll.Obtain(id)

            If Not _idioma.Codigo = "es" Then
                bll.Delete(_idioma.Id)
            End If

            Dim _listaPalabras As New List(Of Palabra)
            _listaPalabras = bllPalabra.GetAll().Where(Function(o) o.IdIdioma.Equals(_idioma.Id)).ToList
            For Each _palabra As Palabra In _listaPalabras
                bllPalabra.Delete(_palabra.Id)
            Next

            Return Ok()

        End Function

        '<RolesValidos(EnumPermiso.IdiomaPuedeGestionar)>
        <HttpGet>
        <AllowAnonymous>
        Public Function Obtener(id As Guid) As IHttpActionResult
            Return Ok(bll.Obtain(id))
        End Function


    End Class


End Namespace