﻿Imports System.Net
Imports System.Web.Http
Imports System.IO

Public Class ImageController
    Inherits ApiController


    ' PUT api/<controller>/5
    <HttpPut>
    Public Sub PutValue(Id As Guid)


        If HttpContext.Current.Request.Files.Count = 1 Then

            Dim o As Object

            o = HttpContext.Current.Request.Files(0)

            If Not o Is Nothing And o.ContentLength > 0 Then

                Dim d As String
                Dim dd As String
                d = o.ContentType()

                If d = "image/jpeg" Then
                    dd = "jpg"
                End If

                Dim filename As String = Id.ToString & "." & dd

                o.saveAs(Path.Combine(HttpContext.Current.Server.MapPath("~/uploads"), filename))


            End If



        End If





    End Sub


End Class
