﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers
    Public Class RolController
        Inherits ApiController

        Dim bll As IBLL(Of Rol)
        Dim bllRolPermiso As IRolPermisoBLL
        Dim bllUsuarioRol As IUsuarioRolBLL

        Public Sub New(bllrol As IBLL(Of Rol), bllRolPermiso_ As IRolPermisoBLL, bllUsuarioRol_ As IUsuarioRolBLL)
            bll = bllrol
            bllRolPermiso = bllRolPermiso_
            bllUsuarioRol = bllUsuarioRol_
        End Sub

        '<HttpPost()>
        '<ActionName("roles")>
        '<AllowAnonymous>
        'Public Function TraerRoles(id As Guid) As IHttpActionResult
        '    Dim _usuariosRoles As New List(Of UsuarioRol)
        '    _usuariosRoles = bllUsuarioRol.GetAll().Where(Function(u) u.Usuario.Id.Equals(id)).ToList()

        '    Dim _roles As New List(Of Rol)
        '    For Each ur As UsuarioRol In _usuariosRoles
        '        _roles.Add(ur.Rol)
        '    Next

        '    Return Ok(_roles)

        'End Function

        <HttpGet>
        Public Function TraerPermisoPorRol(id As Guid) As IHttpActionResult
            Dim _permisos As New List(Of Permiso)
            Dim _rolPermiso As New List(Of RolPermiso)

            _rolPermiso = bllRolPermiso.GetAll().Where(Function(h) h.Rol.Id.Equals(id)).ToList()

            For Each RolPermiso As RolPermiso In _rolPermiso
                _permisos.Add(RolPermiso.Permiso)
            Next
            Return Ok(AutoMapper.Mapper.Map(Of List(Of Permiso))(_permisos))

        End Function


        <HttpPost>
        <RolesValidos(EnumPermiso.BitacoraPuedeGestionar)>
        Public Function ConsultarTodos() As IHttpActionResult
            Return Ok(AutoMapper.Mapper.Map(Of IList(Of Rol))(bll.GetAll().ToList()))
            'Return Ok(bll.GetAll().ToList)
        End Function

        <HttpPut()>
        <AllowAnonymous>
        Public Function guardar(rol_ As ConsultarRolPermisoDTO) As IHttpActionResult

            'If validar(rol_) Then

            Dim _rol As New Rol
            _rol.Id = rol_.Id
            _rol.Nombre = rol_.Nombre

            bll.SaveOrUpdate(_rol)

            'error permiso rol
            Dim _rolPermisoAnt As New List(Of RolPermiso)
            Dim _agregar As Boolean

            _rolPermisoAnt = bllRolPermiso.GetAll().Where(Function(r) r.Rol.Id.Equals(_rol.Id)).ToList

            Dim _rolPermiso As RolPermiso
            If Not rol_.Permisos Is Nothing Then

                For Each _permiso As Permiso In rol_.Permisos
                    _agregar = True
                    For Each rolpermisoAnt As RolPermiso In _rolPermisoAnt
                        If rolpermisoAnt.Permiso.Id = _permiso.Id Then
                            _agregar = False
                        End If
                    Next

                    If _agregar = True Then
                        _rolPermiso = New RolPermiso
                        _rolPermiso.Permiso = _permiso
                        _rolPermiso.Rol = _rol
                        bllRolPermiso.SaveOrUpdate(_rolPermiso)
                    End If

                Next

            End If

            Return Ok(_rol)

            'Else

            'Return BadRequest()

            'End If


        End Function

        'Private Function validar(rol_ As ConsultarRolPermisoDTO) As Boolean

        '    Dim _lista As New List(Of Rol)

        '    'If rol_.Id.Equals(Guid.Empty) Then

        '    _lista = bll.GetAll().ToList()

        '    For Each item As Rol In _lista
        '        If item.Nombre = rol_.Nombre Then
        '            Return False
        '        End If
        '    Next

        '    'End If

        '    Return True

        'End Function

        <HttpDelete>
        Public Function Eliminar(id As Guid) As IHttpActionResult

            Dim _rol As Rol
            _rol = bll.Obtain(id)
            If _rol.Nombre = "Administrador" Then
                'No se puede eliminar administrador
                Return BadRequest()
            End If


            Dim _usuarioRol As List(Of UsuarioRol)
            Dim _id As Guid = Guid.Parse(HttpContext.Current.User.Identity.Name)
            _usuarioRol = bllUsuarioRol.GetAll().Where(Function(r) r.Usuario.Id.Equals(_id)).Where(Function(h) h.Rol.Id.Equals(id)).ToList()
            If _usuarioRol.Count > 0 Then
                'No se puede eliminar el rol actual
                Return BadRequest()
            End If


            _usuarioRol = bllUsuarioRol.GetAll().Where(Function(h) h.Rol.Id.Equals(id)).ToList()
            If _usuarioRol.Count > 0 Then
                'No se puede eliminar un rol que tiene asignado un usuario
                Return BadRequest()
            End If

            'borro primero los roles-permiso
            Dim _rolPermisoAnt As New List(Of RolPermiso)
            _rolPermisoAnt = bllRolPermiso.GetAll().Where(Function(r) r.Rol.Id.Equals(id)).ToList
            For Each ro As RolPermiso In _rolPermisoAnt
                bllRolPermiso.Delete(ro.Id)
            Next

            'borro el rol
            bll.Delete(_rol.Id)
            Return Ok()

        End Function

    End Class


End Namespace