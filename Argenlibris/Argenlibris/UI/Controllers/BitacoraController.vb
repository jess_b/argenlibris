﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers
    Public Class BitacoraController
        Inherits ApiController

        Dim bll As IBLL(Of Bitacora)
        Public Sub New(bllbitacora As IBLL(Of Bitacora))
            bll = bllbitacora
        End Sub

        <HttpPost>
        <RolesValidos(EnumPermiso.BitacoraPuedeGestionar)>
        Public Function ConsultarTodos(dto As ConsultarBitacoraDTO) As IHttpActionResult

            If dto.Usuario.Equals(Guid.Empty) Then

                If dto.TipoOperacion = Nothing Then

                    Return Ok(AutoMapper.Mapper.Map(Of IList(Of Bitacora)) _
       (bll.GetAll().Where(Function(o) o.Fecha.Date >= dto.FechaDesde).Where(Function(p) p.Fecha.Date <= dto.FechaHasta)).ToList)

                Else

                    Return Ok(AutoMapper.Mapper.Map(Of IList(Of Bitacora)) _
(bll.GetAll().Where(Function(o) o.Fecha.Date >= dto.FechaDesde).Where(Function(p) p.Fecha.Date <= dto.FechaHasta).Where(Function(m) m.Mensaje.ToString.Equals(dto.TipoOperacion.ToString))).ToList)

                    '             Return Ok(AutoMapper.Mapper.Map(Of IList(Of Bitacora)) _
                    '(bll.GetAll().Where(Function(m) m.Mensaje.ToString.Equals(dto.TipoOperacion.ToString))).ToList)

                    '             Return Ok(AutoMapper.Mapper.Map(Of IList(Of Bitacora)) _
                    '(bll.GetAll().Where(Function(m) m.Mensaje.ToString.Equals(dto.TipoOperacion.ToString)).Where(Function(o) o.Fecha.Date >= dto.FechaDesde).Where(Function(p) p.Fecha.Date <= dto.FechaHasta).Where(Function(q) q.Usuario.Id.Equals(dto.Usuario))).ToList)


                End If
                

            Else

                If dto.TipoOperacion = Nothing Then

                    Return Ok(AutoMapper.Mapper.Map(Of IList(Of Bitacora)) _
       (bll.GetAll().Where(Function(o) o.Fecha.Date >= dto.FechaDesde).Where(Function(p) p.Fecha.Date <= dto.FechaHasta).Where(Function(q) q.Usuario.Id.Equals(dto.Usuario))).ToList)

                Else

                    Return Ok(AutoMapper.Mapper.Map(Of IList(Of Bitacora)) _
       (bll.GetAll().Where(Function(o) o.Fecha.Date >= dto.FechaDesde).Where(Function(p) p.Fecha.Date <= dto.FechaHasta).Where(Function(q) q.Usuario.Id.Equals(dto.Usuario)).Where(Function(m) m.Mensaje.ToString.Equals(dto.TipoOperacion.ToString))).ToList)


                End If
                
            End If


        End Function

        <HttpGet>
        <RolesValidos(EnumPermiso.BitacoraPuedeGestionar)>
        Public Function Obtener(id As Guid) As IHttpActionResult
            Return Ok(bll.Obtain(id))
        End Function

        '<RolesValidos(EnumPermiso.BitacoraPuedeGestionar)>

        '<HttpPut>
        '<AllowAnonymous>
        'Public Function consultar(dto As ConsultarBitacoraDTO) As IHttpActionResult

        '    'Return Ok(bll.GetAll().Where(Function(o) o.Fecha.Equals(dto.Fecha)).Where(Function(p) p.TipoDeMensaje.Equals(dto.TipoDeMensaje)).FirstOrDefault())

        '    Return Ok(bll.GetAll.Where(Function(u) u.Fecha.Equals(dto.Fecha)).ToList())


        'End Function
    End Class
End Namespace