﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad


Namespace Controllers
    Public Class AuthController
        Inherits ApiController

        Dim bll As IUsuarioBLL

        Dim bllIdioma As IIdiomaBLL
        Dim bllPalabra As IPalabraBLL

        Dim bllRol As IRolBLL
        Dim bllPermiso As IPermisoBLL
        Dim bllUsuarioRol As IUsuarioRolBLL
        Dim bllRolPermiso As IRolPermisoBLL

        Public Sub New(bllUsuario As IUsuarioBLL, bllidioma_ As IIdiomaBLL, bllpalabra_ As IPalabraBLL, bllRol_ As IRolBLL, bllPermiso_ As IPermisoBLL, bllUsuarioRol_ As IUsuarioRolBLL, bllRolPermiso_ As IRolPermisoBLL)

            bll = bllUsuario
            bllIdioma = bllidioma_
            bllPalabra = bllpalabra_
            bllRol = bllRol_
            bllUsuarioRol = bllUsuarioRol_
            bllPermiso = bllPermiso_
            bllRolPermiso = bllRolPermiso_

        End Sub

        <HttpPost()>
        <AllowAnonymous>
        Public Function consultar(dto As ConsultarUsuarioDTO) As IHttpActionResult


            VerificarIdiomas()
            VerificarUsuarios()

            Dim usuario As Usuario

            dto.PasswordNueva = Encriptador.GetInstance().EncriptarAHash(dto.PasswordNueva)

            'primera vez
            'If _flag = False Then


            'Try
            usuario = bll.GetAll().Where(Function(o) o.NombreUsuario.Equals(dto.NombreUsuario)).Where(Function(p) p.Password.Equals(dto.PasswordNueva)).FirstOrDefault()
            'Catch ex As Exception

            'End Try


            If (usuario Is Nothing) Then
                Return Unauthorized()
            Else
                If usuario.Activo Then
                    Dim token As String = ManejadorToken.Login(usuario.Id)
                    Dim response As New LoginResponseDTO
                    response.Token = token
                    response.UsuarioId = usuario.Id

                    Return Ok(response)
                Else
                    Return NotFound()
                    'Return Unauthorized()
                End If

            End If

            'End If

        End Function

        Private Sub VerificarIdiomas()

            Dim _id As Guid

            If Not bllIdioma.GetAll.Count > 0 Then
                _id = CargarIdiomaDefault()
                CargarPalabrasDefault(_id)
            End If

        End Sub

        Private Function CargarIdiomaDefault() As Guid

            Dim _idioma As New Idioma
            _idioma.Codigo = "es"
            _idioma.Descripcion = "Español"

            bllIdioma.SaveOrUpdate(_idioma)

            Return _idioma.Id

        End Function

        Private Sub CargarPalabrasDefault(id_ As Guid)

            CargarSideBar(id_)

            CargarPerfil(id_)

            CargarTablero(id_)

            CargarIdiomas(id_)

            CargarTraducciones(id_)

            CargarBitacora(id_)

            CargarFooter(id_)

            CargarProductos(id_)

            CargarPedidos(id_)

            CargarEncuestas(id_)

            CargarCategoria(id_)

            CargarReclamos(id_)

            CargarCtaCte(id_)

            CargarTarjeta(id_)

            CargarVentas(id_)

            CargarGestionUsuarios(id_)

            CargarNovedades(id_)

        End Sub

#Region "Traducciones"

        Private Sub CargarSideBar(id_ As Guid)
            'de 0 a 6
            Dim _Palabra As New Palabra
            _Palabra.Clave = "Perfil"
            _Palabra.Valor = "Perfil"

            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Salir"
            _Palabra.Valor = "Salir"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Tablero"
            _Palabra.Valor = "Tablero"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Seguridad"
            _Palabra.Valor = "Seguridad"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Gestion de Idiomas"
            _Palabra.Valor = "Gestion de Idiomas"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Gestion de Traducciones"
            _Palabra.Valor = "Gestion de Traducciones"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Gestion de Bitacora"
            _Palabra.Valor = "Gestion de Bitacora"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

        End Sub

        Private Sub CargarPerfil(id_ As Guid)
            'de 7 a 17
            Dim _Palabra As New Palabra
            _Palabra.Clave = "Mi perfil"
            _Palabra.Valor = "Mi perfil"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Mantenga su perfil actualizado"
            _Palabra.Valor = "Mantenga su perfil actualizado"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Datos del perfil"
            _Palabra.Valor = "Datos del perfil"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Nombre de usuario"
            _Palabra.Valor = "Nombre de usuario"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Apellido"
            _Palabra.Valor = "Apellido"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Nombre"
            _Palabra.Valor = "Nombre"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Email"
            _Palabra.Valor = "Email"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Fecha de nacimiento"
            _Palabra.Valor = "Fecha de nacimiento"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Contrasenia nueva"
            _Palabra.Valor = "Contrasenia nueva"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Repita la contrasenia"
            _Palabra.Valor = "Repita la contrasenia"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Guardar"
            _Palabra.Valor = "Guardar"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

        End Sub

        Private Sub CargarTablero(id_ As Guid)
            'de 18 a 21

            Dim _Palabra As New Palabra
            _Palabra.Clave = "Resumen"
            _Palabra.Valor = "Resumen"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Logs"
            _Palabra.Valor = "Logs"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Eventos"
            _Palabra.Valor = "Eventos"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Mensajes"
            _Palabra.Valor = "Mensajes"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

        End Sub

        Private Sub CargarIdiomas(id_ As Guid)
            'de 22 a 28
            Dim _Palabra As New Palabra
            _Palabra.Clave = "Aqui encontrara todos los idiomas del sistema"
            _Palabra.Valor = "Aqui encontrara todos los idiomas del sistema"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Idiomas"
            _Palabra.Valor = "Idiomas"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Codigo"
            _Palabra.Valor = "Codigo"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Descripcion"
            _Palabra.Valor = "Descripcion"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Acciones"
            _Palabra.Valor = "Acciones"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Nuevo"
            _Palabra.Valor = "Nuevo"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Idioma"
            _Palabra.Valor = "Idioma"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

        End Sub

        Private Sub CargarTraducciones(id_ As Guid)
            'de 29 a 31
            Dim _Palabra As New Palabra
            _Palabra.Clave = "Aqui encontrara todas las traducciones del sistema"
            _Palabra.Valor = "Aqui encontrara todas las traducciones del sistema"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Clave"
            _Palabra.Valor = "Clave"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Valor"
            _Palabra.Valor = "Valor"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

        End Sub

        Private Sub CargarBitacora(id_ As Guid)
            ' de 32 a 40
            Dim _Palabra As New Palabra
            _Palabra.Clave = "Aqui encontrara bitacora del sistema"
            _Palabra.Valor = "Aqui encontrara bitacora del sistema"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Usuario"
            _Palabra.Valor = "Usuario"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Fecha Desde"
            _Palabra.Valor = "Fecha Desde"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Fecha Hasta"
            _Palabra.Valor = "Fecha Hasta"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Fecha"
            _Palabra.Valor = "Fecha"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Mensaje"
            _Palabra.Valor = "Mensaje"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Tipo Entidad"
            _Palabra.Valor = "Tipo Entidad"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Entidad"
            _Palabra.Valor = "Entidad"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Tipo Mensaje"
            _Palabra.Valor = "Tipo Mensaje"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

        End Sub

        Private Sub CargarCategoria(id_ As Guid)
            '85 a 86
            Dim _Palabra As New Palabra
            _Palabra.Clave = "Gestion de Categorias"
            _Palabra.Valor = "Gestion de Categorias"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Categoria"
            _Palabra.Valor = "Categoria"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

        End Sub

        Private Sub CargarReclamos(id_ As Guid)
            '87 a 94
            Dim _Palabra As New Palabra
            _Palabra.Clave = "Recibido"
            _Palabra.Valor = "Recibido"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Reclamar"
            _Palabra.Valor = "Reclamar"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Reclamo"
            _Palabra.Valor = "Reclamo"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Gestion de Reclamos"
            _Palabra.Valor = "Gestion de Reclamos"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Nota de Credito"
            _Palabra.Valor = "Nota de Credito"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Generar Nota De Credito"
            _Palabra.Valor = "Generar Nota De Credito"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Nota de Debito"
            _Palabra.Valor = "Nota de Debito"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Ventas"
            _Palabra.Valor = "Ventas"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

        End Sub

        Private Sub CargarProductos(id_ As Guid)
            ' de 42 a 64

            Dim _Palabra As New Palabra
            _Palabra.Clave = "Productos"
            _Palabra.Valor = "Productos"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Gestion de Editoriales"
            _Palabra.Valor = "Gestion de Editoriales"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)
            _Palabra = New Palabra
            _Palabra.Clave = "Editorial"
            _Palabra.Valor = "Editorial"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Editoriales"
            _Palabra.Valor = "Editoriales"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Gestion de Pais"
            _Palabra.Valor = "Gestion de Pais"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Pais"
            _Palabra.Valor = "Pais"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Paises"
            _Palabra.Valor = "Paises"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Gestion de Autores"
            _Palabra.Valor = "Gestion de Autores"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Autor"
            _Palabra.Valor = "Autor"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Autores"
            _Palabra.Valor = "Autores"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Gestion de Libros"
            _Palabra.Valor = "Gestion de Libros"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Libro"
            _Palabra.Valor = "Libro"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Libros"
            _Palabra.Valor = "Libros"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "ISBN"
            _Palabra.Valor = "ISBN"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Formato"
            _Palabra.Valor = "Formato"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Titulo"
            _Palabra.Valor = "Titulo"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Sinopsis"
            _Palabra.Valor = "Sinopsis"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Aqui encontrara todas las editoriales del sistema"
            _Palabra.Valor = "Aqui encontrara todas las editoriales del sistema"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Aqui encontrara todos los paises del sistema"
            _Palabra.Valor = "Aqui encontrara todos los paises del sistema"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Aqui encontrara todos los autores del sistema"
            _Palabra.Valor = "Aqui encontrara todos los autores del sistema"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Aqui encontrara todos los libros del sistema"
            _Palabra.Valor = "Aqui encontrara todos los libros del sistema"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Disponibilidad"
            _Palabra.Valor = "Disponibilidad"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Cantidad"
            _Palabra.Valor = "Cantidad"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)
        End Sub

        Private Sub CargarPedidos(id_ As Guid)
            '65 a 78
            Dim _Palabra As New Palabra
            _Palabra.Clave = "Mi cuenta"
            _Palabra.Valor = "Mi cuenta"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Mis Pedidos"
            _Palabra.Valor = "Mis Pedidos"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Pedidos"
            _Palabra.Valor = "Pedidos"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Numero"
            _Palabra.Valor = "Numero"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Total"
            _Palabra.Valor = "Total"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Almacenes"
            _Palabra.Valor = "Almacenes"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Despachado"
            _Palabra.Valor = "Despachado"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Direccion"
            _Palabra.Valor = "Direccion"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Remitos"
            _Palabra.Valor = "Remitos"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Facturas"
            _Palabra.Valor = "Facturas"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Mis Facturas"
            _Palabra.Valor = "Mis Facturas"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Factura"
            _Palabra.Valor = "Factura"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Precio"
            _Palabra.Valor = "Precio"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Cantidad"
            _Palabra.Valor = "Cantidad"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

        End Sub

        Private Sub CargarEncuestas(id_ As Guid)
            'de 79 a 82
            Dim _Palabra As New Palabra
            _Palabra.Clave = "Gestion de Encuestas"
            _Palabra.Valor = "Gestion de Encuestas"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Encuestas"
            _Palabra.Valor = "Encuestas"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)


            _Palabra = New Palabra
            _Palabra.Clave = "Gestion de Preguntas"
            _Palabra.Valor = "Gestion de Preguntas"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)


            _Palabra = New Palabra
            _Palabra.Clave = "Preguntas"
            _Palabra.Valor = "Preguntas"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Respuestas"
            _Palabra.Valor = "Respuestas"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Agregar"
            _Palabra.Valor = "Agregar"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)
        End Sub

        Private Sub CargarFooter(id_ As Guid)
            '41
            Dim _Palabra As New Palabra
            _Palabra.Clave = "Politicas De Seguridad"
            _Palabra.Valor = "Politicas De Seguridad"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)
        End Sub

        Private Sub CargarCtaCte(id_ As Guid)
            '95 a 105
            Dim _Palabra As New Palabra
            _Palabra.Clave = "Mi Cuenta Corriente"
            _Palabra.Valor = "Mi Cuenta Corriente"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Debe"
            _Palabra.Valor = "Debe"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Haber"
            _Palabra.Valor = "Haber"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Observaciones"
            _Palabra.Valor = "Observaciones"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Total"
            _Palabra.Valor = "Total"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Saldo"
            _Palabra.Valor = "Saldo"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Cuenta Corriente"
            _Palabra.Valor = "Cuenta Corriente"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Cuentas Corrientes"
            _Palabra.Valor = "Cuentas Corrientes"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Tipo de Documento"
            _Palabra.Valor = "Tipo de Documento"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Documento"
            _Palabra.Valor = "Documento"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Reclamos"
            _Palabra.Valor = "Reclamos"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)
        End Sub

        Private Sub CargarTarjeta(id_ As Guid)
            '106 a 111
            Dim _Palabra As New Palabra
            _Palabra.Clave = "Tarjeta de Credito"
            _Palabra.Valor = "Tarjeta de Credito"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Numero de Tarjeta"
            _Palabra.Valor = "Numero de Tarjeta"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)


            _Palabra = New Palabra
            _Palabra.Clave = "Fecha de Vencimiento"
            _Palabra.Valor = "Fecha de Vencimiento"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)


            _Palabra = New Palabra
            _Palabra.Clave = "Tipo de Tarjeta"
            _Palabra.Valor = "Tipo de Tarjeta"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Tarjetas"
            _Palabra.Valor = "Tarjetas"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Empresa"
            _Palabra.Valor = "Empresa"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

        End Sub

        Private Sub CargarVentas(id_ As Guid)
            '112 a 117
            Dim _Palabra As New Palabra
            _Palabra.Clave = "Gestion de Newsletters"
            _Palabra.Valor = "Gestion de Newsletters"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Gestion de Pedidos"
            _Palabra.Valor = "Gestion de Pedidos"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Gestion de Remitos"
            _Palabra.Valor = "Gestion de Remitos"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Gestion de Fichas de Opinion"
            _Palabra.Valor = "Gestion de Fichas de Opinion"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Asunto"
            _Palabra.Valor = "Asunto"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Cuerpo"
            _Palabra.Valor = "Cuerpo"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Gestion de Representantes"
            _Palabra.Valor = "Gestion de Representantes"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Representantes"
            _Palabra.Valor = "Representantes"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Representante"
            _Palabra.Valor = "Representante"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Operario de Ventas"
            _Palabra.Valor = "Operario de Ventas"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Reporte de Ventas"
            _Palabra.Valor = "Reporte de Ventas"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Cliente"
            _Palabra.Valor = "Cliente"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)
        End Sub

        Private Sub CargarGestionUsuarios(id_ As Guid)
            '124 a 127
            Dim _Palabra As New Palabra
            _Palabra.Clave = "Gestion de Usuarios"
            _Palabra.Valor = "Gestion de Usuarios"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Aqui podra encontrar y gestionar todos los usuarios del sistema"
            _Palabra.Valor = "Aqui podra encontrar y gestionar todos los usuarios del sistema"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)


            _Palabra = New Palabra
            _Palabra.Clave = "Activo"
            _Palabra.Valor = "Activo"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)


            _Palabra = New Palabra
            _Palabra.Clave = "Rol"
            _Palabra.Valor = "Rol"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Usuarios"
            _Palabra.Valor = "Usuarios"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Gestion de Roles"
            _Palabra.Valor = "Gestion de Roles"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

        End Sub

        Private Sub CargarNovedades(id_ As Guid)
            '131 a 133
            Dim _Palabra As New Palabra
            _Palabra = New Palabra
            _Palabra.Clave = "Gestion de Novedades"
            _Palabra.Valor = "Gestion de Novedades"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra.Clave = "Novedad"
            _Palabra.Valor = "Novedad"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)

            _Palabra = New Palabra
            _Palabra.Clave = "Mostrar"
            _Palabra.Valor = "Mostrar"
            _Palabra.IdIdioma = id_

            bllPalabra.SaveOrUpdate(_Palabra)



        End Sub

#End Region




        Private Sub VerificarUsuarios()

            Dim _permiso As New Permiso
            Dim _rol As New Rol
            Dim _rolPermiso As New RolPermiso
            Dim _usuario As New Usuario
            Dim _usuarioRol As New UsuarioRol

            If bllRol.GetAll.Count < 1 Then


                _rol.Nombre = "Administrador"
                bllRol.SaveOrUpdate(_rol)

                _permiso.Nombre = "puede gestionar usuario"
                _permiso.Permiso = EnumPermiso.UsuarioPuedeGestionar
                bllPermiso.SaveOrUpdate(_permiso)

                _rolPermiso = New RolPermiso
                _rolPermiso.Permiso = _permiso
                _rolPermiso.Rol = _rol
                bllRolPermiso.SaveOrUpdate(_rolPermiso)

                _permiso = New Permiso
                _permiso.Nombre = "puede gestionar idioma"
                _permiso.Permiso = EnumPermiso.IdiomaPuedeGestionar
                bllPermiso.SaveOrUpdate(_permiso)

                _rolPermiso = New RolPermiso
                _rolPermiso.Permiso = _permiso
                _rolPermiso.Rol = _rol
                bllRolPermiso.SaveOrUpdate(_rolPermiso)

                _permiso = New Permiso
                _permiso.Nombre = "puede gestionar bitacora"
                _permiso.Permiso = EnumPermiso.BitacoraPuedeGestionar
                bllPermiso.SaveOrUpdate(_permiso)

                _rolPermiso = New RolPermiso
                _rolPermiso.Permiso = _permiso
                _rolPermiso.Rol = _rol
                bllRolPermiso.SaveOrUpdate(_rolPermiso)

                _permiso = New Permiso
                _permiso.Nombre = "puede gestionar ventas"
                _permiso.Permiso = EnumPermiso.VentasPuedeGestionar
                bllPermiso.SaveOrUpdate(_permiso)

                _rolPermiso = New RolPermiso
                _rolPermiso.Permiso = _permiso
                _rolPermiso.Rol = _rol
                bllRolPermiso.SaveOrUpdate(_rolPermiso)

                _permiso = New Permiso
                _permiso.Nombre = "puede gestionar marketing"
                _permiso.Permiso = EnumPermiso.MarketingPuedeGestionar
                bllPermiso.SaveOrUpdate(_permiso)

                _rolPermiso = New RolPermiso
                _rolPermiso.Permiso = _permiso
                _rolPermiso.Rol = _rol
                bllRolPermiso.SaveOrUpdate(_rolPermiso)

                _permiso = New Permiso
                _permiso.Nombre = "puede gestionar almacenes"
                _permiso.Permiso = EnumPermiso.AlmacenesPuedeGestionar
                bllPermiso.SaveOrUpdate(_permiso)

                _rolPermiso = New RolPermiso
                _rolPermiso.Permiso = _permiso
                _rolPermiso.Rol = _rol
                bllRolPermiso.SaveOrUpdate(_rolPermiso)

                _permiso = New Permiso
                _permiso.Nombre = "puede gestionar Productos"
                _permiso.Permiso = EnumPermiso.ProductosPuedeGestionar
                bllPermiso.SaveOrUpdate(_permiso)

                _rolPermiso = New RolPermiso
                _rolPermiso.Permiso = _permiso
                _rolPermiso.Rol = _rol
                bllRolPermiso.SaveOrUpdate(_rolPermiso)



                _usuario.NombreUsuario = "jess"
                _usuario.Nombre = "Jesica"
                _usuario.Apellido = "Belen"
                _usuario.Password = "luna"
                _usuario.Email = "jess.n.belen@gmail.com"
                _usuario.Activo = True
                _usuario.FechaNacimiento = "24.06.1988"
                bll.SaveOrUpdate(_usuario)


                _usuarioRol.Rol = _rol
                _usuarioRol.Usuario = _usuario
                bllUsuarioRol.SaveOrUpdate(_usuarioRol)

            End If


        End Sub

        <HttpPut()>
        <AllowAnonymous>
        Public Function Recuperar(dto As ConsultarUsuarioDTO) As IHttpActionResult 'As Usuario

            Dim _usuario As New Usuario
            _usuario.Email = dto.Email


            Try
                _usuario = bll.GetAll.Where(Function(u) u.Email.Equals(dto.Email)).First
            Catch ex As Exception
                'si no encuentra el mail del usuario
                Return NotFound()
            End Try


            If Not _usuario Is Nothing Then
                bll.RecuperarPorMail(_usuario)
                Return Ok()
            End If

            Return Nothing


        End Function

    End Class



End Namespace

