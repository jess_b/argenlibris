﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO
Imports Argenlibris.Seguridad

Namespace Controllers
    Public Class MisPermisosController
        Inherits ApiController
        Dim bll As IBLL(Of Permiso)
        Dim blluRol As IUsuarioRolBLL
        Dim _rolPermisoBLL As IRolPermisoBLL

        Public Sub New(bllpermiso As IBLL(Of Permiso), bllurol_ As IUsuarioRolBLL, rolPermisoBLL_ As IRolPermisoBLL)
            bll = bllpermiso
            blluRol = bllurol_
            _rolPermisoBLL = rolPermisoBLL_
        End Sub

        <HttpPost>
        Public Function ConsultarTodos(id As Guid) As IHttpActionResult
            Dim _Permisos As New List(Of Permiso)
            Dim _UsuarioRol As New List(Of UsuarioRol)
            Dim _rolPermiso As New List(Of RolPermiso)
            'Dim _permiso As Permiso

            _UsuarioRol = (AutoMapper.Mapper.Map(Of IList(Of UsuarioRol))(blluRol.GetAll().Where(Function(o) o.Usuario.Id.Equals(id)).ToList()))

            Dim _Roles As New List(Of Rol)
            For Each _ur As UsuarioRol In _UsuarioRol
                ''_Roles.Add(_ur.Rol)

                _rolPermiso = _rolPermisoBLL.GetAll.Where(Function(p) p.Rol.Id.Equals(_ur.Rol.Id)).ToList
                For Each rolpermiso As RolPermiso In _rolPermiso
                    _Permisos.Add(AutoMapper.Mapper.Map(Of Permiso)(rolpermiso.Permiso))
                Next

            Next

            ''For Each _rol As Rol In _Roles
            ''    _rolPermiso = _rolPermisoBLL.GetAll.Where(Function(p) p.Rol.Id.Equals(_rol.Id)).ToList
            ''    For Each rolpermiso As RolPermiso In _rolPermiso
            ''        '_permiso = New Permiso
            ''        '_permiso = bll.GetAll().Where(Function(p) p.Id.Equals(rolpermiso.Permiso)).FirstOrDefault
            ''        _Permisos.Add(rolpermiso.Permiso)
            ''    Next
            ''    'For Each _permiso As Permiso In _rol.Permisos
            ''    '    _Permisos.Add(_permiso)
            ''    'Next
            ''Next

            Return Ok(_Permisos)

        End Function


        '<HttpPut()>
        'Public Function guardar(dto As ConsultarPermisoDTO) As Permiso
        '    Dim _permiso As New Permiso
        '    _permiso.Nombre = dto.Nombre
        '    bll.SaveOrUpdate(_permiso)
        '    Return bll.Obtain(_permiso.Id)
        'End Function

        '<HttpDelete>
        'Public Function Eliminar(id As Guid) As IHttpActionResult

        '    Dim _permiso As Permiso
        '    _permiso = bll.Obtain(id)
        '    bll.Delete(_permiso.Id)
        '    Return Ok()

        'End Function

    End Class
End Namespace