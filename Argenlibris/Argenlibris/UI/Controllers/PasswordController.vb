﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.DTO
Imports Argenlibris.Domain
Imports Argenlibris.Seguridad


Public Class PasswordController
    Inherits ApiController

    Dim bll As IUsuarioBLL

    Public Sub New(bllUsuario As IUsuarioBLL)

        bll = bllUsuario


    End Sub



    ' GET api/<controller>
    Public Function GetValues() As IEnumerable(Of String)
        Return New String() {"value1", "value2"}
    End Function

    ' GET api/<controller>/5
    Public Function GetValue(ByVal id As Integer) As String
        Return "value"
    End Function

    ' POST api/<controller>
    Public Function PostValue(<FromBody()> ByVal value As CambiarPasswordDTO) As IHttpActionResult
        Dim usu As Usuario = bll.GetAll().Where(Function(e) e.Id.Equals(Guid.Parse(HttpContext.Current.User.Identity.Name))).FirstOrDefault()
        If (IsNothing(usu)) Then
            Return Content(HttpStatusCode.InternalServerError, "error")
        Else
            If usu.Password.Equals(Encriptador.GetInstance.EncriptarAHash(value.PasswordAnterior)) Then
                usu.Password = value.PasswordNuevo
                bll.SaveOrUpdate(usu)
                Return Ok(usu)
            Else
                Return Content(HttpStatusCode.NotModified, "error")
            End If
        End If

    End Function

    ' PUT api/<controller>/5
    Public Sub PutValue(ByVal id As Integer, <FromBody()> ByVal value As String)

    End Sub

    ' DELETE api/<controller>/5
    Public Sub DeleteValue(ByVal id As Integer)

    End Sub
End Class
