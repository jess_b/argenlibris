﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO



Public Class RepresentantesController
    Inherits ApiController


    Dim bll As IRepresentanteBLL
    Dim bllUsuarios As IUsuarioBLL
    Dim bllRol As IUsuarioRolBLL
    Dim bllPermiso As IPermisoBLL
    Dim bllRolPermiso As IRolPermisoBLL

    Public Sub New(bllRolPermiso_ As IRolPermisoBLL, bll_ As IRepresentanteBLL, bllUsuarios_ As IUsuarioBLL, bllRol_ As IUsuarioRolBLL, bllPermiso_ As IPermisoBLL)
        bll = bll_
        bllUsuarios = bllUsuarios_
        bllRol = bllRol_
        bllPermiso = bllPermiso_
        bllRolPermiso = bllRolPermiso_
    End Sub

    ' GET api/<controller>
    Public Function GetValues() As IHttpActionResult
        'traigo los clientes
        Dim result As IList = New List(Of Usuario)
        Dim permisoVentas As Permiso = bllPermiso.GetAll().Where(Function(o) o.Permiso = EnumPermiso.VentasPuedeGestionar).FirstOrDefault()

        Dim clientes As IList(Of Usuario) = bllUsuarios.GetAll().ToList
        If Not permisoVentas Is Nothing Then
            Dim rolesVentas As IList = bllRolPermiso.GetAll().Where(Function(o) o.Permiso.Id.Equals(permisoVentas.Id)).ToList


            For Each i As RolPermiso In rolesVentas
                For Each j As UsuarioRol In bllRol.GetAll().Where(Function(o) o.Rol.Id.Equals(i.Rol.Id)).ToList

                    If Not result.Contains(j.Usuario) Then
                        result.Add(j.Usuario)
                    End If
                Next

            Next

            Dim r As New RepresentacionDTO

            r.Clientes = AutoMapper.Mapper.Map(Of IList(Of Usuario))(clientes)

            r.Vendedores = AutoMapper.Mapper.Map(Of IList(Of Usuario))(result)


            Return Ok(AutoMapper.Mapper.Map(Of RepresentacionDTO)(r))



        End If
        Return Ok(" ")

    End Function

    ' GET api/<controller>/5
    Public Function GetValue(ByVal id As Guid) As IHttpActionResult

        Return Ok(AutoMapper.Mapper.Map(Of RepresentacionCuenta)(bll.GetAll().Where(Function(o) o.Cliente.Id.Equals(id)).FirstOrDefault()))

    End Function

    ' POST api/<controller>
    <RolesValidos(EnumPermiso.VentasPuedeGestionar)>
    Public Function PostValue() As IHttpActionResult

        Return Ok(AutoMapper.Mapper.Map(Of IList(Of RepresentacionCuenta))(bll.GetAll().ToList()))
    End Function

    ' PUT api/<controller>/5
    Public Function PutValue(<FromBody()> ByVal value As RepresentacionCuenta) As IHttpActionResult
        bll.SaveOrUpdate(value)

        Return Ok(value)
    End Function

    ' DELETE api/<controller>/5
    <HttpDelete>
    Public Function Delete(id As Guid) As IHttpActionResult
        bll.Delete(id)
        Return Ok()
    End Function

End Class
