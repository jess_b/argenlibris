﻿Imports System.Net
Imports System.Web.Http
Imports BLL
Imports Argenlibris.Domain
Imports Argenlibris.DTO

Namespace Controllers
    Public Class IdiomaDosController
        Inherits ApiController

        Dim bll As IBLL(Of Idioma)
        Dim bllPalabra As IPalabraBLL
        Public Sub New(bllIdioma As IBLL(Of Idioma), bllPalabra_ As IPalabraBLL)
            bll = bllIdioma
            bllPalabra = bllPalabra_
        End Sub


        <HttpPost>
        <RolesValidos(EnumPermiso.IdiomaPuedeGestionar)>
        Public Function Validar(idioma As Idioma) As IHttpActionResult

            If Not idioma.Codigo Is Nothing Then

                Return Ok(bll.GetAll.Where(Function(u) u.Codigo.Equals(idioma.Codigo)).FirstOrDefault)

            ElseIf Not idioma.Descripcion Is Nothing Then

                Return Ok(bll.GetAll.Where(Function(u) u.Descripcion.Equals(idioma.Descripcion)).FirstOrDefault)

            End If

            Return BadRequest()


        End Function





    End Class


End Namespace