﻿Imports Argenlibris.Seguridad
Imports System.Threading.Tasks
Imports System.Net.Http
Imports Argenlibris.Domain

Public Class RolesValidosAttribute
    Inherits System.Web.Http.Filters.ActionFilterAttribute





    Dim _roles As EnumTipoDeError()

    Sub New(ByVal ParamArray roles As EnumTipoDeError())
        _roles = roles

    End Sub

    Public Overrides Function OnActionExecutingAsync(actionContext As Http.Controllers.HttpActionContext, cancellationToken As Threading.CancellationToken) As Threading.Tasks.Task


        For Each r As EnumTipoDeError In _roles
            If Not HttpContext.Current.User.IsInRole(r.ToString()) Then
                'actionContext.cResponse.StatusCode = 403
                actionContext.Response = New HttpResponseMessage(Net.HttpStatusCode.Forbidden)




                Return Task.FromResult(Of Object)(Nothing)
            End If
        Next


        Return MyBase.OnActionExecutingAsync(actionContext, cancellationToken)
    End Function


End Class



'For Each r As Rol In _roles
'           If Not HttpContext.Current.User.IsInRole(r.ToString()) Then
'               filterContext.HttpContext.Response.StatusCode = 403

'           End If
'       Next