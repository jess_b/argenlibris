﻿Imports System.Security.Principal
Imports BLL
Imports Argenlibris.Domain
Imports System.Web.Http

Public Class ArgenlibrisPrincipal
    Implements IPrincipal

    Private _identity As IIdentity
    Public Sub New(usuarioId As Guid)
        _identity = New GenericIdentity(usuarioId.ToString())
       
    End Sub

    Public ReadOnly Property Identity As IIdentity Implements IPrincipal.Identity
        Get
            Return _identity
        End Get
    End Property

    Public Function IsInRole(role As String) As Boolean Implements IPrincipal.IsInRole

        Dim p As EnumPermiso
        If p.Equals(EnumPermiso.Administrador) Then Return True

        p = [Enum].Parse(GetType(EnumTipoDeError), role)
        Dim usuario As Usuario
        Dim id As Guid = Guid.Parse(HttpContext.Current.User.Identity.Name)

        Dim _usuarioBLL As UsuarioBLL = Iniciador.ObtenerUsuarioBLL

        usuario = _usuarioBLL.GetAll().Where(Function(o) o.Id.Equals(id)).FirstOrDefault()

        'Dim _usuarioRol As New List(Of UsuarioRol)
        Dim _usuarioRol As IList(Of UsuarioRol)
        'Dim _rolPermiso As New List(Of RolPermiso)
        Dim _rolPermiso As IList(Of RolPermiso)
        Dim _usuarioRolBLL As UsuarioRolBLL = Iniciador.ObtenerUsuarioRolBLL
        Dim _rolPermisoBLL As RolPermisoBLL = Iniciador.ObtenerRolPermisoBLL

        'If Not _usuarioRolBLL Is Nothing Then


        _usuarioRol = _usuarioRolBLL.GetAll.Where(Function(u) u.Usuario.Id.Equals(usuario.Id)).ToList



        For Each usuarioRol_ As UsuarioRol In _usuarioRol
            _rolPermiso = _rolPermisoBLL.GetAll.Where(Function(u) u.Rol.Id.Equals(usuarioRol_.Rol.Id)).ToList

            For Each RolPermiso As RolPermiso In _rolPermiso
            
                If RolPermiso.Permiso.Permiso.Equals(p) Then
                    Return True
                End If
            Next
            'For Each permiso As Permiso In _rolPermiso
            '    If permiso.Permiso.Equals(p) Then
            '        Return True
            '    End If
            'Next
        Next

        'End If

        'For Each _rol As Rol In usuario.Roles

        '    For Each permiso As Permiso In _rol.Permisos
        '        If permiso.Permiso.Equals(p) Then
        '            Return True
        '        End If
        '    Next

        'Next

        Return False

    End Function
End Class
