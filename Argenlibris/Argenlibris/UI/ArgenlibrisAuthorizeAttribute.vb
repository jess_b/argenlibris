﻿Imports System.Web.Http.Filters

Public Class ArgenlibrisAuthorizeAttribute
    Inherits AuthorizationFilterAttribute


    Public Overrides Function OnAuthorizationAsync(actionContext As Http.Controllers.HttpActionContext, cancellationToken As Threading.CancellationToken) As Threading.Tasks.Task

        actionContext.Response.StatusCode = 200


        Return MyBase.OnAuthorizationAsync(actionContext, cancellationToken)
    End Function
End Class
