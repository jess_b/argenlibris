﻿Imports Argenlibris.Domain
Imports Repositorio
Public Interface IUsuarioRolBLL
    Inherits IBLL(Of UsuarioRol)

End Interface
Public Class UsuarioRolBLL
    Inherits BaseBLL(Of UsuarioRol)
    Implements IUsuarioRolBLL

    Dim _validador As IValidadorFactory


    Private Repositorio As IRepositorio(Of UsuarioRol)

    Sub New(repositorio As IRepositorio(Of UsuarioRol), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
        'Me.Repositorio = repositorio
    End Sub

End Class
