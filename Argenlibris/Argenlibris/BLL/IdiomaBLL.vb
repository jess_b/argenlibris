﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface IIdiomaBLL
    Inherits IBLL(Of Idioma)



End Interface

Public Class IdiomaBLL
    Inherits BaseBLL(Of Idioma)
    Implements IIdiomaBLL

    Private Repositorio As IRepositorio(Of Idioma)

    Sub New(repositorio As IRepositorio(Of Idioma), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub

    
End Class
