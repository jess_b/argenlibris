﻿Imports Argenlibris.Domain
Imports Repositorio

Public Interface IChatMessageBLL
    Inherits IBLL(Of ChatMessage)



End Interface

Public Class ChatMessageBLL
    Inherits BaseBLL(Of ChatMessage)
    Implements IChatMessageBLL




    Private Repositorio As IRepositorio(Of ChatMessage)

    Sub New(repositorio As IRepositorio(Of ChatMessage), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub

End Class
