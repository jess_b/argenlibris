﻿Public Interface IValidadorFactory
    Function GetValidator(Of T)() As IValidador(Of T)
End Interface
