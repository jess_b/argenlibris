﻿Public Class ErrorNegocio
    Public Property Clave As String
    Public Property Valor As String
End Class
Public Class ExcepcionDeNegocio
    Inherits Exception

    Private _nvc As MyNameValueCollection
    Public ReadOnly Property Errores As IList(Of ErrorNegocio)
        Get
            Dim p As IList(Of ErrorNegocio)
            For Each k In _nvc.Keys
                p = New List(Of ErrorNegocio)
                Dim e As New ErrorNegocio()
                e.Valor = _nvc(k)
                e.Clave = k
                p.Add(e)

                Return p
            Next k



            Return _nvc.Cast(Of String).Select(Function(e) _nvc(e))
        End Get
    End Property

    Sub New(nvc As MyNameValueCollection)
        ' TODO: Complete member initialization 
        _nvc = nvc
    End Sub



    Public Overrides ReadOnly Property Message As String
        Get

            Return "Error de negocio"
        End Get
    End Property
End Class
