﻿Imports Argenlibris.Domain

Public Class PalabraValidator
    Implements IValidador(Of Palabra)


    Private nvc As New MyNameValueCollection

    Public Function Validar(entity As Palabra) As MyNameValueCollection Implements IValidador(Of Palabra).Validar

        If entity.Clave.Equals(String.Empty) Then
            nvc.Add("campo Clave obligatorio", "el campo Clave es obligatorio")
        End If

        If entity.Valor.Equals(String.Empty) Then
            nvc.Add("campo obligatorio", "el campo Valor es obligatorio")
        End If

        If entity.IdIdioma.Equals(String.Empty) Then
            nvc.Add("campo obligatorio", "el campo IdIdioma es obligatorio")
        End If

        Return nvc

    End Function

End Class
