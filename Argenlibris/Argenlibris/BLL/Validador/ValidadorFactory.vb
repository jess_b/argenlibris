﻿Imports System.Reflection

Public Class ValidadorFactory
    Implements IValidadorFactory

  

    Public Function GetValidator1(Of T)() As IValidador(Of T) Implements IValidadorFactory.GetValidator
        Dim validator As IValidador(Of T) = Nothing
        Try
            validator = Assembly.GetExecutingAssembly().CreateInstance(String.Format("{0}.{1}Validator", Me.GetType.Namespace, GetType(T).Name))
        Catch ex As Exception

        End Try
        If validator Is Nothing Then validator = New MockValidator(Of T)

        Return validator
    End Function
End Class
