﻿Imports Argenlibris.Domain
Public Class PermisoValidator
    Implements IValidador(Of Permiso)

    Private nvc As New MyNameValueCollection
    Public Function Validar(entity As Permiso) As MyNameValueCollection Implements IValidador(Of Permiso).Validar

        If entity.Permiso.Equals(String.Empty) Then
            nvc.Add("campo vacío", "el campo no puede ser vacío")
        End If
        Return nvc
    End Function

End Class
