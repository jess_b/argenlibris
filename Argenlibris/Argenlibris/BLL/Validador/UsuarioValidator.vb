﻿Imports Argenlibris.Domain
Public Class UsuarioValidator
    Implements IValidador(Of Usuario)

    Private nvc As New MyNameValueCollection
    Public Function Validar(entity As Usuario) As MyNameValueCollection Implements IValidador(Of Usuario).Validar

        If (entity.NombreUsuario Is Nothing) Then
            nvc.Add("error de usuario", "el campo no puede ser nulo")
        End If

        If entity.Password.Equals(String.Empty) And Not entity.Id.Equals(Guid.Empty) Then
            nvc.Add("campo Password obligatorio", "el password no puede estar vacio")
        End If

        'If entity.NombreUsuario.Equals(String.Empty) Then
        '    nvc.Add("campo NombreUsuario obligatorio", "el nombre de usuario no puede estar vacio")
        'End If

        If entity.Email.Equals(String.Empty) Then
            nvc.Add("campo Mail obligatorio", "el mail no puede estar vacio")
        End If

        'Dim _edad As Integer
        '_edad = Today.Year - entity.FechaNacimiento
        'If entity.FechaNacimiento Then
        '    nvc.Add("campo Mail obligatorio", "el mail no puede estar vacio")
        'End If

        Return nvc

    End Function

End Class
