﻿Imports System.Net.Mail
Imports Argenlibris.Domain
Imports Repositorio
Imports Spire.Pdf
Imports System.IO

Public Interface IEmailBLL
    Inherits IBLL(Of Entity)

    Sub SetVariables(usuarios_ As List(Of Usuario))
    Sub EnviarMailDeRegistro(usuario_ As Usuario, url_ As String)
    Sub EnviarMailDeRecupero(usuario_ As Usuario, url_ As String)
    Sub EnviarMailDeFactura(usuario_ As Usuario, factura_ As DocumentoCabecera)
    Sub EnviarNewsletter(usuarios_ As List(Of Usuario), libros_ As List(Of Libro), newsletter_ As Newsletter, ruta_ As String, rutaImagen_ As String)
    Sub EnviarNovedad(usuarios_ As List(Of Usuario), libro_ As Libro, ruta_ As String)
    Sub EnviarMailContacto(contacto_ As Contacto)
    Sub EnviarMailDeRegistroNovedad(mail_ As String)
    Sub EnviarMailNovedades(visitantes_ As List(Of CategoriaVisitante), novedad_ As Novedad)
    Sub EnviarMailContraseña(usuario_ As Usuario)

End Interface

Public Class EmailBLL
    'Inherits BaseBLL(Of Entity)
    Implements IEmailBLL







    Dim SmtpServer As New SmtpClient()
    Dim mail As New MailMessage()
    'Dim _validador As IValidadorFactory

    Sub New(validador_ As IValidadorFactory)
        '_validador = validador_
    End Sub

    Public Sub EnviarMailDeRegistro(usuario_ As Usuario, url_ As String) Implements IEmailBLL.EnviarMailDeRegistro

        Dim _usuarios As New List(Of Usuario)
        _usuarios.Add(usuario_)

        Try

            SetVariables(_usuarios)

            mail.Subject = "Activar Cuenta - Argenlibris"

            mail.Body = "Bienvenida/o " & usuario_.Nombre & "<br/> Hemos recibido tu solicitud de registro. Esto es lo que tiene que hacer para activar su cuenta: <br> 1. Confirmar tu identidad haciendo clic <a href='" & url_ & "?activar=" & usuario_.Id.ToString() & "'>  en este enlace  <a/> . Gracias"

            mail.IsBodyHtml = True

            SmtpServer.Send(mail)

            MsgBox("Mensaje Enviado")

        Catch ex As Exception

            MsgBox(ex.ToString)

        End Try

    End Sub

    Public Sub EnviarMailDeRecupero(usuario_ As Usuario, url_ As String) Implements IEmailBLL.EnviarMailDeRecupero

        Dim _usuarios As New List(Of Usuario)
        _usuarios.Add(usuario_)

        Try

            SetVariables(_usuarios)

            mail.Subject = "Recupero de Contraseña- Argenlibris"

            mail.Body = "Su nueva contraseña es	" & _
                        "inicio01"

            SmtpServer.Send(mail)

            MsgBox("Mensaje Enviado")

        Catch ex As Exception

            MsgBox(ex.ToString)

        End Try

    End Sub

    Public Sub EnviarMailDeFactura(usuario_ As Usuario, factura_ As DocumentoCabecera) Implements IEmailBLL.EnviarMailDeFactura

        Dim _usuarios As New List(Of Usuario)
        _usuarios.Add(usuario_)

        Try

            SetVariables(_usuarios)

            mail.Subject = "Confirmación de compra - Argenlibris"

            mail.Body = "Estimada/o " & usuario_.Nombre & _
            "<br/> Hemos despachado su pedido. <br/> Aquí encontrará su factura " & factura_.Numero & _
            "<br/> Gracias por su compra!"

            mail.IsBodyHtml = True

            Dim _dir As String = "C:\Users\jess\Desktop\Facturas\Factura " + CStr(factura_.Numero) + ".pdf"
            Dim _nombre As String = "Factura Nro " + CStr(factura_.Numero) + ".pdf"
            Dim stream As FileStream = File.OpenRead(_dir)
            'Dim stream As FileStream = File.OpenRead("C:\Users\jess\Desktop\Facturas\Factura 0.pdf")
            Dim attachment As Attachment = New Attachment(stream, _nombre)
            'Dim attachment As Attachment = New Attachment(stream, "FriendlyName.pdf")
            mail.Attachments.Add(attachment)

            SmtpServer.Send(mail)

            'MsgBox("Mensaje Enviado")

        Catch ex As Exception

            MsgBox(ex.ToString)

        End Try

    End Sub

    Public Sub EnviarNewsletter(usuarios_ As List(Of Usuario), libros_ As List(Of Libro), newsletter_ As Newsletter, ruta_ As String, rutaImagen_ As String) Implements IEmailBLL.EnviarNewsletter

        Try

            SetVariables(usuarios_)
            SetNewletter(libros_, newsletter_, ruta_, rutaImagen_)

        Catch ex As Exception

        End Try

    End Sub

    Public Sub EnviarNovedad(usuarios_ As List(Of Usuario), libro_ As Libro, ruta_ As String) Implements IEmailBLL.EnviarNovedad

        Try
            SetVariables(usuarios_)
            SetNovedad(libro_, ruta_)
        Catch ex As Exception

        End Try

    End Sub


    Private Sub SetNewletter(libros_ As List(Of Libro), newsletter_ As Newsletter, ruta_ As String, rutaImagen_ As String)

        ' Creates instance of StreamReader class
        Dim sr As StreamReader = New StreamReader(ruta_)

        ' Finally, loads file to string
        Dim FileContent As String = sr.ReadToEnd()

        Dim string1, string2, string3 As String

        Dim c As String

        mail.Subject = newsletter_.Asunto

        'c = "<div class='col-md-9'>"
        c = "<div class='row'>"

        For Each Libro In libros_
            c = c & "<div class='col-md-4'> <div class='card-block'> <h4 class='card-title'>" + Libro.Titulo + "</h4> <p class='card-text'> " + Libro.Autor.Apellido + ", " + Libro.Autor.Nombre + "</p> <p class='card-text'> $" + Libro.Precio.ToString + "</p> </div> </div>"
        Next

        c = c & "</div>"

        'c = "<div class='row'>"

        'For Each Libro In libros_
        '    c = c & "<div class='col-md-4'>" + Libro.Titulo + "</div>"
        'Next

        'c = c & "</div>"

        string1 = FileContent.Replace("libros", c)

        string2 = string1.Replace("imagenes", rutaImagen_)

        string3 = string2.Replace("Newsletter.Cuerpo", newsletter_.Cuerpo)

        mail.IsBodyHtml = True

        mail.Body = string3

        SmtpServer.Send(mail)

    End Sub

    Private Sub SetVariables(usuarios_ As List(Of Usuario)) Implements IEmailBLL.SetVariables

        Try

            SmtpServer.Host = "smtp.gmail.com"
            SmtpServer.Port = 587
            SmtpServer.EnableSsl = True
            SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network
            SmtpServer.UseDefaultCredentials = False
            SmtpServer.Credentials = New Net.NetworkCredential("argenlibris@gmail.com", "dostoievski")
            mail = New MailMessage()
            mail.From = New MailAddress("argenlibris@gmail.com")
            For Each _usuario As Usuario In usuarios_
                mail.Bcc.Add(_usuario.Email)
            Next


        Catch ex As Exception

        End Try

    End Sub

    Private Sub SetNovedad(libro_ As Libro, ruta_ As String)

        ' Creates instance of StreamReader class
        Dim sr As StreamReader = New StreamReader(ruta_)

        ' Finally, loads file to string
        Dim FileContent As String = sr.ReadToEnd()

        Dim string1, string2, string3 As String

        Dim c As String

        mail.Subject = "Tenemos novedades para vos"

        c = "<div class='row'>"

        c = c & "<div class='col-md-4'> <div class='card-block'> <h4 class='card-title'>" + libro_.Sinopsis + "</h4> </div> </div>"

        c = c & "</div>"

        string1 = FileContent.Replace("Libro.Sinopsis", c)

        string2 = string1.Replace("Categoria.Descripcion ", libro_.Categoria.Descripcion)

        string3 = string2.Replace("Libro.Titulo", libro_.Titulo)

        mail.IsBodyHtml = True

        mail.Body = string3

        SmtpServer.Send(mail)
    End Sub

#Region "Comentado"
    Public Sub Delete1(id As Object) Implements IBLL(Of Entity).Delete

    End Sub

    Public Function GetAll1() As IQueryable(Of Entity) Implements IBLL(Of Entity).GetAll

    End Function

    Public Function Obtain1(id As Object) As Entity Implements IBLL(Of Entity).Obtain

    End Function

    Public Sub OnAfterSaveOrUpdate1(entity As Entity) Implements IBLL(Of Entity).OnAfterSaveOrUpdate

    End Sub

    Public Sub OnBeforeDelete1(entity As Entity) Implements IBLL(Of Entity).OnBeforeDelete

    End Sub

    Public Sub OnBeforeSaveOrUpdate1(entity As Entity) Implements IBLL(Of Entity).OnBeforeSaveOrUpdate

    End Sub

    Public Function SaveOrUpdate1(Entity As Entity) As Entity Implements IBLL(Of Entity).SaveOrUpdate

    End Function

    Public Function Validar1(entity As Entity) As Boolean Implements IBLL(Of Entity).Validar

    End Function
#End Region



    Public Sub EnviarMailContacto(contacto_ As Contacto) Implements IEmailBLL.EnviarMailContacto

        Dim sGUID As String

        Try
            SmtpServer.Host = "smtp.gmail.com"
            SmtpServer.Port = 587
            SmtpServer.EnableSsl = True
            SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network
            SmtpServer.UseDefaultCredentials = False
            SmtpServer.Credentials = New Net.NetworkCredential("argenlibris@gmail.com", "dostoievski")
            mail = New MailMessage()
            mail.From = New MailAddress("argenlibris@gmail.com")

            mail.To.Add("argenlibris@gmail.com")

            sGUID = System.Guid.NewGuid.ToString()

            mail.Subject = "Formulario de contacto: " & sGUID

            mail.Body = "Nombre: " & contacto_.Nombre & "<br/> Mail: " & contacto_.Mail & "<br/> Observacion: " & contacto_.Observacion

            mail.IsBodyHtml = True

            SmtpServer.Send(mail)

            MsgBox("Mensaje Enviado")

        Catch ex As Exception

            MsgBox(ex.ToString)

        End Try
    End Sub

    Public Sub EnviarMailDeRegistroNovedad(mail_ As String) Implements IEmailBLL.EnviarMailDeRegistroNovedad

        Try

            SmtpServer.Host = "smtp.gmail.com"
            SmtpServer.Port = 587
            SmtpServer.EnableSsl = True
            SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network
            SmtpServer.UseDefaultCredentials = False
            SmtpServer.Credentials = New Net.NetworkCredential("argenlibris@gmail.com", "dostoievski")
            mail = New MailMessage()
            mail.From = New MailAddress("argenlibris@gmail.com")
            mail.Bcc.Add(mail_)

            mail.Subject = "Registración a Novedades"

            mail.Body = "Usted se ha registrado a las novedades de Argenlibros.  <br/> Recibirá las novedades de nuestra página de manera automática. <br/> <br/> Atte, El equipo de Argenlibris."

            mail.IsBodyHtml = True

            SmtpServer.Send(mail)

            MsgBox("Mensaje Enviado")

        Catch ex As Exception

        End Try

    End Sub


    Public Sub EnviarMailNovedades(visitantes_ As List(Of CategoriaVisitante), novedad_ As Novedad) Implements IEmailBLL.EnviarMailNovedades

        Try

            SmtpServer.Host = "smtp.gmail.com"
            SmtpServer.Port = 587
            SmtpServer.EnableSsl = True
            SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network
            SmtpServer.UseDefaultCredentials = False
            SmtpServer.Credentials = New Net.NetworkCredential("argenlibris@gmail.com", "dostoievski")
            mail = New MailMessage()
            mail.From = New MailAddress("argenlibris@gmail.com")

            For Each _visitante As CategoriaVisitante In visitantes_
                mail.Bcc.Add(_visitante.Mail)
            Next

            mail.Subject = "Novedad: " + novedad_.Titulo

            mail.Body = novedad_.Descripcion

            mail.IsBodyHtml = True

            SmtpServer.Send(mail)

            MsgBox("Mensaje Enviado")

        Catch ex As Exception


        End Try
    End Sub

    Public Sub EnviarMailContraseña(usuario_ As Usuario) Implements IEmailBLL.EnviarMailContraseña
        Try

            SmtpServer.Host = "smtp.gmail.com"
            SmtpServer.Port = 587
            SmtpServer.EnableSsl = True
            SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network
            SmtpServer.UseDefaultCredentials = False
            SmtpServer.Credentials = New Net.NetworkCredential("argenlibris@gmail.com", "dostoievski")
            mail = New MailMessage()
            mail.From = New MailAddress("argenlibris@gmail.com")


            mail.Bcc.Add(usuario_.Email)


            mail.Subject = "Olvido de contraseña"

            mail.Body = "Estimado,  <br/> Su nueva contraseña temporal es: <b> inicio01 </b> <br/> Cambie su contraseña a la brevedad. <br/> Atte, El equipo de Argenlibris."

            mail.IsBodyHtml = True

            SmtpServer.Send(mail)

            MsgBox("Mensaje Enviado")

        Catch ex As Exception


        End Try

    End Sub
End Class
