﻿Imports Repositorio
Imports Argenlibris.Domain



Public Interface IBackupBLL
    Inherits IBLL(Of BackupFile)
    Function doBackup() As String
    Sub restoreBackup(id As Guid)
End Interface


Public Class BackupBLL
    Inherits BaseBLL(Of BackupFile)
    Implements IBackupBLL




    Private repositorio_ As IBackupRepositorio


    Sub New(repositorio As IBackupRepositorio, validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
        repositorio_ = repositorio
    End Sub


    Public Sub restoreBackup(id As Guid) Implements IBackupBLL.restoreBackup

        Dim file As BackupFile = GetAll().Where(Function(o) o.Id = id).FirstOrDefault
        If (Not file Is Nothing) Then
            repositorio_.RestoreBackup(file.Nombre)
        End If

    End Sub


    Public Function doBackup() As String Implements IBackupBLL.doBackup
        Return repositorio_.DoBackup


    End Function

    
End Class
