﻿Imports Argenlibris.Domain
Imports Repositorio
Public Interface IRolBLL
    Inherits IBLL(Of Rol)

End Interface

Public Class RolBLL
    Inherits BaseBLL(Of Rol)
    Implements IRolBLL


    Dim _validador As IValidadorFactory


    Private Repositorio As IRepositorio(Of Rol)

    Sub New(repositorio As IRepositorio(Of Rol), validador As IValidadorFactory) ', encriptador As IEncriptador)
        MyBase.New(repositorio, validador)
        'Me.Repositorio = repositorio
    End Sub
End Class
