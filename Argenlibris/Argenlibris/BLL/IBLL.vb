﻿Public Interface IBLL(Of T As Class)

    Sub OnBeforeSaveOrUpdate(entity As T)
    Sub OnAfterSaveOrUpdate(entity As T)
    Sub OnBeforeDelete(entity As T)
    Function SaveOrUpdate(Entity As T) As T
    Function GetAll() As IQueryable(Of T)
    Function Obtain(id As Object) As T
    Sub Delete(id As Object)
    Function Validar(entity As T) As Boolean


End Interface
