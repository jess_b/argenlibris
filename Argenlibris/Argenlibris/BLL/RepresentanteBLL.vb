﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface IRepresentanteBLL
    Inherits IBLL(Of RepresentacionCuenta)



End Interface

Public Class RepresentanteBLL
    Inherits BaseBLL(Of RepresentacionCuenta)
    Implements IRepresentanteBLL

    Private Repositorio As IRepositorio(Of RepresentacionCuenta)

    Sub New(repositorio As IRepositorio(Of RepresentacionCuenta), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub

    
End Class
