﻿Imports Repositorio

Public Class BaseBLL(Of T As Class)
    Implements IBLL(Of T)

    Dim _validador As IValidadorFactory

    Private Repositorio As IRepositorio(Of T)

    Sub New(repositorio As IRepositorio(Of T), validador As IValidadorFactory)
        Me.Repositorio = repositorio
        _validador = validador
    End Sub


    Public Sub Delete(id As Object) Implements IBLL(Of T).Delete
        Dim entity As T
        entity = Repositorio.Obtain(id)
        Repositorio.Delete(entity)
    End Sub

    Public Function Obtain(id As Object) As T Implements IBLL(Of T).Obtain
        Return Repositorio.Obtain(id)
    End Function

    Public Sub OnAfterSaveOrUpdate(entity As T) Implements IBLL(Of T).OnAfterSaveOrUpdate

    End Sub

    Public Sub OnBeforeDelete(entity As T) Implements IBLL(Of T).OnBeforeDelete

    End Sub

    Public Sub OnBeforeSaveOrUpdate(entity As T) Implements IBLL(Of T).OnBeforeSaveOrUpdate
        Try
            Dim nvc As New MyNameValueCollection
            nvc = _validador.GetValidator(Of T).Validar(entity)
            If nvc.Count > 0 Then Throw New ExcepcionDeNegocio(nvc)


        Catch be As ExcepcionDeNegocio
            Throw be
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Overridable Function SaveOrUpdate(Entity As T) As T Implements IBLL(Of T).SaveOrUpdate

        Dim nvc As MyNameValueCollection = _validador.GetValidator(Of T).Validar(Entity)
        If nvc.Count > 0 Then
            Throw New ExcepcionDeNegocio(nvc)
        Else
            Repositorio.SaveOrUpdate(Entity)
        End If
        Return Entity
    End Function

    Public Function GetAll() As IQueryable(Of T) Implements IBLL(Of T).GetAll
        Dim _t As IQueryable(Of T)
        _t = Repositorio.GetAll()
        Return _t
    End Function

    Public Overridable Function Validar(entity As T) As Boolean Implements IBLL(Of T).Validar
        Return True
    End Function


End Class
