﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface IUsuarioBLL
    Inherits IBLL(Of Usuario)


    Sub RecuperarPorMail(usuario_ As Usuario)
    Function CambiarPerfil(Entity As Usuario) As Usuario

End Interface

Public Class UsuarioBLL '(Of T As Class)
    Inherits BaseBLL(Of Usuario)
    Implements IUsuarioBLL


    'Dim _validador As IValidadorFactory
    Dim bllEmail As IEmailBLL

    Private Repositorio As IRepositorio(Of Usuario)


    Sub New(repositorio As IRepositorio(Of Usuario), validador As IValidadorFactory, email_ As IEmailBLL)
        MyBase.New(repositorio, validador)
        bllEmail = email_
    End Sub


    Public Function CambiarPerfil(Entity As Usuario) As Usuario Implements IUsuarioBLL.CambiarPerfil

        'sin encriptacion
        Try
            MyBase.SaveOrUpdate(Entity)
        Catch exn As ExcepcionDeNegocio
            Throw exn
        Catch ex As Exception
            Throw ex
        End Try



        Return Entity
    End Function

    Public Overrides Function SaveOrUpdate(Entity As Usuario) As Usuario Implements IBLL(Of Usuario).SaveOrUpdate

        'CON encriptacion
        Try
            OnAfterSaveOrUpdate(Entity)
            MyBase.SaveOrUpdate(Entity)
        Catch exn As ExcepcionDeNegocio
            Throw exn
        Catch ex As Exception
            Throw ex
        End Try



        Return Entity

    End Function

    Public Overrides Function Validar(entity As Usuario) As Boolean Implements IBLL(Of Usuario).Validar

        If entity.NombreUsuario Is Nothing Then
            Return False
        End If

        If entity.Password Is Nothing Then
            Return False
        End If

        Return True

    End Function

    Public Overloads Sub OnAfterSaveOrUpdate(entity As Usuario) Implements IBLL(Of Usuario).OnAfterSaveOrUpdate

        'Encripta la contraseña 
        Dim _encriptador As Encriptador = Encriptador.GetInstance

        'If entity.Id.Equals(Guid.Empty) Then
        If Not entity.Password Is Nothing Then
            entity.Password = _encriptador.EncriptarAHash(entity.Password)
        End If


        'End If


    End Sub

    Public Sub RecuperarPorMail(usuario_ As Usuario) Implements IUsuarioBLL.RecuperarPorMail

        Dim _nuevaPassword As String

        Dim _encriptador As Encriptador = Encriptador.GetInstance

        _nuevaPassword = "inicio01"

        usuario_.Password = _encriptador.EncriptarAHash(_nuevaPassword)

        SaveOrUpdate(usuario_)

        'bllEmail.EnviarMailContraseña(usuario_)


    End Sub


End Class
