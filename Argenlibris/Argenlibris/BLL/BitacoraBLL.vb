﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface IBitacoraBLL
    Inherits IBLL(Of Bitacora)
End Interface


Public Class BitacoraBLL
    Inherits BaseBLL(Of Bitacora)
    Implements IBitacoraBLL


    Private Repositorio As IRepositorio(Of Bitacora)


    Sub New(repositorio As IRepositorio(Of Bitacora), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
        'Me.Repositorio = repositorio
    End Sub


    Public Overrides Function SaveOrUpdate(Entity As Bitacora) As Bitacora Implements IBLL(Of Bitacora).SaveOrUpdate

        Entity.Fecha = Date.Now
        Return MyBase.SaveOrUpdate(Entity)

    End Function

End Class
