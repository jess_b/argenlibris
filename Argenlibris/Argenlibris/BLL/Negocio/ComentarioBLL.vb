﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface IComentarioBLL
    Inherits IBLL(Of Comentario)



End Interface

Public Class ComentarioBLL
    Inherits BaseBLL(Of Comentario)
    Implements IComentarioBLL

    Private Repositorio As IRepositorio(Of Comentario)

    Sub New(repositorio As IRepositorio(Of Comentario), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub

End Class
