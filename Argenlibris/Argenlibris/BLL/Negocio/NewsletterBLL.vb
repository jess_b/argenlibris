﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface INewsletterBLL
    Inherits IBLL(Of Newsletter)



End Interface
Public Class NewsletterBLL
    Inherits BaseBLL(Of Newsletter)
    Implements INewsletterBLL

    Private Repositorio As IRepositorio(Of Newsletter)

    Sub New(repositorio As IRepositorio(Of Newsletter), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub
End Class
