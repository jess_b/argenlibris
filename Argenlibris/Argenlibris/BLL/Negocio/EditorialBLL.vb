﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface IEditorialBLL
    Inherits IBLL(Of Editorial)



End Interface

Public Class EditorialBLL

    Inherits BaseBLL(Of Editorial)
    Implements IEditorialBLL

    Private Repositorio As IRepositorio(Of Editorial)

    Sub New(repositorio As IRepositorio(Of Editorial), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub

End Class
