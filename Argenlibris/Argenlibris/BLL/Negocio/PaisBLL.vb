﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface IPaisBLL
    Inherits IBLL(Of Pais)



End Interface

Public Class PaisBLL

    Inherits BaseBLL(Of Pais)
    Implements IPaisBLL

    Private Repositorio As IRepositorio(Of Pais)

    Sub New(repositorio As IRepositorio(Of Pais), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub

End Class
