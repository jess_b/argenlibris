﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface ICategoriaBLL
    Inherits IBLL(Of Categoria)



End Interface

Public Class CategoriaBLL

    Inherits BaseBLL(Of Categoria)
    Implements ICategoriaBLL

    Private Repositorio As IRepositorio(Of Categoria)

    Sub New(repositorio As IRepositorio(Of Categoria), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub

End Class
