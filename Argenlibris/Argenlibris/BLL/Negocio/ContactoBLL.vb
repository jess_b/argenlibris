﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface IContactoBLL
    Inherits IBLL(Of Contacto)



End Interface

Public Class ContactoBLL

    Inherits BaseBLL(Of Contacto)
    Implements IContactoBLL

    Private Repositorio As IRepositorio(Of Contacto)

    Sub New(repositorio As IRepositorio(Of Contacto), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub

End Class
