﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface IAutorBLL
    Inherits IBLL(Of Autor)



End Interface

Public Class AutorBLL

    Inherits BaseBLL(Of Autor)
    Implements IAutorBLL

    Private Repositorio As IRepositorio(Of Autor)

    Sub New(repositorio As IRepositorio(Of Autor), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub

End Class
