﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface ICategoriaClienteBLL
    Inherits IBLL(Of CategoriaCliente)



End Interface

Public Class CategoriaClienteBLL
    Inherits BaseBLL(Of CategoriaCliente)
    Implements ICategoriaClienteBLL

    Private Repositorio As IRepositorio(Of CategoriaCliente)

    Sub New(repositorio As IRepositorio(Of CategoriaCliente), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub
End Class
