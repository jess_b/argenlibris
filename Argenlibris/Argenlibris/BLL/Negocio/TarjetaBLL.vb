﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface ITarjetaBLL
    Inherits IBLL(Of Tarjeta)



End Interface
Public Class TarjetaBLL

    Inherits BaseBLL(Of Tarjeta)
    Implements ITarjetaBLL


    Private Repositorio As IRepositorio(Of Tarjeta)

    Sub New(repositorio As IRepositorio(Of Tarjeta), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub

    ''Public Sub Delete1(id As Object) Implements IBLL(Of Tarjeta).Delete

    ''End Sub

    'Public Function GetAll1() As IQueryable(Of Tarjeta) Implements IBLL(Of Tarjeta).GetAll
    '    'desencriptar
    '    Dim _encriptador As Encriptador = Encriptador.GetInstance
    '    Dim _tarjetas As IQueryable(Of Tarjeta)
    '    _tarjetas = MyBase.GetAll()
    '    For Each _tarjeta As Tarjeta In _tarjetas
    '        If Not _tarjeta.NumeroTarjeta.Equals(String.Empty) Then
    '            _tarjeta.NumeroTarjeta = _encriptador.Desencriptar(_tarjeta.NumeroTarjeta)
    '        End If
    '    Next
    '    Return _tarjetas
    'End Function

    'Public Function Obtain1(id As Object) As Tarjeta Implements IBLL(Of Tarjeta).Obtain
    '    'desencriptar
    '    Dim _encriptador As Encriptador = Encriptador.GetInstance
    '    Dim _tarjeta As New Tarjeta
    '    _tarjeta = MyBase.Obtain(id)

    '    If Not _tarjeta.NumeroTarjeta.Equals(String.Empty) Then
    '        _tarjeta.NumeroTarjeta = _encriptador.Desencriptar(_tarjeta.NumeroTarjeta)
    '    End If
    '    Return _tarjeta
    'End Function

    ''Public Sub OnAfterSaveOrUpdate1(entity As Tarjeta) Implements IBLL(Of Tarjeta).OnAfterSaveOrUpdate
    ''    'encriptar
    ''End Sub

    ''Public Sub OnBeforeDelete1(entity As Tarjeta) Implements IBLL(Of Tarjeta).OnBeforeDelete
    ''    'encriptar
    ''End Sub

    'Public Sub OnBeforeSaveOrUpdate1(entity As Tarjeta) Implements IBLL(Of Tarjeta).OnBeforeSaveOrUpdate
    '    'encriptar
    '    Dim _encriptador As Encriptador = Encriptador.GetInstance
    '    If Not entity.NumeroTarjeta.Equals(String.Empty) Then
    '        entity.NumeroTarjeta = _encriptador.Encriptar(entity.NumeroTarjeta)
    '    End If
    'End Sub

    'Public Function SaveOrUpdate1(Entity As Tarjeta) As Tarjeta Implements IBLL(Of Tarjeta).SaveOrUpdate
    '    Me.OnBeforeSaveOrUpdate1(Entity)
    '    MyBase.SaveOrUpdate(Entity)
    'End Function

    ''Public Function Validar1(entity As Tarjeta) As Boolean Implements IBLL(Of Tarjeta).Validar

    ''End Function
End Class
