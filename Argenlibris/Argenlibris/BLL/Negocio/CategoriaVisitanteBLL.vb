﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface ICategoriaVisitanteBLL
    Inherits IBLL(Of CategoriaVisitante)



End Interface

Public Class CategoriaVisitanteBLL
    Inherits BaseBLL(Of CategoriaVisitante)
    Implements ICategoriaVisitanteBLL

    Private Repositorio As IRepositorio(Of CategoriaVisitante)

    Sub New(repositorio As IRepositorio(Of CategoriaVisitante), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub
End Class
