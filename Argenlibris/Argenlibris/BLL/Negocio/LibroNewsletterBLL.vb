﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface ILibroNewsletterBLL
    Inherits IBLL(Of LibroNewsletter)



End Interface
Public Class LibroNewsletterBLL
    Inherits BaseBLL(Of LibroNewsletter)
    Implements ILibroNewsletterBLL

    Private Repositorio As IRepositorio(Of LibroNewsletter)

    Sub New(repositorio As IRepositorio(Of LibroNewsletter), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub
End Class
