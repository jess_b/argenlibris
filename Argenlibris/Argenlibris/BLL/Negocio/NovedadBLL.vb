﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface INovedadBLL
    Inherits IBLL(Of Novedad)



End Interface

Public Class NovedadBLL

    Inherits BaseBLL(Of Novedad)
    Implements INovedadBLL

    Private Repositorio As IRepositorio(Of Novedad)

    Sub New(repositorio As IRepositorio(Of Novedad), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub

End Class
