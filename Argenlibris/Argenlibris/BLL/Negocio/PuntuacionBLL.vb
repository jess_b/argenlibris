﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface IPuntuacionBLL
    Inherits IBLL(Of Puntuacion)



End Interface

Public Class PuntuacionBLL
    Inherits BaseBLL(Of Puntuacion)
    Implements IPuntuacionBLL


    Private Repositorio As IRepositorio(Of Puntuacion)

    Sub New(repositorio As IRepositorio(Of Puntuacion), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub

    Public Overloads Function SaveOrUpdate(Entity As Puntuacion) As Puntuacion Implements IBLL(Of Puntuacion).SaveOrUpdate
        'Dim _puntuacionAnt As Puntuacion
        '_puntuacionAnt = Me.GetAll().Where(Function(o) o.Libro.Id = Entity.Libro.Id).FirstOrDefault

        'If _puntuacionAnt Is Nothing Then
        '    'primer puntuacion. tomo la puntuacion que viene en Entity
        '    Entity.CantidadVotos = Entity.CantidadVotos + 1
        '    Return (MyBase.SaveOrUpdate(Entity))
        'Else
        '    _puntuacionAnt.CantidadVotos = _puntuacionAnt.CantidadVotos + 1
        '    '( puntant + puntact) / cantvotos
        '    _puntuacionAnt.Puntuacion = (_puntuacionAnt.Puntuacion + Entity.Puntuacion) / _puntuacionAnt.CantidadVotos
        '    Return (MyBase.SaveOrUpdate(_puntuacionAnt))
        'End If

        Return (MyBase.SaveOrUpdate(Entity))
    End Function



    'Public Overloads Sub OnBeforeSaveOrUpdate1(entity As Puntuacion) Implements IBLL(Of Puntuacion).OnBeforeSaveOrUpdate


    'End Sub


End Class
