﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface IEncuestaBLL
    Inherits IBLL(Of Encuesta)



End Interface

Public Class EncuestaBLL
    Inherits BaseBLL(Of Encuesta)
    Implements IEncuestaBLL

    Private Repositorio As IRepositorio(Of Encuesta)

    Sub New(repositorio As IRepositorio(Of Encuesta), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub

End Class
