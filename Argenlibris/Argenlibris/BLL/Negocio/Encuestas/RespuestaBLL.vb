﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface IRespuestaBLL
    Inherits IBLL(Of Respuesta)



End Interface

Public Class RespuestaBLL
    Inherits BaseBLL(Of Respuesta)
    Implements IRespuestaBLL

    Private Repositorio As IRepositorio(Of Respuesta)

    Sub New(repositorio As IRepositorio(Of Respuesta), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub

End Class
