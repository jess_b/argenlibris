﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface IPreguntaBLL
    Inherits IBLL(Of Pregunta)



End Interface

Public Class PreguntaBLL
    Inherits BaseBLL(Of Pregunta)
    Implements IPreguntaBLL

    Private Repositorio As IRepositorio(Of Pregunta)

    Sub New(repositorio As IRepositorio(Of Pregunta), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub

End Class
