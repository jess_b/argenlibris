﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface IClienteRespuestaBLL
    Inherits IBLL(Of ClienteRespuesta)



End Interface
Public Class ClienteRespuestaBLL
    Inherits BaseBLL(Of ClienteRespuesta)
    Implements IClienteRespuestaBLL

    Private Repositorio As IRepositorio(Of ClienteRespuesta)

    Sub New(repositorio As IRepositorio(Of ClienteRespuesta), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub

End Class
