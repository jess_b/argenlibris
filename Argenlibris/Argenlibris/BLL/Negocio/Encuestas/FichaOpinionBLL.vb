﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface IFichaOpinionBLL
    Inherits IBLL(Of FichaDeOpinion)



End Interface
Public Class FichaOpinionBLL
    Inherits BaseBLL(Of FichaDeOpinion)
    Implements IFichaOpinionBLL

    Private Repositorio As IRepositorio(Of FichaDeOpinion)

    Sub New(repositorio As IRepositorio(Of FichaDeOpinion), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub

End Class
