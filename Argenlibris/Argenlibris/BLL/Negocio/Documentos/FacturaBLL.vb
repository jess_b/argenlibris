﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface IFacturaBLL
    Inherits IBLL(Of Factura)


End Interface

Public Class FacturaBLL
    Inherits BaseBLL(Of Factura)
    Implements IFacturaBLL

    Sub New(repositorio As IRepositorio(Of DocumentoCabecera), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub

End Class
