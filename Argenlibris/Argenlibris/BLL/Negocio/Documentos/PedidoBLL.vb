﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface IPedidoBLL
    Inherits IBLL(Of PedidoCabecera)



End Interface
Public Class PedidoBLL
    Inherits BaseBLL(Of PedidoCabecera)
    Implements IPedidoBLL



    Private Repositorio As IRepositorio(Of PedidoCabecera)

    Sub New(repositorio As IRepositorio(Of PedidoCabecera), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub


    Public Sub OnBeforeSaveOrUpdate1(entity As PedidoCabecera) Implements IBLL(Of PedidoCabecera).OnBeforeSaveOrUpdate
        If entity.Numero < 1 Then
            Dim _numero As Integer = Me.GetAll().Count
            entity.Numero = _numero + 1
        End If

        entity.Observacion = "Su Pedido está siendo preparado en Almacenes"

        If entity.Resuelto Then
            entity.Observacion = "Se genero Nota de Credito"
        ElseIf entity.Reclamado Then
            entity.Observacion = "Pedido Reclamado"
        ElseIf entity.Recibido Then
            entity.Observacion = "Pedido Recibido"
        ElseIf entity.Despachado Then
            entity.Observacion = "Su pedido ha sido despachado"
        End If

    End Sub

    Public Function SaveOrUpdate1(Entity As PedidoCabecera) As PedidoCabecera Implements IBLL(Of PedidoCabecera).SaveOrUpdate
        Me.OnBeforeSaveOrUpdate1(Entity)
        Return MyBase.SaveOrUpdate(Entity)
    End Function


End Class
