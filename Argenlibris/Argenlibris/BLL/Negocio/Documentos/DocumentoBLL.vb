﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface IDocumentoBLL
    Inherits IBLL(Of DocumentoCabecera)

    Sub GuardarEnPDF(FacturaCabecera_ As DocumentoCabecera)


End Interface

Public Class DocumentoBLL
    Inherits BaseBLL(Of DocumentoCabecera)
    Implements IDocumentoBLL




    Private Repositorio As IRepositorio(Of DocumentoCabecera)
    Dim _PDFGenerator As New PDFGenerator


    Sub New(repositorio As IRepositorio(Of DocumentoCabecera), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub


    Public Sub GuardarEnPDF(FacturaCabecera_ As DocumentoCabecera) Implements IDocumentoBLL.GuardarEnPDF
        ' _PDFGenerator.crearDocumento(FacturaCabecera_)
    End Sub


    Public Sub OnBeforeSaveOrUpdate1(entity As DocumentoCabecera) Implements IBLL(Of DocumentoCabecera).OnBeforeSaveOrUpdate
        Dim _numero As Integer = Me.GetAll().Where(Function(d) d.TipoDocumento.ToString.Contains(entity.TipoDocumento.ToString)).Count
        If Not entity.Numero > 0 Then
            entity.Numero = _numero + 1
        End If

    End Sub

    Public Function SaveOrUpdate1(Entity As DocumentoCabecera) As DocumentoCabecera Implements IBLL(Of DocumentoCabecera).SaveOrUpdate
        Me.OnBeforeSaveOrUpdate1(Entity)
        Return MyBase.SaveOrUpdate(Entity)
    End Function

End Class
