﻿Imports Spire.Pdf
Imports Spire.Pdf.Tables
Imports Spire.Pdf.Graphics
Imports System.Drawing
Imports Argenlibris.Domain

Public Class PDFGenerator


    Public Sub crearDocumento(factura_ As DocumentoCabecera)
        'Create a pdf document.
        Dim doc As New PdfDocument()
        'margin
        Dim unitCvtr As New PdfUnitConvertor()
        Dim margin As New PdfMargins()

        margin.Top = unitCvtr.ConvertUnits(2.54F, PdfGraphicsUnit.Centimeter, PdfGraphicsUnit.Point)
        margin.Bottom = margin.Top
        margin.Left = unitCvtr.ConvertUnits(3.17F, PdfGraphicsUnit.Centimeter, PdfGraphicsUnit.Point)
        margin.Right = margin.Left
        ' Create new page
        Dim page As PdfPageBase = doc.Pages.Add(PdfPageSize.A4, margin)
        Dim y As Single = 30
        'title
        Dim brush1 As PdfBrush = PdfBrushes.Black
        Dim font1 As New PdfTrueTypeFont(New Font("Arial", 16.0F, FontStyle.Bold))
        Dim format1 As New PdfStringFormat(PdfTextAlignment.Center)
        page.Canvas.DrawString("Factura", font1, brush1, page.Canvas.ClientSize.Width / 2, y, format1)
        y = y + font1.MeasureString("Factura", format1).Height
        y = y + 5
        page.Canvas.DrawString("B", font1, brush1, page.Canvas.ClientSize.Width / 2, y, format1)
        'page.Canvas.DrawString(factura_.LetraDocumento, font1, brush1, page.Canvas.ClientSize.Width / 2, y, format1)
        y = y + font1.MeasureString(factura_.LetraDocumento, format1).Height
        y = y + 5

        Dim fontArgenlibris As New PdfTrueTypeFont(New Font("Bookman Old Style", 20.0F, FontStyle.Italic))
        Dim fontCabecera As New PdfTrueTypeFont(New Font("Arial", 12.0F, FontStyle.Regular))
        Dim formatRight As New PdfStringFormat(PdfTextAlignment.Right)
        Dim formatLeft As New PdfStringFormat(PdfTextAlignment.Left)
        Dim _textoCabecera As String

        y = y + 20

        _textoCabecera = "Argenlibris"
        page.Canvas.DrawString(_textoCabecera, fontArgenlibris, brush1, 5, y, formatLeft)

        y = y + fontArgenlibris.MeasureString(_textoCabecera, format1).Height

        _textoCabecera = "Av. Directorio 2100, Caballito"
        page.Canvas.DrawString(_textoCabecera, fontCabecera, brush1, 5, y)
        _textoCabecera = "Número de Factura: " + CStr(factura_.Numero)
        page.Canvas.DrawString(_textoCabecera, fontCabecera, brush1, page.Canvas.ClientSize.Width / 2, y, formatLeft)

        y = y + font1.MeasureString(_textoCabecera, format1).Height

        _textoCabecera = "Caballito, CABA, CP 1406"
        page.Canvas.DrawString(_textoCabecera, fontCabecera, brush1, 5, y)
        _textoCabecera = "Fecha: " + CStr(Date.Now.Day) + "/" + CStr(Date.Now.Month) + "/" + CStr(Date.Now.Year)
        page.Canvas.DrawString(_textoCabecera, fontCabecera, brush1, page.Canvas.ClientSize.Width / 2, y, formatLeft)

        y = y + font1.MeasureString(_textoCabecera, format1).Height

        _textoCabecera = "CUIT: 30-54008821-3"
        page.Canvas.DrawString(_textoCabecera, fontCabecera, brush1, 5, y, formatLeft)
        _textoCabecera = "Cliente: " + factura_.Cliente.Apellido + ", " + factura_.Cliente.Nombre
        page.Canvas.DrawString(_textoCabecera, fontCabecera, brush1, page.Canvas.ClientSize.Width / 2, y, formatLeft)

        y = y + font1.MeasureString(_textoCabecera, format1).Height

        _textoCabecera = "Dirección: " + factura_.Direccion
        page.Canvas.DrawString(_textoCabecera, fontCabecera, brush1, page.Canvas.ClientSize.Width / 2, y, formatLeft)

        y = y + font1.MeasureString(_textoCabecera, formatLeft).Height



        y = y + 30
        Dim data As [String]() = New [String](factura_.Posiciones.Count) {}
        Dim linea As String
        Dim j As Integer = 0
        linea = "Libro;Cantidad;Precio"
        data(j) = linea
        For Each _posiciones As DocumentoPosicion In factura_.Posiciones
            j = j + 1
            linea = ""
            linea = CStr(_posiciones.Libro.Titulo) + ";" + CStr(_posiciones.Cantidad) + ";" + CStr(_posiciones.Precio)
            data(j) = linea
        Next

        Dim dataSource As [String]()() = New [String](data.Length - 1)() {}
        For i As Integer = 0 To data.Length - 1
            dataSource(i) = data(i).Split(";"c)
        Next


        Dim table As New PdfTable()
        table.Style.CellPadding = 2
        table.Style.HeaderSource = PdfHeaderSource.Rows
        table.Style.HeaderRowCount = 1
        table.Style.ShowHeader = True
        table.DataSource = dataSource
        Dim result As PdfLayoutResult = table.Draw(page, New PointF(0, y))
        'y = y + result.Bounds.Height + 5
        'Dim brush2 As PdfBrush = PdfBrushes.Gray
        'Dim font2 As New PdfTrueTypeFont(New Font("Arial", 9.0F))
        'page.Canvas.DrawString([String].Format("* {0} libros en la lista.", factura_.Posiciones.Count), font2, brush2, 5, y)

        '12.03
        y = y + result.Bounds.Height + 5
        Dim brush As PdfBrush = New PdfSolidBrush(Color.Black)
        page.Canvas.DrawRectangle(brush, New RectangleF(New Point(x:=0, y:=y), New SizeF(page.Canvas.ClientSize.Width, 15)))

        'y = y + result.Bounds.Height + 5
        Dim fontTotal As New PdfTrueTypeFont(New Font("Arial", 10.0F, FontStyle.Regular))
        Dim brush2 As PdfBrush = PdfBrushes.White
        _textoCabecera = "Total: $" + CStr(factura_.Total)
        page.Canvas.DrawString(_textoCabecera, fontTotal, brush2, (page.Canvas.ClientSize.Width / 2) + 70, y, formatLeft)


        'Save pdf file.
        Dim _archivo As String
        Dim _ruta As String
        _ruta = "C:\Users\jess\Desktop\Facturas\"
        _archivo = _ruta + "Factura " + CStr(factura_.Numero) + ".pdf"

        doc.SaveToFile(_archivo)
        doc.Close()
        System.Diagnostics.Process.Start(_archivo)
    End Sub

End Class
