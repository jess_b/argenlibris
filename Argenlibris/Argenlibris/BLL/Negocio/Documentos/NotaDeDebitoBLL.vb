﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface INotaDeDebitoBLL
    Inherits IBLL(Of NotaDeDebito)


End Interface
Public Class NotaDeDebitoBLL
    Inherits BaseBLL(Of NotaDeDebito)
    Implements INotaDeDebitoBLL


    Private Repositorio As IRepositorio(Of NotaDeDebito)

    Sub New(repositorio As IRepositorio(Of NotaDeDebito), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub

End Class
