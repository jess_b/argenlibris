﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface IDocumentoPosicionBLL
    Inherits IBLL(Of DocumentoPosicion)




End Interface
Public Class DocumentoPosicionBLL
    Inherits BaseBLL(Of DocumentoPosicion)
    Implements IDocumentoPosicionBLL


    Private Repositorio As IRepositorio(Of DocumentoPosicion)



    Sub New(repositorio As IRepositorio(Of DocumentoPosicion), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub
End Class
