﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface INotaDeCreditoBLL
    Inherits IBLL(Of NotaDeCredito)


End Interface

Public Class NotaDeCreditoBLL
    Inherits BaseBLL(Of NotaDeCredito)
    Implements INotaDeCreditoBLL


    Private Repositorio As IRepositorio(Of NotaDeCredito)

    Sub New(repositorio As IRepositorio(Of NotaDeCredito), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub

  
End Class
