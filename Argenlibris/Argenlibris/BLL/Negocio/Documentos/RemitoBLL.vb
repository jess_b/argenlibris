﻿Imports Argenlibris.Domain
Imports Repositorio
Imports Argenlibris.Seguridad

Public Interface IRemitoBLL
    Inherits IBLL(Of RemitoCabecera)



End Interface

Public Class RemitoBLL
    Inherits BaseBLL(Of RemitoCabecera)
    Implements IRemitoBLL


    Private Repositorio As IRepositorio(Of RemitoCabecera)

    Sub New(repositorio As IRepositorio(Of RemitoCabecera), validador As IValidadorFactory)
        MyBase.New(repositorio, validador)
    End Sub

    Public Sub OnBeforeSaveOrUpdate1(entity As RemitoCabecera) Implements IBLL(Of RemitoCabecera).OnBeforeSaveOrUpdate
        Dim _numero As Integer = Me.GetAll().Count
        entity.Numero = _numero + 1
    End Sub

    Public Function SaveOrUpdate1(Entity As RemitoCabecera) As RemitoCabecera Implements IBLL(Of RemitoCabecera).SaveOrUpdate
        Me.OnBeforeSaveOrUpdate1(Entity)
        Return MyBase.SaveOrUpdate(Entity)
    End Function


End Class
