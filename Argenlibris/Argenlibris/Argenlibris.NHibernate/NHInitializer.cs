﻿using NHibernate;
using NHibernate.Bytecode;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Mapping.ByCode;

using System.Reflection;
using System.Collections.Generic;
using NHibernate.Mapping;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Context;

namespace Argenlibris.NHibernate
{
    public class NHibernateInitializer
    {
        public static Configuration Initialize()
        {
            var configuration = new Configuration();

            configuration
                .Proxy(p => p.ProxyFactoryFactory<DefaultProxyFactoryFactory>())
                .DataBaseIntegration(db =>
                {
                    db.ConnectionStringName = "ArgenlibrisConnectionString";
                    db.Dialect<MsSql2012Dialect>();
                    db.BatchSize = 500;
                    db.LogFormattedSql = true;
                    db.LogSqlInConsole = true;
                    //db.AutoCommentSql = true;
                })
                .CurrentSessionContext<WebSessionContext>();

            var mapper = new ConventionModelMapper();
            mapper.WithConventions(configuration);

            return configuration;
        }

        public static void UpdateSchema(Configuration configuration, ISessionFactory sessionFactory)
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                //configuration.CreateIndexesForForeignKeys(); //ya mysql lo hace por defecto
                new SchemaUpdate(configuration).Execute(true, true);
            }
        }
    }
}
