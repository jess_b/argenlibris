﻿using NHibernate;
using NHibernate.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Argenlibris.NHibernate
{
    public class NhDelegatingHandler : DelegatingHandler
    {

        public ISessionFactory sessionFactory { get; set; }
        public ISession session { get; set; }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() =>
            {

                // Setup the session
                sessionFactory = (ISessionFactory)request.GetDependencyScope().GetService(typeof(ISessionFactory));
                session = sessionFactory.OpenSession();

                CurrentSessionContext.Bind(session);
                session.BeginTransaction();

                return new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent("An unknown issue occurred after creating the session")
                };
            })
            .ContinueWith(task => base.SendAsync(request, cancellationToken).Result)
            .ContinueWith(task =>
            {
                var sessionFactory = (ISessionFactory)request.GetDependencyScope().GetService(typeof(ISessionFactory));

                // Cleanup the session
                var session = sessionFactory.GetCurrentSession();

                var transaction = session.Transaction;
                if (transaction != null && transaction.IsActive)
                {
                    transaction.Commit();
                }
                session = CurrentSessionContext.Unbind(sessionFactory);
                session.Close();
                return task.Result;
            });
        }



    }
}
