﻿using NHibernate.Mapping.ByCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Argenlibris.NHibernate
{
    public interface IConventionOverride
    {
        void Override(ModelMapper mapper);
    }
}
