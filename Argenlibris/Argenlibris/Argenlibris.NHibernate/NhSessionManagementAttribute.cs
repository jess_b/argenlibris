﻿using NHibernate;
using NHibernate.Context;
using NHibernate.Exceptions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Argenlibris.NHibernate
{
    public class NhSessionManagementAttribute : ActionFilterAttribute
    {
        private ISessionFactory SessionFactory
        {
            get
            {
                return (ISessionFactory)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(ISessionFactory));
            }
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            try
            {
 var session = SessionFactory.OpenSession();
            CurrentSessionContext.Bind(session);
            session.BeginTransaction();
            }
            catch (Exception)
            {
                
                throw;
            }
           
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            try
            {
                var session = SessionFactory.GetCurrentSession();
                var transaction = session.Transaction;
                if (transaction != null && transaction.IsActive)
                {
                    if (actionExecutedContext.Exception == null)
                        transaction.Commit();
                    else
                        transaction.Rollback();
                }
                session = CurrentSessionContext.Unbind(SessionFactory);
                session.Close();
            }
            catch (GenericADOException ex)
            {
                var session = SessionFactory.GetCurrentSession();
                var transaction = session.Transaction;
                if (transaction != null && transaction.IsActive)
                { transaction.Rollback();
                    var sql = ex.InnerException as SqlException;
                    if (sql != null && ( sql.Number == 2601 ||  sql.Number == 2627))
                    {
                        actionExecutedContext.Response = new HttpResponseMessage(HttpStatusCode.NotImplemented);

                                              // Here's where to handle the unique constraint violation

                    }
                   
                }
               
            }
            catch (Exception)
            {
                var session = SessionFactory.GetCurrentSession();
                var transaction = session.Transaction;
                if (transaction != null && transaction.IsActive)
                {
                  
                        transaction.Rollback();
                }
                throw ;
            }
          
        }

    }
}
