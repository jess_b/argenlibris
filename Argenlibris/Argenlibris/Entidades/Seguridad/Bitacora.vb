﻿Public Class Bitacora
    Inherits Entity

    Dim _fecha As Date
    Public Overridable Property Fecha As Date
        Get
            Return _fecha
        End Get
        Set(value As Date)
            _fecha = value
        End Set
    End Property

    Dim _mensaje As EnumTipoMensaje
    Public Overridable Property Mensaje As EnumTipoMensaje
        Get
            Return _mensaje
        End Get
        Set(value As EnumTipoMensaje)
            _mensaje = value
        End Set
    End Property

    Dim _idEntidad As Guid
    Public Overridable Property IdEntidad As Guid
        Get
            Return _idEntidad
        End Get
        Set(value As Guid)
            _idEntidad = value
        End Set
    End Property

    Dim _tipoEntidad As String
    Public Overridable Property TipoDeEntidad As String
        Get
            Return _tipoEntidad
        End Get
        Set(value As String)
            _tipoEntidad = value
        End Set
    End Property

    Dim _TipoDeMensaje As EnumTipoDeError
    Public Overridable Property TipoDeMensaje As EnumTipoDeError
        Get
            Return _TipoDeMensaje
        End Get
        Set(value As EnumTipoDeError)
            _TipoDeMensaje = value
        End Set
    End Property

    Dim _usuario As Usuario
    Public Overridable Property Usuario As Usuario
        Get
            Return _usuario
        End Get
        Set(value As Usuario)
            _usuario = value
        End Set
    End Property

End Class
