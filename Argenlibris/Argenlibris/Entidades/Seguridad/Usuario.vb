﻿Public Class Usuario
    Inherits Entity

    Public Sub New()

    End Sub

    Dim _nombreUsu As String
    Public Overridable Property NombreUsuario As String
        Get
            Return _nombreUsu
        End Get
        Set(value As String)
            _nombreUsu = value
        End Set
    End Property

    Dim _nombre As String
    Public Overridable Property Nombre As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
        End Set
    End Property

    Dim _apellido As String
    Public Overridable Property Apellido As String
        Get
            Return _apellido
        End Get
        Set(value As String)
            _apellido = value
        End Set
    End Property

    Dim _password As String
    Public Overridable Property Password As String
        Get
            Return _password
        End Get
        Set(value As String)
            _password = value
        End Set
    End Property

    Dim _email As String
    Public Overridable Property Email As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property

    Dim _direccion As String
    Public Overridable Property Direccion As String
        Get
            Return _direccion
        End Get
        Set(value As String)
            _direccion = value
        End Set

    End Property
    Dim _fechaNac As Date
    Public Overridable Property FechaNacimiento As DateTime?
   
    Public Overridable Property Activo As Boolean

End Class