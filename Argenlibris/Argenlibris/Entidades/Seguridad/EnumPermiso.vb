﻿Public Enum EnumPermiso

    ' Administración
    UsuarioPuedeGestionar
    IdiomaPuedeGestionar
    BitacoraPuedeGestionar

    ' Negocio
    VentasPuedeGestionar
    MarketingPuedeGestionar
    AlmacenesPuedeGestionar

    ' Administrador
    Administrador

    ' Productos
    ProductosPuedeGestionar




End Enum
