﻿Public Class UsuarioRol
    Inherits Entity

    Public Sub New()
        '_roles = New List(Of Rol)
    End Sub

    Dim _usuario As Usuario
    Public Overridable Property Usuario As Usuario
        Get
            Return _usuario
        End Get
        Set(value As Usuario)
            _usuario = value
        End Set
    End Property

    Dim _rol As Rol
    Public Overridable Property Rol As Rol
        Get
            Return _rol
        End Get
        Set(value As Rol)
            _rol = value
        End Set
    End Property

    'Dim _roles As IList(Of Rol)
    'Public Overridable Property Roles As IList(Of Rol)
    '    Get
    '        Return _roles
    '    End Get
    '    Set(value As IList(Of Rol))
    '        _roles = value
    '    End Set
    'End Property

End Class
