﻿Public MustInherit Class Entity

    Dim _id As Guid
    Public Overridable Property Id As Guid
        Get
            Return _id
        End Get
        Set(value As Guid)
            _id = value
        End Set
    End Property




End Class
