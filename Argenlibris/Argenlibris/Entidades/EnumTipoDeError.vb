﻿Public Enum EnumTipoDeError

    E
    W
    I
    S

End Enum
Public Enum EnumTipoMensaje

    Delete
    Create
    Save

End Enum
Public Enum EnumTipoDocumento

    Factura
    NotaDeCredito
    NotaDeDebito


End Enum
Public Enum EnumTipoTarjeta
    Visa
    Mastercard
End Enum