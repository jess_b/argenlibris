﻿Public Class Idioma
    Inherits Entity

    Dim _codigo As String
    Public Overridable Property Codigo As String
        Get
            Return _codigo
        End Get
        Set(value As String)
            _codigo = value
        End Set
    End Property

    Dim _descripcion As String
    Public Overridable Property Descripcion As String
        Get
            Return _descripcion
        End Get
        Set(value As String)
            _descripcion = value
        End Set
    End Property


End Class
