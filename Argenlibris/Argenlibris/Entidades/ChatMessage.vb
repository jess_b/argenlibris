﻿Public Class ChatMessage
    Inherits Entity
    Public Overridable Property NombreUsuario As String
    Public Overridable Property FechaMensaje As DateTime
    Public Overridable Property Mensaje As String
    Public Overridable Property IdRepresentante As Guid
End Class
