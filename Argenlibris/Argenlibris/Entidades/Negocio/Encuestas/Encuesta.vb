﻿Public Class Encuesta
    Inherits EncuestaBase

    Dim _fechaDesde As Date
    Public Overridable Property FechaDesde As Date
        Get
            Return _fechaDesde
        End Get
        Set(value As Date)
            _fechaDesde = value
        End Set
    End Property

    Dim _fechaHasta As Date
    Public Overridable Property FechaHasta As Date
        Get
            Return _fechaHasta
        End Get
        Set(value As Date)
            _fechaHasta = value
        End Set
    End Property

    Dim _libro As Libro
    Public Overridable Property Libro As Libro
        Get
            Return _libro
        End Get
        Set(value As Libro)
            _libro = value
        End Set
    End Property

End Class
