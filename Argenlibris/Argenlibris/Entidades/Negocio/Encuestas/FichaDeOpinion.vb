﻿Public Class FichaDeOpinion
    Inherits EncuestaBase

    Dim _fechaDesde As Date
    Public Overridable Property FechaDesde As Date
        Get
            Return _fechaDesde
        End Get
        Set(value As Date)
            _fechaDesde = value
        End Set
    End Property

    Dim _fechaHasta As Date
    Public Overridable Property FechaHasta As Date
        Get
            Return _fechaHasta
        End Get
        Set(value As Date)
            _fechaHasta = value
        End Set
    End Property

    'Dim _pedido As PedidoCabecera
    'Public Overridable Property Pedido As PedidoCabecera
    '    Get
    '        Return _pedido
    '    End Get
    '    Set(value As PedidoCabecera)
    '        _pedido = value
    '    End Set
    'End Property

End Class
