﻿Public Class ClienteRespuesta
    Inherits Entity

    Public Overridable Property Cliente As Usuario
    Public Overridable Property Respuesta As Respuesta
    Public Overridable Property Fecha As Date

End Class
