﻿Public Class Respuesta
    Inherits Entity

    Dim _descripcion As String
    Public Overridable Property Descripcion As String
        Get
            Return _descripcion
        End Get
        Set(value As String)
            _descripcion = value
        End Set
    End Property

    Dim _pregunta As Pregunta
    Public Overridable Property Pregunta As Pregunta
        Get
            Return _pregunta
        End Get
        Set(value As Pregunta)
            _pregunta = value
        End Set
    End Property

End Class
