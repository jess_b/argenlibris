﻿Public Class Pregunta
    Inherits Entity

    Dim _descripcion As String
    Public Overridable Property Descripcion As String
        Get
            Return _descripcion
        End Get
        Set(value As String)
            _descripcion = value
        End Set
    End Property

    Dim _encuesta As EncuestaBase
    Public Overridable Property Encuesta As EncuestaBase
        Get
            Return _encuesta
        End Get
        Set(value As EncuestaBase)
            _encuesta = value
        End Set
    End Property

End Class
