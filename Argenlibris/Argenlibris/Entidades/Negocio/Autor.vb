﻿Public Class Autor
    Inherits Entity

    Dim _nombre As String
    Public Overridable Property Nombre As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
        End Set
    End Property

    Dim _apellido As String
    Public Overridable Property Apellido As String
        Get
            Return _apellido
        End Get
        Set(value As String)
            _apellido = value
        End Set
    End Property

    Dim _pais As Pais
    Public Overridable Property PaisDeOrigen As Pais
        Get
            Return _pais
        End Get
        Set(value As Pais)
            _pais = value
        End Set
    End Property

End Class
