﻿Public Class RemitoCabecera

    Inherits Entity

    Public Overridable Property Numero As Integer
    Public Overridable Property Fecha As Date
    Public Overridable Property Cliente As Usuario
    Public Overridable Property Posiciones As IList(Of RemitoPosicion)
    Public Overridable Property Direccion As String


    Sub New()
        Posiciones = New List(Of RemitoPosicion)
    End Sub

End Class
