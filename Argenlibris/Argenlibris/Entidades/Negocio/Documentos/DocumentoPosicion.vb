﻿Public Class DocumentoPosicion
    Inherits Entity

    Public Overridable Property Libro As Libro
    Public Overridable Property Cantidad As Integer
    Public Overridable Property Precio As Double

End Class
