﻿Public Class DocumentoCabecera
    Inherits Entity

    Public Overridable Property Numero As Integer
    Public Overridable Property Fecha As Date
    Public Overridable Property Total As Double
    Public Overridable Property Cliente As Usuario
    Public Overridable Property Posiciones As IList(Of DocumentoPosicion)
    Public Overridable Property Direccion As String
    Public Overridable Property LetraDocumento As String
    Public Overridable Property TipoDocumento As EnumTipoDocumento
    Public Overridable Property Observacion As String
    Public Overridable Property MontoADebitar As Double

    Sub New()
        Posiciones = New List(Of DocumentoPosicion)
    End Sub

End Class
