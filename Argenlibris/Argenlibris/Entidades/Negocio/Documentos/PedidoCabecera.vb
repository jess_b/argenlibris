﻿Public Class PedidoCabecera
    Inherits Entity

    Public Overridable Property Numero As Integer
    Public Overridable Property Fecha As Date
    Public Overridable Property Total As Double
    Public Overridable Property Cliente As Usuario
    Public Overridable Property Posiciones As IList(Of PedidoPosicion)
    Public Overridable Property Direccion As String
    Public Overridable Property Despachado As Boolean
    Public Overridable Property Recibido As Boolean
    Public Overridable Property Reclamado As Boolean
    Public Overridable Property Resuelto As Boolean
    Public Overridable Property Observacion As String
    Public Overridable Property ObservacionContable As String

    Sub New()
        Posiciones = New List(Of PedidoPosicion)
    End Sub

End Class
