﻿Public Class Puntuacion
    Inherits Entity

    Dim _libro As Libro
    Public Overridable Property Libro As Libro
        Get
            Return _libro
        End Get
        Set(value As Libro)
            _libro = value
        End Set
    End Property

    Dim _puntuacion As Double
    Public Overridable Property Puntuacion As Double
        Get
            Return _puntuacion
        End Get
        Set(value As Double)
            _puntuacion = value
        End Set
    End Property

    Dim _cantidad As Integer
    Public Overridable Property CantidadVotos As Integer
        Get
            Return _cantidad
        End Get
        Set(value As Integer)
            _cantidad = value
        End Set
    End Property

    Dim _usuario As Usuario
    Public Overridable Property Usuario As Usuario
        Get
            Return _usuario
        End Get
        Set(value As Usuario)
            _usuario = value
        End Set
    End Property


End Class
