﻿Public Class Contacto
    Inherits Entity

    Dim _mail As String
    Public Overridable Property Mail As String
        Get
            Return _mail
        End Get
        Set(value As String)
            _mail = value
        End Set
    End Property

    Dim _nombre As String
    Public Overridable Property Nombre As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
        End Set
    End Property

    Dim _observacion As String
    Public Overridable Property Observacion As String
        Get
            Return _observacion
        End Get
        Set(value As String)
            _observacion = value
        End Set
    End Property

End Class
