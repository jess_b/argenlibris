﻿Public Class Novedad
    Inherits Entity
    Public Overridable Property FileName As String
    Dim _titulo As String
    Public Overridable Property Titulo As String
        Get
            Return _titulo
        End Get
        Set(value As String)
            _titulo = value
        End Set
    End Property

    Dim _descripcion As String
    Public Overridable Property Descripcion As String
        Get
            Return _descripcion
        End Get
        Set(value As String)
            _descripcion = value
        End Set
    End Property

    Dim _visible As Boolean
    Public Overridable Property Visible As Boolean
        Get
            Return _visible
        End Get
        Set(value As Boolean)
            _visible = value
        End Set
    End Property

    Dim _categoria As Categoria
    Public Overridable Property Categoria As Categoria
        Get
            Return _categoria
        End Get
        Set(value As Categoria)
            _categoria = value
        End Set
    End Property

    Public Overridable Property Enviado As Boolean

End Class
