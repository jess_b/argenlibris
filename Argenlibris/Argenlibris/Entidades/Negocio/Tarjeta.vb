﻿Public Class Tarjeta
    Inherits Entity

    Public Overridable Property NumeroTarjeta As String
    Public Overridable Property FechaVencimiento As String
    'Public Overridable Property FechaVencimiento As Date
    Public Overridable Property TipoDeTarjeta As EnumTipoTarjeta
    Public Overridable Property Cliente As Usuario
    Public Overridable Property CVC As Integer

End Class
