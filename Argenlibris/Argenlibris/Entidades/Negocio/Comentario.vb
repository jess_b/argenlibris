﻿Public Class Comentario
    Inherits Entity

    Dim _libro As Libro
    Public Overridable Property Libro As Libro
        Get
            Return _libro
        End Get
        Set(value As Libro)
            _libro = value
        End Set
    End Property

    Dim _comentario As String
    Public Overridable Property Comentario As String
        Get
            Return _comentario
        End Get
        Set(value As String)
            _comentario = value
        End Set
    End Property

    Dim _fecha As Date
    Public Overridable Property Fecha As Date
        Get
            Return _fecha
        End Get
        Set(value As Date)
            _fecha = value
        End Set
    End Property

    Dim _nombre As String
    Public Overridable Property Nombre As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
        End Set
    End Property

End Class
