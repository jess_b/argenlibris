﻿Public Class Libro
    Inherits Entity


    Public Overridable Property Favoritos As IList(Of FavoritoCliente)
    Public Overridable Property Leidos As IList(Of LeidoCliente)

    Public Overridable Property FileName As String
    Public Overridable Property FechaCreacion As Nullable(Of Date)
    Public Sub New()
        Favoritos = New List(Of FavoritoCliente)
        Leidos = New List(Of LeidoCliente)

    End Sub
    Dim _isbn As String
    Public Overridable Property ISBN As String
        Get
            Return _isbn
        End Get
        Set(value As String)
            _isbn = value
        End Set
    End Property


    Dim _formato As EnumTipoFormato
    Public Overridable Property Formato As EnumTipoFormato
        Get
            Return _formato
        End Get
        Set(value As EnumTipoFormato)
            _formato = value
        End Set
    End Property

    Dim _titulo As String
    Public Overridable Property Titulo As String
        Get
            Return _titulo
        End Get
        Set(value As String)
            _titulo = value
        End Set
    End Property

    Dim _sinopsis As String
    Public Overridable Property Sinopsis As String
        Get
            Return _sinopsis
        End Get
        Set(value As String)
            _sinopsis = value
        End Set
    End Property

    Dim _autor As Autor
    Public Overridable Property Autor As Autor
        Get
            Return _autor
        End Get
        Set(value As Autor)
            _autor = value
        End Set
    End Property

    Dim _editorial As Editorial
    Public Overridable Property Editorial As Editorial
        Get
            Return _editorial
        End Get
        Set(value As Editorial)
            _editorial = value
        End Set
    End Property

    Dim _disponibilidad As Integer
    Public Overridable Property Disponibilidad As Integer
        Get
            Return _disponibilidad
        End Get
        Set(value As Integer)
            _disponibilidad = value
        End Set
    End Property

    Dim _precio As Double
    Public Overridable Property Precio As Double
        Get
            Return _precio
        End Get
        Set(value As Double)
            _precio = value
        End Set
    End Property


    Public Overridable Property Costo As Double


    Dim _categoria As Categoria
    Public Overridable Property Categoria As Categoria
        Get
            Return _categoria
        End Get
        Set(value As Categoria)
            _categoria = value
        End Set
    End Property

    Public Overridable Property Enviado As Boolean

End Class
