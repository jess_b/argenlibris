﻿Public Class Newsletter
    Inherits Entity

    Public Overridable Property Fecha As DateTime
    Public Overridable Property Nombre As String
    Public Overridable Property Asunto As String
    Public Overridable Property Cuerpo As String
    Public Overridable Property Enviado As Boolean

    'Public Overridable Property Libros As List(Of Libro)


End Class